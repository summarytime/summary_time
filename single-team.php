<?php
/**
 * The Template for displaying all single-team posts.
 *
 * @package tl
 */

get_header(); ?>
		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); $bc_bg_class	=	tlf_get_option('title_bg_color','');  ?> 
<div class="tl-single-outer-wrapper">

<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>"<?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
    <div class="container">
	<div class="tl-breadcrumb-page-title">
        <header>
            <h1 class="page-title">
            <?php the_title(); ?>
            </h1>
            
            <?php
			
			?>
        </header><!-- .entry-header -->
        </div><!-- .tl-breadcrumb-page-title -->
		
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>
	</div>	
		
    </div>
</div>
<div class="main-content main-content-padding">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">			
			<div class="tl-contentsingle-wrapper">			
					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <!-- .entry-header -->
                            <div class="single-content-post-wrapper">
                                <div class="entry-content">
                                	<div class="row">
                                    <?php if(tlf_get_option('show_single_featured_image','')):?>
                                    <div class="col-sm-6 col-md-6 col-xs-12">
                                    <div class="entry-content-thumbnail">
                                        <?php if(get_post_meta( get_the_ID(), 'team_member_image', true )):?>
                                        <?php
										$image_id = tl_get_image_id(get_post_meta( get_the_ID(), 'team_member_image', true ));
										$image_thumb = wp_get_attachment_image_src($image_id, 'medium');
										?>
                                  		<img src="<?php echo $image_thumb[0]; ?>" alt="" class="content-image" />
                                  		<?php endif;?>
                                    </div>
                                    </div>
                                    <?php endif;?>
                                    <div class="col-sm-6 col-md-6 col-xs-12">
                                    <header>
                                        <h1 class="page-title">
                                        <?php the_title(); ?>
                                        </h1>
                                    </header>
                                    <div class="entry-meta">
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_designation', true )?'<h4 class="member-designation">'.get_post_meta( get_the_ID(), 'team_member_designation', true ).'</h4>':'';?>
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_address', true )?wpautop('<strong>Address:</strong> '.get_post_meta( get_the_ID(), 'team_member_address', true )):'';?>
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_office_phone', true )?wpautop('<strong>Office Phone No.:</strong> '.get_post_meta( get_the_ID(), 'team_member_office_phone', true )):'';?>
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_mobile_phone', true )?wpautop('<strong>Mobile No.:</strong> '.get_post_meta( get_the_ID(), 'team_member_mobile_phone', true )):'';?>
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_fax', true )?wpautop('<strong>Fax No.:</strong> '.get_post_meta( get_the_ID(), 'team_member_fax', true )):'';?>
                                        <?php echo get_post_meta( get_the_ID(), 'team_member_email', true )?wpautop('<strong>Email:</strong> '.get_post_meta( get_the_ID(), 'team_member_email', true )):'';?>
                                    </div><!-- .entry-meta -->
                                    <div class="team-social-info">
                                        <div class="social-media-div">
                                          <ul class="socials unstyled">
                                            <?php if(get_post_meta( get_the_ID(), 'team_member_facebook_url', true )):?>
                                            <li>
                                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_facebook_url', true );?>" class="fb">
                                             <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                            </li>
                                            <?php endif;?>
                                            <?php if(get_post_meta( get_the_ID(), 'team_member_twitter_url', true )):?>
                                            <li>
                                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_twitter_url', true );?>" class="tw">
                                             <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                            </li>
                                            <?php endif;?>
                                            <?php if(get_post_meta( get_the_ID(), 'team_member_linkedin_url', true )):?>
                                            <li>
                                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_linkedin_url', true );?>" class="dri">
                                             <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </a>
                                            </li>
                                            <?php endif;?>
                                            <?php if(get_post_meta( get_the_ID(), 'team_member_google_plus_url', true )):?>
                                            <li>
                                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_google_plus_url', true );?>" class="beh">
                                             <i class="fa fa-google-plus" aria-hidden="true"></i>
                                            </a>
                                            </li>
                                            <?php endif;?>
                                          </ul>
                                         </div>
                                     </div>
                                    </div>
                                    </div>
                                  
                                   	<div class="entry-meta">
                                	</div><!-- .entry-meta -->
                                    <div class="tl-singleteam-article">
                                    <?php echo wpautop(get_post_meta( get_the_ID(), 'team_member_description', true ));?>
                                    </div>
                                    <?php tl_link_pages(); ?>
                                </div><!-- .entry-content -->
                            </div><!-- .single-content-post-wrapper -->

                            <footer class="entry-meta">
                                <?php edit_post_link( __( 'Edit', 'tl' ), '<span class="edit-link">', '</span>' ); ?>
                            </footer><!-- .entry-meta -->
                        </article><!-- #post-## -->

					<?php endwhile; // end of the loop. ?>
				</div>
			
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <div class="col-sm-12 col-md-4">
            	<?php get_sidebar(); ?>
            </div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->


</div><!-- close .srishti-outer-wrapper -->
<?php get_footer(); ?>