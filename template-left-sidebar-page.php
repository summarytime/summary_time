<?php
/**
 * Template Name: Page With Left Sidebar
 *
 *
 * @package tl
 */

get_header(); ?>

<div class="tl-outer-wrapper">
<?php while ( have_posts() ) : the_post(); ?>

		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); $bc_bg_class	=	tlf_get_option('title_bg_color','');?>
		
<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>"<?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
    <div class="container">
        <header>
			<?php if(get_post_meta( get_the_ID(), 'tl_hide_title', true )!='on'):?>
            <div class="<?php echo (get_post_meta( get_the_ID(), 'tl_title_position', true ))?get_post_meta( get_the_ID(), 'tl_title_position', true ):'';?>">
            <h1 class="page-title">
            <?php the_title(); ?>
            </h1>
            </div>
            <?php endif;
           
            ?>
        </header><!-- .entry-header -->
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>
    </div>
</div>
<div class="main-content tl-page-leftsidebar">

	<div class="container">
	<div class="row">

		<div class="col-sm-4">
		<?php get_sidebar(); ?>
		</div>

		<div class="col-sm-8">
				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();
				?>
		</div>

	</div>
	</div>
</div>
<?php endwhile; // end of the loop. ?>
</div>

<?php get_footer(); ?>
