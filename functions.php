<?php
/**
 * tl functions and definitions
 *
 * @package tl
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

if ( ! function_exists( 'tl_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function tl_setup() {
	global $cap, $content_width;

	// Add html5 behavior for some theme elements
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    // This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	/**
	 * Add default posts and comments RSS feed links to head
	*/
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	*/
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable support for Post Formats
	*/
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	*/
	add_theme_support( 'custom-background', apply_filters( 'tl_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );
	
	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on tl, use a find and replace
	 * to change 'tl' to the name of your theme in all the template files
	*/
	load_theme_textdomain( 'tl', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
	register_nav_menus( array(
		'primary'  => __( 'Header Top Menu', 'tl' ),
		) );

}
endif; // tl_setup
add_action( 'after_setup_theme', 'tl_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function tl_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'tl' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
	if(tlf_get_option('footer_column','')):
	for($i=1;$i<=tlf_get_option('footer_column','');$i++){
		register_sidebar( array(
			'name'          => __( 'Footer-'.$i, 'tl' ),
			'id'            => 'footer-'.$i,
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
	endif;
}
add_action( 'widgets_init', 'tl_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function tl_scripts() {

	// Import the necessary tl Bootstrap WP CSS additions
	wp_enqueue_style( 'tl-bootstrap-wp', get_template_directory_uri() . '/tl-components/includes/css/bootstrap-wp.css' );


	
	// load bootstrap css
	wp_enqueue_style( 'tl-bootstrap', get_template_directory_uri() . '/tl-components/includes/resources/bootstrap/css/bootstrap.min.css' );

	// load Font Awesome css
	wp_enqueue_style( 'tl-font-awesome', get_template_directory_uri() . '/tl-components/includes/css/font-awesome.min.css', false, '4.1.0' );
	
	// Themelines Font
	wp_enqueue_style( 'tlpb-slider-full',get_template_directory_uri() . '/tl-components/includes/css/fonts.css' );

	//theme page style 
	wp_enqueue_style( 'tl-all-theme', get_template_directory_uri() . '/tl-components/includes/css/all-theme-inner.css' );
	
	// load tl styles
	wp_enqueue_style( 'tl-style', get_stylesheet_uri() );
	
	// load navbar styles
	wp_enqueue_style( 'tl-navbar-style', get_template_directory_uri() . '/tl-components/includes/css/custom-navbar.css' );

	// load bootstrap js
	wp_enqueue_script('tl-bootstrapjs', get_template_directory_uri().'/tl-components/includes/resources/bootstrap/js/bootstrap.min.js', array('jquery') );
	
	// load parallax footer js
			////wp_enqueue_script('tl-parallax-footer', get_template_directory_uri().'/tl-components/includes/js/footer-reveal.min.js', array('jquery'),'',true );

	// load bootstrap wp js
	wp_enqueue_script( 'tl-bootstrapwp', get_template_directory_uri() . '/tl-components/includes/js/bootstrap-wp.js', array('jquery') );

	wp_enqueue_script( 'tl-skip-link-focus-fix', get_template_directory_uri() . '/tl-components/includes/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	// Owl carousel
	wp_enqueue_style( 'tlpb-owl-carousel',get_template_directory_uri() . '/tl-components/includes/css/owl.carousel.min.css', array(), '2.1.1', false );
	
	wp_enqueue_script( 'tlpb-owl-carousel',get_template_directory_uri() . '/tl-components/includes/js/owl.carousel.min.js', array(), '2.1.1', false );
	
	
	
	
	wp_enqueue_script( 'tl-custom', get_template_directory_uri() . '/tl-components/includes/js/corporate-js/tl-custom-script.js', array('jquery'),'',true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'tl-keyboard-image-navigation', get_template_directory_uri() . '/tl-components/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

}
add_action( 'wp_enqueue_scripts', 'tl_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/tl-components/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/tl-components/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/tl-components/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/tl-components/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/tl-components/includes/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/tl-components/includes/bootstrap-wp-navwalker.php';

/**
 * Adds WooCommerce support
 */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
/**
 * Import Themelines Theme Settings
 */
require_once get_template_directory() . '/tl-components/includes/theme-settings.php';

/**
 * Import Themelines Page Builder
 */
require_once get_template_directory() . '/tl-components/page-builder/base.php';
require_once get_template_directory() . '/tl-components/page-builder/pb-function.php';
/**
 * Import Themelines Custom Widget
 */
require_once get_template_directory() . '/tl-components/tl-custom-sidebars/tl-custom-sidebars.php';
/**
 * Import Themelines Custom Breadcrumb
 */
require_once get_template_directory() . '/tl-components/tl-custom-breadcrumb/tl-custom-breadcrumb.php';
/**
 * Import Themelines Custom Metabox
 */
require_once get_template_directory() . '/tl-components/tl-custom-metabox/tl-custom-metabox.php';
/**
 * Import Themelines Custom Post Types
 */
require_once get_template_directory() . '/tl-components/tl-custom-post-types/index.php';
/**
 * Import Themelines Post Order
 */
require_once get_template_directory() . '/tl-components/tl-custom-post-order/tl-custom-post-order.php';
/**
 * Import Themelines Shortcodes
 */
require_once get_template_directory() . '/tl-components/tl-shortcodes/tl-shortcodes.php';
/**
 * Import Themelines SEO
 */
require_once get_template_directory() . '/tl-components/tl-seo/tl-seo.php';
/**
 * Import Themelines Custom Widgets
 */
require_once get_template_directory() . '/tl-components/tl-custom-widgets/index.php';
/**
 * Import Required Plugin Checker
 */
require_once get_template_directory() . '/plugin-activation/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/plugin-activation/required-plugins.php';