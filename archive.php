<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tl
 */

get_header(); ?>
		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); $bc_bg_class	=	tlf_get_option('title_bg_color','');  ?> 
<div class="tl-archive-outer-wrapper">
<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>"<?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
<div class="container">
        <header>
            <h1 class="page-title">
                <?php
                    if ( is_category() ) :
                        single_cat_title();

                    elseif ( is_tag() ) :
                        single_tag_title();

                    elseif ( is_author() ) :
                        /* Queue the first post, that way we know
                         * what author we're dealing with (if that is the case).
                        */
                        the_post();
                        printf( __( 'Author: %s', 'tl' ), '<span class="vcard">' . get_the_author() . '</span>' );
                        /* Since we called the_post() above, we need to
                         * rewind the loop back to the beginning that way
                         * we can run the loop properly, in full.
                         */
                        rewind_posts();

                    elseif ( is_day() ) :
                        printf( __( 'Day: %s', 'tl' ), '<span>' . get_the_date() . '</span>' );

                    elseif ( is_month() ) :
                        printf( __( 'Month: %s', 'tl' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

                    elseif ( is_year() ) :
                        printf( __( 'Year: %s', 'tl' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

                    elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                        _e( 'Asides', 'tl' );

                    elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                        _e( 'Images', 'tl');

                    elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                        _e( 'Videos', 'tl' );

                    elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                        _e( 'Quotes', 'tl' );

                    elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                        _e( 'Links', 'tl' );

                    else :
                        _e( 'Archives', 'tl' );

                    endif;
                ?>
            </h1>
        </header><!-- .page-header -->
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>	
        
    </div>
</div>
<div class="main-content main-content-padding">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner tl-cp-main-left1 tl-archive-inner-left col-sm-12 col-md-8">
	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>


		

			<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php tl_content_nav( 'nav-below' ); ?>
            <?php //tl_pagination(); ?>
		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>



		

			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <div class="col-sm-12 col-md-4">
            	<?php get_sidebar(); ?>
            </div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
</div>
<?php get_footer(); ?>
