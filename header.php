<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package tl
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php if(tlf_get_option( 'favicon_uploader', '' )):?>
    <?php $ext = pathinfo(tlf_get_option( 'favicon_uploader', '' ), PATHINFO_EXTENSION);?>
	<link rel="icon" href="<?php echo tlf_get_option( 'favicon_uploader', '' );?>" type="image/<?php echo $ext;?>" sizes="16x16">
    <?php endif;?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<!--google web-font --> 
	<link href="https://fonts.googleapis.com/css?family=Oswald|Ubuntu:300,400,500,700" rel="stylesheet">

	<?php wp_head(); ?>
    
    <?php if(tlf_get_option( 'header_script', '' )):?>
    <script>
		<?php echo tlf_get_option( 'header_script', '' );?>
	</script>
    <?php endif;?>
</head>

<body <?php body_class(); ?>>
	<?php if(tlf_get_option( 'body_script', '' )):?>
    <script>
		<?php echo tlf_get_option( 'body_script', '' );?>
	</script>
    <?php endif;?>
	<?php do_action( 'before' ); ?>
	
	<!-- page loading -->
	<?php if((is_home() || is_front_page()) && tlf_get_option('show_loader','')):?>
	<div id="tl-clean-loading">
		<div id="loading-center">
			<div id="loading-center-absolute">
				<div class="object" id="object_one"></div>
				<div class="object" id="object_two" style="left:20px;"></div>
				<div class="object" id="object_three" style="left:40px;"></div>
				<div class="object" id="object_four" style="left:60px;"></div>
				<div class="object" id="object_five" style="left:80px;"></div>
			</div>
		</div> 
	</div>
    <?php endif;?>
<a href="javascript:void(0);" id="scroll-top" title="Scroll to Top" style="display: none;">Top<span></span></a> 


<?php 
if(tlf_get_option('header_layout','')){
	$layout = tlf_get_option('header_layout','');
}else{
	$layout = 'default';
}
get_template_part( 'content', 'header-'.$layout ); ?>