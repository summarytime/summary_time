<div class="tl-staticinner-navigation">
	  <div class="navbar-wrapper">

		<div class="container">
        <nav class="navbar navbar-inverse <?php echo tlf_get_option('enable_sticky_header','')?'navbar-static-top':'';?>">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"><?php _e('Toggle navigation','tl') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>">
              	<?php if(tlf_get_option( 'logo_position', '' )=='left'){
							$logo_cls = 'pull-left';
					}elseif(tlf_get_option( 'logo_position', '' )=='right'){
							$logo_cls = 'pull-right';
					}elseif(tlf_get_option( 'logo_position', '' )=='center'){
							$logo_cls = 'text-center';
					}else{
							$logo_cls = '';
					}
					if(tlf_get_option( 'description_position', '' )=='left'){
							$desc_cls = 'pull-left';
					}elseif(tlf_get_option( 'logo_position', '' )=='right'){
							$desc_cls = 'pull-right';
					}elseif(tlf_get_option( 'logo_position', '' )=='center'){
							$desc_cls = 'text-center';
					}else{
							$desc_cls = '';
					}
					?>
				<?php if(tlf_get_option( 'logo_uploader', '' )):?>
                    <img src="<?php echo tlf_get_option( 'logo_uploader', '' );?>" class="<?php echo $logo_cls;?>">
                <?php else:?>
                    <?php if(tlf_get_option( 'text_logo', '' )):?>
                    <div class="site-title <?php echo $logo_cls;?>"><?php echo tlf_get_option( 'text_logo', '' );?></div>
                    <?php else:?>
                    <div class="site-title <?php echo $logo_cls;?>"><?php bloginfo( 'name' ); ?></div>
                    <?php endif;?>
                <?php endif;?>
                </a>
                <?php if(tlf_get_option( 'show_site_description', '' )):?>
                    <?php if(tlf_get_option( 'site_description', '' )):?>
                    <p class="site-description lead <?php echo $desc_cls;?>"><?php echo tlf_get_option( 'site_description', '' ); ?></p>
                    <?php else:?>
                    <p class="site-description lead <?php echo $desc_cls;?>"><?php bloginfo( 'description' ); ?></p>
                    <?php endif;?>
                <?php endif;?>
            </div>
			<div class="tl-static-navmenu pull-right">
				<div class="tl-croporate-nav-inner">
						<?php 
						wp_nav_menu(
							array(
								'theme_location' 	=> 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'container_id'      => 'navbar',
								'container_class'   => 'collapse navbar-collapse',
								'menu_class' 		=> 'nav navbar-nav pull-right',
								'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
								'menu_id'			=> 'main-menu',
								'walker' 			=> new wp_bootstrap_navwalker()
							)
						); 
						?>
			
					<div class="nav-searchicon"><a class="column md-trigger" data-toggle="modal" data-target="#myModalsearch" href="#"><i class="material-icons">search</i></a></div>	
				</div>
			</div>
			<div class="nav-searchicon tl-mobile"><a class="column md-trigger" data-toggle="modal" data-target="#myModalsearch" href="#"><i class="material-icons">search</i></a></div>
        
        </nav>
		 </div> 

    </div>
    </div>
<?php if(tlf_get_option('enable_sticky_header','')):?>
<script>
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = jQuery('.tl-staticinner-navigation').outerHeight();

jQuery(window).scroll(function(event){
	didScroll = true;
});

setInterval(function() {
	if (didScroll) {
		hasScrolled();
		didScroll = false;
	}
}, 250);

function hasScrolled() {
	var st = jQuery(this).scrollTop();
	
	// Make sure they scroll more than delta
	if(Math.abs(lastScrollTop - st) <= delta)
		return;
	
	// If they scrolled down and are past the navbar, add class .nav-up.
	// This is necessary so you never see what is "behind" the navbar.
	if (st > lastScrollTop && st > navbarHeight){
		// Scroll Down
		jQuery('.tl-staticinner-navigation').removeClass('nav-down').addClass('nav-up');
		jQuery('.nav-down').css('top', '-150px', 'opacity', '0');
	} else {
		// Scroll Up
		if(st + jQuery(window).height() < jQuery(document).height()) {
			jQuery('.tl-staticinner-navigation').removeClass('nav-up').addClass('nav-down');
			jQuery('.nav-up').css('top', '0', 'opacity', '1');
		}
	}
	
	lastScrollTop = st;
}
</script>
<?php endif;?>