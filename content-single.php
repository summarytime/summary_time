<?php
/**
 * @package tl
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="single-content-post-wrapper">
	<div class="entry-content">
    	<?php if(tlf_get_option('show_single_featured_image','')):?>
		<div class="entry-content-thumbnail">
			<?php the_post_thumbnail('single-img'); ?>
		</div>
        <?php endif;?>
        <?php if(tlf_get_option('show_single_date','')):?>
		<div class="entry-meta">
			<?php tl_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif;?>
		<div class="tl-singlepost-article">
		<?php the_content(); ?>
		</div>
		<?php tl_link_pages(); ?>
	</div><!-- .entry-content -->
</div><!-- .single-content-post-wrapper -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'tl' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'tl' ) );

			if ( ! tl_categorized_blog() && tlf_get_option('show_single_categories','')) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'tl' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'tl' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list && tlf_get_option('show_single_categories','') && tlf_get_option('show_single_tags','')) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'tl' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'tl' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit', 'tl' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
