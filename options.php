<?php

/**

 * A unique identifier is defined to store the options in the database and reference them from the theme.

 */

function tlframework_option_name() {

	// Change this to use your theme slug

	return 'tl-framework-theme';

}



/**

 * Defines an array of options that will be used to generate the settings page and be saved in the database.

 * When creating the 'id' fields, make sure to use all lowercase and no spaces.

 *

 * If you are making your theme translatable, you should replace 'tl'

 * with the actual text domain for your theme.  Read more:

 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain

 */



function tlframework_options() {



	// Test data

	$test_array = array(

		'one' => __( 'One', 'tl' ),

		'two' => __( 'Two', 'tl' ),

		'three' => __( 'Three', 'tl' ),

		'four' => __( 'Four', 'tl' ),

		'five' => __( 'Five', 'tl' )

	);



	// Multicheck Array

	$multicheck_array = array(

		'one' => __( 'French Toast', 'tl' ),

		'two' => __( 'Pancake', 'tl' ),

		'three' => __( 'Omelette', 'tl' ),

		'four' => __( 'Crepe', 'tl' ),

		'five' => __( 'Waffle', 'tl' )

	);



	// Multicheck Defaults

	$multicheck_defaults = array(

		'one' => '1',

		'five' => '1'

	);



	// Background Defaults

	$background_defaults = array(

		'color' => '',

		'image' => '',

		'repeat' => 'repeat',

		'position' => 'top center',

		'attachment'=>'scroll' );



	// Typography Defaults

	$typography_defaults = array(

		'size' => '15px',

		'face' => 'georgia',

		'style' => 'bold',

		'color' => '#bada55' );



	// Typography Options

	$typography_options = array(

		'sizes' => array( '6','12','14','16','20' ),

		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),

		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),

		'color' => false

	);



  $font_weights = array(

  		 "0" =>	"Default",

	   "100" => "Thin (100)",

	   "200" => "Extra Light (200)",

	   "300" => "Light (300)",

	   "400" => "Normal (400)",

	   "500" => "Medium (500)",

	   "600" => "Semi Bold (600)",

	   "700" => "Bold (700)",

	   "800" => "Bolder (800)",

	   "900" => "Extra Bold (900)",

  );

  

  $background = array(

  		'default'=>'Default',

		'color-turquoise'=>'Turquoise',

		'color-greensea'=>'Green Sea',

		'color-sunflower'=>'Sunflower',

		'color-orange'=>'Orange',

		'color-emrald'=>'Emrald',

		'color-nephritis'=>'Nephritis',

		'color-carrot'=>'Carrot',

		'color-pumpkin'=>'Pumpkin',

		'color-peterriver'=>'Peter River',

		'color-belizehole'=>'Belize Hole',

		'color-alizarin'=>'Alizarin',

		'color-pomegranate'=>'Pomegranate',

		'color-amethyst'=>'Amethyst',

		'color-wisteria'=>'Wisteria',

		'color-wetasphalt'=>'Wet Asphalt',

		'color-midnightblue'=>'Midnight Blue',

		'color-brown'=>'Brown',

		'color-cyan'=>'Cyan',

		'color-teal'=>'Teal',

		'color-grey'=>'Grey',

		'color-green'=>'Green',

		'color-lightgreen'=>'Light Green',

		'color-lime'=>'Lime',

		'color-yellow'=>'Yellow',

		'color-amber'=>'Amber',

		'color-darkgrey'=>'Dark Grey',

		'color-mignightblack'=>'Mignight Black',

		'color-black'=>'Black',

		'color-white'=>'White',

		'color-cloud'=>'Cloud',

		'color-silver'=>'Silver',

		'color-concrete'=>'Concrete',

		'color-asbestos'=>'Asbestos',

  );

  

  $font = array(

  		'default'=>'Default',

		'font_turquoise'=>'Turquoise',

		'font_greensea'=>'Green Sea',

		'font_sunflower'=>'Sunflower',

		'font_orange'=>'Orange',

		'font_emrald'=>'Emrald',

		'font_nephritis'=>'Nephritis',

		'font_carrot'=>'Carrot',

		'font_pumpkin'=>'Pumpkin',

		'font_peterriver'=>'Peter River',

		'font_belizehole'=>'Belize Hole',

		'font_alizarin'=>'Alizarin',

		'font_pomegranate'=>'Pomegranate',

		'font_amethyst'=>'Amethyst',

		'font_wisteria'=>'Wisteria',

		'font_wetasphalt'=>'Wet Asphalt',

		'font_midnightblue'=>'Midnight Blue',

		'font_brown'=>'Brown',

		'font_cyan'=>'Cyan',

		'font_teal'=>'Teal',

		'font_green'=>'Green',

		'font_lightgreen'=>'Light Green',

		'font_lime'=>'Lime',

		'font_yellow'=>'Yellow',

		'font_amber'=>'Amber',

		'font_grey'=>'Grey',

		'font_darkgrey'=>'Dark Grey',

		'font_mignightblack'=>'Mignight Black',

		'font_black'=>'Black',

		'font_white'=>'White',

		'font_cloud'=>'Cloud',

		'font_silver'=>'Silver',

		'font_concrete'=>'Concrete',

		'font_asbestos'=>'Asbestos',

  );

  

  $breadcrumb = array(

 	''				=>'Default',

	'pull-left'		=>'Left',

	'pull-right'	=>'Right',

	'text-center'	=>'Center',

  );

  

  $page_title_bg_color = array(

 		''						=>'Default',

		'color-turquoise'		=>'Turquoise',

		'color-greensea'		=>'Green Sea',

		'color-sunflower'		=>'Sunflower',

		'color-orange'			=>'Orange',

		'color-emrald'			=>'Emrald',

		'color-nephritis'		=>'Nephritis',

		'color-carrot'			=>'Carrot',

		'color-pumpkin'			=>'Pumpkin',

		'color-peterriver'		=>'Peter River',

		'color-belizehole'		=>'Belize Hole',

		'color-alizarin'		=>'Alizarin',

		'color-pomegranate'		=>'Pomegranate',

		'color-amethyst'		=>'Amethyst',

		'color-wisteria'		=>'Wisteria',

		'color-wetasphalt'		=>'Wet Asphalt',

		'color-midnightblue'	=>'Midnight Blue',

		'color-brown'			=>'Brown',

		'color-cyan'			=>'Cyan',

		'color-teal'			=>'Teal',

		'color-grey'			=>'Grey',

		'color-green'			=>'Green',

		'color-lime'			=>'Lime',

		'color-yellow'			=>'Yellow',

		'color-amber'			=>'Amber',

		'color-darkgrey'		=>'Dark Grey',

		'color-mignightblack'	=>'Mignight Black',

		'color-black'			=>'Black',

		'color-white'			=>'White',

		'color-cloud'			=>'Cloud',

		'color-silver'			=>'Silver',

		'color-concrete'		=>'Concrete',

		'color-asbestos'		=>'Asbestos'

  );



	// Pull all the categories into an array

	$options_categories = array();

	$options_categories_obj = get_categories();

	foreach ($options_categories_obj as $category) {

		$options_categories[$category->cat_ID] = $category->cat_name;

	}



	// Pull all tags into an array

	$options_tags = array();

	$options_tags_obj = get_tags();

	foreach ( $options_tags_obj as $tag ) {

		$options_tags[$tag->term_id] = $tag->name;

	}





	// Pull all the pages into an array

	$options_pages = array();

	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );

	$options_pages[''] = 'Select a page:';

	foreach ($options_pages_obj as $page) {

		$options_pages[$page->ID] = $page->post_title;

	}



	// If using image radio buttons, define a directory path

	$imagepath =  get_template_directory_uri() . '/tl-framework/images/';

	$tlf_options_seo 	= array("summary" => "Summary","summary_large_image" => "Summary With Large Image");



	$options = array();

	

	$options[] = array(

		'name' => __( 'General Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Show Breadcrumb', 'tl' ),

		'desc' => __( 'Show Breadcrumb.', 'tl' ),

		'id' => 'show_breadcrumb',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Breadcrumb Position', 'tl' ),

		'desc' => __( 'Breadcrumb Position.', 'tl' ),

		'id' => 'breadcrumb_position',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $breadcrumb

	);

	

	$options[] = array(

		'name' => __( 'Title Background Color Scheme', 'tl' ),

		'desc' => __( 'Title Background Color Scheme', 'tl' ),

		'id' => 'title_bg_color',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $page_title_bg_color

	);

	

	$options[] = array(

		'name' => __( 'Title Background Color', 'tl' ),

		'desc' => __( 'Title background color', 'tl' ),

		'id' => 'breadcrumb_background_color',

		'std' => '',

		'type' => 'color'

	);

	

	/*$options[] = array(

		'name' => __( 'Breadcrumb Background Image', 'tl' ),

		'desc' => __( 'This will upload the breadcrumb background image of your theme.', 'tl' ),

		'id' => 'breadcrumb_image_uploader',

		'type' => 'upload'

	);*/

	

	$options[] = array(

		'name' => __( 'Header Settings', 'tl' ),

		'type' => 'heading'

	);

	

	/*$options[] = array(

		'name' => __( 'Header Layout', 'tl' ),

		'desc' => __( 'Header layout.', 'tl' ),

		'id' => 'header_layout',

		'std' => 'default',

		"type" => "images",

		"options" => array(

				'default' => $imagepath.'header-default.png',

				'layout1' => $imagepath.'header-1.png',

				'layout2' => $imagepath.'header-2.png',

				'layout3' => $imagepath.'header-3.png',

			)

		);*/

	

	$options[] = array(

		'name' => __( 'Logo Uploader', 'tl' ),

		'desc' => __( 'This will upload the logo of your theme.', 'tl' ),

		'id' => 'logo_uploader',

		'type' => 'upload'

	);

	

	$options[] = array(

		'name' => __( 'Text Logo', 'tl' ),

		'desc' => __( 'Text logo if there is no image logo present.', 'tl' ),

		'id' => 'text_logo',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Show Site Description', 'tl' ),

		'desc' => __( 'Show Site Description.', 'tl' ),

		'id' => 'show_site_description',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Alternative Site Description', 'tl' ),

		'desc' => __( 'This will replace the default site description.', 'tl' ),

		'id' => 'site_description',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Favicon Uploader', 'tl' ),

		'desc' => __( 'This will upload the favicon of your theme.', 'tl' ),

		'id' => 'favicon_uploader',

		'type' => 'upload'

	);	

	

	$options[] = array(

		'name' => __( 'Enable Sticky Header', 'tl' ),

		'desc' => __( 'This will convert the header to a fixed top header.', 'tl' ),

		'id' => 'enable_sticky_header',

		'type' => 'checkbox'

	);	

	

	$options[] = array(

		'name' => __( 'Show Header Top', 'tl' ),

		'desc' => __( 'Show header top which consists of phone number, fax number, email and social icons.', 'tl' ),

		'id' => 'show_header_top',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Phone Number', 'tl' ),

		'desc' => __( 'Show phone number at header.', 'tl' ),

		'id' => 'show_phone_number',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Email', 'tl' ),

		'desc' => __( 'Show email at header.', 'tl' ),

		'id' => 'show_email',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Social Icons', 'tl' ),

		'desc' => __( 'Show social icons at header.', 'tl' ),

		'id' => 'show_social_icon',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Header Top Background Color Scheme', 'tl' ),

		'desc' => __( 'Header top background color scheme.', 'tl' ),

		'id' => 'header_top_background_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $background

	);

	

	$options[] = array(

		'name' => __( 'Header Top Text Color Scheme', 'tl' ),

		'desc' => __( 'Header top text color scheme.', 'tl' ),

		'id' => 'header_top_text_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $font

	);

	

	$options[] = array(

		'name' => __( 'Header Bottom Background Color Scheme', 'tl' ),

		'desc' => __( 'Header bottom background color scheme.', 'tl' ),

		'id' => 'header_bottom_background_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $background

	);

	

	$options[] = array(

		'name' => __( 'Header Bottom Text Color Scheme', 'tl' ),

		'desc' => __( 'Header bottom text color scheme.', 'tl' ),

		'id' => 'header_bottom_text_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $font

	);

	$options[] = array(
		'name' => __( 'Show Loader', 'tl' ),
		'desc' => __( 'Show loader.', 'tl' ),
		'id' => 'show_loader',
		'type' => 'checkbox'
	);

	$options[] = array(

		'name' => __( 'Footer Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Footer Copyright Text', 'tl' ),

		'desc' => __( 'Enter the footer copyright text', 'tl' ),

		'id' => 'footer_copyright_text',

		'std' => '',

		'type' => 'textarea'

	);

	

	$options[] = array(

		'name' => __( 'Show Footer Widget', 'tl' ),

		'desc' => __( 'Show footer widget', 'tl' ),

		'id' => 'show_footer_widget',

		'std' => '1',

		'type' => 'checkbox'

	);	

	

	$options[] = array(

		'name' => __( 'Enable Parallax Footer', 'tl' ),

		'desc' => __( 'Enable Parallax Footer', 'tl' ),

		'id' => 'enable_parallax_footer',

		'std' => '0',

		'type' => 'checkbox'

	);	

	

	$options[] = array(

		'name' => __( 'Footer Column', 'tl' ),

		'desc' => __( 'Footer column.', 'tl' ),

		'id' => 'footer_column',

		'std' => 'right',

		"type" 		=> "select",

		"options" 	=> array(

				'1'=>'1',

				'2'=>'2',

				'3'=>'3',

				'4'=>'4',

			),

	);

	

	$options[] = array(

		'name' => __( 'Footer Top Background Color Scheme', 'tl' ),

		'desc' => __( 'Footer top background color scheme.', 'tl' ),

		'id' => 'footer_top_background_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $background

	);

	

	$options[] = array(

		'name' => __( 'Footer Top Text Color Scheme', 'tl' ),

		'desc' => __( 'Footer top text color scheme.', 'tl' ),

		'id' => 'footer_top_text_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $font

	);

	

	$options[] = array(

		'name' => __( 'Footer Bottom Background Color Scheme', 'tl' ),

		'desc' => __( 'Footer bottom background color scheme.', 'tl' ),

		'id' => 'footer_bottom_background_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $background

	);

	

	$options[] = array(

		'name' => __( 'Footer Bottom Text Color Scheme', 'tl' ),

		'desc' => __( 'Footer bottom text color scheme.', 'tl' ),

		'id' => 'footer_bottom_text_color_scheme',

		'std' => 'default',

		"type" 		=> "select",

		"options" 	=> $font

	);

	

	$options[] = array(

		'name' => __( 'Social Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Facebook URL', 'tl' ),

		'desc' => __( 'Enter the facebook URL', 'tl' ),

		'id' => 'facebook_url',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Twitter URL', 'tl' ),

		'desc' => __( 'Enter the Twitter URL', 'tl' ),

		'id' => 'twitter_url',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Linkedin URL', 'tl' ),

		'desc' => __( 'Enter the Linkedin URL', 'tl' ),

		'id' => 'linkedin_url',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'YouTube Url', 'tl' ),

		'desc' => __( 'Enter the You Tube', 'tl' ),

		'id' => 'youtube_url',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Google Plus URL', 'tl' ),

		'desc' => __( 'Enter the Google Plus URL', 'tl' ),

		'id' => 'google_plus_url',

		'std' => '',

		'type' => 'text'

	);	

	

	$options[] = array(

		'name' => __( 'Email', 'tl' ),

		'desc' => __( 'Enter the Email ID', 'tl' ),

		'id' => 'email_id',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Phone Number', 'tl' ),

		'desc' => __( 'Enter the Phone Number', 'tl' ),

		'id' => 'phone_number',

		'std' => '',

		'type' => 'text'

	);



	$options[] = array(

		'name' => __( 'Analytics and Script Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Write Header Script', 'tl' ),

		'desc' => __( 'Write header script inside &lt;head&gt; tag', 'tl' ),

		'id' => 'header_script',

		'std' => '',

		'type' => 'textarea'

	);

	

	$options[] = array(

		'name' => __( 'Write Body Script', 'tl' ),

		'desc' => __( 'Write script inside &lt;body&gt; tag', 'tl' ),

		'id' => 'body_script',

		'std' => '',

		'type' => 'textarea'

	);

	

	$options[] = array(

		'name' => __( 'Write Footer Script', 'tl' ),

		'desc' => __( 'Write script just before closing &lt;&#47;body&gt; tag', 'tl' ),

		'id' => 'footer_script',

		'std' => '',

		'type' => 'textarea'

	);

	

	

	

	// SEO Options



$options[] = array( 	"name" 		=> "SEO Tools",

						"type" 		=> "heading",

				);

				

$options[] = array( "name" 		=> "SEO Options",

						"desc" 		=> "Enable or Disable SEO Options Metabox in your theme",

						"id" 		=> "tl_seo",

						"std" 		=> 'on',

						"type" 		=> "radio",

						"options" 	=> array(

							'on' 			=> 'Enable',

							'off' 			=> 'Disable'

						)

				);				



$options[] = array( "name" 		=> "Title Separator",

						"desc" 		=> "Choose the symbol to use as your title separator. This will display, for instance, between your post title and site name. Symbols are shown in the size they'll appear in in search results.",

						"id" 		=> "seo_title_seperator",

						"std" 		=> "",

						"type" 		=> "radio",

						"options" 	=> array(

							'-' 	=> '-',

							'–' 	=> '–',

							'—'		=> '—',

							'·' 	=> '·',

							'•' 	=> '•',

							'*' 	=> '*',

							'⋆' 	=> '⋆',

							'|' 	=> '|',

							'~' 	=> '~',

							'«' 	=> '«',

							'»' 	=> '»',

							'<' 	=> '<',

							'>' 	=> '>'

						)

				);



$options[] = array( 	"name" 		=> "Front Page Settings:",

						"desc" 		=> "",

						"id" 		=> "",

						"std" 		=> "#",

						"type" 		=> ""

				);



$options[] = array( 	"name" 		=> "Image",

						"desc" 		=> "Upload Image.",

						"id" 		=> "seo_facebook_front_image",

						"std" 		=> "",

						"type" 		=> "upload"

				);

				

$options[] = array( 	"name" 		=> "Title",

						"desc" 		=> "Title.",

						"id" 		=> "seo_facebook_title",

						"std" 		=> "",

						"type" 		=> "text"

				);

				

$options[] = array( 	"name" 		=> "Description",

						"desc" 		=> "Description.",

						"id" 		=> "seo_facebook_description",

						"std" 		=> "",

						"type" 		=> "textarea"

				);

				

$options[] = array( 	"name" 		=> "Default Settings:",

						"desc" 		=> "",

						"id" 		=> "",

						"std" 		=> "#",

						"type" 		=> ""

				);

				

$options[] = array( 	"name" 		=> "Image",

						"desc" 		=> "Upload Image.",

						"id" 		=> "seo_facebook_default_image",

						"std" 		=> "",

						"type" 		=> "upload"

				);

				

$options[] = array( 	"name" 		=> "Facebook Page URL",

						"desc" 		=> "Facebook Page URL.",

						"id" 		=> "seo_facebook_page_url",

						"std" 		=> "#",

						"type" 		=> "text"

				);

				

$options[] = array( 	"name" 		=> "Twitter Username",

						"desc" 		=> "Twitter Username.",

						"id" 		=> "seo_twitter_username",

						"std" 		=> "",

						"type" 		=> "text"

				);

				

$options[] = array( 	"name" 		=> "The default card type to use",

						"desc" 		=> "Select the default card type.",

						"id" 		=> "seo_twitter_default_card_type",

						"std" 		=> "1",

						"type" 		=> "select",

						"options" 	=> $tlf_options_seo

				);

				

$options[] = array( 	"name" 		=> "Google Publisher Page",

						"desc" 		=> "Google Publisher Page.",

						"id" 		=> "seo_google_publisher_page",

						"std" 		=> "",

						"type" 		=> "text"

				);



	$options[] = array( "name" 		=> "Optimization",

						"type" 		=> "heading",

				);	

				

	$options[] = array( "name" 		=> "Page Speed Optimization",

						"desc" 		=> "Enable or Disable Page Speed Optimization",

						"id" 		=> "tl_page_speed",

						"std" 		=> 'off',

						"type" 		=> "radio",

						"options" 	=> array(

							'on' 			=> 'Enable',

							'off' 			=> 'Disable'

						)

				);

				

	$options[] = array( "name" 		=> "Defer/Async Scripts",

						"desc" 		=> "Enable or Disable Defer/Async Scripts",

						"id" 		=> "tl_defer_script",

						"std" 		=> 'off',

						"type" 		=> "radio",

						"options" 	=> array(

							'defer' 		=> 'Defer',

							'async' 		=> 'Async',

							'off' 			=> 'Disable'

						)

				);

	

	$options[] = array( "name" 		=> "Exclude Scripts from Defer/Async Script",

						"desc" 		=> "Exclude scripts from defer/async. Write comma seperated scripts.",

						"id" 		=> "tl_exclude_defer_script",

						'placeholder' => 'eg. jquery.js,bootstrap.min.js',

						"std" 		=> 'jquery.js,',

						"type" 		=> "textarea",

				);

				

	$options[] = array( "name" 		=> "Place All Scripts To Footer",

						"desc" 		=> "Place All Scripts To Footer.",

						"id" 		=> "tl_place_script_footer",

						"std" 		=> '0',

						"type" 		=> "checkbox",

				);

	$options[] = array( "name" 		=> "Exclude Scripts From Placing At Footer",

						"desc" 		=> "To exclude scripts from placing at footer you need to put the names of the scripts according to the syntax (comma seperated along with the script name with pipeline).",

						"id" 		=> "tl_exclude_footer_script",

						'placeholder' => 'syntax: script-name1|script-source1,script-name2|script-source2, eg. jquery|http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',

						"std" 		=> '',

						"type" 		=> "textarea",

				);

				

	$options[] = array( "name" 		=> "Remove Version From All Stylesheets",

						"desc" 		=> "Remove Version From All Stylesheets.",

						"id" 		=> "remove_version_stylesheet",

						"std" 		=> '0',

						"type" 		=> "checkbox",

				);

				

	$options[] = array( "name" 		=> "Remove Version From All Scripts",

						"desc" 		=> "Remove Version From All Scripts.",

						"id" 		=> "remove_version_script",

						"std" 		=> '0',

						"type" 		=> "checkbox",

				);

				

	$options[] = array(

		'name' => __( 'Blog Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Blog Details Page Option', 'tl' ),

		'id' => 'blog_details_page_option',

		'std' => '',

		'type' => 'info'

	);

	

	$options[] = array(

		'name' => __( 'Show Blog Title', 'tl' ),

		'desc' => __( 'Show blog title', 'tl' ),

		'id' => 'show_single_blog_title',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Featured Image', 'tl' ),

		'desc' => __( 'Show featured image', 'tl' ),

		'id' => 'show_single_featured_image',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Date', 'tl' ),

		'desc' => __( 'Show date', 'tl' ),

		'id' => 'show_single_date',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Categories', 'tl' ),

		'desc' => __( 'Show categories', 'tl' ),

		'id' => 'show_single_categories',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Tags', 'tl' ),

		'desc' => __( 'Show tags', 'tl' ),

		'id' => 'show_single_tags',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Comments On Single Page', 'tl' ),

		'desc' => __( 'Show comments on single page', 'tl' ),

		'id' => 'show_comment_single_page',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Validate Comment By Math Calculation', 'tl' ),

		'desc' => __( 'Validate comment by math calculation', 'tl' ),

		'id' => 'validate_comment',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( 'Show Post Navigation', 'tl' ),

		'desc' => __( 'Show post navigation', 'tl' ),

		'id' => 'show_post_navigation',

		'std' => '1',

		'type' => 'checkbox'

	);

	

	$options[] = array(

		'name' => __( '404 Page settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Upload 404 Image', 'tl' ),

		'desc' => __( 'Upload 404 Image', 'tl' ),

		'id' => 'image_404',

		'std' => '',

		'type' => 'upload'

	);

	

	$options[] = array(

		'name' => __( 'Write 404 Content', 'tl' ),

		'desc' => __( 'Write 404 Content', 'tl' ),

		'id' => 'content_404',

		'std' => '',

		'type' => 'editor'

	);

	

	$options[] = array(

		'name' => __( 'Post Order Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$post_types = get_post_types(array(

                                'show_ui' => true,

                                'show_in_menu' => true,

                              ), 'objects');

	$post_list = array();

	foreach ($post_types as $post_type) {

		if ($post_type->name == 'attachment')

			continue;

		$post_list[$post_type->name] = $post_type->label;

	}

	

	$options[] = array(

		'name' => __( 'Check To Sort Post Types', 'tl' ),

		'desc' => __( 'Check To Sort Post Types', 'tl' ),

		'id' => 'post_type_sort',

		'std' => '',

		'type' => 'multicheck',

		"options" 	=> $post_list

	);

	

	$tax_list = array();

	$taxonomies = get_taxonomies(array(

                                'show_ui' => true,

                                    ), 'objects');



	foreach ($taxonomies as $taxonomy) {

		if ($taxonomy->name == 'post_format')

			continue;

		$tax_list[$taxonomy->name] = $taxonomy->label;

	}

	

	$options[] = array(

		'name' => __( 'Check To Sort Taxonomies', 'tl' ),

		'desc' => __( 'Check To Sort Taxonomies', 'tl' ),

		'id' => 'taxonomy_sort',

		'std' => '',

		'type' => 'multicheck',

		"options" 	=> $tax_list

	);

	

	$options[] = array(

		'name' => __( 'Custom CSS', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Write Custom CSS', 'tl' ),

		'desc' => __( 'Write custom css', 'tl' ),

		'id' => 'custom_css',

		'std' => '',

		'type' => 'textarea'

	);

	

	$options[] = array(

		'name' => __( 'Admin Settings', 'tl' ),

		'type' => 'heading'

	);

	

	$options[] = array(

		'name' => __( 'Upload Admin Login Screen Image', 'tl' ),

		'desc' => __( 'Upload admin login screen image', 'tl' ),

		'id' => 'admin_login_image',

		'std' => '',

		'type' => 'upload'

	);

	

	$options[] = array(

		'name' => __( 'Logo Background Color', 'tl' ),

		'desc' => __( 'Logo background color', 'tl' ),

		'id' => 'logo_background_color',

		'std' => '',

		'type' => 'color'

	);

	

	$options[] = array(

		'name' => __( 'Logo URL', 'tl' ),

		'desc' => __( 'Logo url', 'tl' ),

		'id' => 'logo_url',

		'std' => '',

		'type' => 'text'

	);

	

	$options[] = array(

		'name' => __( 'Logo Title', 'tl' ),

		'desc' => __( 'Logo title', 'tl' ),

		'id' => 'logo_title',

		'std' => '',

		'type' => 'text'

	);



	return $options;

}