<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package tl
 */

get_header(); ?>
<div class="archive-page-header">
    <div class="container">
        <header>
            <h1 class="page-title">
            <?php _e( 'Error 404', 'tl' ); ?>
            </h1>
            </div>
        </header><!-- .entry-header -->
    </div>
</div>
<div class="tl-404-page">
<div class="main-content">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner tl-page-not-found-90 tl-page-not-found-91 col-sm-12 col-md-8">

	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>
	<section class="content-padder error-404 not-found">

		<header>
			<?php echo do_shortcode(tlf_get_option('content_404',''));?>
		</header><!-- .page-header -->

		<div class="page-content clearfix">

			
			
			<div class="warning-wrapper">
            	<?php if(tlf_get_option('image_404','')):?>
				<img class="image-404" src="<?php echo tlf_get_option('image_404','');?>" alt="" />
                <?php endif;?>
				<div class="error-404-search"><?php get_search_form(); ?></div>
			</div>
            
			

		</div><!-- .page-content -->

	</section><!-- .content-padder -->

			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	<div class="col-sm-12 col-md-4">		
	<?php get_sidebar(); ?>
	</div>		
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
</div><!-- close .tl-404-page -->

<?php get_footer(); ?>