<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tl
 */

get_header(); ?>
<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
<?php $bc_bg_class	=	tlf_get_option('title_bg_color','');  ?> 
<div class="archive tl-archive-outer-wrapper">
<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>" <?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
<div class="container">
<div class="tl-breadcrumb-page-title">
        <header>
            <h1 class="page-title">
                <?php _e( 'Blog', 'tl' );?>
            </h1>
            <?php
                // Show an optional term description.
                $term_description = term_description();
                if ( ! empty( $term_description ) ) :
                    printf( '<div class="taxonomy-description">%s</div>', $term_description );
                endif;
            ?>
        </header><!-- .entry-header -->
        </div><!-- .tl-breadcrumb-page-title -->
        
    </div>
</div>
<div class="main-content main-content-padding">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner tl-cp-main-left1 tl-archive-inner-left col-sm-12 col-md-8">
	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>


		

			<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php tl_content_nav( 'nav-below' ); ?>
            <?php //tl_pagination(); ?>
		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>



		

			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <div class="col-sm-12 col-md-4">
            	<?php get_sidebar(); ?>
            </div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
</div>
<?php get_footer(); ?>