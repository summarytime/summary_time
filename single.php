<?php
/**
 * The Template for displaying all single posts.
 *
 * @package tl
 */

get_header(); ?>
<div class="tl-single-outer-wrapper">
<?php while ( have_posts() ) : the_post(); ?>

		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); $bc_bg_class	=	tlf_get_option('title_bg_color','');  ?> 
		
<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>"<?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
    <div class="container">
 <div class="tl-breadcrumb-page-title">
        <header>
			<?php if(get_post_meta( get_the_ID(), 'tl_hide_title', true )!='on'):?>
            <div class="<?php echo (get_post_meta( get_the_ID(), 'tl_title_position', true ))?get_post_meta( get_the_ID(), 'tl_title_position', true ):'';?>">
            <h1 class="page-title">
            <?php the_title(); ?>
            </h1>
            </div>
            <?php endif;
            
            ?>
        </header><!-- .entry-header -->
    </header><!-- .tl-breadcrumb-page-title -->
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>
    </div>
</div>
<div class="main-content main-content-padding">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">			
			<div class="tl-contentsingle-wrapper">
						<?php get_template_part( 'content', 'single' ); ?>
						<?php if(tlf_get_option('show_post_navigation','')):?>
						<?php tl_content_nav( 'nav-below' ); ?>
                        <?php endif;?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( (comments_open() || '0' != get_comments_number()) && tlf_get_option('show_comment_single_page',''))
								comments_template();
						?>
				</div>
			
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <div class="col-sm-12 col-md-4">
            	<?php get_sidebar(); ?>
            </div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php endwhile; // end of the loop. ?>

</div><!-- close .srishti-outer-wrapper -->
<?php get_footer(); ?>