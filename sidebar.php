<?php
/**
 * The sidebar containing the main widget area
 *
 * @package tl
 */
?>



		<?php // add the class "panel" below here to wrap the sidebar in Bootstrap style ;) ?>
		
		
		<div class="sidebar-padder tl-cp-sidebar1 tl-cp-sidebar2 sidebar">

			<?php do_action( 'before_sidebar' ); ?>
            <?php $sidebar = get_post_meta(get_the_ID(),'themelines_sidebar', true)?get_post_meta(get_the_ID(),'themelines_sidebar', true):'sidebar-1';?>
			<?php if ( ! dynamic_sidebar( $sidebar ) ) : ?>

				<aside id="search" class="widget widget_search">
					<?php get_search_form(); ?>
				</aside>

				<aside id="archives" class="widget widget_archive">
					<h3 class="widget-title"><?php _e( 'Archives', 'tl' ); ?></h3>
					<ul>
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

				<aside id="meta" class="widget widget_meta">
					<h3 class="widget-title"><?php _e( 'Meta', 'tl' ); ?></h3>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</aside>

			<?php endif; ?>

 </div>
 
 
 <!--</div> -->