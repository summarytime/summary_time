<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tl
 */
?>

<section class="no-results not-found tl-search-nothing-found">

	<div class="search-image-wrapper">
	<header>
		<h1 class="page-title"><?php _e( 'Nothing Found', 'tl' ); ?></h1>
	</header><!-- .page-header -->
	</div>
	
	<div class="search-image"><i class="material-icons">search</i></div>
	
	<div class="page-content">
		<?php if ( (is_front_page() || is_home()) && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'tl' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'tl' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'tl' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
