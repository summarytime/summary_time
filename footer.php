<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package tl
 */
?>

<footer id="colophon" class="site-footer <?php echo tlf_get_option('enable_parallax_footer','')?'tl-parallax-footer':'';?>" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
<div class="<?php echo (tlf_get_option('footer_top_background_color_scheme','') && tlf_get_option('footer_top_background_color_scheme','')!='default')?tlf_get_option('footer_top_background_color_scheme'):'footer-background';?>">
<div class="tl-corporate-footer-s1 footer-middle">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner">
            	<?php if(tlf_get_option('show_footer_widget','')):?>
            	<div class="<?php echo tlf_get_option('enable_parallax_footer','')?'tl-footer-parallax-top':'';?> <?php echo (tlf_get_option('footer_top_text_color_scheme','') && tlf_get_option('footer_top_text_color_scheme','')!='default')?tlf_get_option('footer_top_text_color_scheme'):'footer-color';?>">
                	<?php if(tlf_get_option('footer_column','')):?>
                    <?php $col = 12/tlf_get_option('footer_column','');?>
                	<?php for($i=1;$i<=tlf_get_option('footer_column','');$i++):?>
                    <?php if ( is_active_sidebar( 'footer-'.$i ) ) : ?>
                    <div class="col-lg-<?php echo $col;?> col-md-<?php echo $col;?> col-sm-<?php echo $col;?> col-xs-12">
                        <?php dynamic_sidebar( 'footer-'.$i ); ?>
                    </div>
                    <?php endif;?>
                    <?php endfor;?>
                    <?php endif;?>
                   
            	</div>
                <?php endif;?>
			</div><!-- close .sitefooter-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .footer-middle -->
</div>

<div class="<?php echo (tlf_get_option('footer_bottom_background_color_scheme','') && tlf_get_option('footer_bottom_background_color_scheme','')!='default')?tlf_get_option('footer_bottom_background_color_scheme'):'footer-background';?>">			
<div class="tl-corporate-footer-s1 footer-bottom tl-footer-copyright">
		<div class="container">
			<div class="row">	
				<div class="site-info col-sm-12 <?php echo tlf_get_option('enable_parallax_footer','')?'tl-footer-parallax-bottom':'';?> <?php echo tlf_get_option('enable_parallax_footer','')?'tl-footer-parallax-top':'';?> <?php echo (tlf_get_option('footer_bottom_text_color_scheme','') && tlf_get_option('footer_bottom_text_color_scheme','')!='default')?tlf_get_option('footer_bottom_text_color_scheme'):'footer-color';?>">
					<?php do_action( 'tl_credits' ); ?>
					<?php if(tlf_get_option( 'footer_copyright_text', '' )):?>
					<?php echo tlf_get_option( 'footer_copyright_text', '' );?>
					<?php else:?>
					<a href="http://wordpress.org/" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', 'tl' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', 'tl' ), 'WordPress' ); ?></a>
					<span class="sep"> | </span>
					<a class="credits" href="https://themelines.com/" target="_blank" title="Themes and Plugins developed by ThemeLines" alt="Themes and Plugins developed by ThemeLines"><?php _e('Themes and Plugins developed by ThemeLines.','tl') ?> </a>
					<?php endif;?>
			
				</div><!-- close .site-info -->
			</div><!-- close .row -->	
		</div><!-- close .container -->	
	</div><!-- close .tl-corporate-footer-s1 -->
</div>
	
</footer><!-- close #colophon -->

<?php wp_footer(); ?>
<?php if(tlf_get_option('enable_parallax_footer','')):?>
<script>
jQuery(function() {
	jQuery(".tl-parallax-footer").footerReveal({
		shadow : true,
		shadowOpacity: 0.8,
		zIndex : -101,
	});
});
</script>
<?php endif;?>
<?php if(tlf_get_option( 'footer_script', '' )):?>
<script>
	<?php echo tlf_get_option( 'footer_script', '' );?>
</script>
<?php endif;?>

<!-- 
Modal make an appointment 
-->
	<div class="make-an-appointment">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Make An Appointment</h4>
				  </div>
				  <div class="modal-body">
					Choose Option...
					<p><i class="fa fa-heartbeat" aria-hidden="true"></i></p>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				  </div>
				</div>
			</div>
		</div>
	</div>
<!-- Modal make an appointment -->	
<!-- home search 
-->
	<div class="tl-navigation-search-modal">
		<div class="modal fade" id="myModalsearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Search Your Query</h4>
				  </div>
				  <div class="modal-body">
				  
				  
					<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-9">
						<label>
							<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'tl' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'tl' ); ?>">
						</label>
					</div>
					<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">					
						<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'tl' ); ?>">
					</div>	
					</form>
					
					
					<p><i class="fa fa-heartbeat" aria-hidden="true"></i></p>
				  </div>
				  <div class="modal-footer">
				<!--	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				  </div>
				</div>
			</div>
		</div>
	</div>
<!-- Home Search end -->	
	
<!-- menu search section start 
<div class="search-ex">
	<div class="container">
		<div class="md-modal md-effect-12 color2" id="modal-12">
		<div class="md-close"></div>
			<div class="md-content padding-sp">
				<h4> Search Your Query</h4>
				<form class="sp-form">
					<div class="form-group">
						<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter your search...">
						<button type="submit" class="btn btn-default sp-text-search"><i class="fa fa-search"></i></button>
					  </div>							
				</form>
			</div>
		</div>
		<div class="md-overlay"></div>
	</div>
</div> -->

<!-- tl-google map -->
<script>
	jQuery(document).ready(function(){
		jQuery(".tl-google-map").click(function(){
			jQuery(".g-location").slideToggle("slow");
		});
	});
</script>
<script>
	jQuery(document).ready(function(){
		equalheight = function(container){

		var currentTallest = 0,
			 currentRowStart = 0,
			 rowDivs = new Array(),
			 jQueryel,
			 topPosition = 0;
		 jQuery(container).each(function() {

		   jQueryel = jQuery(this);
		   jQuery(jQueryel).height('auto')
		   topPostion = jQueryel.position().top;

		   if (currentRowStart != topPostion) {
			 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			   rowDivs[currentDiv].height(currentTallest);
			 }
			 rowDivs.length = 0; // empty the array
			 currentRowStart = topPostion;
			 currentTallest = jQueryel.height();
			 rowDivs.push(jQueryel);
		   } else {
			 rowDivs.push(jQueryel);
			 currentTallest = (currentTallest < jQueryel.height()) ? (jQueryel.height()) : (currentTallest);
		  }
		   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			 rowDivs[currentDiv].height(currentTallest);
		   }
		 });
		}

		/*jQuery(window).load(function() {
		  	equal_height();
		});
		jQuery(window).resize(function(){
		  	equal_height();
		});*/
	});
	
	/*function equal_height(){
		 equalheight('.tl-grid-portfolio-1 .portfolio-wrapper');
		 equalheight('.tl-col-height li');
		 equalheight('.team-div-col-2 .team-height');
		 equalheight('.team-div-col-3 .team-height');
		 equalheight('.team-div-col-4 .team-height');
		 equalheight('.tl-postslide-dark .item');
		 equalheight('.tl-4col-grid .tl-postmeta-box');
	}*/
</script>
<script>
//footer fixed 
 /*jQuery('footer').footerReveal();
	jQuery(function() {
		jQuery(".tl-parallax-footer").footerReveal({
		shadow : true,
		shadowOpacity: 0.8,
		zIndex : -100
		});
		});*/
</script>
<script>
 jQuery(window).load(function() {
	 jQuery("#tl-clean-loading").delay(1500).fadeOut(1000);
		jQuery("#loading-center").click(function() {
			jQuery("#loading").fadeOut(500);
	})
})
</script>
<!-- scroll to top -->
<script type='text/javascript'>
jQuery(document).ready(function(){ 
   jQuery(window).scroll(function(){ 
        if (jQuery(this).scrollTop() > 100) { 
            jQuery('#scroll-top').fadeIn(); 
        } else { 
            jQuery('#scroll-top').fadeOut(); 
        } 
    }); 
    jQuery('#scroll-top').click(function(){ 
        jQuery("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});


</script>



</body>
</html>
