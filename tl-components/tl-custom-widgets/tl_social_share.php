<?php
/******************TL SOCIAL SHARE WIDGETS***********************/

add_action( 'widgets_init', 'tl_social_share_widget' );


function tl_social_share_widget() {
	register_widget( 'TL_SOCIAL_SHARE_Widget' );
}

class TL_SOCIAL_SHARE_Widget extends WP_Widget {
	
	function __construct() {
		$widget_ops = array( 'classname' => 'tl-social_share', 'description' => __('A widget to activate and deactivate social shares', 'tl-social_share') );
		$control_ops = array( 'width' => 270, 'height' => 350, 'id_base' => 'tl-social_share-widget' );
		parent::__construct(
		'tl-social_share', 
		__('TL Social Share', 'tl'),
		array( 'description' => __( 'A widget to activate and deactivate social shares', 'tl' ), ) 
		);
	}


	function widget( $args, $instance ) {
		extract( $args );
		//Our variables from the widget settings.
		$title			= 	apply_filters('widget_title', $instance['title'] );
		$facebook			= 	($instance[ 'facebook' ])?true:false;
		$youtube			= 	($instance[ 'youtube' ])?true:false;
		$twitter			= 	($instance[ 'twitter' ])?true:false;
		$linkedIn			= 	($instance[ 'linkedIn' ])?true:false;
		$pinterest			= 	($instance[ 'pinterest' ])?true:false;
		$google_Plus		= 	($instance[ 'google_Plus' ])?true:false;
		$tumblr				= 	($instance[ 'tumblr' ])?true:false;
		$instagram			= 	($instance[ 'instagram' ])?true:false;
		$reddit				= 	($instance[ 'reddit' ])?true:false;
	//$vk					= 	($instance[ 'vk' ])?true:false;
		$flickr				= 	($instance[ 'flickr' ])?true:false;
	//$vine					= 	($instance[ 'vine' ])?true:false;
	//$meetup				= 	($instance[ 'meetup' ])?true:false;
	//$ask_fm				= 	($instance[ 'ask_fm' ])?true:false;
	//$classmates			= 	($instance[ 'classmates' ])?true:false;
		
		$siteUrl = get_the_permalink();
		?>
<?php echo $before_widget;?>
                   <?php echo $before_title;?> <?php echo $title;?> <?php echo $after_title;?>
				   <ul>
				   <?php echo ($facebook == true)?'<li><a href="https://www.facebook.com/sharer/sharer.php?u='.$siteUrl.'" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo ($twitter == true)?'<li><a href="https://twitter.com/intent/tweet?text=YOUR-TITLE&url='.$siteUrl.'"&via=TWITTER-HANDLE" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo ($linkedIn == true)?'<li><a href="https://www.linkedin.com/shareArticle?mini=true&url='.$siteUrl.'"&title=&summary=&source=YOUR-URL" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo ($pinterest == true)?'<li><a href="https://pinterest.com/pin/create/button/?url='.$siteUrl.'"&description=&media=" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo ($google_Plus == true)?'<li><a href="https://plus.google.com/share?url='.$siteUrl.'"><i class="fa fa-google-plus" aria-hidden="true" target="_blank"></i></a></li>':''; ?>
				   <?php echo ($tumblr == true)?'<li><a href="http://www.tumblr.com/share/link?url='.$siteUrl.'"&description=YOUR-DESCRIPTION" target="_blank"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>':''; ?>
				   <?php //echo ($instagram == true)?'<li><a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo ($reddit == true)?'<li><a href="http://www.reddit.com/submit?url='.$siteUrl.'"&title=YOUR_TITLE" target="_blank"><i class="fa fa-reddit" aria-hidden="true"></i></a></li>':''; ?>
				   <?php //echo ($flickr == true)?'<li><a href="" target="_blank"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>':''; ?>
				   </ul>
                  
<?php echo $after_widget;?>
<?php 
//echo $after_widget;
}
	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML 
		$instance['title'] 			= strip_tags( $new_instance['title'] );
		$instance['facebook'] 		= 	$new_instance[ 'facebook' ];
		$instance['youtube']		= 	$new_instance[ 'youtube' ];
		$instance['twitter']		= 	$new_instance[ 'twitter' ];
		$instance['linkedIn']		= 	$new_instance[ 'linkedIn' ];
		$instance['pinterest']		= 	$new_instance[ 'pinterest' ];
		$instance['google_Plus']	= 	$new_instance[ 'google_Plus' ];
		$instance['tumblr'] 		= 	$new_instance[ 'tumblr' ];
		$instance['instagram']		= 	$new_instance[ 'instagram' ];
		$instance['reddit']			= 	$new_instance[ 'reddit' ];
	//$instance['vk']				= 	$new_instance[ 'vk' ];
		$instance['flickr']			= 	$new_instance[ 'flickr' ];
	//$instance['vine']				= 	$new_instance[ 'vine' ];
	//$instance['meetup']			= 	$new_instance[ 'meetup' ];
	//$instance['ask_fm']			= 	$new_instance[ 'ask_fm' ];
	//$instance['classmates']		= 	$new_instance[ 'classmates' ];
		return $instance;
	}

	
	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TL Social Sharings', 'tl-social-share'), 'name' => __('TL Social Sharings', 'tl-social-share'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'tl-social-share'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        <ul>
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="facebook" <?php echo (isset($instance['facebook']) && $instance['facebook'])?'checked':''; ?>/> Facebook</label></li>
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="youtube" <?php echo (isset($instance['youtube']) && $instance['youtube'])?'checked':''; ?>/> YouTube</label></li>-->
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="twitter" <?php echo (isset($instance['twitter']) && $instance['twitter'])?'checked':''; ?>/> Twitter</label></li>
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'linkedIn' ); ?>" value="linkedIn" <?php echo (isset($instance['linkedIn']) && $instance['linkedIn'])?'checked':''; ?>/> LinkedIn</label></li>
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" value="pinterest" <?php echo (isset($instance['pinterest']) && $instance['pinterest'])?'checked':''; ?>/> Pinterest</label></li>
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'google_Plus' ); ?>" value="google plus" <?php echo (isset($instance['google_Plus']) && $instance['google_Plus'])?'checked':''; ?>/> Google Plus+</label></li>
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'tumblr' ); ?>" value="tumblr" <?php echo (isset($instance['tumblr']) && $instance['tumblr'])?'checked':''; ?>/> Tumblr</label></li>
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'instagram' ); ?>" value="instagram" <?php echo (isset($instance['instagram']) && $instance['instagram'])?'checked':''; ?>/> Instagram</label></li>-->
			<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'reddit' ); ?>" value="reddit" <?php echo (isset($instance['reddit']) && $instance['reddit'])?'checked':''; ?>/> Reddit</label></li>
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'vk' ); ?>" value="vk"> VK</label></li>-->
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'flickr' ); ?>" value="flickr" <?php echo (isset($instance['flickr']) && $instance['flickr'])?'checked':''; ?>/> Flickr</label></li>-->
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'vine' ); ?>" value="vine"> Vine</label></li>-->
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'meetup' ); ?>" value="meetup"> Meetup</label></li>
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'ask_fm' ); ?>" value="ask_fm"> Ask.fm</label></li>-->
		<!--<li><label><input type="checkbox" name="<?php echo $this->get_field_name( 'classmates' ); ?>" value="classmates"> ClassMates</label></li>-->
		</ul>
         
        
	<?php
	}
}