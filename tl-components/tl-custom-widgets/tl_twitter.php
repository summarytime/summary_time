<?php
/******************TL TWITTER WIDGETS***********************/

class tl_twitter_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
		'tl_twitter_widget', 
		__('TL Twitter', 'tl'),
		array( 'description' => __( 'A widget to show twitter feeds', 'tl' ), ) 
		);
	}
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		$twitter_customer_key = $instance[ 'twitter_customer_key' ];
		$twitter_customer_secret = $instance[ 'twitter_customer_secret' ];
		$twitter_access_token = $instance[ 'twitter_access_token' ];
		$twitter_access_token_secret = $instance[ 'twitter_access_token_secret' ];
		$twitter_screen_name = $instance[ 'twitter_screen_name' ];
		$number = ($instance[ 'number' ])?$instance[ 'number' ]:3;
?>
		<?php echo $before_widget;?>
		<div class="tweet-bolg"> 
        	<?php echo $before_title;?><?php echo $title;?><?php echo $after_title;?>
            <?php
				include_once('twitteroauth/twitteroauth.php');
				$connection = new TwitterOAuth($twitter_customer_key, $twitter_customer_secret, $twitter_access_token, $twitter_access_token_secret);	
				$my_tweets = $connection->get('statuses/user_timeline', array('screen_name' => $twitter_screen_name, 'count' => $number));
				//print_r($my_tweets);		
				echo '<ul class="tweet">';
				for($i=0; $i<$number; $i++){
				echo '<li class="tweet-item">';
				if($my_tweets[$i]){
					if(isset($my_tweets->errors))
					{           
						echo 'Error :'. $my_tweets->errors[$i]->code. ' - '. $my_tweets->errors[$i]->message;
					}else{
?>
					<a class="tweet_icon" href="#" target="_blank" rel="nofollow">
						<i class="fa fa-twitter"></i>
					</a>
					
					<a class="tweet_name" href="<?php echo $my_tweets[$i]->user->profile_image_url;?>" target="_blank">				
						<?php echo $my_tweets[$i]->user->name;?>
					</a>
					<span class="tweet_text">
						<?php echo makeClickableLinks($my_tweets[$i]->text);?>
					</span>
					<div class="tweet_control">
						<?php
							$dt = DateTime::createFromFormat('D M j H:i:s P Y', $my_tweets[$i]->created_at);
							echo $post_date = $dt->format('l jS F Y\, h:i:s A');
						?>
					</div>
<?php
					}
				}
				echo '</li>';
				}
				echo '</ul>';
?> 
        </div>
        <?php echo $after_widget;?> 
<?php
	}
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['twitter_customer_key'] = $new_instance[ 'twitter_customer_key' ];
		$instance['twitter_customer_secret'] = $new_instance[ 'twitter_customer_secret' ];
		$instance['twitter_access_token'] = $new_instance[ 'twitter_access_token' ];
		$instance['twitter_access_token_secret'] = $new_instance[ 'twitter_access_token_secret' ];
		$instance['twitter_screen_name'] = $new_instance[ 'twitter_screen_name' ];
		$instance['number'] = $new_instance[ 'number' ];
		return $instance;
	}
	public function form( $instance ) {
		
		//Set up some default widget settings.
		$defaults = array( 'title' => __('TL Twitter', 'tl'), 'name' => __('TL Twitter', 'tl'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	?>
        
         <p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo isset($instance['title'])?$instance['title']:''; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'twitter_customer_key' ); ?>"><?php _e('Customer Key:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter_customer_key' ); ?>" name="<?php echo $this->get_field_name( 'twitter_customer_key' ); ?>" value="<?php echo isset($instance['twitter_customer_key'])?$instance['twitter_customer_key']:''; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'twitter_customer_secret' ); ?>"><?php _e('Customer Secret:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter_customer_secret' ); ?>" name="<?php echo $this->get_field_name( 'twitter_customer_secret' ); ?>" value="<?php echo isset($instance['twitter_customer_secret'])?$instance['twitter_customer_secret']:''; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'twitter_access_token' ); ?>"><?php _e('Access Token:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter_access_token' ); ?>" name="<?php echo $this->get_field_name( 'twitter_access_token' ); ?>" value="<?php echo isset($instance['twitter_access_token'])?$instance['twitter_access_token']:''; ?>" style="width:100%;" />
		</p>
        
         <p>
			<label for="<?php echo $this->get_field_id( 'twitter_access_token_secret' ); ?>"><?php _e('Access Token Secret:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter_access_token_secret' ); ?>" name="<?php echo $this->get_field_name( 'twitter_access_token_secret' ); ?>" value="<?php echo isset($instance['twitter_access_token_secret'])?$instance['twitter_access_token_secret']:''; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'twitter_screen_name' ); ?>"><?php _e('Screen Name:', 'tl'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter_screen_name' ); ?>" name="<?php echo $this->get_field_name( 'twitter_screen_name' ); ?>" value="<?php echo isset($instance['twitter_screen_name'])?$instance['twitter_screen_name']:''; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Number of Posts:', 'tl'); ?></label>
            <select id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" style="width:100%;">
            	<option value="1" <?php echo (isset($instance['number']) && $instance['number']==1)?'selected="selected"':'';?>>1</option>
                <option value="2" <?php echo (isset($instance['number']) && $instance['number']==2)?'selected="selected"':'';?>>2</option>
                <option value="3" <?php echo (isset($instance['number']) && $instance['number']==3)?'selected="selected"':'';?>>3</option>
                <option value="4" <?php echo (isset($instance['number']) && $instance['number']==4)?'selected="selected"':'';?>>4</option>
                <option value="5" <?php echo (isset($instance['number']) && $instance['number']==5)?'selected="selected"':'';?>>5</option>
            </select>
		</p>
        
	<?php
	}

}
add_action( 'widgets_init', 'tl_twitter_widget' );
function tl_twitter_widget() {
	register_widget( 'tl_twitter_widget' );
}
function makeClickableLinks($s) {
  return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a target="blank" rel="nofollow" href="$1" target="_blank">$1</a>', $s);
}
