<?php
/******************TL RECENT POSTS WIDGETS***********************/

add_action( 'widgets_init', 'tl_recent_post_widget' );


function tl_recent_post_widget() {
	register_widget( 'TL_RECENT_POST_Widget' );
}

class TL_RECENT_POST_Widget extends WP_Widget {
	
	function __construct() {
		$widget_ops = array( 'classname' => 'tl-recent_post', 'description' => __('A widget to show recent blog posts', 'tl-recent_post') );
		$control_ops = array( 'width' => 270, 'height' => 350, 'id_base' => 'tl-recent_post-widget' );
		parent::__construct(
		'tl-recent_post', 
		__('TL Recent Posts', 'tl'),
		array( 'description' => __( 'A widget to show recent blog posts', 'tl' ), ) 
		);
	}

	/*function TL_RECENT_POST_Widget() {
		$widget_ops = array( 'classname' => 'tl-recent_post', 'description' => __('A widget to show recent blog posts', 'tl-recent_post') );
		
		$control_ops = array( 'width' => 270, 'height' => 350, 'id_base' => 'tl-recent_post-widget' );
		
		$this->WP_Widget( 'tl-recent_post-widget', __('TL Recent Posts', 'tl-recent-post'), $widget_ops, $control_ops );
	}*/

	function widget( $args, $instance ) {
		extract( $args );
		//Our variables from the widget settings.
		$title			= 	apply_filters('widget_title', $instance['title'] );
		$number			= 	($instance[ 'number' ])?$instance[ 'number' ]:3;
		$showImage 		= 	($instance[ 'image' ])?true:false;
		$showDate 		= 	($instance[ 'date' ])?true:false;
		$showExcerpt 	= 	($instance[ 'excerpt' ])?true:false;
		$excerptLength	= 	($instance[ 'excerptLength' ])?$instance[ 'excerptLength' ]:0;
		$categories		= 	$instance[ 'categories' ]?implode(',',$instance[ 'categories' ]):false;
		?>
<?php echo $before_widget;?>
                   <?php echo $before_title;?> <?php echo $title;?> <?php echo $after_title;?>
                  <ul class="tl_recent_posts">
                    <?php 
					  $args = array(
						'order'             => 'DESC',
						'posts_per_page'    => $number,
						'orderby'           => 'date'
					  );
					  if($categories){
						  $args['category_name'] = $categories;
					  }
					  $blogPosts = new WP_Query($args);
					  if($blogPosts->have_posts()):
					  while ($blogPosts->have_posts() ) : $blogPosts->the_post();
					 ?>
                     <li>
                        <?php if(has_post_thumbnail() && $showImage == true):?>
                        <div class="tl_recent_posts_img">
                        	<a href="<?php the_permalink();?>">
                        	<?php the_post_thumbnail(array(60,60));?>
                            </a>
                        </div>
                        <?php endif;?>
                        <div class="recent_posts_content">
                        	<a class="post_title read_more" href="<?php the_permalink();?>"><?php the_title();?></a>
                            <?php echo ($showDate == true)?'<p class="tl-recent-date"><a href="'.get_day_link( get_the_date('Y'), get_the_date('m'), get_the_date('d') ).'"><time class="post-date updated" datetime="'.get_the_date().'"> <i class="fa fa-calendar"></i> '.get_the_date('F j, Y').'</time></a></p>':'';?>
                            
                            <?php 
							if($showExcerpt == true):
							echo '<p>'.substr(get_the_excerpt(), 0,$excerptLength).'</p>';
							endif;
							?>
                        </div>
                        <div class="clearfix"></div>
                     </li>   
                     <?php
					  endwhile;
					  endif;
					 ?>
   				</ul>
<?php echo $after_widget;?>
<?php 
//echo $after_widget;
}
	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML 
		$instance['title'] 			= strip_tags( $new_instance['title'] );
		$instance['number'] 		= $new_instance[ 'number' ];
		$instance['image']			= $new_instance[ 'image' ];
		$instance['date']			= $new_instance[ 'date' ];
		$instance['excerpt']		= $new_instance[ 'excerpt' ];
		$instance['excerptLength']	= $new_instance[ 'excerptLength' ];
		$instance['categories']		= $new_instance[ 'categories' ];
		return $instance;
	}

	
	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TL Recent Posts', 'tl-recent-post'), 'name' => __('TL Recent Posts', 'tl-recent-post'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	?>
        
         <p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'tl-recent-post'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        
        
        <!---------------- my include----------------------->
        <p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>"><input type="checkbox" name="<?php echo $this->get_field_name( 'image' ); ?>"  value="image" <?php echo (isset($instance['image']) && $instance['image'])?'checked':''; ?> /><?php _e('Show Image', 'tl-recent-post'); ?></label>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'date' ); ?>"><input type="checkbox" name="<?php echo $this->get_field_name( 'date' ); ?>"  value="date" <?php echo (isset($instance['date']) && $instance['date'])?'checked':''; ?> /><?php _e('Show Date', 'tl-recent-post'); ?></label>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'excerpt' ); ?>"><input type="checkbox" name="<?php echo $this->get_field_name( 'excerpt' ); ?>" value="excerpt" <?php echo (isset($instance['excerpt']) && $instance['excerpt'])?'checked':''; ?>><?php _e('Show Excerpt', 'tl-recent-post'); ?></label>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'excerptLength' ); ?>"><?php _e('Excerpt Length:', 'tl-recent-post'); ?></label>
			<input id="<?php echo $this->get_field_id( 'excerptLength' ); ?>" name="<?php echo $this->get_field_name( 'excerptLength' ); ?>" type="number" value="<?php echo (isset($instance['excerptLength']) && $instance['excerptLength'])?$instance['excerptLength']:0; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'categories' ); ?>"><?php _e('Categories:', 'tl-recent-post'); ?></label>
            <select id="<?php echo $this->get_field_id( 'categories' ); ?>" name="<?php echo $this->get_field_name( 'categories[]' ); ?>" style="width:100%;" multiple="multiple">
                <?php
					$categories = get_categories();
					$categories_len = count($categories);
				?>
            	<?php for($j=0;$j<$categories_len;$j++):?>
          		<option value="<?php echo $categories[$j]->slug;?>" <?php echo (isset($instance['categories']) && in_array($categories[$j]->slug,$instance['categories']))?'selected="selected"':'';?>><?php echo $categories[$j]->slug; ?></option>
                <?php endfor;?>
            </select>
		</p>
        <!----------------------end my include----------------------------->
        
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Number of Posts:', 'tl-recent-post'); ?></label>
            <select id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" style="width:100%;">
            	<?php for($i=1;$i<=15;$i++):?>
                <option value="<?php echo $i;?>" <?php echo (isset($instance['number']) && $instance['number']==$i)?'selected="selected"':'';?>><?php echo $i;?></option>
                <?php endfor;?>
            </select>
		</p>
        
	<?php
	}
}