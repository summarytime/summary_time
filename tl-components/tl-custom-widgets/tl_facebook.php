<?php
/******************TL FACEBOOK WIDGETS***********************/

class tl_facebook_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
		'tl_facebook_widget', 
		__('TL Facebook', 'tl'),
		array( 'description' => __( 'A widget to show facebook feeds', 'tl' ), ) 
		);
	}
	public function widget( $args, $instance ) {
		extract( $args );
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$facebook_url = $instance['facebook-url'];
		$facebook_name = $instance['facebook-name'];
?>
		<?php echo $before_widget;?> 
        <?php echo $before_title;?> <?php echo $title;?> <?php echo $after_title;?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-page" data-href="<?php echo $facebook_url;?>" data-tabs="timeline" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $facebook_url;?>"><a href="<?php echo $facebook_url;?>"><?php echo $facebook_name;?></a></blockquote></div></div>
        <?php echo $after_widget;
	}
    public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['facebook-url'] = $new_instance['facebook-url'];
		$instance['facebook-name'] = $new_instance['facebook-name'];
		return $instance;
	}
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => __('TL Facebook', 'tl'), 'name' => __('TL Facebook', 'tl'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'tl'); ?></label>
		<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo isset($instance['title'])?$instance['title']:''; ?>" style="width:100%;" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'facebook-url' ); ?>"><?php _e('Page URL:', 'tl'); ?></label>
		<input id="<?php echo $this->get_field_id( 'facebook-url' ); ?>" name="<?php echo $this->get_field_name( 'facebook-url' ); ?>" value="<?php echo isset($instance['facebook-url'])?$instance['facebook-url']:''; ?>" style="width:100%;" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'facebook-name' ); ?>"><?php _e('Page Name:', 'tl'); ?></label>
		<input id="<?php echo $this->get_field_id( 'facebook-name' ); ?>" name="<?php echo $this->get_field_name( 'facebook-name' ); ?>" value="<?php echo isset($instance['facebook-name'])?$instance['facebook-name']:''; ?>" style="width:100%;" />
	</p>
<?php
	}

}

add_action( 'widgets_init', 'tl_facebook_widget' );
function tl_facebook_widget() {
	register_widget( 'tl_facebook_widget' );
}