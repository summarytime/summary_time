<?php
/******************TL SOCIAL LINK WIDGETS***********************/

add_action( 'widgets_init', 'tl_social_links_widget' );


function tl_social_links_widget() {
	register_widget( 'TL_SOCIAL_LINKS_Widget' );
}

class TL_SOCIAL_LINKS_Widget extends WP_Widget {
	
	function __construct() {
		$widget_ops = array( 'classname' => 'tl-social_link', 'description' => __('A widget to open social link page', 'tl-social_link') );
		$control_ops = array( 'width' => 270, 'height' => 350, 'id_base' => 'tl-social_link-widget' );
		parent::__construct(
		'tl-social_link', 
		__('TL Social Link', 'tl'),
		array( 'description' => __( 'A widget to open social link page', 'tl' ), ) 
		);
	}


	function widget( $args, $instance ) {
		extract( $args );
		$es = "target =\"_blank\"";
		//Our variables from the widget settings.
		$title				= 	apply_filters('widget_title', $instance['title'] );
		$open_in_newTab		=	(!empty($instance[ 'open_in_newTab' ]))?'_blank':'';
		$facebook			= 	(!empty($instance[ 'facebook' ]))?$instance[ 'facebook' ]:'';
		$youtube			= 	(!empty($instance[ 'youtube' ]))?$instance[ 'youtube' ]:'';
		$twitter			= 	(!empty($instance[ 'twitter' ]))?$instance[ 'twitter' ]:'';
		$linkedIn			= 	(!empty($instance[ 'linkedIn' ]))?$instance[ 'linkedIn' ]:'';
		$pinterest			= 	(!empty($instance[ 'pinterest' ]))?$instance[ 'pinterest' ]:'';
		$google_Plus		= 	(!empty($instance[ 'google_Plus' ]))?$instance[ 'google_Plus' ]:'';
		$tumblr				= 	(!empty($instance[ 'tumblr' ]))?$instance[ 'tumblr' ]:'';
		$instagram			= 	(!empty($instance[ 'instagram' ]))?$instance[ 'instagram' ]:'';
		$reddit				= 	(!empty($instance[ 'reddit' ]))?$instance[ 'reddit' ]:'';
		$vk					= 	(!empty($instance[ 'vk' ]))?$instance[ 'vk' ]:'';
		$flickr				= 	(!empty($instance[ 'flickr' ]))?$instance[ 'flickr' ]:'';
		$vine				= 	(!empty($instance[ 'vine' ]))?$instance[ 'vine' ]:'';
		$meetup				= 	(!empty($instance[ 'meetup' ]))?$instance[ 'meetup' ]:'';
	//$ask_fm				= 	(!empty($instance[ 'ask_fm' ]))?$instance[ 'ask_fm' ]:'';
	//$classmates			= 	(!empty($instance[ 'classmates' ]))?$instance[ 'classmates' ]:'';
		?>
<?php echo $before_widget;?>
                   <?php echo $before_title;?> <?php echo $title;?> <?php echo $after_title;?>
				   
				   <ul>
				   <?php echo (!empty($facebook))?'<li><a href="'.$facebook.'" target="'.$open_in_newTab.'"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($youtube))?'<li><a href="'.$youtube.'" target="'.$open_in_newTab.'"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($twitter))?'<li><a href="'.$twitter.'" target="'.$open_in_newTab.'"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($linkedIn))?'<li><a href="'.$linkedIn.'" target="'.$open_in_newTab.'"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($pinterest))?'<li><a href="'.$pinterest.'" target="'.$open_in_newTab.'"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($google_Plus))?'<li><a href="'.$google_Plus.'" target="'.$open_in_newTab.'"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($tumblr))?'<li><a href="'.$tumblr.'" target="'.$open_in_newTab.'"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($instagram))?'<li><a href="'.$instagram.'" target="'.$open_in_newTab.'"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($reddit))?'<li><a href="'.$reddit.'" target="'.$open_in_newTab.'"><i class="fa fa-reddit" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($vk))?'<li><a href="'.$vk.'" target="'.$open_in_newTab.'"><i class="fa fa-vk" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($flickr))?'<li><a href="'.$flickr.'" target="'.$open_in_newTab.'"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>':''; ?>
				   <?php echo (!empty($meetup))?'<li><a href="'.$meetup.'" target="'.$open_in_newTab.'"><i class="fa fa-meetup" aria-hidden="true"></i></a></li>':''; ?>
				   </ul>
                  
<?php echo $after_widget;?>
<?php 
}
	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML 
		$instance['title'] 				= 	strip_tags( $new_instance['title'] );
		$instance['open_in_newTab'] 	= 	$new_instance[ 'open_in_newTab' ];
		$instance['facebook'] 			= 	$new_instance[ 'facebook' ];
		$instance['youtube']			= 	$new_instance[ 'youtube' ];
		$instance['twitter']			= 	$new_instance[ 'twitter' ];
		$instance['linkedIn']			= 	$new_instance[ 'linkedIn' ];
		$instance['pinterest']			= 	$new_instance[ 'pinterest' ];
		$instance['google_Plus']		= 	$new_instance[ 'google_Plus' ];
		$instance['tumblr'] 			= 	$new_instance[ 'tumblr' ];
		$instance['instagram']			= 	$new_instance[ 'instagram' ];
		$instance['reddit']				= 	$new_instance[ 'reddit' ];
		$instance['vk']					= 	$new_instance[ 'vk' ];
		$instance['flickr']				= 	$new_instance[ 'flickr' ];
		$instance['vine']				= 	$new_instance[ 'vine' ];
		$instance['meetup']				= 	$new_instance[ 'meetup' ];
	//$instance['ask_fm']				= 	$new_instance[ 'ask_fm' ];
	//$instance['classmates']			= 	$new_instance[ 'classmates' ];
		return $instance;
	}

	
	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => __('TL Social Links', 'tl-social-links'), 'name' => __('TL Social Links', 'tl-social-link'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'tl-social-link'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'open_in_newTab' ); ?>"><input type="checkbox" name="<?php echo $this->get_field_name( 'open_in_newTab' ); ?>" <?php echo (isset($instance['open_in_newTab']) && $instance['open_in_newTab'])?'checked':''; ?> value="newtab"/><?php _e(' Open In New Tab', 'tl-social-link'); ?></label>
	   </p>
	   
	   
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'facebook' ); ?>" placeholder="Facebook Link......" value="<?php echo isset($instance['facebook'])?$instance['facebook']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'youtube' ); ?>" placeholder="Youtube Link......" value="<?php echo isset($instance['youtube'])?$instance['youtube']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'twitter' ); ?>" placeholder="Twitter Link......" value="<?php echo isset($instance['twitter'])?$instance['twitter']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'linkedIn' ); ?>" placeholder="LinkedIn Link......" value="<?php echo isset($instance['linkedIn'])?$instance['linkedIn']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" placeholder="Pinterest Link......" value="<?php echo isset($instance['pinterest'])?$instance['pinterest']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'google_Plus' ); ?>" placeholder="Google Plus+ Link......" value="<?php echo isset($instance['google_Plus'])?$instance['google_Plus']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'tumblr' ); ?>" placeholder="Tumblr Link......" value="<?php echo isset($instance['tumblr'])?$instance['tumblr']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'instagram' ); ?>" placeholder="Instagram Link......" value="<?php echo isset($instance['instagram'])?$instance['instagram']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'reddit' ); ?>" placeholder="Reddit Link......" value="<?php echo isset($instance['reddit'])?$instance['reddit']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'vk' ); ?>" placeholder="VK Link......" value="<?php echo isset($instance['vk'])?$instance['vk']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'flickr' ); ?>" placeholder="Flickr Link......" value="<?php echo isset($instance['flickr'])?$instance['flickr']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'vine' ); ?>" placeholder="Vine Link......" value="<?php echo isset($instance['vine'])?$instance['vine']:''; ?>"/>
	   </p>
	   <p>
			<input type="text" name="<?php echo $this->get_field_name( 'meetup' ); ?>" placeholder="Meetup Link......" value="<?php echo isset($instance['meetup'])?$instance['meetup']:''; ?>"/>
	   </p>
         
        
	<?php
	}
}