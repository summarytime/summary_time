<?php

add_action( 'wp_ajax_nopriv_update_tl_page_builder_pg', 'update_tl_page_builder_pg' );

add_action( 'wp_ajax_update_tl_page_builder_pg', 'update_tl_page_builder_pg' );

function update_tl_page_builder_pg(){

	if(!empty($_POST)){

		$allowed = array( 

			'a' => array(

				'href' => array()

			)

		);

		update_post_meta($_POST['postID'], 'tl_pb_full_html', $_POST['full_html']);

		echo (get_post_meta( $_POST['postID'], 'tl_pb_full_html', true ))?get_post_meta( $_POST['postID'], 'tl_pb_full_html', true ):'';

	}

	exit;

}



add_action( 'wp_ajax_nopriv_create_pb_post_html', 'create_pb_post_html' );

add_action( 'wp_ajax_create_pb_post_html', 'create_pb_post_html' );

function create_pb_post_html(){
$key = $_POST['key'];
?>
<div class="tl-pb-post">

<div class="row">

	<div class="tl-post-select-part">

		<ul>
            <li>
				<label>Post Types:</label>
				<select name="tlpb[<?php echo $key;?>][post_type]" class="post-text opt" id="post-type">
					<option value="post">Post</option>
                    <option value="page">Page</option>
                    <option value="team">Team</option>
                    <option value="testimonial">Testimonials</option>
				</select>
			</li>
            
            <li>
				<label>Order:</label>
				<select name="tlpb[<?php echo $key;?>][post_order]" id="post-order" class="post-text opt">
					<option value="date-desc">Date: Newest First</option>
                    <option value="date-asc">Date: Oldest First</option>
                    <option value="title-asc">Title: A - Z</option>
                    <option value="title-desc">Title: Z - A</option>
                    <option value="comment_count-desc">Most Comments</option>
                    <option value="menu_order-asc">Page Order</option>
                    <option value="rand-asc">Random</option>
				</select>
			</li>
            
            <li class="post-from">
				<label>From:</label>
				<select name="tlpb[<?php echo $key;?>][post_from]" class="post-text opt" id="post-from">
                	<option value="all">All Posts</option>
                    <?php $category = get_terms( 'category',array('hide_empty' => false)  );?>
                    <?php if(isset($category) && $category):?>
					<?php foreach($category as $cat):?>
					<option value="<?php echo $cat->slug;?>"><?php echo $cat->name;?></option>
					<?php endforeach;?>
                    <?php endif;?>
				</select>
			</li>
            
            <li class="page-from" style="display:none;">
            	<label>From:</label>
                <select name="tlpb[<?php echo $key;?>][page_from]" class="post-text opt" id="page-from">
					<?php $args = array(
                        'sort_order' => 'asc',
                        'sort_column' => 'post_title',
                        'hierarchical' => 1,
                        'exclude' => '',
                        'include' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'authors' => '',
                        'child_of' => 0,
                        'parent' => -1,
                        'exclude_tree' => '',
                        'number' => '',
                        'offset' => 0,
                        'post_type' => 'page',
                        'post_status' => 'publish'
                    ); 
                    $pages = get_pages($args);
                    ?>
                    <option value="all">All Pages</option>
                    <optgroup label="--Child Of--">
						<?php if(isset($pages) && $pages):?>
                       
                        <?php foreach($pages as $page):?>
                        <option value="<?php echo $page->ID;?>"><?php echo $page->post_title;?></option>
                        <?php endforeach;?>
                        <?php endif;?>
                    </optgroup>
                </select>
            </li>
            
			<li>
				<label>Number of Items ( -1 For all Items ):</label>
                <input type="number" name="tlpb[<?php echo $key;?>][post_number]" value="-1" id="post-number" class="post-num-control post-text"/>
				<input type="hidden" name="tlpb[<?php echo $key;?>][post_number]" value="-1" class="post-num-control-length" id="post-number-length" />
			</li>
            
            <li>
				<label>Item Offset:</label>
                <input type="number" name="tlpb[<?php echo $key;?>][post_offset]" value="0" id="post-offset" class="post-text post-num-control"/>
				<input type="hidden" name="tlpb[<?php echo $key;?>][post_offset]" value="0" class="post-num-control-length" id="post-offset-length" />
			</li>

			<li>
				<label>Columns:</label>
				<select name="tlpb[<?php echo $key;?>][post_column]" class="post-text opt" id="post-column">
					<?php for($i=2;$i<=4;$i++):?>
					<option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php endfor;?>
				</select>
			</li>
            
            <li class="only-post-page">
				<label>Featured Image:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_feature]" value="on" checked />Show Featured Image
			</li>
            
            <li class="only-post-page">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][post_style]" class="post-text opt" id="post-style">
					<option value="column">Column View</option>
                    <option value="horizontal">Horizontal View</option>
                    <option value="masonry">Masonry</option>
				</select>
			</li>
            
            <li class="only-team" style="display:none;">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][team_style]" class="post-text opt" id="team-style">
					<option value="column">Column View</option>
                    <option value="slder">Slider</option>
				</select>
			</li>
            
            <li class="only-testimonial" style="display:none;">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][testimonial_style]" class="post-text opt" id="testimonial-style">
					<option value="chat">Chat View</option>
                    <option value="normal">Normal Slider</option>
				</select>
			</li>
            
            <li>
				<label>Elements:</label>
				<p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_title" />Show Title</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_date" />Show Date</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_author" />Show Author</p>
                <p class="only-post"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_category" />Show Categories</p>
                <p class="only-post"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_tag" />Show Tags</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_comment_count" />Show Comment Count</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_name" />Show Name</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_designation" />Show Designation</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_image" />Show Image</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_contact_details" />Show Contact Details</p>
                <p class="only-team" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_social_icons" />Show Social Icons</p>
                <p class="only-post-page-team"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_details_page_link" />Enable Link To Details Page</p>
			</li>
            
            <li class="only-post-page">
				<label>Excerpt:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_excerpt]" value="on" checked/>Show Excerpt (Applicable for Posts and also Pages without Themelines Page Builder)
			</li>
            
            <li class="only-post-page">
				<label>Excerpt Length (Characters):</label>
				<input type="number" name="tlpb[<?php echo $key;?>][post_length]" value="50" class="post-num-control" id="post-character"/>
                <input type="hidden" name="tlpb[<?php echo $key;?>][post_length]" value="50" class="post-num-control-length" id="post-character-length" />
			</li>
            
            <li class="only-post-page">
				<label>Pagination:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_pagination]" value="on"/>Pagination
			</li>
			<?php
				$animatehtml = '<option value="no">No Animation</option>
		<optgroup label="Attention Seekers">
		  <option value="bounce">bounce</option>
          <option value="flash">flash</option>
          <option value="pulse">pulse</option>
          <option value="rubberBand">rubberBand</option>
          <option value="shake">shake</option>
          <option value="swing">swing</option>
          <option value="tada">tada</option>
          <option value="wobble">wobble</option>
          <option value="jello">jello</option>
        </optgroup>
        <optgroup label="Bouncing Entrances">
          <option value="bounceIn">bounceIn</option>
          <option value="bounceInDown">bounceInDown</option>
          <option value="bounceInLeft">bounceInLeft</option>
          <option value="bounceInRight">bounceInRight</option>
          <option value="bounceInUp">bounceInUp</option>
        </optgroup>
        <optgroup label="Bouncing Exits">
          <option value="bounceOut">bounceOut</option>
          <option value="bounceOutDown">bounceOutDown</option>
          <option value="bounceOutLeft">bounceOutLeft</option>
          <option value="bounceOutRight">bounceOutRight</option>
          <option value="bounceOutUp">bounceOutUp</option>
        </optgroup>
        <optgroup label="Fading Entrances">
          <option value="fadeIn">fadeIn</option>
          <option value="fadeInDown">fadeInDown</option>
          <option value="fadeInDownBig">fadeInDownBig</option>
          <option value="fadeInLeft">fadeInLeft</option>
          <option value="fadeInLeftBig">fadeInLeftBig</option>
          <option value="fadeInRight">fadeInRight</option>
          <option value="fadeInRightBig">fadeInRightBig</option>
          <option value="fadeInUp">fadeInUp</option>
          <option value="fadeInUpBig">fadeInUpBig</option>
        </optgroup>
        <optgroup label="Fading Exits">
          <option value="fadeOut">fadeOut</option>
          <option value="fadeOutDown">fadeOutDown</option>
          <option value="fadeOutDownBig">fadeOutDownBig</option>
          <option value="fadeOutLeft">fadeOutLeft</option>
          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
          <option value="fadeOutRight">fadeOutRight</option>
          <option value="fadeOutRightBig">fadeOutRightBig</option>
          <option value="fadeOutUp">fadeOutUp</option>
          <option value="fadeOutUpBig">fadeOutUpBig</option>
        </optgroup>
		<optgroup label="Flippers">
          <option value="flip">flip</option>
          <option value="flipInX">flipInX</option>
          <option value="flipInY">flipInY</option>
          <option value="flipOutX">flipOutX</option>
          <option value="flipOutY">flipOutY</option>
        </optgroup>
        <optgroup label="Lightspeed">
          <option value="lightSpeedIn">lightSpeedIn</option>
          <option value="lightSpeedOut">lightSpeedOut</option>
        </optgroup>
        <optgroup label="Rotating Entrances">
          <option value="rotateIn">rotateIn</option>
          <option value="rotateInDownLeft">rotateInDownLeft</option>
          <option value="rotateInDownRight">rotateInDownRight</option>
          <option value="rotateInUpLeft">rotateInUpLeft</option>
          <option value="rotateInUpRight">rotateInUpRight</option>
        </optgroup>
        <optgroup label="Rotating Exits">
          <option value="rotateOut">rotateOut</option>
          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
          <option value="rotateOutDownRight">rotateOutDownRight</option>
          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
          <option value="rotateOutUpRight">rotateOutUpRight</option>
        </optgroup>

        <optgroup label="Sliding Entrances">
          <option value="slideInUp">slideInUp</option>
          <option value="slideInDown">slideInDown</option>
          <option value="slideInLeft">slideInLeft</option>
          <option value="slideInRight">slideInRight</option>
        </optgroup>
        <optgroup label="Sliding Exits">
          <option value="slideOutUp">slideOutUp</option>
          <option value="slideOutDown">slideOutDown</option>
          <option value="slideOutLeft">slideOutLeft</option>
          <option value="slideOutRight">slideOutRight</option>
        </optgroup>
        <optgroup label="Zoom Entrances">
          <option value="zoomIn">zoomIn</option>
          <option value="zoomInDown">zoomInDown</option>
          <option value="zoomInLeft">zoomInLeft</option>
          <option value="zoomInRight">zoomInRight</option>
          <option value="zoomInUp">zoomInUp</option>
        </optgroup>
        <optgroup label="Zoom Exits">
          <option value="zoomOut">zoomOut</option>
          <option value="zoomOutDown">zoomOutDown</option>
          <option value="zoomOutLeft">zoomOutLeft</option>
          <option value="zoomOutRight">zoomOutRight</option>
          <option value="zoomOutUp">zoomOutUp</option>
        </optgroup>
        <optgroup label="Specials">
          <option value="hinge">hinge</option>
          <option value="rollIn">rollIn</option>
          <option value="rollOut">rollOut</option>
        </optgroup>';
		$delay_html = '';
		for($i=200;$i<=2000;$i=$i+200){
			$delay_html .= '<option value="'.$i.'ms">'.$i.'ms</option>';
		}
		$duration_html = '';
		for($i=200;$i<=2000;$i=$i+200){
			$duration_html .= '<option value="'.$i.'ms">'.$i.'ms</option>';
		}
			?>
			<li>
				<label>Items Animation:</label>
				<select name="tlpb[<?php echo $key;?>][animation]" class="post-text opt" id="animation">
					<?php echo $animatehtml;?>
				</select>
            </li>
            <li>
				<label>Items Animation Delay:</label>
				<select name="tlpb[<?php echo $key;?>][animation_delay]" class="post-text opt" id="animation_delay">
					<?php echo $delay_html;?>
				</select>
            </li>
            <li>
				<label>Items Animation Duration:</label>
				<select name="tlpb[<?php echo $key;?>][animation_duration]" class="post-text opt" id="animation_duration">
					<?php echo $duration_html;?>
				</select>
            </li>

		</ul>

	</div>  

</div>

</div>
<?php

exit;	

}

add_action( 'wp_ajax_nopriv_select_slider', 'select_slider' );

add_action( 'wp_ajax_select_slider', 'select_slider' );

function select_slider(){
		$terms = get_terms( 'slider_category',array('hide_empty' => false)  );
		if ( $terms ):
		if($_POST['tlslider']){
			$tlslider = explode(',',$_POST['tlslider']);
		}
?>
		<label>Select Category</label>
<?php
		echo '<select name="slider" id="slider" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>" <?php echo ($_POST['tlslider'] && in_array($term->term_id, $tlslider))?'selected="selected"':'';?>><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add slider category to use slider.';
		endif;
	exit;

}
add_action( 'wp_ajax_nopriv_create_editor', 'create_editor' );

add_action( 'wp_ajax_create_editor', 'create_editor' );

function create_editor(){
	

$settings = array(

				'media_buttons'	=> true,

				'textarea_name' => $_POST['id'],

				'textarea_rows'	=> 5

			);

wp_editor( '', $_POST['id'], $settings);

	exit;

}

add_action( 'wp_ajax_nopriv_col_config', 'col_config' );

add_action( 'wp_ajax_col_config', 'col_config' );

function col_config(){

?>
		<?php if($_POST['col']==1):?>
		<select name="col_config" class="col_config form-control">
            <option value="12" <?php if($_POST['colconfig']=="3,9") echo 'selected';?>>100%</option>
        </select>
        <?php endif;?>
        
		<?php if($_POST['col']==2):?>
		<select name="col_config" class="col_config form-control">
            <option value="3,9" <?php if($_POST['colconfig']=="3,9") echo 'selected';?>>25%+75%</option>
            <option value="4,8" <?php if($_POST['colconfig']=="4,8") echo 'selected';?>>40%+60%</option>
            <option value="6,6" <?php if($_POST['colconfig']=="6,6") echo 'selected';?>>50%+50%</option>
            <option value="8,4" <?php if($_POST['colconfig']=="8,4") echo 'selected';?>>60%+40%</option>
            <option value="9,3" <?php if($_POST['colconfig']=="9,3") echo 'selected';?>>75%+25%</option>
        </select>
        <?php endif;?>
        
        <?php if($_POST['col']==3):?>
		<select name="col_config" class="col_config form-control">
            <option value="3,6,3" <?php if($_POST['colconfig']=="3,6,3") echo 'selected';?>>25%+50%+25%</option>
            <option value="3,3,6" <?php if($_POST['colconfig']=="3,3,6") echo 'selected';?>>25%+25%+50%</option>
            <option value="4,4,4" <?php if($_POST['colconfig']=="4,4,4") echo 'selected';?>>33%+33%+33%</option>
            <option value="6,3,3" <?php if($_POST['colconfig']=="6,3,3") echo 'selected';?>>50%+25%+25%</option>
        </select>
        <?php endif;?>
        
        <?php if($_POST['col']==4):?>
		<select name="col_config" class="col_config form-control">
            <option value="3,3,3,3" <?php if($_POST['colconfig']=="3,3,3,3") echo 'selected';?>>25%+25%+25%+25%</option>
        </select>
        <?php endif;?>
<?php
	exit;

}

add_action( 'wp_ajax_nopriv_template_save', 'template_save' );
add_action( 'wp_ajax_template_save', 'template_save' );

function template_save(){
	$option = get_option($_POST['template']);
	if(isset($option) && $option){
		die('no');
	}else{
		update_option( $_POST['template'], stripslashes($_POST['template_val']), 'yes' );
		$tlpbtemplate = get_option('page_builder_template');
		if(isset($tlpbtemplate) && $tlpbtemplate){
			update_option( 'page_builder_template', $tlpbtemplate.','.$_POST['template'], 'yes' );
		}else{
			update_option( 'page_builder_template', $_POST['template'], 'yes' );
		}
		$tlpbtemplate = get_option('page_builder_template');
	  	if($tlpbtemplate){
	  		$template = explode(',',$tlpbtemplate);
	  	}
?>
	<select name="template_name" class="form-control">
	<option value="">--Select Template--</option>
    <?php if($tlpbtemplate):?>
	<?php foreach($template as $tpl):?>
    <option value="<?php echo $tpl;?>" <?php echo ($_POST['template']==$tpl)?'selected="selected"':'';?>><?php echo $tpl;?></option>
    <?php endforeach;?>
    <?php endif;?>
</select>
<?php
		exit;
	}
}

add_action( 'wp_ajax_nopriv_template_load', 'template_load' );
add_action( 'wp_ajax_template_load', 'template_load' );

function template_load(){
	$tlpbtemplate = get_option('page_builder_template');
	  if($tlpbtemplate){
	  	$template = explode(',',$tlpbtemplate);
	  }
?>
<select name="template_name" class="form-control">
	<option value="">--Select Template--</option>
    <?php if($tlpbtemplate):?>
	<?php foreach($template as $tpl):?>
    <option value="<?php echo $tpl;?>" <?php echo ($_POST['template']==$tpl)?'selected="selected"':'';?>><?php echo $tpl;?></option>
    <?php endforeach;?>
    <?php endif;?>
</select>
<?php
exit;
}
add_action( 'wp_ajax_nopriv_load_tpl_html', 'load_tpl_html' );
add_action( 'wp_ajax_load_tpl_html', 'load_tpl_html' );

function load_tpl_html(){
	if($_POST['load_template']!=''){
		$option = get_option($_POST['load_template']);
		echo $option;
	}else{
		echo get_post_meta( $_POST['id'], 'tl_pb_full_html', true );
	}
	exit;
}

add_action( 'wp_ajax_nopriv_delete_template', 'delete_template' );
add_action( 'wp_ajax_delete_template', 'delete_template' );

function delete_template(){
	$option = get_option($_POST['template']);
	if(isset($option) && $option){
		delete_option( $_POST['template']);
		$tlpbtemplate = get_option('page_builder_template');
		$template = explode(',',$tlpbtemplate);
		if(($key = array_search($_POST['template'], $template)) !== false) {
			unset($template[$key]);
		}
		if(!empty($template)){
			$new_template = implode(',',$template);
		}
		if(isset($new_template) && $new_template){
			update_option( 'page_builder_template', $new_template, 'yes' );
		}else{
			update_option( 'page_builder_template', '', 'yes' );
		}
?>
		<select name="template_name" class="form-control">
            <option value="">--Select Template--</option>
            <?php if($template):?>
            <?php foreach($template as $tpl):?>
            <option value="<?php echo $tpl;?>"><?php echo $tpl;?></option>
            <?php endforeach;?>
            <?php endif;?>
        </select>
<?php
	}else{
		die('no');
	}
		exit;
	
}

add_action( 'wp_ajax_editor_creator', 'editor_creator' );
add_action('wp_ajax_nopriv_editor_creator', 'editor_creator');
function editor_creator() {
$settings = array(
				'media_buttons'	=> true,
				'textarea_name' => 'tl_content',
				'textarea_rows'	=> 5
			);
wp_editor( '', 'tl_content', $settings);
exit();
}

add_action( 'wp_ajax_create_content_area', 'create_content_area' );
function create_content_area(){
	wp_enqueue_style( 'wp-color-picker' );        
	wp_enqueue_script( 'wp-color-picker' );
	$key = $_GET['key'];
	$settings = array(
				'wpautop'=> true,
				'media_buttons'	=> true,
				'textarea_name' => 'tlpb['.$key.'][content]',
				'textarea_rows'	=> 10,
				'tinymce'=> false
			);
	wp_editor('', $key.'-developer-content', $settings);
	
?>
<?php
	\_WP_Editors::enqueue_scripts();
    print_footer_scripts();
   \_WP_Editors::editor_js();
?>
	<script>
	
        jQuery('#<?php echo $key;?>-developer-content').keyup(function(){
			jQuery(this).html(jQuery(this).val());
		});
		jQuery('#<?php echo $key;?>-developer-content').mouseover(function(){
			jQuery(this).html(jQuery('#<?php echo $key;?>-developer-content').val());
		});
		jQuery('#<?php echo $key;?>-developer-content').change(function(){
			jQuery(this).html(jQuery('#<?php echo $key;?>-developer-content').val());
		});
    
	</script>
<?php
	exit();
}
add_action( 'wp_ajax_content_add', 'content_add' );
function content_add(){
	$settings = array(
				'wpautop'=>true,
				'media_buttons'	=> false,
				'textarea_name' => 'content_add',
				'textarea_rows'	=> 10,
				'tinymce'=>false
			);
	wp_editor('', 'content_add', $settings);
?>
<?php
	/*
	\_WP_Editors::enqueue_scripts();
    print_footer_scripts();
    \_WP_Editors::editor_js();*/
	exit();
}

add_action( 'wp_ajax_fetch_widget', 'fetch_widget' );
function fetch_widget(){
	$sidebars = $GLOBALS['wp_registered_sidebars'];
	if(isset($sidebars) && $sidebars){
?>
		<select name="select_sidebar" class="form-control">
<?php
		foreach($sidebars as $sidebar):
?>
        	<option value="<?php echo ucwords( $sidebar['id'] );?>"><?php echo ucwords( $sidebar['name'] );?></option>      
<?php
		endforeach;
?>
		</select>
<?php
	}else{
		echo '<div class="no-widget">No Custom Widget Found. Go to <a href="themes.php">Appearance</a> > <a href="widgets.php">Widgets</a> to create one.</div>';
	}
	wp_die();
}

add_action( 'wp_ajax_select_single_slider', 'select_single_slider' );
function select_single_slider(){
	$args=array(
	  'post_type' => 'slider',
	  'post_status' => 'publish',
	  'posts_per_page' => -1,
	);
	$query = new WP_Query($args);
	if( $query->have_posts() ) {
	echo '
	<label>Select Slide as Banner</label>
	<select name="select_banner_slide" class="form-control">';
  	while ($query->have_posts()) : $query->the_post();
?>
	<option value="<?php echo get_the_ID();?>" rel="<?php the_title();?>"><?php the_title();?></option>
<?php
	endwhile; 
	echo '</select>';
?>
<script>
jQuery("[name='select_banner_slide']").change( function(){
	var val = jQuery(this).val();
	jQuery("[name='select_banner_slide'] option").each(function(){
		jQuery(this).removeAttr("selected");
	  if (jQuery(this).val() == val){
		jQuery(this).attr("selected","selected");
	  }

	});

});
</script>
<?php
	}else{
		echo 'Please add slides under slider post type to use this option.';
	}
	wp_die();
}

add_action( 'wp_ajax_nopriv_select_post_slider', 'select_post_slider' );

add_action( 'wp_ajax_select_post_slider', 'select_post_slider' );

function select_post_slider(){
		$terms = get_terms( 'category',array('hide_empty' => false)  );
		if ( $terms ):
?>
		<label>Select Category</label>
<?php
		echo '<select name="slider" id="slider" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>"><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add post category to use post banner.';
		endif;
	exit;

}
add_action( 'wp_ajax_contact_form_list', 'contact_form_list' );
add_action( 'wp_ajax_contact_form_list', 'contact_form_list' );
function contact_form_list(){
		$args = array ( 'post_type' => 'wpcf7_contact_form','orderby' => 'post_title', 'order' => 'ASC');
		query_posts( $args );
		if ( have_posts() ):
		?>
		<select name="contact_form" id="contact_form" class="form-control">
        <?php
			while ( have_posts() ) :the_post(); 
		?>
		<option value='[contact-form-7 id="<?php echo get_the_ID();?>" title="<?php the_title();?>"]'><?php the_title();?></option>
        <?php
		endwhile;
		?>
		</select>
        <?php
		else:
		echo 'Please add atleast one contact form. Add <a href="admin.php?page=wpcf7" target="_blank">Contact Form 7</a> now.';
		endif;
	exit;
}
function tlpb_scripts(){
	if(is_page()):
		$id= get_the_ID();
		$id= get_the_ID();
		//$grid = get_post_meta( $id, 'tlpb_struct', true );
		wp_enqueue_style( 'tlpb-animate-css',get_template_directory_uri() . '/includes/css/animate.css' );
		wp_enqueue_script( 'tlpb-wow-js',get_template_directory_uri() . '/includes/js/wow.min.js' );
		/**********************************Starts*************************/
		wp_enqueue_script( 'tlpb-custom-js',get_template_directory_uri() . '/includes/js/tl_custom.js','',true );
				
		/**********************************Ends*************************/
		wp_enqueue_style( 'tlpb-slider-full',get_template_directory_uri() . '/includes/css/fonts.css' );
		wp_enqueue_style( 'tlpb-fancybox',get_template_directory_uri() . '/includes/fancybox/jquery.fancybox.css' );
		wp_enqueue_script( 'tlpb-fancybox',get_template_directory_uri() . '/includes/fancybox/jquery.fancybox.js' );
		wp_enqueue_style( 'tlpb-fancybox-helper',get_template_directory_uri() . '/includes/fancybox/helpers/jquery.fancybox-buttons.css' );
		wp_enqueue_script( 'tlpb-fancybox-helper',get_template_directory_uri() . '/includes/fancybox/helpers/jquery.fancybox-buttons.js' );
		wp_enqueue_script( 'tlpb-fancybox-media',get_template_directory_uri() . '/includes/fancybox/helpers/jquery.fancybox-media.js' );
		wp_enqueue_style( 'tlpb-fancybox-thumb',get_template_directory_uri() . '/includes/fancybox/helpers/jquery.fancybox-thumbs.css' );
		wp_enqueue_script( 'tlpb-fancybox-thumb',get_template_directory_uri() . '/includes/fancybox/helpers/jquery.fancybox-thumbs.js' );
		/**********************************Starts*************************/
		/*wp_enqueue_style( 'tlpb-animated-masonry',get_template_directory_uri() . '/includes/css/animated-masonry-gallery.css' );*/
				
		//wp_enqueue_style( 'tlpb-masonry',get_template_directory_uri() . '/includes/css/masonary-1.css' );
		//wp_enqueue_style( 'tlpb-gallery-layout',get_template_directory_uri() . '/includes/css/isotope.css' );
		/**********************************Ends*************************/
		//wp_enqueue_script( 'tlpb-jquery-isotope',get_template_directory_uri() . '/includes/js/jquery.isotope.min.js','', true );
		//wp_enqueue_script( 'tlpb-jquery-isotope',get_template_directory_uri() . '/includes/js/isotope.min.js','', true );
		/*wp_enqueue_script( 'tlpb-animated-masonry',get_template_directory_uri() . '/includes/js/animated-masonry-gallery.js' );*/
		wp_enqueue_script( 'tlpb-jquery-easing',get_template_directory_uri() . '/includes/js/jquery.easing.min.js' );
		wp_enqueue_script( 'tlpb-jquery-mixitup',get_template_directory_uri() . '/includes/js/jquery.mixitup.min.js' );
		
			//tl-close js
			////wp_enqueue_script( 'tlpb-jquery-fittext',get_template_directory_uri() . '/includes/js/jquery.fittext.js' );
	    wp_enqueue_script( 'tlpb-global-custom',get_template_directory_uri() . '/includes/js/tl-global-custom.js' );
		
		
		//wp_enqueue_script( 'tlpb-gallery-main',get_template_directory_uri() . '/includes/js/main.js', '', true );
		//wp_enqueue_script( 'tlpb-gallery-layout',get_template_directory_uri() . '/includes/js/masonry.pkgd.min.js', '', true );
		/**********************************Starts*************************/
			////wp_enqueue_style( 'tlpb-slider-tl',get_template_directory_uri() . '/includes/css/banner.css' );
			////wp_enqueue_style( 'tlpb-slider',get_template_directory_uri() . '/includes/css/post-carosual.css' );
			////wp_enqueue_style( 'tlpb-gallery-layout',get_template_directory_uri() . '/includes/css/layout.css' );
			////wp_enqueue_style( 'tlpb-grid-blog',get_template_directory_uri() . '/includes/css/grid-blog.css' );
			////wp_enqueue_style( 'tlpb-grid-blog-masonry',get_template_directory_uri() . '/includes/css/masonry-blog.css' );
			////wp_enqueue_style( 'tlpb-team-style',get_template_directory_uri() . '/includes/css/team-style.css' );
			////wp_enqueue_style( 'tlpb-testimonial-style',get_template_directory_uri() . '/includes/css/testimonials.css' );
			////wp_enqueue_style( 'tlpb-module',get_template_directory_uri() . '/includes/css/module.css' );
			////wp_enqueue_style( 'tlpb-additional',get_template_directory_uri() . '/includes/css/tlpb_additional.css' );
			////wp_enqueue_style( 'tlpb-button',get_template_directory_uri() . '/includes/css/tl-button.css' );
		/**********************************Ends*************************/
		wp_enqueue_style( 'tlpb-builder',get_template_directory_uri() . '/includes/css/tl-builder-style.css' );
		
	
	endif;
}
add_action( 'wp_enqueue_scripts', 'tlpb_scripts' );

add_action( 'wp_ajax_select_gallery_category', 'select_gallery_category' );

function select_gallery_category(){
		$terms = get_terms( 'gallery_category',array('hide_empty' => false)  );
?>
		<div class="row">
        <div class="tl-pb-pop-col">
<?php
		if ( $terms ):
?>
		<label>Select Gallery Category</label>
<?php
		echo '<select name="gallery_category" id="gallery_category" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>"><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add gallery category to use gallery.';
		endif;
?>
		</div>
        </div>
<?php
	exit;

}
function hide_editor(){
	if(isset($_GET['post']) && $_GET['post'] && get_option( 'tlpb_status') && get_option( 'tlpb_status')=='true'){
		$pst = get_post();
		if($pst->post_type=='page'){
			echo '<style>#postdivrich, #tl_page_builder, #tl_page_builder_template, #postimagediv, #tl_contact_metabox{display:none;}</style>';
		}
	}
}
add_filter('admin_head','hide_editor');

include 'shortcodes/index.php';

add_action('media_buttons', 'add_tlpb_button');
function add_tlpb_button() {
	if(get_option( 'tlpb_status') && get_option( 'tlpb_status')=='true'){
		$pst = get_post();
		if($pst->post_type=='page'){
			echo '<a href="javascript:void(0);" id="tlpb-btn" class="button"><i class="fa fa-plus-circle"></i>  Themelines Page Builder</a>';
		}
	}
}

add_action( 'wp_ajax_nopriv_post_from_change', 'post_from_change' );
add_action( 'wp_ajax_post_from_change', 'post_from_change' );
function post_from_change(){
?>
<label>From:</label>
<select name="tlpb[<?php echo $_POST['key'];?>][post_from]" class="post-text opt" id="post-from">
    <option value="all">All Posts</option>
    <?php $category = get_terms( 'category',array('hide_empty' => false)  );?>
    <?php if(isset($category) && $category):?>
    <?php foreach($category as $cat):?>
    <option value="<?php echo $cat->slug;?>" <?php echo $_POST['post_from']==$cat->slug?'selected':'';?>><?php echo $cat->name;?></option>
    <?php endforeach;?>
    <?php endif;?>
</select>
<?php
	exit();
}

add_action( 'wp_ajax_nopriv_page_from_change', 'page_from_change' );
add_action( 'wp_ajax_page_from_change', 'page_from_change' );
function page_from_change(){
?>
<label>From:</label>
<select name="tlpb[<?php echo $_POST['key'];?>][page_from]" class="post-text opt" id="page-from">
    <?php $args = array(
        'sort_order' => 'asc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => -1,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    ); 
    $pages = get_pages($args);
    ?>
    <option value="all">All Pages</option>
    <optgroup label="--Child Of--">
        <?php if(isset($pages) && $pages):?>
       
        <?php foreach($pages as $page):?>
        <option value="<?php echo $page->ID;?>" <?php echo $_POST['page_from']==$page->ID?'selected':'';?>><?php echo $page->post_title;?></option>
        <?php endforeach;?>
        <?php endif;?>
    </optgroup>
</select>
<?php
	exit();
}

add_action( 'wp_ajax_nopriv_load_gallery', 'load_gallery' );
add_action( 'wp_ajax_load_gallery', 'load_gallery' );
function load_gallery(){
	$cats = explode(',',$_POST['categories']);
	$tax_query = array();
	$tax_query[] = array(
		'taxonomy' => 'gallery_category',
		'field' => 'id',
		'terms' => $cats
	);
	$args = array(
		'post_type' => 'gallery',
		'tax_query' => $tax_query,
		'posts_per_page' => $_POST['items'],
		'offset' => $_POST['offset'],
		'post_status' => 'publish',
	);
	$loop = new WP_Query($args);
	if($loop->have_posts()):
	$gal_cat_slug = array();
	$gal_cat = array();
	foreach($cats as $cat){
		$gal_cat_details = get_term_by('id', $cat, 'gallery_category');
		$gal_cat_slug[] = $gal_cat_details->slug;
		$gal_cat[] = $gal_cat_details->name;
	}
	$style = '';
	$col = 12/$_POST['gal_grid'];
	$count = $_POST['count']+1;
		while($loop->have_posts()):
		$loop->the_post();
		if(has_post_thumbnail()):
		$cats = '';
		$cats_class = '';
		$gallery_terms = array();
		$term_list = wp_get_post_terms(get_the_ID(), 'gallery_category', array("fields" => "all"));
		foreach($term_list as $terms){
			$gallery_terms[] = $terms->slug;
		}
		$j=1;
		$cats = implode(' ',$gallery_terms);
		$fancy_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
	?>
    <div class="col-lg-<?php echo $col;?> col-sm-6 col-xs-12 portfolio <?php echo $cats;?> <?php echo $_POST['type']=='no-padding'?'no-padding':'';?>" data-cat="<?php echo $cats;?>">
            <div class="portfolio-wrapper port<?php echo $_POST['key'];?>">	
                <div class="image-block-folio">		
                    <?php the_post_thumbnail('large', array('class'=>'gal-img'));?>
                    <div class="hover-icon-folio">
                       <?php if($_POST['onclick']=='fancybox'):?>
                        <a class="fancybox-<?php echo $_POST['key'];?> gal-icon" rel="<?php echo $key;?>" href="<?php echo $fancy_image[0];?>" title="<?php the_title();?>">
                            <i class="fa fa-search"></i>
                        </a>
                        <?php elseif($_POST['onclick']=='new_page'):?>
                        <a class="gal-icon" href="<?php the_permalink();?>">
                            <i class="fa fa-link"></i>
                        </a>
                        <?php else:?>
                        <i class="fa fa-picture-o"></i>
                        <?php endif;?>
                    </div>
                </div>
                <div class="label">
                    <div class="label-text">
                        <?php if($onclick!='fancybox' || $onclick!='new_page'):?>
                        <div class="text-title"><?php the_title();?></div>
                        <?php else:?>
                        <a class="text-title" href="javascript:void(0);"><?php the_title();?></a>
                        <?php endif;?>
                    </div>
                    <div class="label-bg"></div>
                </div>
            </div>
        </div>
	<?php
		//if ($count%$_POST['gal_grid'] == 0) echo '<div class="clearfix"></div>';
        $count++;
        endif;
        endwhile;
?>
<?php
	else:
		echo 'no';
	endif;
	exit();
}