TlPb={
	toTitleCase:function(str){
			return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	},
	openMedia:function(evt){
		var $this = evt,
		frame = frame || {}, props, image;
		// If the media frame already exists, reopen it.
			if ('function' === typeof frame.open) {
				frame.open();
				return;
			}
			// Create the media frame.
			frame = wp.media.frames.frame = wp.media({
				title: $this.data('title'),
				className: 'media-frame tl-pb-builder-uploader',
				button: {
					text: $this.data('buttonText')
				},
				multiple: false
			});
			// When an image is selected, run a callback.
			frame.on('select', function () {
				// We set multiple to false so only get one image from the uploader
				var attachment = frame.state().get('selection').first().toJSON();
				// Remove the attachment caption
				attachment.caption = '';
				// Build the image
				props = wp.media.string.props(
					{},
					attachment
				);
				$this.css('background-image', 'url(' + attachment.url + ')');
				$this.attr('data-attachId', attachment.id);
				$this.find('.fa').removeClass('fa-picture-o').addClass('fa-times-circle');
				var id = $this.closest('.tl-pb-section').attr('id');
				
				var key  =$this.closest('.tl-pb-section').attr('id');
				$this.find('#this-image').val(attachment.url);
				/*// Show the image
				$placeholder.css('background-image', 'url(' + attachment.url + ')');
				$parent.addClass('ttfmake-has-image-set');
				// Record the chosen value
				$input.val(attachment.id);
				// Hide the link to set the image
				$add.hide();
				// Show the remove link
				$remove.show();*/
			});
			// Finally, open the modal
			frame.open();
	},

	colColumnHTML:function(c,d,key){
		var sm = (d!='' ? 12/d : 12), totalHtml='';
		//alert(jQuery('#'+key).find('[name="'+key+'-column-config"]').val());
		var result = c.split(',');
		var l = result.length;
		var i = 0;
		//alert(c);
		var tlpbrow = jQuery('#'+key).find('.tl-pb-row').html();
		/*var html = '<div class="tl-pb-col col-sm-'+sm+'"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_sec_con_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_con_edit" href="javascript:;"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="tl_pb_sec_con_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div data-attachId="" data-title="Set Image" class="tl-pb-section-content-background"><i class="fa fa-picture-o" aria-hidden="true"></i></div></div><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div></div></div>';*/
		//if(tlpbrow==''){
			while(i<l){
				totalHtml+='<div class="tl-pb-col col-sm-'+result[i]+'"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_sec_con_sett" href="javascript:;" title="Item Settings"><i class="fa fa-cog" aria-hidden="true"><input type="hidden" name="tlpb['+key+'][sec_class][]" id="this_sec_class" value=""><input type="hidden" name="tlpb['+key+'][sec_id][]" id="this_sec_id" value=""><input type="hidden" name="tlpb['+key+'][sec_grid][]" id="this_sec_grid" value="'+result[i]+'"><input type="hidden" name="tlpb['+key+'][image_position][]" id="this_image_position" value="top"><input type="hidden" name="tlpb['+key+'][animation][]" id="this_animation" value="no"><input type="hidden" name="tlpb['+key+'][animation_delay][]" id="this_animation_delay" value="1200ms"><input type="hidden" name="tlpb['+key+'][animation_duration][]" id="this_animation_duration" value="1000ms"></i></a><a class="tl_pb_sec_con_edit" href="javascript:void(0);" title="Edit Content"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="tl_pb_sec_con_module" href="javascript:void(0);" title="Select Module"><i class="fa fa-th" aria-hidden="true"></i></a><a class="tl_pb_sec_con_del" href="javascript:;" title="Delete this item"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div data-attachId="" data-title="Set Image" class="tl-pb-section-content-background"><input type="hidden" id="this-image" name="tlpb['+key+'][image][]" value=""><i class="fa fa-picture-o" aria-hidden="true"></i></div></div><div class="tl_pb_sec_con_edit" style="cursor:pointer;"><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div><textarea name="tlpb['+key+'][content][]" id="this-content" style="visibility:hidden;"></textarea></div></div></div>';
				i++;
			}
		//}else{
			//totalHtml+=jQuery('#'+key).find('.tl-pb-row').html();
		//}
		return totalHtml;
	},
	
	colBannerHTML:function(key){
		var html = '<div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_ban_sett" href="javascript:;" title="Item Settings"><i class="fa fa-cog" aria-hidden="true"><input type="hidden" name="tlpb['+key+'][sec_class][]" id="this_sec_class" value=""><input type="hidden" name="tlpb['+key+'][sec_id][]" id="this_sec_id" value=""><input type="hidden" name="tlpb['+key+'][animation][]" id="this_animation" value="no"><input type="hidden" name="tlpb['+key+'][animation_delay][]" id="this_animation_delay" value="1200ms"><input type="hidden" name="tlpb['+key+'][animation_duration][]" id="this_animation_duration" value="1000ms"></i></a><a class="tl_pb_sec_con_edit" href="javascript:void(0);" title="Edit Content"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="tl_pb_sec_con_del" href="javascript:;" title="Delete this item"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div data-attachId="" data-title="Set Image" class="tl-pb-section-content-background"><input type="hidden" id="this-image" name="tlpb['+key+'][image][]" value=""><div class="admin-banner-iconfont"><i class="fa fa-picture-o" aria-hidden="true"></i></div><div class="admin-slider-image-text"></div></div></div><div class="tl_pb_sec_con_edit" style="display:none;"><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div><textarea name="tlpb['+key+'][content][]" id="this-content" style="visibility:hidden;"></textarea></div></div></div>';
		return html;
},

	colGalleryHTML:function(c,key){
		var sm = ((c && c!='') ? 12/c : 12), totalHtml='';
		var html = '<div class="tl-pb-col col-sm-'+sm+'"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_sec_gallery_sett" href="javascript:;" title="Item Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><!--<a class="tl_pb_sec_con_edit" href="#openModal"><i class="fa fa-pencil" aria-hidden="true"></i></a>--><a class="tl_pb_sec_con_del_gal" href="javascript:;" title="Delete this item"><i class="fa fa-trash-o" aria-hidden="true"></i></a><input type="hidden" name="tlpb['+key+'][sec_class][]" id="this_sec_class" value=""><input type="hidden" name="tlpb['+key+'][sec_id][]" id="this_sec_id" value=""><input type="hidden" name="tlpb['+key+'][sec_title][]" id="this_sec_title" value=""><input type="hidden" name="tlpb['+key+'][sec_category][]" id="this_sec_category" value=""><input type="hidden" name="tlpb['+key+'][sec_url][]" id="this_sec_url" value=""><input type="hidden" name="tlpb['+key+'][animation][]" id="this_animation" value="no"><input type="hidden" name="tlpb['+key+'][animation_delay][]" id="this_animation_delay" value="1200ms"><input type="hidden" name="tlpb['+key+'][animation_duration][]" id="this_animation_duration" value="1000ms"></div><div data-attachId="" data-title="Set Image" class="tl-pb-section-content-background"><input type="hidden" id="this-image" name="tlpb['+key+'][image][]" value=""><i class="fa fa-picture-o" aria-hidden="true"></i></div></div><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div></div></div>';
		for(var i = 0; i < c; i++){
			totalHtml+=html;
		}
		return totalHtml;
	},
	moduleElements:function(key){
		var html = '<div class="row"><ul><li><a href="javascript:void(0);" class="module-items" data-target="Widget" data-id="'+key+'"><i class="fa fa-tachometer" aria-hidden="true"></i><p>Widget</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Block" data-id="'+key+'"><i class="fa fa-square" aria-hidden="true"></i><p>Block</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Call To Action" data-id="'+key+'"><i class="fa fa-phone-square" aria-hidden="true"></i><p>Call To Action</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Contact" data-id="'+key+'"><i class="fa fa-envelope" aria-hidden="true"></i><p>Contact</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Post Types" data-id="'+key+'"><i class="fa fa-tags" aria-hidden="true"></i><p>Post Types</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Banner or Slider" data-id="'+key+'"><i class="fa fa-picture-o" aria-hidden="true"></i><p>Banner/Slider</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Gallery" data-id="'+key+'"><i class="fa fa-camera" aria-hidden="true"></i><p>Gallery</p></a></li><li><a href="javascript:void(0);" class="module-items" data-target="Developer Mode" data-id="'+key+'"><i class="fa fa-terminal" aria-hidden="true"></i><p>Developer Mode</p></a></li></ul></div>';
		return html;
	},
	CreateFJSON:function(){
		return 1;	
	},
}

function tl_pb_genarate(){
	this.id = Math.round(new Date().getTime() + (Math.random() * 100));
	return this.id;
}

function m7_resize_thickbox(){
	var TB_WIDTH = 429;
	var TB_HEIGHT = 551;
	var ml = TB_WIDTH / 2;
	ml = '-'+ml;
  jQuery(document).find('#TB_window').width(TB_WIDTH).height(TB_HEIGHT).css({'margin-left': '-' + parseInt((( TB_WIDTH - 50 ) / 2),10) + 'px'});
}

function module_resize_thickbox(){
	var TB_WIDTH = 429;
	var TB_HEIGHT = 290;
	var ml = TB_WIDTH / 2;
	ml = '-'+ml;
  jQuery(document).find('#TB_window').width(TB_WIDTH).height(TB_HEIGHT).css({'margin-left': '-' + parseInt((( TB_WIDTH - 50 ) / 2),10) + 'px'});
}
//tinyMCE.execCommand("mceAddEditor", false, 'tinymce_data2');
jQuery(document).ready(function($) { 
var tlPbGenarate = new tl_pb_genarate();
//tinyMCE.execCommand('mceAddEditor', false, 'tinymce_data2')
//tinyMCE.execCommand('mceAddControl', false, 'tinymce_data2')
	$(document).on('click','.tl-pb-section-content-background',function(){
		TlPb.openMedia($(this));
	});
	
	$(document).on('click','.fa-times-circle:not(.tl-pb-section-content-background)',function(e){
		e.preventDefault();
		e.stopPropagation();
		$(this).addClass('fa-picture-o').closest('.tl-pb-section-content-background').find('#this-image').val('');
		$(this).removeClass('fa-times-circle').addClass('fa-picture-o').closest('.tl-pb-section-content-background').attr('data-attachId','').css('background-image', 'none');
		$(this).html('');
	});

	$(document).on('click','.fa-times-circle:not(.tl-pb-section-content-background)',function(e){
		e.preventDefault();
		e.stopPropagation();
		$(this).closest('#this-image').val('');
		$(this).removeClass('fa-times-circle').addClass('fa-picture-o').closest('.tl-pb-section-post-background').find('#this-image').attr('data-attachId','').attr('data-bgUrl','').css('background-image', 'none');
		$(this).removeClass('fa-times-circle').addClass('fa-picture-o').closest('.tl-pb-section-post-background').attr('data-attachId','').attr('data-bgUrl','').css('background-image', 'none');
	});

	$(document).on('click','#tl-pb-menu-item-column',function(){
		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(

				'<div class="row"><div class="tl-pb-pop-col"><label>Select Column</label><select class="form-control" name="select_column" id="select_column"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_column_set">Add</button></div></div>'

			)
		);
		
		tb_show("Configure Column", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );
		
	});
	$(document).on('click','#btn_column_set',function(e){
		var key = tl_pb_genarate();
		var column = $('#select_column').val();
		if(column==1){
			var arr = '12';
		}else{
			if(column==2){
				var arr = '6,6';
			}else{
				if(column==3){
					var arr = '4,4,4';
				}else{
					if(column==4){
						var arr = '3,3,3,3';
					}else{
						if(column==5){
							var arr = '3,2,2,2,3';
						}else{
							if(column==6){
								var arr = '2,2,2,2,2,2';
							}else{
								if(column==7){
									var arr = '1,2,2,2,2,2,1';
								}else{
									if(column==8){
										var arr = '1,1,2,2,2,2,1,1';
									}else{
										if(column==9){
											var arr = '1,1,1,2,2,2,1,1,1';
										}else{
											if(column==10){
												var arr = '1,1,1,1,2,2,1,1,1,1';
											}else{
												if(column==11){
													var arr = '1,1,1,1,1,2,1,1,1,1,1';
												}else{
													if(column==12){
														var arr = '1,1,1,1,1,1,1,1,1,1,1,1';
													}else{
														var arr = '4,4,4';
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Container</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><a href="javascript:;" class="tl-pb-column-add-item-link"><div class="tl-pb-column-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="column"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html(TlPb.colColumnHTML(arr,column,key));
		jQuery( document ).tooltip();
		jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);
		jQuery('.tl-pb-row').sortable({
		    handle: '.tl-pb-section-content-sortable',
			placeholder: 'sortable-placeholder',
			forcePlaceholderSizeType: true,
			distance: 2,
			tolerance: 'pointer',
			//zIndex: 99999,
			cursor: 'move'
    });
	tb_remove();

		/*$(".tl-pb-col").sortable({

	     	//containment: "document",

	       // items: "> div",

	        //connectWith: '.tl-pb-col',

	        handle: '.tl-pb-section-content-sortable',

	        cursor: 'move'

    	});*/

	});
	
	$(document).on('click','.tl-pb-column-add-item-link',function(e){
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Column</label><select class="form-control" name="select_column" id="select_column"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div></div>'+
				'<input type="hidden" name="key" id="key" value="'+$(this).closest('.tl-pb-section').attr('id')+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_column_set_change">Change</button></div></div>'
			)
		);
		tb_show("Configure Column", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		m7_resize_thickbox(); 
		jQuery(window).on( 'resize', m7_resize_thickbox );
	});
	
	$(document).on('click','#btn_column_set_change',function(e){
		var column = $('#select_column').val();
		if(column==1){
			var arr = '12';
		}else{
			if(column==2){
				var arr = '6,6';
			}else{
				if(column==3){
					var arr = '4,4,4';
				}else{
					if(column==4){
						var arr = '3,3,3,3';
					}else{
						if(column==5){
							var arr = '3,2,2,2,3';
						}else{
							if(column==6){
								var arr = '2,2,2,2,2,2';
							}else{
								if(column==7){
									var arr = '1,2,2,2,2,2,1';
								}else{
									if(column==8){
										var arr = '1,1,2,2,2,2,1,1';
									}else{
										if(column==9){
											var arr = '1,1,1,2,2,2,1,1,1';
										}else{
											if(column==10){
												var arr = '1,1,1,1,2,2,1,1,1,1';
											}else{
												if(column==11){
													var arr = '1,1,1,1,1,2,1,1,1,1,1';
												}else{
													if(column==12){
														var arr = '1,1,1,1,1,1,1,1,1,1,1,1';
													}else{
														var arr = '4,4,4';
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		var key = $('#key').val();
		$('#'+key).find('.tl-pb-row').append(TlPb.colColumnHTML(arr,column,key));
		tb_remove();
	});
	$(document).on('click','#tl-pb-menu-item-full-column',function(){
		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(

				'<div class="row"><div class="tl-pb-pop-col"><label>Select Column</label><select class="form-control" name="select_column" id="select_column"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_full_column_set">Add</button></div></div>'

			)
		);
		
		tb_show("Configure Column", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );
		
	});
	$(document).on('click','#btn_full_column_set',function(e){
		var key = tl_pb_genarate();
		var column = $('#select_column').val();
		if(column==1){
			var arr = '12';
		}else{
			if(column==2){
				var arr = '6,6';
			}else{
				if(column==3){
					var arr = '4,4,4';
				}else{
					if(column==4){
						var arr = '3,3,3,3';
					}else{
						if(column==5){
							var arr = '3,2,2,2,3';
						}else{
							if(column==6){
								var arr = '2,2,2,2,2,2';
							}else{
								if(column==7){
									var arr = '1,2,2,2,2,2,1';
								}else{
									if(column==8){
										var arr = '1,1,2,2,2,2,1,1';
									}else{
										if(column==9){
											var arr = '1,1,1,2,2,2,1,1,1';
										}else{
											if(column==10){
												var arr = '1,1,1,1,2,2,1,1,1,1';
											}else{
												if(column==11){
													var arr = '1,1,1,1,1,2,1,1,1,1,1';
												}else{
													if(column==12){
														var arr = '1,1,1,1,1,1,1,1,1,1,1,1';
													}else{
														var arr = '4,4,4';
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Full Width</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><a href="javascript:;" class="tl-pb-full-column-add-item-link"><div class="tl-pb-full-column-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="full_column"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html(TlPb.colColumnHTML(arr,column,key));
		jQuery( document ).tooltip();
		jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);
		jQuery('.tl-pb-row').sortable({
		    handle: '.tl-pb-section-content-sortable',
			placeholder: 'sortable-placeholder',
			forcePlaceholderSizeType: true,
			distance: 2,
			tolerance: 'pointer',
			//zIndex: 99999,
			cursor: 'move'
    });
	tb_remove();

		/*$(".tl-pb-col").sortable({

	     	//containment: "document",

	       // items: "> div",

	        //connectWith: '.tl-pb-col',

	        handle: '.tl-pb-section-content-sortable',

	        cursor: 'move'

    	});*/

	});
	
	$(document).on('click','.tl-pb-full-column-add-item-link',function(e){
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Column</label><select class="form-control" name="select_column" id="select_column"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div></div>'+
				'<input type="hidden" name="key" id="key" value="'+$(this).closest('.tl-pb-section').attr('id')+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_full_column_set_change">Change</button></div></div>'
			)
		);
		tb_show("Configure Column", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		m7_resize_thickbox(); 
		jQuery(window).on( 'resize', m7_resize_thickbox );
	});
	
	$(document).on('click','#btn_full_column_set_change',function(e){
		var column = $('#select_column').val();
		if(column==1){
			var arr = '12';
		}else{
			if(column==2){
				var arr = '6,6';
			}else{
				if(column==3){
					var arr = '4,4,4';
				}else{
					if(column==4){
						var arr = '3,3,3,3';
					}else{
						if(column==5){
							var arr = '3,2,2,2,3';
						}else{
							if(column==6){
								var arr = '2,2,2,2,2,2';
							}else{
								if(column==7){
									var arr = '1,2,2,2,2,2,1';
								}else{
									if(column==8){
										var arr = '1,1,2,2,2,2,1,1';
									}else{
										if(column==9){
											var arr = '1,1,1,2,2,2,1,1,1';
										}else{
											if(column==10){
												var arr = '1,1,1,1,2,2,1,1,1,1';
											}else{
												if(column==11){
													var arr = '1,1,1,1,1,2,1,1,1,1,1';
												}else{
													if(column==12){
														var arr = '1,1,1,1,1,1,1,1,1,1,1,1';
													}else{
														var arr = '4,4,4';
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		var key = $('#key').val();
		$('#'+key).find('.tl-pb-row').append(TlPb.colColumnHTML(arr,column,key));
		tb_remove();
	});

	$(document).on('click','#tl-pb-menu-item-banner',function(e){
		var key = tl_pb_genarate();
		var $this = $(this);
		var interval	=	'<select name="interval" id="interval" class="form-control">';
		for(var i=500;i<=10000;i+=500)
		{
			interval	+=	'<option value="'+i+'">'+i+'ms</option>'
		}
		interval	+=	'</select>';
		$('.post_slider_div').load(ajaxurl,{action:'select_post_slider'},function(m){});
		$('.single_slider_div').load(ajaxurl,{action:'select_single_slider'},function(m){});
		$('.slider_div').load(ajaxurl,{action:'select_slider'},function(m){});
		
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Type</label><select class="form-control" name="banner_type" id="bannertype"><option value="">--Select Banner--</option><option value="banner">Banner</option><option value="slider">Slider</option><option value="others">Others</option></select></div></div>'+
				'<div class="banner_config"></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="banner_type"></div></div></div>'+
				'<input type="hidden" name="hidden_ban_key" id="hidden_ban_key" value="">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_banner_type_set">Change</button></div></div>'
			)
		);
		
		$( "#bannertype" ).change(function() {
		  if($(this).val()=='banner'){
			  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="parallax" value="1"> Use as Parallax</div></div>');
			  $('.banner_config').append('<input type="hidden" name="select_banner" id="select_banner" value="themelines_banner">');
			  $('.banner_config').append('<div class="row"><div class="tl-pb-pop-col"><label>Select Layout</label><select class="form-control" name="select_layout" id="layout"><option value="full_width">Full Width</option><option value="box">Box</option></select></div></div>');
			  $('.banner_type').html($('.single_slider_div').html());
		  }else{
			  if($(this).val()=='slider'){
				  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Select Slider</label><select class="form-control" name="select_slider_criteria" id="select_slider"><option value="post_slider">Post Slider</option><option value="themelines_slider">Themelines Slider</option></select></div></div>');
				  $('.banner_config').append('<div class="row"><div class="tl-pb-pop-col"><label>Select Layout</label><select class="form-control" name="select_layout" id="layout"><option value="full_width">Full Width</option><option value="box">Box</option></select></div></div><div class="row"><div class="tl-pb-pop-col"><label>Interval</label>'+interval+'</div></div>');
				  //$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
				  $('.banner_type').html($('.post_slider_div').html());
			  }else{
				  if($(this).val()=='others'){
					  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="slider_shortcode" name="shortcode"></textarea></div></div>');
					  $('.banner_type').html('');
				  }else{
					  if($(this).val()==''){
							$('.banner_config').html('');
					  		$('.banner_type').html('');
					  }
				  }
			  }
		  }
		  /*$( '[name="select_banner"]' ).change(function() {
			if($(this).val()=='themelines_banner'){
				   $('.banner_type').html($('.single_slider_div').html());
				 //$('.banner_type').load(ajaxurl,{action:'select_single_slider'},function(m){});
			}else{
				$('.banner_type').html('');
			}
		  });*/
		  $( '[name="select_slider_criteria"]' ).change(function() {
			if($(this).val()=='themelines_slider'){
				$('.banner_type').html($('.slider_div').html());
			 	//$('.banner_type').load(ajaxurl,{action:'select_slider'},function(m){});
		  	}else{
				if($(this).val()=='post_slider'){
					$('.banner_type').html($('.post_slider_div').html());
					//$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
				}else{
					$('.banner_type').html('');
				}
			}
		  });
		});
		
		if($this.closest('.tl-pb-section').attr('id')){
			var key = $this.closest('.tl-pb-section').attr('id');
			$('#hidden_ban_key').val(key);
			$('[name="banner_type"]').val($('[name="tlpb['+key+'][type]"]').val()).change();
			if($('[name="tlpb['+key+'][type]"]').val()=='banner'){
				if($('[name="tlpb['+key+'][banner_parallax]"]').val()=='1'){
					$('[name="parallax"]').prop('checked', true);
				}
				$('[name="select_banner"]').val($('[name="tlpb['+key+'][subtype]"]').val()).change();
				$('[name="select_layout"]').val($('[name="tlpb['+key+'][layout]"]').val()).change();
				$('[name="select_banner_slide"]').val($('[name="tlpb['+key+'][slider]"]').val()).change();
			}
			if($('[name="tlpb['+key+'][type]"]').val()=='slider'){
				$('[name="select_slider_criteria"]').val($('[name="tlpb['+key+'][subtype]"]').val()).change();
				$('[name="select_layout"]').val($('[name="tlpb['+key+'][layout]"]').val()).change();
				$('[name="interval"]').val($('[name="tlpb['+key+'][interval]"]').val()).change();
				var valArr = $('[name="tlpb['+key+'][category]"]').val().split(',');
				size = valArr.length;
				for (i = 0; i < size; i++) {
					$("#slider option[value='" + valArr[i] + "']").attr("selected", 1);
				}
				//$("#slider").multiSelect("refresh");
			}
			if($('[name="tlpb['+key+'][type]"]').val()=='other'){
				$('[name="banner_type"]').val('others').change();
				$('[name="shortcode"]').val($('[name="tlpb['+key+'][shortcode]"]').val());
			}
		}
		
		/*$( '[name="select_text_postion"]' ).change(function() {
			if($(this).val()=='both')
				$('.banner_text').html('<div class="row"><div class="tl-pb-pop-col"><label>Select Text Position</label><select name="select_text_position" class="form-control"><option value="center">Center</option><option value="both">Left and Right</option><option value="left">Left</option><option value="right">Right</option></select></div></div>');
			  }else{
				  $('.banner_config').html('');
			  }
		});*/

		tb_show("Configure Banner", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );
		
		/*$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_ban_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <a href="javascript:;" class="tl-pb-gallery-add-item-link" title="Add new item"><div class="tl-pb-gallery-add-item"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></a><input type="hidden" name="tl_pb_key[]" value="'+key+'"><input type="hidden" name="'+key+'-type" value="banner"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html(TlPb.colGalleryHTML(1));

		$('.tl-pb-row').sortable({

		    handle: '.tl-pb-section-content-sortable',

			placeholder: 'sortable-placeholder',

			forcePlaceholderSizeType: true,

			distance: 2,

			tolerance: 'pointer',

			cursor: 'move'

    	});*/

	});

$(document).on('click','#btn_banner_type_set',function(){
	var key = tl_pb_genarate();
	var oldkey = 0;
	if($('#hidden_ban_key').val()!=''){
		var key = $('#hidden_ban_key').val();
		//$('#'+key).remove();
		var oldkey = $('#hidden_ban_key').val();
	}
	if($('#bannertype').val()==''){
		jAlert('Please select the type you want to add','Notice');
	}else{
		if($('#bannertype').val()=='banner'){
			if ($('[name="parallax"]').is(':checked')) {
				var parallax = '1';
			}else{
				var parallax = '0';
			}
			var layout = $('[name="select_layout"]').val();
			if($('[name="select_banner"]').val()=='custom_banner'){
				
				if(key==oldkey){
					var keytitle = $('[name="tlpb['+key+'][title]"]').val();
					if(typeof keytitle == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
						var keytitle = $('[name="tlpb['+key+'][title]"]').val();
					}
					var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
					if(typeof keytitle_position == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
						var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
					}
					var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
					if(typeof keysubtitle == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
						var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
					}
					var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
					if(typeof keysubtitle_position == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
						var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
					}
					var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
					if(typeof keytitle_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
						var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
					}
					var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
					if(typeof keysubtitle_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
						var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
					}
					var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
					if(typeof keycontent_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
						var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
					}
					
					var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
					if(typeof keymargin_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
						var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
					}
					var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
					if(typeof keymargin_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
						var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
					}
					var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
					if(typeof keymargin_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
						var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
					}
					var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
					if(typeof keymargin_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
						var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
					}
					var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
					if(typeof keymargin_custom_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
						var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
					}
					var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
					if(typeof keymargin_custom_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
						var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
					}
					var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
					if(typeof keymargin_custom_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
						var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
					}
					var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
					if(typeof keymargin_custom_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
						var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
					}
					
					var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
					if(typeof keypadding_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
						var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
					}
					var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
					if(typeof keypadding_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
						var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
					}
					var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
					if(typeof keypadding_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
						var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
					}
					var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
					if(typeof keypadding_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
						var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
					}
					var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
					if(typeof keypadding_custom_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
						var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
					}
					var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
					if(typeof keypadding_custom_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
						var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
					}
					var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
					if(typeof keypadding_custom_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
						var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
					}
					var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
					if(typeof keypadding_custom_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
						var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
					}
					
					var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
					if(typeof title_tag == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
						var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
					}
					var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
					if(typeof subtitle_tag == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
						var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
					}
					
					var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
					if(typeof title_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
						var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
					}
					var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
					if(typeof title_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
						var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
					}
					var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
					if(typeof title_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
						var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
					}
					var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
					if(typeof title_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
						var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
					}
					var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
					if(typeof subtitle_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
						var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
					}
					var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
					if(typeof subtitle_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
						var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
					}
					var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
					if(typeof subtitle_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
						var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
					}
					var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
					if(typeof subtitle_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
						var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
					}
					var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
					if(typeof content_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
						var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
					}
					var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
					if(typeof content_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
						var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
					}
					var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
					if(typeof content_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
						var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
					}
					var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
					if(typeof content_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
						var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
					}
					var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
					if(typeof background_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
						var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
					}
					
					var keyclass = $('[name="tlpb['+key+'][class]"]').val();
					if(typeof keyclass == 'undefined'){
						$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
						var keyclass = $('[name="tlpb['+key+'][class]"]').val();
					}
					var keyid = $('[name="tlpb['+key+'][id]"]').val();
					if(typeof keyid == 'undefined'){
						$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
						var keyid = $('[name="tlpb['+key+'][id]"]').val();
					}
					var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
					if(typeof keybgcolor == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
						var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
					}
					var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
					if(typeof keybgimage == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
						var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
					}
					var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
					if(typeof keyparallax == 'undefined'){
						$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
						var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
					}
					var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
					if (typeof imgattr !== typeof undefined && imgattr !== false) {
						var keybgid = imgattr;
					}else{
						var keybgid = '';
					}
					$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Custom Banner</h4></div><div class="tl-pb-right-bar"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><a href="javascript:;" class="tl-pb-banner-add-item-link"><div class="tl-pb-banner-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="banner"><input type="hidden" name="tlpb['+key+'][subtype]" value="custom_banner"><input type="hidden" name="tlpb['+key+'][banner_parallax]" value="'+parallax+'"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
					$('#'+key).find('.tl-pb-row').html(TlPb.colBannerHTML(key));
				}else{
					$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Custom Banner</h4></div><div class="tl-pb-right-bar"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><a href="javascript:;" class="tl-pb-banner-add-item-link"><div class="tl-pb-banner-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="banner"><input type="hidden" name="tlpb['+key+'][subtype]" value="custom_banner"><input type="hidden" name="tlpb['+key+'][banner_parallax]" value="'+parallax+'"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html(TlPb.colBannerHTML(key));
				}
				jQuery( document ).tooltip();
				jQuery('.tl-pb-row').sortable({
					handle: '.tl-pb-section-content-sortable',
					placeholder: 'sortable-placeholder',
					forcePlaceholderSizeType: true,
					distance: 2,
					tolerance: 'pointer',
					cursor: 'move'
				});	
			}else{
				if($('[name="select_banner"]').val()=='themelines_banner'){
					var slider = [];
					var rel = [];
					//$.each($("[name='select_slider'] option:selected"), function(){            
            			var rel = $("[name='select_banner_slide'] option:selected").attr('rel');
						var slider = $("[name='select_banner_slide'] option:selected").val();
						//slider.push($(this).val());
						//rel.push($(this).attr('rel'));
        			//});
					if(slider!=''){
						if(key==oldkey){
							var keytitle = $('[name="tlpb['+key+'][title]"]').val();
							if(typeof keytitle == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
								var keytitle = $('[name="tlpb['+key+'][title]"]').val();
							}
							var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
							if(typeof keytitle_position == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
								var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
							}
							var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
							if(typeof keysubtitle == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
								var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
							}
							var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
							if(typeof keysubtitle_position == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
								var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
							}
							var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
							if(typeof keytitle_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
								var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
							}
							var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
							if(typeof keysubtitle_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
								var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
							}
							var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
							if(typeof keycontent_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
								var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
							}
							
							var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
							if(typeof keymargin_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
								var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
							}
							var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
							if(typeof keymargin_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
								var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
							}
							var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
							if(typeof keymargin_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
								var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
							}
							var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
							if(typeof keymargin_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
								var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
							}
							var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
							if(typeof keymargin_custom_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
								var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
							}
							var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
							if(typeof keymargin_custom_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
								var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
							}
							var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
							if(typeof keymargin_custom_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
								var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
							}
							var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
							if(typeof keymargin_custom_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
								var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
							}
							
							var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
							if(typeof keypadding_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
								var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
							}
							var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
							if(typeof keypadding_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
								var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
							}
							var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
							if(typeof keypadding_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
								var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
							}
							var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
							if(typeof keypadding_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
								var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
							}
							var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
							if(typeof keypadding_custom_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
								var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
							}
							var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
							if(typeof keypadding_custom_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
								var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
							}
							var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
							if(typeof keypadding_custom_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
								var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
							}
							var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
							if(typeof keypadding_custom_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
								var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
							}
							
							var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
							if(typeof title_tag == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
								var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
							}
							var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
							if(typeof subtitle_tag == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
								var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
							}
							
							var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
							if(typeof title_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
								var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
							}
							var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
							if(typeof title_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
								var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
							}
							var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
							if(typeof title_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
								var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
							}
							var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
							if(typeof title_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
								var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
							}
							var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
							if(typeof subtitle_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
								var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
							}
							var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
							if(typeof subtitle_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
								var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
							}
							var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
							if(typeof subtitle_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
								var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
							}
							var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
							if(typeof subtitle_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
								var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
							}
							var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
							if(typeof content_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
								var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
							}
							var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
							if(typeof content_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
								var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
							}
							var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
							if(typeof content_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
								var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
							}
							var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
							if(typeof content_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
								var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
							}
							var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
							if(typeof background_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
								var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
							}
							
							var keyclass = $('[name="tlpb['+key+'][class]"]').val();
							if(typeof keyclass == 'undefined'){
								$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
								var keyclass = $('[name="tlpb['+key+'][class]"]').val();
							}
							var keyid = $('[name="tlpb['+key+'][id]"]').val();
							if(typeof keyid == 'undefined'){
								$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
								var keyid = $('[name="tlpb['+key+'][id]"]').val();
							}
							var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
							if(typeof keybgcolor == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
								var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
							}
							var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
							if(typeof keybgimage == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
								var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
							}
							var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
							if(typeof keyparallax == 'undefined'){
								$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
								var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
							}
							var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
							if (typeof imgattr !== typeof undefined && imgattr !== false) {
								var keybgid = imgattr;
							}else{
								var keybgid = '';
							}
							
							$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Themelines Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="banner"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_banner"><input type="hidden" name="tlpb['+key+'][banner_parallax]" value="'+parallax+'"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
							$('#'+key).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][slider]" id="'+key+'-slider" value="'+slider+'"></h3></div></div></div></div></div>');
						}else{
							$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Themelines Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="banner"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_banner"><input type="hidden" name="tlpb['+key+'][banner_parallax]" value="'+parallax+'"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][slider]" id="'+key+'-slider" value="'+slider+'"></h3></div></div></div></div></div>');
						}
						jQuery( document ).tooltip();
						jQuery('.tl-pb-row').sortable({
							handle: '.tl-pb-section-content-sortable',
							placeholder: 'sortable-placeholder',
							forcePlaceholderSizeType: true,
							distance: 2,
							tolerance: 'pointer',
							cursor: 'move'
						});	
					}
					
				}
			}
		}else{
			if($('#bannertype').val()=='slider'){
				var layout = $('[name="select_layout"]').val();
				var interval	= $('#interval').val();
				if ($('[name="parallax"]').is(':checked')) {
					var parallax = '1';
				}else{
					var parallax = '0';
				}
				if($('[name="select_slider_criteria"]').val()=='post_slider'){
					var category= [];
					var rel= [];
					$.each($("[name='slider'] option:selected"), function(){            
            			category.push($(this).val());
						rel.push($(this).attr('rel'));
        			});
					if(category!=''){
						if(key==oldkey){
							var keytitle = $('[name="tlpb['+key+'][title]"]').val();
							if(typeof keytitle == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
								var keytitle = $('[name="tlpb['+key+'][title]"]').val();
							}
							var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
							if(typeof keytitle_position == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
								var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
							}
							var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
							if(typeof keysubtitle == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
								var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
							}
							var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
							if(typeof keysubtitle_position == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
								var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
							}
							var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
							if(typeof keytitle_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
								var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
							}
							var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
							if(typeof keysubtitle_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
								var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
							}
							var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
							if(typeof keycontent_color == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
								var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
							}
							
							var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
							if(typeof keymargin_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
								var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
							}
							var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
							if(typeof keymargin_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
								var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
							}
							var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
							if(typeof keymargin_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
								var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
							}
							var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
							if(typeof keymargin_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
								var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
							}
							var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
							if(typeof keymargin_custom_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
								var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
							}
							var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
							if(typeof keymargin_custom_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
								var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
							}
							var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
							if(typeof keymargin_custom_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
								var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
							}
							var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
							if(typeof keymargin_custom_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
								var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
							}
							
							var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
							if(typeof keypadding_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
								var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
							}
							var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
							if(typeof keypadding_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
								var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
							}
							var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
							if(typeof keypadding_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
								var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
							}
							var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
							if(typeof keypadding_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
								var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
							}
							var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
							if(typeof keypadding_custom_top == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
								var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
							}
							var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
							if(typeof keypadding_custom_bottom == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
								var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
							}
							var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
							if(typeof keypadding_custom_left == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
								var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
							}
							var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
							if(typeof keypadding_custom_right == 'undefined'){
								$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
								var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
							}
							
							var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
							if(typeof title_tag == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
								var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
							}
							var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
							if(typeof subtitle_tag == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
								var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
							}
							
							var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
							if(typeof title_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
								var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
							}
							var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
							if(typeof title_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
								var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
							}
							var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
							if(typeof title_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
								var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
							}
							var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
							if(typeof title_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
								var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
							}
							var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
							if(typeof subtitle_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
								var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
							}
							var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
							if(typeof subtitle_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
								var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
							}
							var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
							if(typeof subtitle_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
								var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
							}
							var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
							if(typeof subtitle_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
								var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
							}
							var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
							if(typeof content_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
								var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
							}
							var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
							if(typeof content_animation == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
								var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
							}
							var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
							if(typeof content_animation_delay == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
								var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
							}
							var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
							if(typeof content_animation_duration == 'undefined'){
								$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
								var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
							}
							var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
							if(typeof background_color_class == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
								var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
							}
							
							var keyclass = $('[name="tlpb['+key+'][class]"]').val();
							if(typeof keyclass == 'undefined'){
								$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
								var keyclass = $('[name="tlpb['+key+'][class]"]').val();
							}
							var keyid = $('[name="tlpb['+key+'][id]"]').val();
							if(typeof keyid == 'undefined'){
								$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
								var keyid = $('[name="tlpb['+key+'][id]"]').val();
							}
							var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
							if(typeof keybgcolor == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
								var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
							}
							var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
							if(typeof keybgimage == 'undefined'){
								$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
								var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
							}
							var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
							if(typeof keyparallax == 'undefined'){
								$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
								var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
							}
							var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
							if (typeof imgattr !== typeof undefined && imgattr !== false) {
								var keybgid = imgattr;
							}else{
								var keybgid = '';
							}
							
							$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Post Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="slider"><input type="hidden" name="tlpb['+key+'][subtype]" value="post_slider"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
							$('#'+key).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3><input type="hidden" name="tlpb['+key+'][interval]" id="'+key+'-interval" value="'+interval+'"></div></div></div></div></div>');
						}else{
							$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Post Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="slider"><input type="hidden" name="tlpb['+key+'][subtype]" value="post_slider"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3><input type="hidden" name="tlpb['+key+'][interval]" id="'+key+'-interval" value="'+interval+'"></div></div></div></div></div>');
						}
						jQuery( document ).tooltip();
						jQuery('.tl-pb-row').sortable({
							handle: '.tl-pb-section-content-sortable',
							placeholder: 'sortable-placeholder',
							forcePlaceholderSizeType: true,
							distance: 2,
							tolerance: 'pointer',
							cursor: 'move'
						});
					}else{
						jAlert('Please select at least one category.','Notice');
						return false;
					}
				}else{
					if($('[name="select_slider_criteria"]').val()=='themelines_slider'){
						var category= [];
						var rel= [];
						$.each($("[name='slider'] option:selected"), function(){            
							category.push($(this).val());
							rel.push($(this).attr('rel'));
						});
						if(category!=''){
							if(key==oldkey){
								var keytitle = $('[name="tlpb['+key+'][title]"]').val();
								if(typeof keytitle == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
									var keytitle = $('[name="tlpb['+key+'][title]"]').val();
								}
								var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
								if(typeof keytitle_position == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
									var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
								}
								var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
								if(typeof keysubtitle == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
									var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
								}
								var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
								if(typeof keysubtitle_position == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
									var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
								}
								var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
								if(typeof keytitle_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
									var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
								}
								var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
								if(typeof keysubtitle_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
									var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
								}
								var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
								if(typeof keycontent_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
									var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
								}
								
								var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
								if(typeof keymargin_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
									var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
								}
								var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
								if(typeof keymargin_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
									var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
								}
								var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
								if(typeof keymargin_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
									var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
								}
								var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
								if(typeof keymargin_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
									var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
								}
								var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
								if(typeof keymargin_custom_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
									var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
								}
								var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
								if(typeof keymargin_custom_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
									var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
								}
								var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
								if(typeof keymargin_custom_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
									var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
								}
								var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
								if(typeof keymargin_custom_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
									var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
								}
								
								var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
								if(typeof keypadding_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
									var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
								}
								var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
								if(typeof keypadding_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
									var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
								}
								var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
								if(typeof keypadding_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
									var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
								}
								var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
								if(typeof keypadding_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
									var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
								}
								var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
								if(typeof keypadding_custom_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
									var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
								}
								var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
								if(typeof keypadding_custom_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
									var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
								}
								var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
								if(typeof keypadding_custom_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
									var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
								}
								var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
								if(typeof keypadding_custom_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
									var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
								}
								
								var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
								if(typeof title_tag == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
									var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
								}
								var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
								if(typeof subtitle_tag == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
									var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
								}
								
								var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
								if(typeof title_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
									var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
								}
								var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
								if(typeof title_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
									var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
								}
								var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
								if(typeof title_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
									var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
								}
								var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
								if(typeof title_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
									var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
								}
								var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
								if(typeof subtitle_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
									var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
								}
								var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
								if(typeof subtitle_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
									var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
								}
								var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
								if(typeof subtitle_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
									var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
								}
								var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
								if(typeof subtitle_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
									var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
								}
								var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
								if(typeof content_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
									var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
								}
								var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
								if(typeof content_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
									var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
								}
								var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
								if(typeof content_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
									var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
								}
								var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
								if(typeof content_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
									var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
								}
								var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
								if(typeof background_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
									var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
								}
								
								var keyclass = $('[name="tlpb['+key+'][class]"]').val();
								if(typeof keyclass == 'undefined'){
									$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
									var keyclass = $('[name="tlpb['+key+'][class]"]').val();
								}
								var keyid = $('[name="tlpb['+key+'][id]"]').val();
								if(typeof keyid == 'undefined'){
									$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
									var keyid = $('[name="tlpb['+key+'][id]"]').val();
								}
								var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
								if(typeof keybgcolor == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
									var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
								}
								var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
								if(typeof keybgimage == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
									var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
								}
								var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
								if(typeof keyparallax == 'undefined'){
									$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
									var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
								}
								var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
								if (typeof imgattr !== typeof undefined && imgattr !== false) {
									var keybgid = imgattr;
								}else{
									var keybgid = '';
								}
								
								$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Themelines Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="slider"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_slider"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
								$('#'+key).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3><input type="hidden" name="tlpb['+key+'][interval]" id="'+key+'-interval" value="'+interval+'"></div></div></div></div></div>');
							}else{
								$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Themelines Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="slider"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_slider"><input type="hidden" name="tlpb['+key+'][layout]" value="'+layout+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3><input type="hidden" name="tlpb['+key+'][interval]" id="'+key+'-interval" value="'+interval+'"></div></div></div></div></div>');
							}
							jQuery( document ).tooltip();
							jQuery('.tl-pb-row').sortable({
								handle: '.tl-pb-section-content-sortable',
								placeholder: 'sortable-placeholder',
								forcePlaceholderSizeType: true,
								distance: 2,
								tolerance: 'pointer',
								cursor: 'move'
							});
						}else{
							jAlert('Please select at least one category.','Notice');
							return false;
						}
					}
				}
			}else{
						var shortcode = jQuery('[name="shortcode"]').val();
						if(shortcode!=''){
							if(key==oldkey){
								var keytitle = $('[name="tlpb['+key+'][title]"]').val();
								if(typeof keytitle == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
									var keytitle = $('[name="tlpb['+key+'][title]"]').val();
								}
								var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
								if(typeof keytitle_position == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
									var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
								}
								var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
								if(typeof keysubtitle == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
									var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
								}
								var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
								if(typeof keysubtitle_position == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
									var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
								}
								var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
								if(typeof keytitle_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
									var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
								}
								var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
								if(typeof keysubtitle_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
									var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
								}
								var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
								if(typeof keycontent_color == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
									var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
								}
								
								var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
								if(typeof keymargin_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
									var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
								}
								var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
								if(typeof keymargin_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
									var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
								}
								var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
								if(typeof keymargin_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
									var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
								}
								var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
								if(typeof keymargin_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
									var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
								}
								var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
								if(typeof keymargin_custom_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
									var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
								}
								var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
								if(typeof keymargin_custom_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
									var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
								}
								var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
								if(typeof keymargin_custom_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
									var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
								}
								var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
								if(typeof keymargin_custom_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
									var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
								}
								
								var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
								if(typeof keypadding_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
									var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
								}
								var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
								if(typeof keypadding_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
									var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
								}
								var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
								if(typeof keypadding_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
									var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
								}
								var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
								if(typeof keypadding_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
									var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
								}
								var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
								if(typeof keypadding_custom_top == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
									var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
								}
								var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
								if(typeof keypadding_custom_bottom == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
									var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
								}
								var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
								if(typeof keypadding_custom_left == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
									var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
								}
								var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
								if(typeof keypadding_custom_right == 'undefined'){
									$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
									var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
								}
								
								var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
								if(typeof title_tag == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
									var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
								}
								var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
								if(typeof subtitle_tag == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
									var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
								}
								
								var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
								if(typeof title_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
									var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
								}
								var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
								if(typeof title_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
									var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
								}
								var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
								if(typeof title_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
									var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
								}
								var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
								if(typeof title_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
									var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
								}
								var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
								if(typeof subtitle_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
									var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
								}
								var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
								if(typeof subtitle_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
									var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
								}
								var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
								if(typeof subtitle_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
									var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
								}
								var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
								if(typeof subtitle_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
									var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
								}
								var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
								if(typeof content_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
									var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
								}
								var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
								if(typeof content_animation == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
									var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
								}
								var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
								if(typeof content_animation_delay == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
									var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
								}
								var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
								if(typeof content_animation_duration == 'undefined'){
									$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
									var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
								}
								var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
								if(typeof background_color_class == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
									var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
								}
								
								var keyclass = $('[name="tlpb['+key+'][class]"]').val();
								if(typeof keyclass == 'undefined'){
									$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
									var keyclass = $('[name="tlpb['+key+'][class]"]').val();
								}
								var keyid = $('[name="tlpb['+key+'][id]"]').val();
								if(typeof keyid == 'undefined'){
									$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
									var keyid = $('[name="tlpb['+key+'][id]"]').val();
								}
								var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
								if(typeof keybgcolor == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
									var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
								}
								var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
								if(typeof keybgimage == 'undefined'){
									$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
									var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
								}
								var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
								if(typeof keyparallax == 'undefined'){
									$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
									var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
								}
								var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
								if (typeof imgattr !== typeof undefined && imgattr !== false) {
									var keybgid = imgattr;
								}else{
									var keybgid = '';
								}
								
								$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Other Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="other"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
								$('#'+key).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+shortcode+'<textarea name="tlpb['+key+'][shortcode]" id="'+key+'-shortcode" style="display:none;">'+shortcode+'</textarea></h3></div></div></div></div></div>');
							}else{
								$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Other Slider</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="row"><div class="tl-pb-row tl-pb-post-row"></div></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="other"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-row').html('<div class="tl-pb-section-content-text"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-banner" id="tl-pb-menu-item-banner" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+shortcode+'<textarea name="tlpb['+key+'][shortcode]" id="'+key+'-shortcode" style="display:none;">'+shortcode+'</textarea></h3></div></div></div></div></div>');
							}
							jQuery( document ).tooltip();
							jQuery('.tl-pb-row').sortable({
								handle: '.tl-pb-section-content-sortable',
								placeholder: 'sortable-placeholder',
								forcePlaceholderSizeType: true,
								distance: 2,
								tolerance: 'pointer',
								cursor: 'move'
							});
						}else{
							jAlert('Please enter the shortcode.','Notice');
							return false;
						}
			}
		}
		tb_remove();
	}
	/*if($('#bannertype').val()=='parallax_banner' || $('#bannertype').val()=='fixed_banner'){
		var banner_type = $('#bannertype').val();
		var layout = $('[name="select_layout"]').val();
		$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_ban_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <a href="javascript:;" class="tl-pb-gallery-add-item-link" title="Add new item"><div class="tl-pb-gallery-add-item"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="'+banner+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][layout]" id="'+key+'-layout"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html(TlPb.colGalleryHTML(1,key));
		jQuery('.tl-pb-row').sortable({
		    handle: '.tl-pb-section-content-sortable',
			placeholder: 'sortable-placeholder',
			forcePlaceholderSizeType: true,
			distance: 2,
			tolerance: 'pointer',
			cursor: 'move'
    	});	
		tb_remove();
	}
	
	if($('#bannertype').val()=='themelines_slider'){	
		if($('#slider').val()!=null){
		var slide = '';
		$('#slider').find('option:selected').each(function(){
			if(slide!=''){
				slide = slide+','+$(this).attr('rel');
			}else{
				slide = $(this).attr('rel');
			}
		});
		
		$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_ban_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content"><div class="tl-pb-row tl-pb-gal-row row"></div></div><input type="hidden" name="tl_pb_key[]" value="'+key+'"><input type="hidden" name="'+key+'-type" value="banner"><input type="hidden" name="'+key+'-banner-type" value="themelines_slider"><input type="hidden" value="" name="'+key+'-title" id="'+key+'-title"><input type="hidden" value="" name="'+key+'-class" id="'+key+'-class"><input type="hidden" value="" name="'+key+'-id" id="'+key+'-id"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html('<div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_sec_con_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"><input type="hidden" name="'+key+'-sec-class[]" id="this_sec_class" value=""><input type="hidden" name="'+key+'-sec-id[]" id="this_sec_id" value=""></i></a><a class="tl_pb_tl_slider_edit" href="javascript:;"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="tl_pb_sec_con_del_gal" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><p class="tl-default-slider-text">ThemeLines Slider ('+slide+')</p><input type="hidden" name="'+key+'-themelines-slider" value="'+$('#slider').val()+'"></div><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div></div></div>');
		jQuery('.tl-pb-row').sortable({
		    handle: '.tl-pb-section-content-sortable',
			placeholder: 'sortable-placeholder',
			forcePlaceholderSizeType: true,
			distance: 2,
			tolerance: 'pointer',
			cursor: 'move'
    	});	
		tb_remove();
		}else{
			jAlert('Please select the themelines sliders you want to add');
		}
	}
	if($('#bannertype').val()=='other'){
		if($('#slidershortcode').val()!=''){	
		$('<div id="'+key+'" class="tl-pb-section"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Banner</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_ban_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content"><div class="tl-pb-row tl-pb-gal-row row"></div></div><input type="hidden" name="tl_pb_key[]" value="'+key+'"><input type="hidden" name="'+key+'-type" value="banner"><input type="hidden" name="'+key+'-banner-type" value="other"><input type="hidden" value="" name="'+key+'-title" id="'+key+'-title"><input type="hidden" value="" name="'+key+'-class" id="'+key+'-class"><input type="hidden" value="" name="'+key+'-id" id="'+key+'-id"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html('<div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a data-title="" class="tl_pb_sec_con_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"><input type="hidden" name="'+key+'-sec-class[]" id="this_sec_class" value=""><input type="hidden" name="'+key+'-sec-id[]" id="this_sec_id" value=""></i></a><a class="tl_pb_other_edit" href="javascript:;"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="tl_pb_sec_con_del_gal" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><p class="tl-default-slider-text">'+$('#slidershortcode').val()+'</p><input type="hidden" name="'+key+'-other-slider" value="'+$('#slidershortcode').val()+'"></div><div class="tl-pb-section-content-text"><h3 class="tl-pb-section-content-text-title"></h3><div class="tl-pb-section-content-text-inner"></div></div></div>');
		jQuery('.tl-pb-row').sortable({
		    handle: '.tl-pb-section-content-sortable',
			placeholder: 'sortable-placeholder',
			forcePlaceholderSizeType: true,
			distance: 2,
			tolerance: 'pointer',
			cursor: 'move'
    	});	
		tb_remove();
		}else{
			jAlert('Shortcode can not be blank');
		}
	}
	
	if($('#bannertype').val()==''){
		jAlert('Please select the banner type');
	}
	//alert($('#slider').val());*/
	jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);
});

$(document).on('click','.tl_pb_tl_slider_edit',function(){
	var id = $(this).closest('.tl-pb-section').attr('id');
	$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
			
				'<div class="row"><div class="tl-pb-pop-col"><div class="banner_type">Select Category</div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col pull-right"><input type="hidden" name="hidden-id" value="'+id+'"><button type="button" class="button button-primary button-large" id="btn_tlslider_set">Change</button></div></div>'

			)
	);
	var tlslider = $('[name="'+id+'-themelines-slider"]').val();
	$('.banner_type').load(ajaxurl,{action:'select_slider',tlslider:tlslider},function(m){});
	tb_show("Configure Themelines Slider", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
	m7_resize_thickbox(); 
	jQuery(window).on( 'resize', m7_resize_thickbox );
		
});
$(document).on('click','#btn_tlslider_set',function(){
	if($('#slider').val()!=null){
		var slide = '';
		$('#slider').find('option:selected').each(function(){
			if(slide!=''){
				slide = slide+','+$(this).attr('rel');
			}else{
				slide = $(this).attr('rel');
			}
		});
		var id = $('[name="hidden-id"]').val();
		$('[name="'+id+'-themelines-slider"]').val($('#slider').val());
		$('#'+id).find('.tl-default-slider-text').text('ThemeLines Slider ('+slide+')');
		tb_remove();
	}else{
		jAlert('Please select the themelines sliders you want to add','Notice');
	}
});

$(document).on('click','.tl_pb_other_edit',function(){
	var id = $(this).closest('.tl-pb-section').attr('id');
	var other = $('[name="'+id+'-other-slider"]').val();
	$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
			
				'<div class="row"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="slidershortcode" name="shortcode">'+other+'</textarea></div>'+

				'<div class="row"><div class="tl-pb-pop-col pull-right"><input type="hidden" name="hidden-id" value="'+id+'"><button type="button" class="button button-primary button-large" id="btn_other_slider_set">Change</button></div></div>'

			)
	);
	tb_show("Configure Other Slider", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
	m7_resize_thickbox(); 
	jQuery(window).on( 'resize', m7_resize_thickbox );
		
});
$(document).on('click','#btn_other_slider_set',function(){
	if($('#slidershortcode').val()!=null){
		var id = $('[name="hidden-id"]').val();
		$('[name="'+id+'-other-slider"]').val($('#slidershortcode').val());
		$('#'+id).find('.tl-default-slider-text').text($('#slidershortcode').val());
		tb_remove();
	}else{
		jAlert('Please add the shortcode of the slider.','Notice');
	}
});

$(document).on('click','.tl_pb_sec_con_edit',function(e){
		
		$('#tlcontent-tmce').trigger('click');
		tinyMCE.get('tlcontent').setContent('');
		$('.modalDialog').css({"opacity":"1","pointer-events":"auto"});
		var content = $(this).closest('.tl-pb-col').find('.tl-pb-section-content-text-inner').html();

		$('.add-content-now').removeClass('add-content-now');
		tinyMCE.get('tlcontent').setContent(content);

		var data_content = tl_pb_genarate();

		$(this).closest('.tl-pb-col').addClass('row_'+data_content);
		
		$('#section_detect').val(data_content);

		/*$('#data-area').html('<input type="hidden" id="data-content" value="'+data_content+'">');*/

		/*tb_show("Content", "#TB_inline?height=502&width=400&inlineId=tl-pb-editor");
		
		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );
		tinymce.execCommand('mceAddControl',true,'tlcontent');*/
		/*$('#tl-pb-editor').show();*/
		

});
$(document).on('click','.close',function(){
	$('.modalDialog').css({"opacity":"0","pointer-events":"none"});
});
$(document).on('click','.close-developer',function(){
	$('.modalDialog-developer').removeAttr('style');
	$('.developer_div').hide();
	$('.developer-button-area').html('');
	$('[name="tlpb[123tlpbkey123][content]"]').val('');
	$('[name="tlpb[123tlpbkey123][content]"]').html('');
});


$(document).on('click','#btn_tl_pb_content_save',function(){
	
		$('#tlcontent-tmce').trigger('click');
		var visual_content = tinyMCE.get('tlcontent').getContent();

		var text_content = $('#tlcontent').val();
		
		if(text_content.length>=visual_content.length){
			var content = text_content;
		}else{
			var content = visual_content;
		}

		var data = $('#section_detect').val();
		$('#section_detect').val('');

		/*$( ".tl_pb_sec_con_edit" ).each(function() {

			if($(this).attr('data-content')==data){

				$(this).parent().parent().parent().addClass('add-content-now');

			}

		});*/
		
		$('.row_'+data).find('.tl-pb-section-content-text-inner').html(content);
		$('.row_'+data).find('.admin-slider-image-text').html(content);
		$('.row_'+data).find('#this-content').html(content);
		$('.row_'+data).removeClass('row_'+data);
		/*
		$('.add-content-now .tl-pb-section-content-text #this-content').html(content);
		$('.add-content-now .tl-pb-section-content-text .tl-pb-section-content-text-inner').html(content);

		$('.add-content-now').removeClass('add-content-now');*/

		//$('#tl-pb-editor').hide();   
		/*tb_remove();
		tinyMCE.execCommand('mceRemoveControl', true, 'tlcontent');*/

		//alert(thisone);

		//$('#tlcontent').val('');
		tinyMCE.get('tlcontent').setContent('');
		$('.modalDialog').css({"opacity":"0","pointer-events":"none"});

		//$(this).closest('.tl-pb-section-content-text').find('.tl-pb-section-content-text-inner').html(content);

	});

$(document).on('click','.close',function(e){
	$('#tlcontent-tmce').trigger('click');
});
/*$(document).on('click','.tl_pb_sec_con_widget',function(e){
	var data_content = tl_pb_genarate();
	$(this).closest('.tl-pb-col').addClass('row_'+data_content);
	$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><label>Select Widget</label><div class="widget"></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col pull-right"><input type="hidden" name="hidden-id" value="'+data_content+'"><button type="button" class="button button-primary button-large" id="btn_widget_set">Add Widget</button> <button type="button" class="button button-danger" id="btn_widget_unset">Remove Widget</button></div></div>'

			)
	);
	$('.widget').load(ajaxurl,{action:'fetch_widget'});
	tb_show("Configure Widget", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
	m7_resize_thickbox(); 
	jQuery(window).on( 'resize', m7_resize_thickbox );
});

$(document).on('click','#btn_widget_set',function(){
	var data = $(this).closest('.tl-pb-sec-sett-pop').find('[name="hidden-id"]').val();
	var sidebar = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_sidebar"]').val();
	
	if(sidebar){
		$('.row_'+data).find('.tl-pb-section-content-text-inner').html(sidebar);
		$('.row_'+data).find('#this-content').html(sidebar);
		$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_widget');
		$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
		$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
		$('.row_'+data).find('.fa-times-circle').trigger('click');
		$('.row_'+data).removeClass('row_'+data);
		tb_remove();
	}else{
		jAlert('Please select widget area.','Notice');
	}
	
	
});

$(document).on('click','#btn_widget_unset',function(){
	var data = $(this).closest('.tl-pb-sec-sett-pop').find('[name="hidden-id"]').val();
	var $row = $('.row_'+data);
	jConfirm('Are you sure you want to remove widget section?', 'Confirmation', function(r){
		if(r==true){
			$row.find('.tl-pb-section-content-text-inner').html('');
			$row.find('#this-content').html('');
			$row.find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_widget').removeClass('tl_pb_sec_con_widget').addClass('tl_pb_sec_con_edit');
			$row.find('.tl_pb_sec_con_edit').css({'pointer-events':'auto','opacity':'1'});
			$row.find('.tl-pb-section-content-background').css({'pointer-events':'auto','opacity':'1'});
			$row.removeClass('row_'+data);
			tb_remove();
		}
	});
});*/

$(document).on('click','.tl_pb_sec_con_module',function(e){
	var data_content = tl_pb_genarate();
	$(this).closest('.tl-pb-col').addClass('row_'+data_content);
	if($(this).hasClass('tl_pb_sec_con_edit_module')){
		var module = $(this).attr('data-module');
		if(module=="Widget"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var id_start = str.indexOf('id="') + 4;
			var id_end = str.indexOf('"',id_start);
			var id = str.substring(id_start,id_end);
			
			var sidebar_start = str.indexOf('sidebar="') + 9;
			var sidebar_end = str.indexOf('"',sidebar_start);
			var sidebar = str.substring(sidebar_start,sidebar_end);
			
			$('.widget').load(ajaxurl,{action:'fetch_widget'});
			var widget_html = $('.widget').html();
			$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><label>Select Widget</label>'+widget_html+'</div>'+
				'<div class="row"><input type="checkbox" name="use_sidebar" value="1"> Use as Sidebar</div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
			)
		);
		$('[name="select_sidebar"]').val(id).change();
		if(sidebar==1){
			$('[name="use_sidebar"]').prop('checked',true);
		}else{
			$('[name="use_sidebar"]').prop('checked',false);
		}
		
		tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		}
		if(module=="Block"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var layout_start = str.indexOf('layout="') + 8;
			var layout_end = str.indexOf('"',layout_start);
			var layout = str.substring(layout_start,layout_end);
			
			var icon_start = str.indexOf('icon="') + 6;
			var icon_end = str.indexOf('"',icon_start);
			var icon = str.substring(icon_start,icon_end);
			
			var title_start = str.indexOf('title="') + 7;
			var title_end = str.indexOf('"',title_start);
			var title = str.substring(title_start,title_end);
			
			var content_start = str.indexOf('"]') + 2;
			var content_end = str.indexOf('[/tl_block]',content_start);
			var content = str.substring(content_start,content_end);
			
			var button_start = str.indexOf('button="') + 8;
			var button_end = str.indexOf('"',button_start);
			var button = str.substring(button_start,button_end);
			
			if(button=='yes'){
				var button_text_start = str.indexOf('button_text="') + 13;
				var button_text_end = str.indexOf('"',button_text_start);
				var button_text = str.substring(button_text_start,button_text_end);
				
				var button_url_start = str.indexOf('button_url="') + 12;
				var button_url_end = str.indexOf('"',button_url_start);
				var button_url = str.substring(button_url_start,button_url_end);
			}else{
				var button_text = '';
				var button_url = '';
			}
			
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					'<div class="row"><label>Select Layout</label><select name="block_layout" class="form-control"><option value="layout-1">Layout-1</option><option value="layout-2">Layout-2</option><option value="layout-3">Layout-3</option><option value="layout-4">Layout-4</option></select></div>'+
					'<div class="row"><label>Icon Class</label><input name="block_icon" type="text" class="form-control" placeholder="eg. fa fa-wordpress" value="'+icon+'"></div>'+
					'<div class="row"><label>Title</label><input name="block_title" type="text" class="form-control" placeholder="Block Title" value="'+title+'"></div>'+
					'<div class="row"><label>Content</label><textarea name="block_content" class="form-control" placeholder="Block Content">'+content+'</textarea></div>'+
					'<div class="row"><input type="checkbox" name="block_button" value="1"> Show Button</div>'+
					'<div class="block_button_area" style="display:none;"><div class="row"><label>Button Text</label><input class="form-control" type="text" name="block_button_text" placeholder="Button Text" value="'+button_text+'"></div>'+
					'<div class="row"><label>Button URL</label><input class="form-control" type="text" name="block_button_url" placeholder="Button URL" value="'+button_url+'"></div></div>'+
					'<input type="hidden" name="data_content" value="'+data_content+'">'+
					'<input type="hidden" name="data_module" value="'+module+'">'+
					'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			$('[name="block_button"]').click(function(){
				if ($(this).is(':checked')) {
					$('.block_button_area').show();
				}else{
					$('.block_button_area').hide();
				}
			});
			
			$('[name="block_layout"]').val(layout).change();
			if(button=='yes'){
				$('[name="block_button"]').trigger('click');
			}
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		}
		
		if(module=="Call To Action"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var button_text_start = str.indexOf('button_text="') + 13;
			var button_text_end = str.indexOf('"',button_text_start);
			var button_text = str.substring(button_text_start,button_text_end);
			
			var button_url_start = str.indexOf('button_url="') + 12;
			var button_url_end = str.indexOf('"',button_url_start);
			var button_url = str.substring(button_url_start,button_url_end);
			
			var button_icon_start = str.indexOf('button_icon="') + 13;
			var button_icon_end = str.indexOf('"',button_icon_start);
			var button_icon = str.substring(button_icon_start,button_icon_end);
			
			var position_start = str.indexOf('position="') + 10;
			var position_end = str.indexOf('"',position_start);
			var position = str.substring(position_start,position_end);
			
			var background_color_start = str.indexOf('background_color="') + 18;
			var background_color_end = str.indexOf('"',background_color_start);
			var background_color = str.substring(background_color_start,background_color_end);
			
			var background_image_start = str.indexOf('background_image="') + 18;
			var background_image_end = str.indexOf('"',background_image_start);
			var background_image = str.substring(background_image_start,background_image_end);
			
			if(background_image){
				var bgimg = '<div data-bgurl="'+background_image+'" data-attachid="39" data-title="Set Image" class="tl-pb-section-post-background" style="background-image: url(&quot;'+background_image+'&quot;);"><i class="fa fa-times-circle" aria-hidden="true"></i></div>';
			}else{
				var bgimg = '<div data-bgUrl="" data-attachId="" data-title="Set Image" class="tl-pb-section-post-background"><i class="fa fa-picture-o" aria-hidden="true"></i></div>';
			}
			
			var content_start = str.indexOf('"]') + 2;
			var content_end = str.indexOf('[/tl_cta]',content_start);
			var content = str.substring(content_start,content_end);
			
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					'<div class="row"><div class="tl-pb-pop-col"><label>Text</label><textarea name="call_to_action_text" class="form-control">'+content+'</textarea></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button Text</label><input type="text" name="call_to_action_button_text" class="form-control" value="'+button_text+'"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button URL</label><input type="text" name="call_to_action_button_url" class="form-control" value="'+button_url+'"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button Icon Class</label><input type="text" name="call_to_action_button_icon" class="form-control" value="'+button_icon+'"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Position</label><select name="call_to_action_position" class="form-control"><option value="left">Text Left - Button Right</option><option value="right">Text Right - Button Left</option><option value="above">Text Above - Button Below</option><option value="below">Text Below - Button Above</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Color</label> <input type="text" name="call_to_action_bgcolor" class="my-color-picker" value="'+background_color+'"/> </div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Image</label>'+bgimg+'</div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			jQuery(document).ready(function(e) {
				jQuery('.my-color-picker').wpColorPicker();
			});
			$('[name="call_to_action_position"]').val(position).change();
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		}
		
		if(module=="Contact"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var address_start 	= str.indexOf('address="') + 9;
			var address_end 	= str.indexOf('"',address_start);
			var address 		= str.substring(address_start,address_end);
			
			var map_start 		= str.indexOf('showmap="') + 9;
			var map_end 		= str.indexOf('"',map_start);
			var map 			= str.substring(map_start,map_end);
			if(map=='1')map_str	=	'checked=checked';else map_str	=	'';
			var email_start 	= str.indexOf('email="') + 7;
			var email_end 		= str.indexOf('"',email_start);
			var email 			= str.substring(email_start,email_end);
			
			var phone_start 	= str.indexOf('phone="') + 7;
			var phone_end 		= str.indexOf('"',phone_start);
			var phone 			= str.substring(phone_start,phone_end);
			
			var contact_start 	= str.indexOf('"]') + 2;
			var contact_end 		= str.indexOf('[/tl_cont]',contact_start);
			var contact 			= str.substring(contact_start,contact_end);
			
			/*var content_start = str.indexOf('"]') + 2;
			var content_end = str.indexOf('[/tl_cta]',content_start);
			var content = str.substring(content_start,content_end);*/
			var contact_html = $('.con-form-area').html();
			$('.con-form-area').load(ajaxurl,{action:'contact_form_list'},function(m){});
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					'<div class="row"><div class="tl-pb-pop-col"><label>Address</label><input type="text" name="contact_address" class="form-control" value="'+address+'"></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Show Map</label><input type="checkbox" '+map_str+' value="1" class="form-control" name="show_map"></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Email</label><input type="text" name="contact_email" class="form-control" value="'+email+'"></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Phone</label><input type="text" name="contact_phone" class="form-control" value="'+phone+'"></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Contact Forms</label>'+contact_html+
					'<input type="hidden" name="data_content" value="'+data_content+'">'+
					'<input type="hidden" name="data_module" value="'+module+'">'+
					'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			$('[name="contact_form"]').val(contact).change();
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		}
		if(module=="Post Types"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var post_type_start 	= str.indexOf('post_type="') + 11;
			var post_type_end 	= str.indexOf('"',post_type_start);
			var post_type 		= str.substring(post_type_start,post_type_end);
			
			var post_order_start = str.indexOf('post_order="') + 12;
			var post_order_end 	 = str.indexOf('"',post_order_start);
			var post_order 		 = str.substring(post_order_start,post_order_end);
			
			var post_element_start 	= str.indexOf('post_element="') + 14;
			var post_element_end 	= str.indexOf('"',post_element_start);
			var post_element		= str.substring(post_element_start,post_element_end);
			
			var animation_start 	= str.indexOf('animation="') + 11;
			var animation_end 	= str.indexOf('"',animation_start);
			var animation		= str.substring(animation_start,animation_end);
			
			var animation_delay_start 	= str.indexOf('animation_delay="') + 17;
			var animation_delay_end 	= str.indexOf('"',animation_delay_start);
			var animation_delay		= str.substring(animation_delay_start,animation_delay_end);
			
			var animation_duration_start 	= str.indexOf('animation_duration="') + 20;
			var animation_duration_end 	= str.indexOf('"',animation_duration_start);
			var animation_duration		= str.substring(animation_duration_start,animation_duration_end);
			
			if(post_type=='post' || post_type=='page'){
				var post_from_start 	= str.indexOf('post_from="') + 11;
				var post_from_end 	= str.indexOf('"',post_from_start);
				var post_from 		= str.substring(post_from_start,post_from_end);
				
				var page_from_start 	= str.indexOf('page_from="') + 11;
				var page_from_end 	= str.indexOf('"',page_from_start);
				var page_from 		= str.substring(page_from_start,page_from_end);
				
				var posts_per_page_start 	= str.indexOf('posts_per_page="') + 16;
				var posts_per_page_end 	= str.indexOf('"',posts_per_page_start);
				var posts_per_page 		= str.substring(posts_per_page_start,posts_per_page_end);
				
				var offset_start 	= str.indexOf('offset="') + 8;
				var offset_end 	= str.indexOf('"',offset_start);
				var offset 		= str.substring(offset_start,offset_end);
				
				var pagination_start 	= str.indexOf('pagination="') + 12;
				var pagination_end 	= str.indexOf('"',pagination_start);
				var pagination 		= str.substring(pagination_start,pagination_end);
				
				var column_start 	= str.indexOf('column="') + 8;
				var column_end 	= str.indexOf('"',column_start);
				var column 		= str.substring(column_start,column_end);
				
				var feature_start 	= str.indexOf('feature="') + 9;
				var feature_end 	= str.indexOf('"',feature_start);
				var feature 		= str.substring(feature_start,feature_end);
				
				var post_excerpt_start 	= str.indexOf('post_excerpt="') + 14;
				var post_excerpt_end 	= str.indexOf('"',post_excerpt_start);
				var post_excerpt		= str.substring(post_excerpt_start,post_excerpt_end);
				
				var post_length_start 	= str.indexOf('post_length="') + 13;
				var post_length_end 	= str.indexOf('"',post_length_start);
				var post_length		= str.substring(post_length_start,post_length_end);
				
				var style = false;
				if(str.search('tlpb_post_column')!=-1){
					style = 'column';
				}
				if(str.search('tlpb_post_horizontal')!=-1){
					style = 'horizontal';
				}
				if(str.search('tlpb_post_masonry')!=-1){
					style = 'masonry';
				}
			}
			
			if(post_type=='team'){
				var posts_per_page_start 	= str.indexOf('post_number="') + 13;
				var posts_per_page_end 	= str.indexOf('"',posts_per_page_start);
				var posts_per_page 		= str.substring(posts_per_page_start,posts_per_page_end);
				
				var offset_start 	= str.indexOf('post_offset="') + 13;
				var offset_end 	= str.indexOf('"',offset_start);
				var offset 		= str.substring(offset_start,offset_end);
				
				var column_start 	= str.indexOf('post_column="') + 13;
				var column_end 	= str.indexOf('"',column_start);
				var column 		= str.substring(column_start,column_end);
				
				var style = false;
				if(str.search('tlpb_team_slider')!=-1){
					style = 'slder';
				}
				if(str.search('tlpb_team_column')!=-1){
					style = 'column';
				}
			}
			
			if(post_type=='testimonial'){
				var posts_per_page_start 	= str.indexOf('post_number="') + 13;
				var posts_per_page_end 	= str.indexOf('"',posts_per_page_start);
				var posts_per_page 		= str.substring(posts_per_page_start,posts_per_page_end);
				
				var offset_start 	= str.indexOf('post_offset="') + 13;
				var offset_end 	= str.indexOf('"',offset_start);
				var offset 		= str.substring(offset_start,offset_end);
				
				var column_start 	= str.indexOf('post_column="') + 13;
				var column_end 	= str.indexOf('"',column_start);
				var column 		= str.substring(column_start,column_end);
				
				var style = false;
				if(str.search('tlpb_testimonial_normal')!=-1){
					style = 'normal';
				}
				if(str.search('tlpb_testimonial_chat')!=-1){
					style = 'chat';
				}
			}
			$('.post_div').load(ajaxurl,{action:'create_pb_post_html',key:'123tlpbkey123'},function(m){duplicate_post_value();post_type_action();});
			var post = $('.post_div').html();
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					post.replace( new RegExp("123tlpbkey123", 'g'), data_content )+
					'<input type="hidden" name="data_content" value="'+data_content+'">'+
					'<input type="hidden" name="data_module" value="'+module+'">'+
					'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			post_type_action();
			duplicate_post_value();
			$('[name="tlpb['+data_content+'][post_type]"]').val(post_type).change();
			$('[name="tlpb['+data_content+'][post_order]"]').val(post_order).change();
			$('[name="tlpb['+data_content+'][post_number]"]').val(posts_per_page).change();
			$('[name="tlpb['+data_content+'][post_offset]"]').val(offset).change();
			$('[name="tlpb['+data_content+'][post_column]"]').val(column).change();
			$('[name="tlpb['+data_content+'][post_element][]"]').each(function(index, element) {
				if(post_element.search($(this).val())!=-1){
					$(this).prop('checked',true);
				}else{
					$(this).prop('checked',false);
				}
			});
			if(post_type=='post' || post_type=='page'){
				$('[name="tlpb['+data_content+'][post_from]"]').val(post_from).change();
				$('[name="tlpb['+data_content+'][page_from]"]').val(page_from).change();
				(pagination=='on')?$('[name="tlpb['+data_content+'][post_pagination]"]').prop('checked', true):$('[name="tlpb['+data_content+'][post_pagination]"]').prop('checked', false);
				(feature=='on')?$('[name="tlpb['+data_content+'][post_feature]"]').prop('checked', true):$('[name="tlpb['+data_content+'][post_feature]"]').prop('checked', false);
				(post_excerpt=='on')?$('[name="tlpb['+data_content+'][post_excerpt]"]').prop('checked', true):$('[name="tlpb['+data_content+'][post_excerpt]"]').prop('checked', false);
				$('[name="tlpb['+data_content+'][post_length]"]').val(post_length).change();
				style?$('[name="tlpb['+data_content+'][post_style]"]').val(style).change():'';
			}
			if(post_type=='team'){
				style?$('[name="tlpb['+data_content+'][team_style]"]').val(style).change():'';
			}
			if(post_type=='testimonial'){
				style?$('[name="tlpb['+data_content+'][testimonial_style]"]').val(style).change():'';
			}
			$('[name="tlpb['+data_content+'][animation]"]').val(animation).change();
			$('[name="tlpb['+data_content+'][animation_delay]"]').val(animation_delay).change();
			$('[name="tlpb['+data_content+'][animation_duration]"]').val(animation_duration).change();
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
		}
		
		if(module=="Banner or Slider"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			$('.post_slider_div').load(ajaxurl,{action:'select_post_slider'},function(m){});
			$('.single_slider_div').load(ajaxurl,{action:'select_single_slider'},function(m){});
			$('.slider_div').load(ajaxurl,{action:'select_slider'},function(m){});
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					'<div class="row"><div class="tl-pb-pop-col"><label>Select Type</label><select class="form-control" name="banner_type" id="bannertype"><option value="">--Select Banner--</option><option value="banner">Banner</option><option value="slider">Slider</option><option value="others">Others</option></select></div></div>'+
					'<div class="banner_config"></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><div class="banner_type"></div></div></div>'+
					'<input type="hidden" name="data_content" value="'+data_content+'">'+
					'<input type="hidden" name="data_module" value="'+module+'">'+
					'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			
			$( "#bannertype" ).change(function() {
			  if($(this).val()=='banner'){
				  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="parallax" value="1"> Use as Parallax</div></div>');
				  $('.banner_config').append('<input type="hidden" name="select_banner" id="select_banner" value="themelines_banner">');
				  $('.banner_config').append('<input type="hidden" name="select_layout" id="layout" value="full_width">');
				  $('.banner_type').html($('.single_slider_div').html());
			  }else{
				  if($(this).val()=='slider'){
					  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Select Slider</label><select class="form-control" name="select_slider_criteria" id="select_slider"><option value="post_slider">Post Slider</option><option value="themelines_slider">Themelines Slider</option></select></div></div>');
					  $('.banner_config').append('<input type="hidden" name="select_layout" id="layout" value="full_width">');
					  //$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
					  $('.banner_type').html($('.post_slider_div').html());
				  }else{
					  if($(this).val()=='others'){
						  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="slider_shortcode" name="shortcode"></textarea></div></div>');
						  $('.banner_type').html('');
					  }else{
						  if($(this).val()==''){
								$('.banner_config').html('');
								$('.banner_type').html('');
						  }
					  }
				  }
			  }
			  $( '[name="select_slider_criteria"]' ).change(function() {
				if($(this).val()=='themelines_slider'){
					$('.banner_type').html($('.slider_div').html());
					//$('.banner_type').load(ajaxurl,{action:'select_slider'},function(m){});
				}else{
					if($(this).val()=='post_slider'){
						$('.banner_type').html($('.post_slider_div').html());
						//$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
					}else{
						$('.banner_type').html('');
					}
				}
			  });
			});
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
			if(str.search('tlpb_themelines_banner')!=-1){
				var banner_parallax_start = str.indexOf('banner_parallax="') + 17;
				var banner_parallax_end   = str.indexOf('"',banner_parallax_start);
				var banner_parallax 	  = str.substring(banner_parallax_start,banner_parallax_end);
				
				var slider_start = str.indexOf('slider="') + 8;
				var slider_end   = str.indexOf('"',slider_start);
				var slider 	  = str.substring(slider_start,slider_end);
				
				$('[name="banner_type"]').val('banner').change();
				if(banner_parallax==1){
					$('[name="parallax"]').prop('checked',true);
				}else{
					$('[name="parallax"]').prop('checked',false);
				}
				$('[name="select_banner_slide"]').val(slider).change();
			}
			if(str.search('tlpb_post_slider')!=-1){
				var category_start = str.indexOf('category="') + 10;
				var category_end   = str.indexOf('"',category_start);
				var category 	  = str.substring(category_start,category_end);
				
				$('[name="banner_type"]').val('slider').change();
				$('[name="select_slider_criteria"]').val('post_slider').change();
				var valArr = category.split(',');
				size = valArr.length;
				for (i = 0; i < size; i++) {
					$("#slider option[value='" + valArr[i] + "']").attr("selected", 1);
				}
			}
			if(str.search('tlpb_themelines_slider')!=-1){
				var category_start = str.indexOf('category="') + 10;
				var category_end   = str.indexOf('"',category_start);
				var category 	  = str.substring(category_start,category_end);
				
				$('[name="banner_type"]').val('slider').change();
				$('[name="select_slider_criteria"]').val('themelines_slider').change();
				var valArr = category.split(',');
				size = valArr.length;
				for (i = 0; i < size; i++) {
					$("#slider option[value='" + valArr[i] + "']").attr("selected", 1);
				}
			}
			if(str.search('tlpb_other')!=-1){
				var shortcode_start = str.indexOf('[tlpb_other]') + 12;
				var shortcode_end 	= str.indexOf('[/tlpb_other]',shortcode_start);
				var shortcode 		= str.substring(shortcode_start,shortcode_end);
				
				$('[name="banner_type"]').val('others').change();
				$('[name="shortcode"]').val(shortcode);
			}
		}
		if(module=="Gallery"){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			$('.gallery_category_div').load(ajaxurl,{action:'select_gallery_category'},function(m){});
			$('#tl-pb-popup-common').html(
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
					'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Type</label><select class="form-control" name="gallery_type" id="gallerytype"><option value="">--Select Gallery--</option><!--<option value="custom_gallery">Custom Gallery</option>--><option value="themelines_gallery">Themelines Gallery</option><option value="others">Others</option></select></div></div>'+
					'<div class="gallery_config"></div>'+
					'<div class="themelines_gal_config" style="display:none;"><div class="row"><div class="tl-pb-pop-col"><label>Items (-1 for all items)</label><input type="number" name="tl_items" id="tl_items" value="-1" class="form-control"></div></div></div>'+
					'<div class="gallery_all" style="display:none;">'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Grid</label><select class="form-control" name="gallery_grid" id="gallerygrid"><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Style</label><select class="form-control" name="gallery_style" id="gallerystyle"><option value="normal">Normal</option><option value="no_padding">No Padding</option><!--<option value="masonry">Masonry</option>--></select></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="filter" value="1">Show Filtering</div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><label>On Click</label><select class="form-control" name="gallery_onclick" id="galleryonclick"><option value="do_nothing">Do Nothing</option><option value="fancybox">Open In Fancybox</option><option value="new_page">Open In New Page</option></select></div></div>'+
					'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="load" value="1">Show Load More</div></div>'+
					'</div>'+
					'<input type="hidden" name="data_content" value="'+data_content+'">'+
					'<input type="hidden" name="data_module" value="'+module+'">'+
					'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button></div></div>'
				)
			);
			
			$( "#gallerytype" ).change(function() {
			  if($(this).val()=='themelines_gallery'){
				  $('.gallery_config').html($('.gallery_category_div').html());
				  //$('.gallery_config').load(ajaxurl,{action:'select_gallery_category'},function(m){});
				  $('.gallery_all').show();
				  $('.themelines_gal_config').show();
			  }else{
				  if($(this).val()=='others'){
					$('.gallery_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="gallery_shortcode" name="gallery_shortcode"></textarea></div></div>');
					$('.gallery_all').hide();
					$('.themelines_gal_config').hide();
				  }else{
					  $('.gallery_config').html('');
					  $('.gallery_all').hide();
					  $('.themelines_gal_config').hide();
				  }
			  }
			});
			tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
			if(str.search('tlpb_normal_gallery')!=-1 || str.search('tlpb_masonry_gallery')!=-1 || str.search('tlpb_no_padding_gallery')!=-1){
				var items_start = str.indexOf('items="') + 7;
				var items_end 	= str.indexOf('"',items_start);
				var items 		= str.substring(items_start,items_end);
				
				var categories_start = str.indexOf('categories="') + 12;
				var categories_end 	= str.indexOf('"',categories_start);
				var categories 		= str.substring(categories_start,categories_end);
				
				var filter_start = str.indexOf('filter="') + 8;
				var filter_end 	= str.indexOf('"',filter_start);
				var filter 		= str.substring(filter_start,filter_end);
				
				var gal_grid_start = str.indexOf('gal_grid="') + 10;
				var gal_grid_end 	= str.indexOf('"',gal_grid_start);
				var gal_grid 		= str.substring(gal_grid_start,gal_grid_end);
				
				var onclick_start = str.indexOf('onclick="') + 9;
				var onclick_end 	= str.indexOf('"',onclick_start);
				var onclick 		= str.substring(onclick_start,onclick_end);
				if(str.search('tlpb_normal_gallery')!=-1){
					var style = "normal";
				}
				if(str.search('tlpb_masonry_gallery')!=-1){
					var style = "masonry";
				}
				if(str.search('tlpb_no_padding_gallery')!=-1){
					var style = "no_padding";
				}
				
				$('[name="gallery_type"]').val('themelines_gallery').change();
				var valArr = categories.split(',');
				size = valArr.length;
				for (i = 0; i < size; i++) {
					$("#gallery_category option[value='" + valArr[i] + "']").attr("selected", 1);
				}
				$('[name="tl_items"]').val(items);
				$('[name="gallery_grid"]').val(gal_grid).change();
				$('[name="gallery_style"]').val(style).change();
				if(filter==1){
					$('[name="filter"]').prop('checked',true);
				}else{
					$('[name="filter"]').prop('checked',false);
				}
				$('[name="gallery_onclick"]').val(onclick).change();
			}
			if(str.search('tlpb_other')!=-1){
				var shortcode_start = str.indexOf('[tlpb_other]') + 12;
				var shortcode_end 	= str.indexOf('[/tlpb_other]',shortcode_start);
				var shortcode 		= str.substring(shortcode_start,shortcode_end);
				
				$('[name="gallery_type"]').val('others').change();
				$('[name="shortcode"]').val(shortcode);
			}
		}
		if(module=='Developer Mode'){
			var str = $(this).closest('.tl-pb-col').find('#this-content').html();
			
			var developer_shortcode_start = str.indexOf('[tlpb_developer]') + 16;
			var developer_shortcode_end 	= str.indexOf('[/tlpb_developer]',developer_shortcode_start);
			var developer_shortcode 		= str.substring(developer_shortcode_start,developer_shortcode_end);
			$('[name="tlpb[123tlpbkey123][content]').val(developer_shortcode.replace('&lt;?','<?').replace('?&gt;', '?>').replace('&lt;','<').replace('&gt;', '>'));
			
			$('.modalDialog-developer').css({'pointer-events':'auto','opacity':'1'});
			$('.developer_div').show();
			$('.developer-button-area').html('<button type="button" class="button button-primary" id="btn_module_set">Change Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><button type="button" class="button button-danger" id="btn_module_unset">Remove Module</button><input type="hidden" name="data_content" value="'+data_content+'"><input type="hidden" name="data_module" value="'+module+'">');
		}
		m7_resize_thickbox(); 	
		jQuery(window).on( 'resize', m7_resize_thickbox );
	}else{
		$('#tl-pb-popup-common').html(
	
				$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(TlPb.moduleElements(data_content))
		);
		tb_show("Configure Module", "#TB_inline?height=302&width=400&inlineId=tl-pb-popup-common");
		module_resize_thickbox(); 
		jQuery(window).on( 'resize', module_resize_thickbox );
	}
});

$(document).on('click','.module-items',function(e){
	$("#TB_window").remove();
	$("body").append("<div id='TB_window'></div>");
	var module = $(this).data('target');
	var data_content = $(this).data('id');
	if(module=="Widget"){
		$('.widget').load(ajaxurl,{action:'fetch_widget'});
		var widget_html = $('.widget').html();
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><label>Select Widget</label>'+widget_html+'</div>'+
				'<div class="row"><input type="checkbox" name="use_sidebar" value="1"> Use as Sidebar</div>'+
'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div>'
			)
		);
	}
	if(module=="Block"){
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><label>Select Layout</label><select name="block_layout" class="form-control"><option value="layout-1">Layout-1</option><option value="layout-2">Layout-2</option><option value="layout-3">Layout-3</option><option value="layout-4">Layout-4</option></select></div>'+
				'<div class="row"><label>Icon Class</label><input name="block_icon" type="text" class="form-control" placeholder="eg. fa fa-wordpress"></div>'+
				'<div class="row"><label>Title</label><input name="block_title" type="text" class="form-control" placeholder="Block Title"></div>'+
				'<div class="row"><label>Content</label><textarea name="block_content" class="form-control" placeholder="Block Content"></textarea></div>'+
				'<div class="row"><input type="checkbox" name="block_button" value="1"> Show Button</div>'+
				'<div class="block_button_area" style="display:none;"><div class="row"><label>Button Text</label><input class="form-control" type="text" name="block_button_text" placeholder="Button Text"></div>'+
				'<div class="row"><label>Button URL</label><input class="form-control" type="text" name="block_button_url" placeholder="Button URL"></div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div>'
			)
		);
		$('[name="block_button"]').click(function(){
			if ($(this).is(':checked')) {
				$('.block_button_area').show();
			}else{
				$('.block_button_area').hide();
			}
		});
		
		
	}
	if(module=="Call To Action"){
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Text</label><textarea name="call_to_action_text" class="form-control"></textarea></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button Text</label><input type="text" name="call_to_action_button_text" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button URL</label><input type="text" name="call_to_action_button_url" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Button Icon Class</label><input type="text" name="call_to_action_button_icon" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Position</label><select name="call_to_action_position" class="form-control"><option value="left">Text Left - Button Right</option><option value="right">Text Right - Button Left</option><option value="above">Text Above - Button Below</option><option value="below">Text Below - Button Above</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Color</label> <input type="text" name="call_to_action_bgcolor" class="my-color-picker"/> </div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Image</label> <div data-bgUrl="" data-attachId="" data-title="Set Image" class="tl-pb-section-post-background"><i class="fa fa-picture-o" aria-hidden="true"></i></div> </div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);
		jQuery(document).ready(function(e) {
			jQuery('.my-color-picker').wpColorPicker();
		});
	}
	if(module=="Contact"){
		var contact_html = $('.con-form-area').html();
		
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Address</label><input type="text" name="contact_address" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Show Map</label><input type="checkbox" name="show_map" class="form-control" value="1"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Email</label><input type="text" name="contact_email" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Phone</label><input type="text" name="contact_phone" class="form-control"></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Contact Form</label>'+contact_html+'</div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);
		$('.con-form-area').load(ajaxurl,{action:'contact_form_list'},function(m){});
	}
	if(module=="Post Types"){
		$('.post_div').load(ajaxurl,{action:'create_pb_post_html',key:'123tlpbkey123'},function(m){duplicate_post_value();
post_type_action();});
		var post = $('.post_div').html();
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				post.replace( new RegExp("123tlpbkey123", 'g'), data_content )+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);
		post_type_action();
		duplicate_post_value();
	}
	if(module=="Banner or Slider"){
		$('.post_slider_div').load(ajaxurl,{action:'select_post_slider'},function(m){});
		$('.single_slider_div').load(ajaxurl,{action:'select_single_slider'},function(m){});
		$('.slider_div').load(ajaxurl,{action:'select_slider'},function(m){});
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Type</label><select class="form-control" name="banner_type" id="bannertype"><option value="">--Select Banner--</option><option value="banner">Banner</option><option value="slider">Slider</option><option value="others">Others</option></select></div></div>'+
				'<div class="banner_config"></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="banner_type"></div></div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);
		
		$( "#bannertype" ).change(function() {
		  if($(this).val()=='banner'){
			  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="parallax" value="1"> Use as Parallax</div></div>');
			  $('.banner_config').append('<input type="hidden" name="select_banner" id="select_banner" value="themelines_banner">');
			  $('.banner_config').append('<input type="hidden" name="select_layout" id="layout" value="full_width">');
			  $('.banner_type').html($('.single_slider_div').html());
		  }else{
			  if($(this).val()=='slider'){
				  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Select Slider</label><select class="form-control" name="select_slider_criteria" id="select_slider"><option value="post_slider">Post Slider</option><option value="themelines_slider">Themelines Slider</option></select></div></div>');
				  $('.banner_config').append('<input type="hidden" name="select_layout" id="layout" value="full_width">');
				  //$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
				  $('.banner_type').html($('.post_slider_div').html());
			  }else{
				  if($(this).val()=='others'){
					  $('.banner_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="slider_shortcode" name="shortcode"></textarea></div></div>');
					  $('.banner_type').html('');
				  }else{
					  if($(this).val()==''){
							$('.banner_config').html('');
					  		$('.banner_type').html('');
					  }
				  }
			  }
		  }
		  /*$( '[name="select_banner"]' ).change(function() {
			if($(this).val()=='themelines_banner'){
				   $('.banner_type').html($('.single_slider_div').html());
				 //$('.banner_type').load(ajaxurl,{action:'select_single_slider'},function(m){});
			}else{
				$('.banner_type').html('');
			}
		  });*/
		  $( '[name="select_slider_criteria"]' ).change(function() {
			if($(this).val()=='themelines_slider'){
				$('.banner_type').html($('.slider_div').html());
			 	//$('.banner_type').load(ajaxurl,{action:'select_slider'},function(m){});
		  	}else{
				if($(this).val()=='post_slider'){
					$('.banner_type').html($('.post_slider_div').html());
					//$('.banner_type').load(ajaxurl,{action:'select_post_slider'},function(m){});
				}else{
					$('.banner_type').html('');
				}
			}
		  });
		});
	}
	if(module=="Gallery"){
		$('.gallery_category_div').load(ajaxurl,{action:'select_gallery_category'},function(m){});
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Type</label><select class="form-control" name="gallery_type" id="gallerytype"><option value="">--Select Gallery--</option><!--<option value="custom_gallery">Custom Gallery</option>--><option value="themelines_gallery">Themelines Gallery</option><option value="others">Others</option></select></div></div>'+
				'<div class="gallery_config"></div>'+
				'<div class="themelines_gal_config" style="display:none;"><div class="row"><div class="tl-pb-pop-col"><label>Items (-1 for all items)</label><input type="number" name="tl_items" id="tl_items" value="-1" class="form-control"></div></div></div>'+
				'<div class="gallery_all" style="display:none;">'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Grid</label><select class="form-control" name="gallery_grid" id="gallerygrid"><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Style</label><select class="form-control" name="gallery_style" id="gallerystyle"><option value="normal">Normal</option><option value="no_padding">No Padding</option><!--<option value="masonry">Masonry</option>--></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="filter" value="1">Show Filtering</div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>On Click</label><select class="form-control" name="gallery_onclick" id="galleryonclick"><option value="do_nothing">Do Nothing</option><option value="fancybox">Open In Fancybox</option><option value="new_page">Open In New Page</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="load" value="1">Show Load More</div></div>'+
				'</div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);
		
		$( "#gallerytype" ).change(function() {
		  if($(this).val()=='themelines_gallery'){
			  $('.gallery_config').html($('.gallery_category_div').html());
			  //$('.gallery_config').load(ajaxurl,{action:'select_gallery_category'},function(m){});
			  $('.gallery_all').show();
			  $('.themelines_gal_config').show();
		  }else{
			  if($(this).val()=='others'){
				$('.gallery_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="gallery_shortcode" name="gallery_shortcode"></textarea></div></div>');
				$('.gallery_all').hide();
				$('.themelines_gal_config').hide();
			  }else{
				  $('.gallery_config').html('');
				  $('.gallery_all').hide();
				  $('.themelines_gal_config').hide();
			  }
		  }
		});
	}
	if(module=="Developer Mode"){
		$('.modalDialog-developer').css({'pointer-events':'auto','opacity':'1'});
		$('.developer_div').show();
		$('.developer-button-area').html('<button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button><input type="hidden" name="data_content" value="'+data_content+'"><input type="hidden" name="data_module" value="'+module+'">');
		tb_remove();
		/*var developer = $('.developer_div').html();
		$('#tl-pb-popup-common').html(
			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col">'+developer.replace( new RegExp("123tlpbkey123", 'g'), data_content )+'</div></div>'+
				'<input type="hidden" name="data_content" value="'+data_content+'">'+
				'<input type="hidden" name="data_module" value="'+module+'">'+
				'<div class="row"><div class="tl-pb-pop-col"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary" id="btn_module_set">Add Module</button><button type="button" class="button button-warning" id="btn_module_listing">Module Listing</button></div></div></div>'
			)
		);*/
	}
	if(module!="Developer Mode"){
		tb_show("Configure "+module, "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");
	}
	m7_resize_thickbox(); 
	jQuery(window).on( 'resize', m7_resize_thickbox );
});

$(document).on('click','#btn_module_listing',function(){
	var data_content = $('[name="data_content"]').val();
	$("#TB_window").remove();
	$("body").append("<div id='TB_window'></div>");
	$('#tl-pb-popup-common').html(
		$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(TlPb.moduleElements(data_content))
	);
	//$('.widget').load(ajaxurl,{action:'fetch_widget'});
	tb_show("Configure Module", "#TB_inline?height=302&width=400&inlineId=tl-pb-popup-common");
	module_resize_thickbox(); 
	jQuery(window).on( 'resize', module_resize_thickbox );
});

$(document).on('click','#btn_module_set',function(){
	if($(this).closest('.tl-pb-sec-sett-pop').find('[name="data_module"]').val()){
		var data = $(this).closest('.tl-pb-sec-sett-pop').find('[name="data_content"]').val();
	}else{
		var data = $('.modalDialog-developer').find('[name="data_content"]').val();
	}
	if($(this).closest('.tl-pb-sec-sett-pop').find('[name="data_module"]').val()){
		var module = $(this).closest('.tl-pb-sec-sett-pop').find('[name="data_module"]').val();
	}else{
		var module = $('.modalDialog-developer').find('[name="data_module"]').val();
	}
	
	if(module=="Widget"){
		var widget = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_sidebar"]').val();
		if ($(this).closest('.tl-pb-sec-sett-pop').find('[name="use_sidebar"]').is(':checked')) {
			var sidebar = $(this).closest('.tl-pb-sec-sett-pop').find('[name="use_sidebar"]').val();
		}else{
			var sidebar = '';
		}
		var widget_shortcode = '[tl_widget id="'+widget+'" sidebar="'+sidebar+'"]';
		if(widget){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Widget</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Widget');
			$('.row_'+data).find('#this-content').html(widget_shortcode);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Widget');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			/*$('.row_'+data).find('.tl-pb-section-content-text').closest('a').css({'pointer-events':'none'});*/
			//$('.row_'+data).find('.tl_pb_sec_con_widget').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please select your widget.','Notice');
		}
	}
	if(module=='Block'){
		var block = '[tl_block ';
		block += 'layout="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_layout"]').val()+'" ';
		block += 'icon="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_icon"]').val()+'" ';
		block += 'title="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_title"]').val()+'"';
		if ($(this).closest('.tl-pb-sec-sett-pop').find('[name="block_button"]').is(':checked')) {
			block += ' button="yes" button_text="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_button_text"]').val()+'" button_url="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_button_url"]').val()+'"';
		}else{
			block += ' button="no"';
		}
		block += ']'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="block_content"]').val()+'[/tl_block]';
		
		if(block){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Block</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Block');
			$('.row_'+data).find('#this-content').html(block);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Block');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			/*$('.row_'+data).find('.tl-pb-section-content-text').closest('a').css({'pointer-events':'none'});*/
			//$('.row_'+data).find('.tl_pb_sec_con_widget').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter all the fields.','Notice');
		}
	}
	
	if(module=='Call To Action'){
		var cta = '[tl_cta ';
		cta += 'button_text="'+$('[name="call_to_action_button_text"]').val()+'" ';
		cta += 'button_url="'+$('[name="call_to_action_button_url"]').val()+'" ';
		cta += 'button_icon="'+$('[name="call_to_action_button_icon"]').val()+'" ';
		cta += 'position="'+$('[name="call_to_action_position"]').val()+'" ';
		cta += 'background_color="'+$('[name="call_to_action_bgcolor"]').val()+'" ';
		cta += 'background_image="'+$(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-section-post-background').attr('data-bgUrl')+'"';
		cta += ']'+$('[name="call_to_action_text"]').val()+'[/tl_cta]';
		if(cta){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Call To Action</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Call To Action');
			$('.row_'+data).find('#this-content').html(cta);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Call To Action');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			/*$('.row_'+data).find('.tl-pb-section-content-text').closest('a').css({'pointer-events':'none'});*/
			//$('.row_'+data).find('.tl_pb_sec_con_widget').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter all the fields.','Notice');
		}
	}
	
	if(module=='Contact'){
		var formVal		=	$(this).closest('.tl-pb-sec-sett-pop').find('[name="contact_form"]').val();
		var map			=	'';
		if($(this).closest('.tl-pb-sec-sett-pop').find('[name="show_map"]').is(':checked'))
			map	+=	'showmap="1" ';
		else
			map	+=	'showmap="0" ';
			var cont = '[tl_cont ';
			cont += 'address="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="contact_address"]').val()+'" ';
			cont += map;
			cont += 'email="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="contact_email"]').val()+'" ';
			cont += 'phone="'+$(this).closest('.tl-pb-sec-sett-pop').find('[name="contact_phone"]').val()+'"]';
			cont += formVal+'[/tl_cont]';
		if(cont){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Contact Form 7</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Contact');
			$('.row_'+data).find('#this-content').html(cont);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Contact');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl_pb_sec_con_widget').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter all the fields.','Notice');
		}
	}
	if(module=='Post Types'){
		//alert($('.row_'+data).closest('.tl-pb-col').find('#this_sec_grid').val());
		var post_type = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_type]"]').val(); 
		var post_order = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_order]"]').val();
		var post_from = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_from]"]').val();
		var page_from = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][page_from]"]').val();
		var posts_per_page = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_number]"]').val();
		var animation = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][animation]"]').val();
		var animation_duration = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][animation_duration]"]').val();
		var animation_delay = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][animation_delay]"]').val();
		var offset = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_offset]"]').val();
		if($(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_pagination]"]').is(':checked')){
			var pagination = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_pagination]"]:checked').val();
		}else{
			var pagination = '';
		}
		var column = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_column]"]').val();
		if($(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_feature]"]').is(':checked')){
			var feature = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_feature]"]:checked').val();
		}else{
			var feature = '';
		}
		if($(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_excerpt]"]').is(':checked')){
			var post_excerpt = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_excerpt]"]:checked').val();
		}else{
			var post_excerpt = '';
		}
		var post_length = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_length]"]').val();
		var post_length = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_length]"]').val();
		var post_element = jQuery.map($('[name="tlpb['+data+'][post_element][]"]:checked'), function (n, i) {
    		return n.value;
		}).join(',');
		var post_style = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][post_style]"]').val();
		var team_style = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][team_style]"]').val();
		var testimonial_style = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tlpb['+data+'][testimonial_style]"]').val();
		var post = false;
		if(post_type=='post' || post_type=='page'){
			if(post_style=='column'){
				var post = '[tlpb_post_column post_type="'+post_type+'" post_order="'+post_order+'" post_from="'+post_from+'" page_from="'+page_from+'" posts_per_page="'+posts_per_page+'" offset="'+offset+'" pagination="'+pagination+'" column="'+column+'" feature="'+feature+'" post_excerpt="'+post_excerpt+'" post_length="'+post_length+'" post_element="'+post_element+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
			if(post_style=='horizontal'){
				var post = '[tlpb_post_horizontal post_type="'+post_type+'" post_order="'+post_order+'" post_from="'+post_from+'" page_from="'+page_from+'" posts_per_page="'+posts_per_page+'" offset="'+offset+'" pagination="'+pagination+'" column="'+column+'" feature="'+feature+'" post_excerpt="'+post_excerpt+'" post_length="'+post_length+'" post_element="'+post_element+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
			if(post_style=='masonry'){
				var post = '[tlpb_post_masonry post_type="'+post_type+'" post_order="'+post_order+'" post_from="'+post_from+'" page_from="'+page_from+'" posts_per_page="'+posts_per_page+'" offset="'+offset+'" pagination="'+pagination+'" column="'+column+'" feature="'+feature+'" post_excerpt="'+post_excerpt+'" post_length="'+post_length+'" post_element="'+post_element+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
		}
		if(post_type=='team'){
			if(team_style=='column'){
				var post = '[tlpb_team_column post_type="'+post_type+'" post_order="'+post_order+'" post_number="'+posts_per_page+'" post_offset="'+offset+'" post_column="'+column+'" post_element="'+post_element+'" key="'+data+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
			if(team_style=='slder'){
				var post = '[tlpb_team_slider post_type="'+post_type+'" post_order="'+post_order+'" post_number="'+posts_per_page+'" post_offset="'+offset+'" post_column="'+column+'" post_element="'+post_element+'" key="'+data+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
		}
		if(post_type=='testimonial'){
			if(testimonial_style=='normal'){
				var post = '[tlpb_testimonial_normal post_type="'+post_type+'" post_order="'+post_order+'" post_number="'+posts_per_page+'" post_offset="'+offset+'" post_column="'+column+'" post_element="'+post_element+'" key="'+data+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
			if(testimonial_style=='chat'){
				var post = '[tlpb_testimonial_chat post_type="'+post_type+'" post_order="'+post_order+'" post_number="'+posts_per_page+'" post_offset="'+offset+'" post_column="'+column+'" post_element="'+post_element+'" key="'+data+'" container="no" animation="'+animation+'" animation_delay="'+animation_delay+'" animation_duration="'+animation_duration+'"]';
			}
		}
		if(post){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Post Types: '+post_type.toUpperCase()+'</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Post Types');
			$('.row_'+data).find('#this-content').html(post);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Post Types');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter the fields correctly.','Notice');
		}
	}
	if(module=='Banner or Slider'){
		var banner_type = $(this).closest('.tl-pb-sec-sett-pop').find('[name="banner_type"]').val();
		var layout = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_layout"]').val();
		var banner_shortcode = false;
		if(banner_type=='banner'){
			if($(this).closest('.tl-pb-sec-sett-pop').find('[name="parallax"]').is(':checked')){
				var parallax = $(this).closest('.tl-pb-sec-sett-pop').find('[name="parallax"]').val();
			}else{
				var parallax = '';
			}
			var banner = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_banner"]').val();
			var banner_id = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_banner_slide"]').val();
			banner_shortcode = '[tlpb_themelines_banner layout="'+layout+'" banner_parallax="'+parallax+'" slider="'+banner_id+'" container="no"]';
		}
		if(banner_type=='slider'){
			var slider = $(this).closest('.tl-pb-sec-sett-pop').find('[name="select_slider_criteria"]').val();
			if(slider=='post_slider'){
				var category= [];
				$(this).closest('.tl-pb-sec-sett-pop').find("[name='slider'] option:selected").each(function(){
					category.push($(this).val());
				});
				banner_shortcode = '[tlpb_post_slider layout="'+layout+'" category="'+category+'" key="'+data+'" container="no"]';
			}
			if(slider=='themelines_slider'){
				var category= [];
				$(this).closest('.tl-pb-sec-sett-pop').find("[name='slider'] option:selected").each(function(){
					category.push($(this).val());
				});
				banner_shortcode = '[tlpb_themelines_slider layout="'+layout+'" category="'+category+'" key="'+data+'" container="no"]';
			}
		}
		if(banner_type=='others'){
			var shortcode = $(this).closest('.tl-pb-sec-sett-pop').find('[name="shortcode"]').val();
			if(shortcode){
				banner_shortcode = '[tlpb_other]'+shortcode+'[/tlpb_other]';
			}
		}
		if(banner_shortcode){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Banner or Slider: '+banner_type.toUpperCase()+'</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Banner or Slider');
			$('.row_'+data).find('#this-content').html(banner_shortcode);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Banner or Slider');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter the fields correctly.','Notice');
		}
	}
	
	if(module=='Gallery'){
		var gallery_type = $(this).closest('.tl-pb-sec-sett-pop').find('[name="gallery_type"]').val();
		var gallery_shortcode = false;
		if(gallery_type=='themelines_gallery'){
			var categories= [];
			$(this).closest('.tl-pb-sec-sett-pop').find("[name='gallery_category'] option:selected").each(function(){
				categories.push($(this).val());
			});
			var items = $(this).closest('.tl-pb-sec-sett-pop').find('[name="tl_items"]').val();
			var gal_grid = $(this).closest('.tl-pb-sec-sett-pop').find('[name="gallery_grid"]').val();
			var style = $(this).closest('.tl-pb-sec-sett-pop').find('[name="gallery_style"]').val();
			if($(this).closest('.tl-pb-sec-sett-pop').find('[name="filter"]').is(':checked')){
				var filter = $(this).closest('.tl-pb-sec-sett-pop').find('[name="filter"]').val();
			}else{
				var filter = '';
			}
			var onclick = $(this).closest('.tl-pb-sec-sett-pop').find('[name="gallery_onclick"]').val();
			if(style=='normal'){
				gallery_shortcode = '[tlpb_normal_gallery key="'+data+'" items="'+items+'" categories="'+categories+'" filter="'+filter+'" gal_grid="'+gal_grid+'" onclick="'+onclick+'" container="no"]';
			}
			if(style=='masonry'){
				gallery_shortcode = '[tlpb_masonry_gallery key="'+data+'" items="'+items+'" categories="'+categories+'" filter="'+filter+'" gal_grid="'+gal_grid+'" onclick="'+onclick+'" container="no"]';
			}
			if(style=='no_padding'){
				gallery_shortcode = '[tlpb_no_padding_gallery key="'+data+'" items="'+items+'" categories="'+categories+'" filter="'+filter+'" gal_grid="'+gal_grid+'" onclick="'+onclick+'" container="no"]';
			}
		}
		if(gallery_type=='others'){
			shortcode = $(this).closest('.tl-pb-sec-sett-pop').find('[name="shortcode"]').val();
			if(shortcode){
				gallery_shortcode = '[tlpb_other]'+shortcode+'[/tlpb_other]';
			}
		}
		if(gallery_shortcode){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Gallery: '+gallery_type.replace( new RegExp("_", 'g'), " " ).toUpperCase()+'</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Gallery');
			$('.row_'+data).find('#this-content').html(gallery_shortcode);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Gallery');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			tb_remove();
		}else{
			jAlert('Please enter the fields correctly.','Notice');
		}
	}
	if(module=='Developer Mode'){
		var developer = $('[name="tlpb[123tlpbkey123][content]"]').val();
		var developer_shortcode = '[tlpb_developer]'+developer+'[/tlpb_developer]';
		if(developer_shortcode){
			$('.row_'+data).find('.tl-pb-section-content-text-inner').html('<h1 style="text-align:center;">Developer Mode</h1>');
			$('.row_'+data).find('.tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit_module').attr('data-module','Developer Mode');
			$('.row_'+data).find('#this-content').html(developer_shortcode);
			$('.row_'+data).find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_edit').removeClass('tl_pb_sec_con_edit').addClass('tl_pb_sec_con_module tl_pb_sec_con_edit_module').attr('data-module','Developer Mode');
			$('.row_'+data).find('.tl_pb_sec_con_edit').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.tl-pb-section-content-background').css({'pointer-events':'none','opacity':'0.3'});
			$('.row_'+data).find('.fa-times-circle').trigger('click');
			$('.row_'+data).removeClass('row_'+data);
			$('.developer_div').hide();
			$('.developer-button-area').html('');
			$('.modalDialog-developer').removeAttr('style');
			$('[name="tlpb[123tlpbkey123][content]"]').val('');
			$('[name="tlpb[123tlpbkey123][content]"]').html('');
			//tb_remove();
		}else{
			jAlert('Please enter the textfield.','Notice');
		}
	}
});

$(document).on('click','#btn_module_unset',function(){
	var data = $(this).closest('.tl-pb-sec-sett-pop').find('[name="data_content"]').val();
	var $row = $('.row_'+data);
	jConfirm('Are you sure you want to remove this module?', 'Confirmation', function(r){
		if(r==true){
			$row.find('.tl_pb_sec_con_module').removeClass('tl_pb_sec_con_edit_module');
			$row.find('.tl-pb-section-content-text-inner').html('');
			$row.find('#this-content').html('');
			$row.find('.tl-pb-section-content-text').closest('.tl_pb_sec_con_module').removeClass('tl_pb_sec_con_module').addClass('tl_pb_sec_con_edit').removeAttr('data-module');
			$row.find('.tl_pb_sec_con_edit').css({'pointer-events':'auto','opacity':'1'});
			$row.find('.tl-pb-section-content-background').css({'pointer-events':'auto','opacity':'1'});
			$row.find('.tl-pb-section-content-text').closest('a').css({'pointer-events':'auto'});
			//$row.find('.tl_pb_sec_con_widget').css({'pointer-events':'auto','opacity':'1'});
			$row.removeClass('row_'+data);
			tb_remove();
		}
	});
});

$(document).on('click','#tl-pb-menu-item-gallery',function(){
		var key = tl_pb_genarate();
		var $this = $(this);
		$('.gallery_category_div').load(ajaxurl,{action:'select_gallery_category'},function(m){});
		
		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop'}).html(
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Type</label><select class="form-control" name="gallery_type" id="gallerytype"><option value="">--Select Gallery--</option><!--<option value="custom_gallery">Custom Gallery</option>--><option value="themelines_gallery">Themelines Gallery</option><option value="others">Others</option></select></div></div>'+
				'<div class="gallery_config"></div>'+
				'<div class="themelines_gal_config" style="display:none;"><div class="row"><div class="tl-pb-pop-col"><label>Items (-1 for all items)</label><input type="number" name="tl_items" id="tl_items" value="-1" class="form-control"></div></div></div>'+
				'<div class="gallery_all" style="display:none;">'+
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Grid</label><select class="form-control" name="gallery_grid" id="gallerygrid"><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Select Gallery Style</label><select class="form-control" name="gallery_style" id="gallerystyle"><option value="normal">Normal</option><option value="no_padding">No Padding</option><!--<option value="masonry">Masonry</option>--></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="filter" value="1">Show Filtering</div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><label>On Click</label><select class="form-control" name="gallery_onclick" id="galleryonclick"><option value="do_nothing">Do Nothing</option><option value="fancybox">Open In Fancybox</option><option value="new_page">Open In New Page</option></select></div></div>'+
				'<div class="row"><div class="tl-pb-pop-col"><input type="checkbox" name="load" value="1">Show Load More</div></div>'+
				'</div>'+
				'<input type="hidden" name="hidden_gal_key" value="" id="hidden_gal_key">'+
				'<div class="row"><div class="tl-pb-pop-col pull-right"><button type="button" class="button button-primary button-large" id="btn_gallery_type_set">Change</button></div></div>'

			)
		);
		
		$( "#gallerytype" ).change(function() {
		  /*if($(this).val()=='custom_gallery'){
			  $('.gallery_config').html('');
			  $('.gallery_all').show();
			  $('.themelines_gal_config').hide();
		  }else{*/
			  if($(this).val()=='themelines_gallery'){
				  $('.gallery_config').html($('.gallery_category_div').html());
				  //$('.gallery_config').load(ajaxurl,{action:'select_gallery_category'},function(m){});
				  $('.gallery_all').show();
				  $('.themelines_gal_config').show();
			  }else{
				  if($(this).val()=='others'){
				  	$('.gallery_config').html('<div class="row"><div class="tl-pb-pop-col"><label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="gallery_shortcode" name="gallery_shortcode"></textarea></div></div>');
					$('.gallery_all').hide();
					$('.themelines_gal_config').hide();
			  	  }else{
					  $('.gallery_config').html('');
					  $('.gallery_all').hide();
					  $('.themelines_gal_config').hide();
				  }
			  }
		  //}
		});
		
		if($this.closest('.tl-pb-section').attr('id')){
			var key = $this.closest('.tl-pb-section').attr('id');
			$('#hidden_gal_key').val(key);
			$('[name="gallery_type"]').val($('[name="tlpb['+key+'][subtype]"]').val()).change();
			if($('[name="tlpb['+key+'][subtype]"]').val()=='themelines_gallery'){
				var valArr = $('[name="tlpb['+key+'][category]"]').val().split(',');
				size = valArr.length;
				for (i = 0; i < size; i++) {
					$("#gallery_category option[value='" + valArr[i] + "']").attr("selected", 1);
				}
				$('[name="tl_items"]').val($('[name="tlpb['+key+'][items]"]').val());
				$('[name="gallery_grid"]').val($('[name="tlpb['+key+'][grid]"]').val()).change();
				$('[name="gallery_style"]').val($('[name="tlpb['+key+'][style]"]').val()).change();
				if($('[name="tlpb['+key+'][filter]"]').val()=='1'){
					$('[name="filter"]').prop('checked', true);
				}
				var loadmo = $('[name="tlpb['+key+'][load]"]').val();
				if(typeof loadmo == 'undefined'){
					$('#'+key).append('<input name="tlpb['+key+'][load]" value="0" type="hidden">');
				}
				if($('[name="tlpb['+key+'][load]"]').val()=='1'){
					$('[name="load"]').prop('checked', true);
				}
				$('[name="gallery_onclick"]').val($('[name="tlpb['+key+'][onclick]"]').val()).change();
			}
			if($('[name="tlpb['+key+'][subtype]"]').val()=='others'){
				$('[name="shortcode"]').val($('[name="tlpb['+key+'][shortcode]"]').val());
			}
		}
		
		tb_show("Configure Gallery", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );
		
		/*

		$('<div id="'+key+'" class="tl-pb-section" data-sectype="gallery"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Gallery</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_gal_sett" href="javascript:;"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <a href="javascript:;" class="tl-pb-gallery-add-item-link" title="Add new item"><div class="tl-pb-gallery-add-item"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></a><input type="hidden" name="tl_pb_key[]" value="'+key+'"><input type="hidden" name="'+key+'-type" value="gallery"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html(TlPb.colGalleryHTML(3));
		
		jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);

		jQuery('.tl-pb-row').sortable({

		    handle: '.tl-pb-section-content-sortable',

			placeholder: 'sortable-placeholder',

			forcePlaceholderSizeType: true,

			distance: 2,

			tolerance: 'pointer',

			cursor: 'move'

    	});*/

});	

$(document).on('click','#btn_gallery_type_set',function(){
	var key = tl_pb_genarate();
	var oldkey = 0;
	if($('#hidden_gal_key').val()!=''){
		var key = $('#hidden_gal_key').val();
		//$('#'+key).remove();
		var oldkey = $('#hidden_gal_key').val();
	}
	var grid = $('[name="gallery_grid"]').val();
	var style = $('[name="gallery_style"]').val();
	if ($('[name="filter"]').is(':checked')) {
		var filter = '1';
	}else{
		var filter = '0';
	}
	if ($('[name="load"]').is(':checked')) {
		var loadm = '1';
	}else{
		var loadm = '0';
	}
	var onclick = $('[name="gallery_onclick"]').val();
	
	/*if($( "#gallerytype" ).val()=='custom_gallery'){
		if(key==oldkey){
			var keytitle = $('[name="tlpb['+key+'][title]"]').val();
			var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
			var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
			var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
			var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
			var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
			var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
			var keypadding = $('[name="tlpb['+key+'][padding]"]').val();
			var keyclass = $('[name="tlpb['+key+'][class]"]').val();
			var keyid = $('[name="tlpb['+key+'][id]"]').val();
			var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
			var keybgimage = $('[name="tlpb['+key+'][background_image]"]').val();
			var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
			var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
			if (typeof imgattr !== typeof undefined && imgattr !== false) {
				var keybgid = imgattr;
			}else{
				var keybgid = '';
			}
				$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Custom Gallery</h4></div><div class="tl-pb-right-bar"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <a href="javascript:;" class="tl-pb-gallery-add-item-link"><div class="tl-pb-gallery-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="custom_gallery"><input type="hidden" name="tlpb['+key+'][grid]" value="'+grid+'"><input type="hidden" name="tlpb['+key+'][style]" value="'+style+'"><input type="hidden" name="tlpb['+key+'][filter]" value="'+filter+'"><input type="hidden" name="tlpb['+key+'][onclick]" value="'+onclick+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'"><input type="hidden" name="tlpb['+key+'][padding]" id="'+key+'-padding" value="'+keypadding+'"><input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
				$('#'+key).find('.tl-pb-gal-row').html(TlPb.colGalleryHTML(grid,key));
		}else{
			$('<div id="'+key+'" class="tl-pb-section" data-sectype="gallery"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Custom Gallery</h4></div><div class="tl-pb-right-bar"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="tl-pb-row tl-pb-gal-row row"></div></div> <a href="javascript:;" class="tl-pb-gallery-add-item-link"><div class="tl-pb-gallery-add-item"><i class="fa fa-plus-circle" aria-hidden="true" title="Add new item"></i></div></a><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="custom_gallery"><input type="hidden" name="tlpb['+key+'][grid]" value="'+grid+'"><input type="hidden" name="tlpb['+key+'][style]" value="'+style+'"><input type="hidden" name="tlpb['+key+'][filter]" value="'+filter+'"><input type="hidden" name="tlpb['+key+'][onclick]" value="'+onclick+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color"><input type="hidden" name="tlpb['+key+'][padding]" id="'+key+'-padding" value="0"><input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html(TlPb.colGalleryHTML(grid,key));	
		}
}else{*/
	if($( "#gallerytype" ).val()=='themelines_gallery'){
		var category= [];
		var rel= [];
		var items = $('#tl_items').val();
		$.each($("[name='gallery_category'] option:selected"), function(){            
			category.push($(this).val());
			rel.push($(this).attr('rel'));
		});
		if(category!=''){
			if(key==oldkey){
				var keytitle = $('[name="tlpb['+key+'][title]"]').val();
				if(typeof keytitle == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
					var keytitle = $('[name="tlpb['+key+'][title]"]').val();
				}
				var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
				if(typeof keytitle_position == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
					var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
				}
				var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
				if(typeof keysubtitle == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
					var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
				}
				var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
				if(typeof keysubtitle_position == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
					var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
				}
				var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
				if(typeof keytitle_color == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
					var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
				}
				var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
				if(typeof keysubtitle_color == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
					var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
				}
				var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
				if(typeof keycontent_color == 'undefined'){
					$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
					var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
				}
				
				var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
				if(typeof keymargin_top == 'undefined'){
					$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
					var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
				}
				var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
				if(typeof keymargin_bottom == 'undefined'){
					$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
					var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
				}
				var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
				if(typeof keymargin_left == 'undefined'){
					$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
					var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
				}
				var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
				if(typeof keymargin_right == 'undefined'){
					$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
					var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
				}
				var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
				if(typeof keymargin_custom_top == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
					var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
				}
				var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
				if(typeof keymargin_custom_bottom == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
					var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
				}
				var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
				if(typeof keymargin_custom_left == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
					var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
				}
				var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
				if(typeof keymargin_custom_right == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
					var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
				}
				
				var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
				if(typeof keypadding_top == 'undefined'){
					$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
					var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
				}
				var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
				if(typeof keypadding_bottom == 'undefined'){
					$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
					var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
				}
				var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
				if(typeof keypadding_left == 'undefined'){
					$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
					var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
				}
				var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
				if(typeof keypadding_right == 'undefined'){
					$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
					var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
				}
				var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
				if(typeof keypadding_custom_top == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
					var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
				}
				var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
				if(typeof keypadding_custom_bottom == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
					var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
				}
				var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
				if(typeof keypadding_custom_left == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
					var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
				}
				var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
				if(typeof keypadding_custom_right == 'undefined'){
					$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
					var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
				}
				
				var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
				if(typeof title_tag == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
					var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
				}
				var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
				if(typeof subtitle_tag == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
					var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
				}
				
				var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
				if(typeof title_color_class == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
					var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
				}
				var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
				if(typeof title_animation == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
					var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
				}
				var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
				if(typeof title_animation_delay == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
					var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
				}
				var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
				if(typeof title_animation_duration == 'undefined'){
					$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
					var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
				}
				var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
				if(typeof subtitle_color_class == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
					var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
				}
				var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
				if(typeof subtitle_animation == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
					var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
				}
				var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
				if(typeof subtitle_animation_delay == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
					var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
				}
				var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
				if(typeof subtitle_animation_duration == 'undefined'){
					$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
					var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
				}
				var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
				if(typeof content_color_class == 'undefined'){
					$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
					var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
				}
				var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
				if(typeof content_animation == 'undefined'){
					$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
					var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
				}
				var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
				if(typeof content_animation_delay == 'undefined'){
					$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
					var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
				}
				var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
				if(typeof content_animation_duration == 'undefined'){
					$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
					var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
				}
				var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
				if(typeof background_color_class == 'undefined'){
					$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
					var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
				}
				
				var keyclass = $('[name="tlpb['+key+'][class]"]').val();
				if(typeof keyclass == 'undefined'){
					$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
					var keyclass = $('[name="tlpb['+key+'][class]"]').val();
				}
				var keyid = $('[name="tlpb['+key+'][id]"]').val();
				if(typeof keyid == 'undefined'){
					$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
					var keyid = $('[name="tlpb['+key+'][id]"]').val();
				}
				var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
				if(typeof keybgcolor == 'undefined'){
					$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
					var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
				}
				var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
				if(typeof keybgimage == 'undefined'){
					$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
					var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
				}
				var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
				if(typeof keyparallax == 'undefined'){
					$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
					var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
				}
				var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
				if (typeof imgattr !== typeof undefined && imgattr !== false) {
					var keybgid = imgattr;
				}else{
					var keybgid = '';
				}
			
				$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Themelines Gallery</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_gallery"><input type="hidden" name="tlpb['+key+'][items]" id="'+key+'-items" value="'+items+'"><input type="hidden" name="tlpb['+key+'][grid]" value="'+grid+'"><input type="hidden" name="tlpb['+key+'][style]" value="'+style+'"><input type="hidden" name="tlpb['+key+'][filter]" value="'+filter+'"><input type="hidden" name="tlpb['+key+'][load]" value="'+loadm+'"><input type="hidden" name="tlpb['+key+'][onclick]" value="'+onclick+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		
'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
				$('#'+key).find('.tl-pb-gal-row').html('<div class="tl-pb-section-content-text" style="display:block;"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3></div></div></div></div></div>');
			}else{
				$('<div id="'+key+'" class="tl-pb-section" data-sectype="gallery"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Themelines Gallery</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="tl-pb-row tl-pb-gal-row row"></div></div> <input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="themelines_gallery"><input type="hidden" name="tlpb['+key+'][items]" id="'+key+'-items" value="'+items+'"><input type="hidden" name="tlpb['+key+'][grid]" value="'+grid+'"><input type="hidden" name="tlpb['+key+'][style]" value="'+style+'"><input type="hidden" name="tlpb['+key+'][filter]" value="'+filter+'"><input type="hidden" name="tlpb['+key+'][load]" value="'+loadm+'"><input type="hidden" name="tlpb['+key+'][onclick]" value="'+onclick+'"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html('<div class="tl-pb-section-content-text" style="display:block;"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+rel+'<input type="hidden" name="tlpb['+key+'][category]" id="'+key+'-category" value="'+category+'"></h3></div></div></div></div></div>');
		}
		}else{
			jAlert('Please select at least one category','Notice');
		}
	}else{
		if($( "#gallerytype" ).val()=='others'){
			var shortcode = $('[name="shortcode"]').val();
			if(shortcode!=''){
				if(key==oldkey){
					var keytitle = $('[name="tlpb['+key+'][title]"]').val();
					if(typeof keytitle == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title" name="tlpb['+key+'][title]" type="hidden">');
						var keytitle = $('[name="tlpb['+key+'][title]"]').val();
					}
					var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
					if(typeof keytitle_position == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-position" name="tlpb['+key+'][title_position]" type="hidden">');
						var keytitle_position = $('[name="tlpb['+key+'][title_position]"]').val();
					}
					var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
					if(typeof keysubtitle == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle" name="tlpb['+key+'][subtitle]" type="hidden">');
						var keysubtitle = $('[name="tlpb['+key+'][subtitle]"]').val();
					}
					var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
					if(typeof keysubtitle_position == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-position" name="tlpb['+key+'][subtitle_position]" type="hidden">');
						var keysubtitle_position = $('[name="tlpb['+key+'][subtitle_position]"]').val();
					}
					var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
					if(typeof keytitle_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-color" name="tlpb['+key+'][title_color]" type="hidden">');
						var keytitle_color = $('[name="tlpb['+key+'][title_color]"]').val();
					}
					var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
					if(typeof keysubtitle_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-color" name="tlpb['+key+'][subtitle_color]" type="hidden">');
						var keysubtitle_color = $('[name="tlpb['+key+'][subtitle_color]"]').val();
					}
					var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
					if(typeof keycontent_color == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-color" name="tlpb['+key+'][content_color]" type="hidden">');
						var keycontent_color = $('[name="tlpb['+key+'][content_color]"]').val();
					}
					
					var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
					if(typeof keymargin_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-top" name="tlpb['+key+'][margin_top]" type="hidden" value="0">');
						var keymargin_top = $('[name="tlpb['+key+'][margin_top]"]').val();
					}
					var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
					if(typeof keymargin_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-bottom" name="tlpb['+key+'][margin_bottom]" type="hidden" value="0">');
						var keymargin_bottom = $('[name="tlpb['+key+'][margin_bottom]"]').val();
					}
					var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
					if(typeof keymargin_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-left" name="tlpb['+key+'][margin_left]" type="hidden" value="0">');
						var keymargin_left = $('[name="tlpb['+key+'][margin_left]"]').val();
					}
					var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
					if(typeof keymargin_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-margin-right" name="tlpb['+key+'][margin_right]" type="hidden" value="0">');
						var keymargin_right = $('[name="tlpb['+key+'][margin_right]"]').val();
					}
					var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
					if(typeof keymargin_custom_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-top" name="tlpb['+key+'][custom_margin_top]" type="hidden" value="0">');
						var keymargin_custom_top = $('[name="tlpb['+key+'][custom_margin_top]"]').val();
					}
					var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
					if(typeof keymargin_custom_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-bottom" name="tlpb['+key+'][custom_margin_bottom]" type="hidden" value="0">');
						var keymargin_custom_bottom = $('[name="tlpb['+key+'][custom_margin_bottom]"]').val();
					}
					var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
					if(typeof keymargin_custom_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-left" name="tlpb['+key+'][custom_margin_left]" type="hidden" value="0">');
						var keymargin_custom_left = $('[name="tlpb['+key+'][custom_margin_left]"]').val();
					}
					var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
					if(typeof keymargin_custom_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-margin-right" name="tlpb['+key+'][custom_margin_right]" type="hidden" value="0">');
						var keymargin_custom_right = $('[name="tlpb['+key+'][custom_margin_right]"]').val();
					}
					
					var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
					if(typeof keypadding_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-top" name="tlpb['+key+'][padding_top]" type="hidden" value="0">');
						var keypadding_top = $('[name="tlpb['+key+'][padding_top]"]').val();
					}
					var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
					if(typeof keypadding_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-bottom" name="tlpb['+key+'][padding_bottom]" type="hidden" value="0">');
						var keypadding_bottom = $('[name="tlpb['+key+'][padding_bottom]"]').val();
					}
					var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
					if(typeof keypadding_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-left" name="tlpb['+key+'][padding_left]" type="hidden" value="0">');
						var keypadding_left = $('[name="tlpb['+key+'][padding_left]"]').val();
					}
					var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
					if(typeof keypadding_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-padding-right" name="tlpb['+key+'][padding_right]" type="hidden" value="0">');
						var keypadding_right = $('[name="tlpb['+key+'][padding_right]"]').val();
					}
					var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
					if(typeof keypadding_custom_top == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-top" name="tlpb['+key+'][custom_padding_top]" type="hidden" value="0">');
						var keypadding_custom_top = $('[name="tlpb['+key+'][custom_padding_top]"]').val();
					}
					var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
					if(typeof keypadding_custom_bottom == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-bottom" name="tlpb['+key+'][custom_padding_bottom]" type="hidden" value="0">');
						var keypadding_custom_bottom = $('[name="tlpb['+key+'][custom_padding_bottom]"]').val();
					}
					var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
					if(typeof keypadding_custom_left == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-left" name="tlpb['+key+'][custom_padding_left]" type="hidden" value="0">');
						var keypadding_custom_left = $('[name="tlpb['+key+'][custom_padding_left]"]').val();
					}
					var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
					if(typeof keypadding_custom_right == 'undefined'){
						$('#'+key).append('<input id="'+key+'-custom-padding-right" name="tlpb['+key+'][custom_padding_right]" type="hidden" value="0">');
						var keypadding_custom_right = $('[name="tlpb['+key+'][custom_padding_right]"]').val();
					}
					
					var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
					if(typeof title_tag == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-tag" name="tlpb['+key+'][title_tag]" type="hidden" value="h2">');
						var title_tag = $('[name="tlpb['+key+'][title_tag]"]').val();
					}
					var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
					if(typeof subtitle_tag == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-tag" name="tlpb['+key+'][subtitle_tag]" type="hidden" value="h3">');
						var subtitle_tag = $('[name="tlpb['+key+'][subtitle_tag]"]').val();
					}
					
					var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
					if(typeof title_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-color-class" name="tlpb['+key+'][title_color_class]" type="hidden" value="default">');
						var title_color_class = $('[name="tlpb['+key+'][title_color_class]"]').val();
					}
					var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
					if(typeof title_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation" name="tlpb['+key+'][title_animation]" type="hidden" value="no">');
						var title_animation = $('[name="tlpb['+key+'][title_animation]"]').val();
					}
					var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
					if(typeof title_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation-delay" name="tlpb['+key+'][title_animation_delay]" type="hidden" value="200ms">');
						var title_animation_delay = $('[name="tlpb['+key+'][title_animation_delay]"]').val();
					}
					var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
					if(typeof title_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-title-animation-duration" name="tlpb['+key+'][title_animation_duration]" type="hidden" value="200ms">');
						var title_animation_duration = $('[name="tlpb['+key+'][title_animation_duration]"]').val();
					}
					var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
					if(typeof subtitle_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-color-class" name="tlpb['+key+'][subtitle_color_class]" type="hidden" value="default">');
						var subtitle_color_class = $('[name="tlpb['+key+'][subtitle_color_class]"]').val();
					}
					var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
					if(typeof subtitle_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation" name="tlpb['+key+'][subtitle_animation]" type="hidden" value="no">');
						var subtitle_animation = $('[name="tlpb['+key+'][subtitle_animation]"]').val();
					}
					var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
					if(typeof subtitle_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation-delay" name="tlpb['+key+'][subtitle_animation_delay]" type="hidden" value="200ms">');
						var subtitle_animation_delay = $('[name="tlpb['+key+'][subtitle_animation_delay]"]').val();
					}
					var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
					if(typeof subtitle_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-subtitle-animation-duration" name="tlpb['+key+'][subtitle_animation_duration]" type="hidden" value="200ms">');
						var subtitle_animation_duration = $('[name="tlpb['+key+'][subtitle_animation_duration]"]').val();
					}
					var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
					if(typeof content_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-color-class" name="tlpb['+key+'][content_color_class]" type="hidden" value="default">');
						var content_color_class = $('[name="tlpb['+key+'][content_color_class]"]').val();
					}
					var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
					if(typeof content_animation == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation" name="tlpb['+key+'][content_animation]" type="hidden" value="no">');
						var content_animation = $('[name="tlpb['+key+'][content_animation]"]').val();
					}
					var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
					if(typeof content_animation_delay == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation-delay" name="tlpb['+key+'][content_animation_delay]" type="hidden" value="200ms">');
						var content_animation_delay = $('[name="tlpb['+key+'][content_animation_delay]"]').val();
					}
					var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
					if(typeof content_animation_duration == 'undefined'){
						$('#'+key).append('<input id="'+key+'-content-animation-duration" name="tlpb['+key+'][content_animation_duration]" type="hidden" value="200ms">');
						var content_animation_duration = $('[name="tlpb['+key+'][content_animation_duration]"]').val();
					}
					var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
					if(typeof background_color_class == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-color-class" name="tlpb['+key+'][background_color_class]" type="hidden" value="default">');
						var background_color_class = $('[name="tlpb['+key+'][background_color_class]"]').val();
					}
					
					var keyclass = $('[name="tlpb['+key+'][class]"]').val();
					if(typeof keyclass == 'undefined'){
						$('#'+key).append('<input id="'+key+'-class" name="tlpb['+key+'][class]" type="hidden" value="">');
						var keyclass = $('[name="tlpb['+key+'][class]"]').val();
					}
					var keyid = $('[name="tlpb['+key+'][id]"]').val();
					if(typeof keyid == 'undefined'){
						$('#'+key).append('<input id="'+key+'-id" name="tlpb['+key+'][id]" type="hidden" value="">');
						var keyid = $('[name="tlpb['+key+'][id]"]').val();
					}
					var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
					if(typeof keybgcolor == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-color" name="tlpb['+key+'][background_color]" type="hidden" value="">');
						var keybgcolor = $('[name="tlpb['+key+'][background_color]"]').val();
					}
					var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
					if(typeof keybgimage == 'undefined'){
						$('#'+key).append('<input id="'+key+'-background-image" name="tlpb['+key+'][background_image]" type="hidden" value="">');
						var keybgimage = $('[name="tlpb['+key+'][background-image]"]').val();
					}
					var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
					if(typeof keyparallax == 'undefined'){
						$('#'+key).append('<input id="'+key+'-parallax" name="tlpb['+key+'][parallax]" type="hidden" value="">');
						var keyparallax = $('[name="tlpb['+key+'][parallax]"]').val();
					}
					var imgattr = $('#'+key).find('.tl-pb-section-content').attr('data-backgoundimageid');
					if (typeof imgattr !== typeof undefined && imgattr !== false) {
						var keybgid = imgattr;
					}else{
						var keybgid = '';
					}
					$('#'+key).html('<div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title">'+keytitle+'</span> Others Gallery</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content '+keyclass+'" data-backgoundimageUrl="'+keybgimage+'" data-backgoundcolor="'+keybgcolor+'" data-backgoundimageid="'+keybgid+'" data-parallax="'+keyparallax+'" data-id="'+keyid+'"><div class="tl-pb-row tl-pb-gal-row row"></div></div> <input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="others"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title" value="'+keytitle+'"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position" value="'+keytitle_position+'"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle" value="'+keysubtitle+'"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position" value="'+keysubtitle_position+'"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color" value="'+keytitle_color+'"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color" value="'+keysubtitle_color+'"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color" value="'+keycontent_color+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="'+keymargin_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="'+keymargin_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="'+keymargin_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="'+keymargin_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="'+keymargin_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="'+keymargin_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="'+keymargin_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="'+keymargin_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="'+keypadding_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="'+keypadding_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="'+keypadding_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="'+keypadding_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="'+keypadding_custom_top+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="'+keypadding_custom_bottom+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="'+keypadding_custom_left+'">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="'+keypadding_custom_right+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="'+title_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="'+subtitle_tag+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="'+title_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="'+title_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="'+title_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="'+subtitle_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="'+subtitle_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="'+subtitle_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="'+subtitle_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="'+content_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="'+content_animation+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="'+content_animation_delay+'">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="'+content_animation_duration+'">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="'+background_color_class+'">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class" value="'+keyclass+'"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id" value="'+keyid+'"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color" value="'+keybgcolor+'"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image" value="'+keybgimage+'"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="'+keyparallax+'">');
					$('#'+key).find('.tl-pb-gal-row').html('<div class="tl-pb-section-content-text" style="display:block;"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+shortcode+'<textarea name="tlpb['+key+'][shortcode]" id="'+key+'-shortcode" style="display:none;">'+shortcode+'</textarea></h3></div></div></div></div></div>');
				}else{
					$('<div id="'+key+'" class="tl-pb-section" data-sectype="gallery"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Others Gallery</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="tl-pb-row tl-pb-gal-row row"></div></div> <input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="gallery"><input type="hidden" name="tlpb['+key+'][subtype]" value="others"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-gal-row').html('<div class="tl-pb-section-content-text" style="display:block;"><div data-backgoundcolor="" data-backgoundimageurl="" data-backgoundimageid="" class="tl-pb-section-content"><div class="row"><div class="tl-pb-row tl-pb-post-row ui-sortable"><div class="tl-pb-col col-sm-12"><div class="tl-pb-section-content-sortable ui-sortable-handle"></div><div class="tl-pb-section-content-uploader"><div class="tl-pb-section-content-buttons"><a href="javascript:;" class="tl-pb-menu-item-gallery" id="tl-pb-menu-item-gallery" data-title="" title="Edit this section"><i aria-hidden="true" class="fa fa-pencil-square-o"></i></a></div></div><h3 class="tl-pb-section-content-text-title">'+shortcode+'<textarea name="tlpb['+key+'][shortcode]" id="'+key+'-shortcode" style="display:none;">'+shortcode+'</textarea></h3></div></div></div></div></div>');
				}
			}else{
				jAlert('Please enter the shortcode.','Notice');
			}
		}else{
			jAlert('Please select gallery type.');
		}
	}
	
//}
	jQuery( document ).tooltip();
	$('html, body').animate({
			scrollTop: $('#'+key).offset().top-32
		  }, 800);
	
	jQuery('.tl-pb-row').sortable({
		handle: '.tl-pb-section-content-sortable',
		placeholder: 'sortable-placeholder',
		forcePlaceholderSizeType: true,
		distance: 2,
		tolerance: 'pointer',
		cursor: 'move'
	});
	tb_remove();
});

$(document).on('click','#tl-pb-menu-item-post',function(){
	$('.post_div').load(ajaxurl,{action:'create_pb_post_html',key:'123tlpbkey123'},function(m){duplicate_post_value();
post_type_action();});
	var post = $('.post_div').html();
	var key = tl_pb_genarate();
	
	$('<div id="'+key+'" class="tl-pb-section" data-sectype="post"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Post Types</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="tl-pb-row tl-pb-post-row row"></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="post_type"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage')).find('.tl-pb-post-row').html(post.replace( new RegExp("123tlpbkey123", 'g'), key ));
	post_type_action();
	duplicate_post_value();
	//load(ajaxurl,{action:'create_pb_post_html',key:key},function(m){});
	jQuery( document ).tooltip();
	jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);	

});

$(document).on('click','.tl_pb_sec_post_sett',function(){

	var id = $(this).closest('.tl-pb-section').attr('id'),

		sec_classes = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('class'),

		sec_id = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-id'),

		sec_title = $(this).closest('.tl-pb-section').find('.tl-pb-section-black-bar-title').text(),

		bgColor = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundcolor'),

		bgImageid = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundimageid'),

		bgUrl = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundimageUrl'),
		
		section_id = $(this).closest('.tl-pb-section').find('[name="'+id+'-id"]').val();
		
		var parallax = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-parallax');
		if(parallax=='1'){
			var selectie = 'checked="checked"';
		}else{
			var selectie = '';
		}

		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop','data-sid':id}).html(

				'<div class="row"><div class="tl-pb-pop-col"><input type="text" name="sec_title" class="form-control" placeholder="Enter section title" value="'+sec_title+'"/></div></div>'+				

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML classes</label><input type="text" name="sec_classes" class="form-control" value="'+sec_classes.replace('tl-pb-section-content','')+'"/></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML ID</label><input type="text" name="sec_id" class="form-control" value="'+section_id+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Background Color</label> <input type="text" name="sec_bgcolor" class="my-color-picker" value="'+bgColor+'"/> </div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Background Image</label> <div data-bgUrl="'+bgUrl+'" data-attachId="'+bgImageid+'" data-title="Set Image" class="tl-pb-section-post-background"><i class="fa fa-picture-o" aria-hidden="true"></i></div> </div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"> <input type="checkbox" name="background_parallax" value="1" '+selectie+'>Use Background Image as Parallax</div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><button type="button" class="button button-primary button-large" id="btn_tl_pb_sec_post_sett">Change</button></div></div>'

			)

		);

		if(bgImageid!=''){

			$('#tl-pb-popup-common').find('.tl-pb-section-post-background').css('background-image', 'url(' + bgUrl + ')').find('.fa').removeClass('fa-picture-o').addClass('fa-times-circle');

		}
		
		jQuery(document).ready(function(e) {
            jQuery('.my-color-picker').wpColorPicker();
        });

		tb_show("Configure Section", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );

});

$(document).on('click','.tl-pb-section-post-background',function(evt){

	var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid');

	var $this = $(this),

		frame = frame || {}, props, image;

		// If the media frame already exists, reopen it.

			if ('function' === typeof frame.open) {

				frame.open();

				return;

			}

			// Create the media frame.

			frame = wp.media.frames.frame = wp.media({

				title: $this.data('title'),

				className: 'media-frame tl-pb-builder-uploader',

				button: {

					text: $this.data('buttonText')

				},

				multiple: false

			});



			// When an image is selected, run a callback.

			frame.on('select', function () {

				// We set multiple to false so only get one image from the uploader

				var attachment = frame.state().get('selection').first().toJSON();



				// Remove the attachment caption

				attachment.caption = '';



				// Build the image

				props = wp.media.string.props(

					{},

					attachment

				);

				$this.css('background-image', 'url(' + attachment.url + ')');

				$this.attr('data-attachId', attachment.id);

				$this.attr('data-bgUrl', attachment.url);

				$this.find('.fa').removeClass('fa-picture-o').addClass('fa-times-circle');

				//$this.append('<input type="hidden" name="" value="'+attachment.url+'">');

				//$(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundimageid',attachment.id);

				

			});



			// Finally, open the modal

			frame.open();

});	

$(document).on('click','#btn_tl_pb_sec_post_sett',function(){ 

		var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid');

		var sec_clases = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_classes"]').val();	
		
		var sec_id = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_id"]').val();			

		$('#'+id).find('.tl-pb-section-black-bar-title').text($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val());

		var title = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val();

        if(sec_clases!='') $('#'+id).find('.tl-pb-section-content').addClass(sec_clases);

        var bgColor = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_bgcolor"]').val(),

	        bgImageid = $(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-section-post-background').attr('data-attachId'),

			bgUrl = $(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-section-post-background').attr('data-bgUrl'); 

        $('#'+id).find('.tl-pb-section-content').attr('data-backgoundcolor',bgColor);

        $('#'+id).find('.tl-pb-section-content').attr('data-backgoundimageid',bgImageid);

        $('#'+id).find('.tl-pb-section-content').attr('data-backgoundimageurl',bgUrl);

		$('#'+id).find('[name="'+id+'-title"]').val(title);
		$('#'+id).find('[name="'+id+'-class"]').val(sec_clases);
		$('#'+id).find('[name="'+id+'-id"]').val(sec_id);
		$('#'+id).find('[name="'+id+'-background-color"]').val(bgColor);
		$('#'+id).find('[name="'+id+'-background-image"]').val(bgUrl);
		var parallax = $(this).closest('.tl-pb-sec-sett-pop').find('[name="background_parallax"]:checked').val();
		if(parallax){
				$('#'+id).find('.tl-pb-section-content').attr('data-parallax',parallax);
				$('#'+id+'-parallax').val(parallax);
			}else{
				$('#'+id).find('.tl-pb-section-content').attr('data-parallax','0');
				$('#'+id+'-parallax').val('0');
			}
		

        $('#tl-pb-popup-common').html('').hide();   

		tb_remove();

	});			

	$(document).on('click','.tl-pb-expand-link',function(){

		//console.log(tinyMCE.execCommand('mceAddControl', false, 'tinymce_data'));

		$(this).data('expanded', ($(this).data('expanded') == 'true' ? 'false' : 'true'));

		//$(this).find('.fa-caret-up').toggleClass('fa-caret-down');

		if($(this).data('expanded') == 'false'){
			$(this).find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-up');
			$(this).closest('a').attr('title','Collapse this section');
			$(this).closest('.tl-pb-section').find('.tl-pb-section-content').slideDown();
		}else{
			$(this).find('.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
			$(this).closest('a').attr('title','Expand this section');
			$(this).closest('.tl-pb-section').find('.tl-pb-section-content').slideUp();
		}

	});
	
	$(document).on('click','#tl-pb-menu-item-developer',function(){
	var developer = $('.developer_div').html();
	var key = tl_pb_genarate();

	$('<div id="'+key+'" class="tl-pb-section" data-sectype="post"><div class="tl-pb-section-black-bar"><div class="tl-pb-left-bar"><h4><span class="tl-pb-section-black-bar-title"></span> Developer Mode</h4></div><div class="tl-pb-right-bar"><a class="tl_pb_sec_sett" href="javascript:;" title="Section Settings"><i class="fa fa-cog" aria-hidden="true"></i></a><a class="tl_pb_sec_dup" href="javascript:;" title="Duplicate this section"><i class="fa fa-clone" aria-hidden="true"></i></a><a class="tl_pb_sec_del" href="javascript:;" title="Delete this section"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="tl-pb-expand-sec"><a data-expanded="true" class="tl-pb-expand-link" href="javascript:;" title="Collapse this section"><i class="fa fa-caret-up" aria-hidden="true"></i></a></div><div class="clear"></div></div><div class="tl-pb-section-content" data-backgoundimageid="" data-backgoundimageUrl="" data-backgoundcolor=""><div class="tl-pb-row tl-pb-post-row row"></div></div><input type="hidden" name="tlpb[key][]" value="'+key+'"><input type="hidden" name="tlpb['+key+'][type]" value="developer"><input type="hidden" name="tlpb['+key+'][title]" id="'+key+'-title"><input type="hidden" name="tlpb['+key+'][title_position]" id="'+key+'-title-position"><input type="hidden" name="tlpb['+key+'][subtitle]" id="'+key+'-subtitle"><input type="hidden" name="tlpb['+key+'][subtitle_position]" id="'+key+'-subtitle-position"><input type="hidden" name="tlpb['+key+'][title_color]" id="'+key+'-title-color"><input type="hidden" name="tlpb['+key+'][subtitle_color]" id="'+key+'-subtitle-color"><input type="hidden" name="tlpb['+key+'][content_color]" id="'+key+'-content-color">'+
		'<input type="hidden" name="tlpb['+key+'][margin_top]" id="'+key+'-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_bottom]" id="'+key+'-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_left]" id="'+key+'-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][margin_right]" id="'+key+'-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_top]" id="'+key+'-custom-margin-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_bottom]" id="'+key+'-custom-margin-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_left]" id="'+key+'-custom-margin-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_margin_right]" id="'+key+'-custom-margin-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_top]" id="'+key+'-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_bottom]" id="'+key+'-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_left]" id="'+key+'-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][padding_right]" id="'+key+'-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_top]" id="'+key+'-custom-padding-top" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_bottom]" id="'+key+'-custom-padding-bottom" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_left]" id="'+key+'-custom-padding-left" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][custom_padding_right]" id="'+key+'-custom-padding-right" value="0">'+
		'<input type="hidden" name="tlpb['+key+'][title_tag]" id="'+key+'-title-tag" value="h2">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_tag]" id="'+key+'-subtitle-tag" value="h3">'+
		'<input type="hidden" name="tlpb['+key+'][title_color_class]" id="'+key+'-title-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation]" id="'+key+'-title-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_delay]" id="'+key+'-title-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][title_animation_duration]" id="'+key+'-title-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_color_class]" id="'+key+'-subtitle-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation]" id="'+key+'-subtitle-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_delay]" id="'+key+'-subtitle-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][subtitle_animation_duration]" id="'+key+'-subtitle-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_color_class]" id="'+key+'-content-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation]" id="'+key+'-content-animation" value="no">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_delay]" id="'+key+'-content-animation-delay" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][content_animation_duration]" id="'+key+'-content-animation-duration" value="200ms">'+
		'<input type="hidden" name="tlpb['+key+'][background_color_class]" id="'+key+'-background-color-class" value="default">'+
		'<input type="hidden" name="tlpb['+key+'][class]" id="'+key+'-class"><input type="hidden" name="tlpb['+key+'][id]" id="'+key+'-id"><input type="hidden" name="tlpb['+key+'][background_color]" id="'+key+'-background-color"><input type="hidden" name="tlpb['+key+'][background_image]" id="'+key+'-background-image"><input type="hidden" name="tlpb['+key+'][parallax]" id="'+key+'-parallax" value="0"></div>').appendTo($('.tl-pb-stage'));
		$("#"+key).find('.tl-pb-post-row').html(developer.replace( new RegExp("123tlpbkey123", 'g'), key ));
		 /*$.ajax({
            type: 'GET',
            url: ajaxurl,
            data: {
                action: 'create_content_area',
				key:key
            },
            success: function(response){
                $("#"+key).find('.tl-pb-post-row').html(response);
            }
        });*/
	jQuery( document ).tooltip();
	jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);
	
});

	$(document).on('click','.tl_pb_sec_gal_sett',function(){

		var id = $(this).closest('.tl-pb-section').attr('id'),

			col_len = $(this).closest('.tl-pb-section').find('.tl-pb-col:visible').length,

			sm =  $(this).closest('.tl-pb-section').find('.tl-pb-col:first').attr('class').replace('tl-pb-col','').trim(); 

			sec_classes = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('class'),

			sec_id = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-id'),

			sec_title = $(this).closest('.tl-pb-section').find('.tl-pb-section-black-bar-title').text(),

			opt_htm='',opt_val = 1; 

			if (typeof sec_id !== typeof undefined && sec_id !== false) {

				var section_id = sec_id;

			}else{

				var section_id = '';

			}

			if(sm!=''){

				var smV = sm.replace('col-sm-','');

				opt_val = 12/parseInt(smV);

			}

			for(var i=1;i<=4;i++){

				opt_htm+='<option '+(opt_val==i ? 'selected="selected"' : "")+' value="'+i+'">'+i+'</option>';

			} 

		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop','data-sid':id}).html(

				'<div class="row"><div class="tl-pb-pop-col"><input type="text" name="sec_title" class="form-control" placeholder="Enter section title" value="'+sec_title+'"/></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Columns</label><select class="tl-pb-col-list pull-right" name="sec_column">'+opt_htm+'</select></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML Classes</label><input type="text" name="sec_classes" class="form-control" value="'+sec_classes.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML ID</label><input type="text" name="sec_id" class="form-control" value="'+section_id.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><button type="button" class="button button-primary button-large" id="btn_tl_pb_sec_gal_sett">Change</button></div></div>'

			)

		);

		tb_show("Configure Section", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );

	});

$(document).on('click','#btn_tl_pb_sec_gal_sett',function(){

		var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid'),

			sec_column = parseInt($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_column"]').val());

		var col_len = $('#'+id).find('.tl-pb-col:visible').length;

			col_len = parseInt(col_len),

			sm = 12/sec_column,

			sec_clases = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_classes"]').val();

			sec_id = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_id"]').val();

		$('#'+id).find('.tl-pb-section-black-bar-title').text($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val());

		/*if(sec_column>col_len){

			var c = sec_column-col_len;

			$('#'+id).find('.tl-pb-row').append(TlPb.colColumnHTML(c));

		}else{

			sec_column=sec_column-1;

			$('#'+id).find(".tl-pb-col:gt("+sec_column+")").remove();

		}*/

		$('#'+id).find(".tl-pb-col").attr('class', function(i, c) {

                return c.replace(/(^|\s)col-sm-\S+/g, ' col-sm-'+sm);

            });

            if(sec_clases!='') $('#'+id).find('.tl-pb-section-content').addClass(sec_clases);  

			if(sec_id!='') $('#'+id).find('.tl-pb-section-content').attr('data-id',sec_id); 

        $('#tl-pb-popup-common').html('').hide();   

		tb_remove();

	});

	$(document).on('click','.tl_pb_sec_ban_sett',function(){

		var id = $(this).closest('.tl-pb-section').attr('id'),

			sec_classes = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('class'),

			sec_id = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-id'),

			sec_title = $(this).closest('.tl-pb-section').find('.tl-pb-section-black-bar-title').text(),

			opt_htm='',opt_val = 1; 

			if (typeof sec_id !== typeof undefined && sec_id !== false) {

				var section_id = sec_id;

			}else{

				var section_id = '';

			}

		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop','data-sid':id}).html(

				'<div class="row"><div class="tl-pb-pop-col"><input type="text" name="sec_title" class="form-control" placeholder="Enter section title" value="'+sec_title+'"/></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML classes</label><input type="text" name="sec_classes" class="form-control" value="'+sec_classes.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML ID</label><input type="text" name="sec_id" class="form-control" value="'+section_id.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><button type="button" class="button button-primary button-large" id="btn_tl_pb_sec_ban_sett">Change</button></div></div>'

			)

		);

		tb_show("Configure Section", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );

	});

$(document).on('click','#btn_tl_pb_sec_ban_sett',function(){

		var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid'),

			sec_column = parseInt($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_column"]').val());

		var col_len = $('#'+id).find('.tl-pb-col:visible').length;

			col_len = parseInt(col_len),

			sm = 12/sec_column,

			sec_clases = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_classes"]').val();

			sec_id = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_id"]').val();

		$('#'+id).find('.tl-pb-section-black-bar-title').text($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val());

		var title = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val();

		$('#'+id+'-title').val(title);

		$('#'+id+'-class').val(sec_clases);

		$('#'+id+'-id').val(sec_id);
		

		/*if(sec_column>col_len){

			var c = sec_column-col_len;

			$('#'+id).find('.tl-pb-row').append(TlPb.colColumnHTML(c));

		}else{

			sec_column=sec_column-1;

			$('#'+id).find(".tl-pb-col:gt("+sec_column+")").remove();

		}*/

		$('#'+id).find(".tl-pb-col").attr('class', function(i, c) {

                return c.replace(/(^|\s)col-sm-\S+/g, ' col-sm-'+sm);

            });

            if(sec_clases!='') $('#'+id).find('.tl-pb-section-content').addClass(sec_clases);  

			if(sec_id!='') $('#'+id).find('.tl-pb-section-content').attr('data-id',sec_id); 

        $('#tl-pb-popup-common').html('').hide();   

		tb_remove();

	});

	
	$(document).on('click','.tl_pb_sec_sett',function(){

		var id = $(this).closest('.tl-pb-section').attr('id'),

			sec_classes = $(this).closest('.tl-pb-section').find('#'+id+'-class').val(),

			sec_id = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-id'),

			sec_title = $(this).closest('.tl-pb-section').find('.tl-pb-section-black-bar-title').text(),
			
			sec_title_position = $(this).closest('.tl-pb-section').find('#'+id+'-title-position').val(),
			
			sec_subtitle = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle').val(),
			
			sec_subtitle_position = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-position').val(),
			
			sec_title_color = $(this).closest('.tl-pb-section').find('#'+id+'-title-color').val(),
			
			sec_subtitle_color = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-color').val(),
			
			sec_content_color = $(this).closest('.tl-pb-section').find('#'+id+'-content-color').val(),
			
			bgColor = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundcolor'),

			bgImageid = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundimageid'),
	
			bgUrl = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-backgoundimageUrl'),
			
			margin_top = $(this).closest('.tl-pb-section').find('#'+id+'-margin-top').val(),
			margin_bottom = $(this).closest('.tl-pb-section').find('#'+id+'-margin-bottom').val(),
			margin_left = $(this).closest('.tl-pb-section').find('#'+id+'-margin-left').val(),
			margin_right = $(this).closest('.tl-pb-section').find('#'+id+'-margin-right').val(),
			custom_margin_top = $(this).closest('.tl-pb-section').find('#'+id+'-custom-margin-top').val(),
			custom_margin_bottom = $(this).closest('.tl-pb-section').find('#'+id+'-custom-margin-bottom').val(),
			custom_margin_left = $(this).closest('.tl-pb-section').find('#'+id+'-custom-margin-left').val(),
			custom_margin_right = $(this).closest('.tl-pb-section').find('#'+id+'-custom-margin-right').val(),
			padding_top = $(this).closest('.tl-pb-section').find('#'+id+'-padding-top').val(),
			padding_bottom = $(this).closest('.tl-pb-section').find('#'+id+'-padding-bottom').val(),
			padding_left = $(this).closest('.tl-pb-section').find('#'+id+'-padding-left').val(),
			padding_right = $(this).closest('.tl-pb-section').find('#'+id+'-padding-right').val(),
			custom_padding_top = $(this).closest('.tl-pb-section').find('#'+id+'-custom-padding-top').val(),
			custom_padding_bottom = $(this).closest('.tl-pb-section').find('#'+id+'-custom-padding-bottom').val(),
			custom_padding_left = $(this).closest('.tl-pb-section').find('#'+id+'-custom-padding-left').val(),
			custom_padding_right = $(this).closest('.tl-pb-section').find('#'+id+'-custom-padding-right').val(),
			title_tag = $(this).closest('.tl-pb-section').find('#'+id+'-title-tag').val(),
			subtitle_tag = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-tag').val(),
			
			title_color_class = $(this).closest('.tl-pb-section').find('#'+id+'-title-color-class').val(),
			title_animation = $(this).closest('.tl-pb-section').find('#'+id+'-title-animation').val(),
			title_animation_delay = $(this).closest('.tl-pb-section').find('#'+id+'-title-animation-delay').val(),
			title_animation_duration = $(this).closest('.tl-pb-section').find('#'+id+'-title-animation-duration').val(),
			subtitle_color_class = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-color-class').val(),
			subtitle_animation = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-animation').val(),
			subtitle_animation_delay = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-animation-delay').val(),
			subtitle_animation_duration = $(this).closest('.tl-pb-section').find('#'+id+'-subtitle-animation-duration').val(),
			content_color_class = $(this).closest('.tl-pb-section').find('#'+id+'-content-color-class').val(),
			content_animation = $(this).closest('.tl-pb-section').find('#'+id+'-content-animation').val(),
			content_animation_delay = $(this).closest('.tl-pb-section').find('#'+id+'-content-animation-delay').val(),
			content_animation_duration = $(this).closest('.tl-pb-section').find('#'+id+'-content-animation-duration').val(),
			background_color_class = $(this).closest('.tl-pb-section').find('#'+id+'-background-color-class').val();
			
			if(typeof sec_title == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title" name="tlpb['+id+'][title]" type="hidden">');
				var sec_title = $('[name="tlpb['+id+'][title]"]').val();
			}
			if(typeof sec_title_position == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-position" name="tlpb['+id+'][title_position]" type="hidden">');
				var sec_title_position = $('[name="tlpb['+id+'][title_position]"]').val();
			}
			if(typeof sec_subtitle == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle" name="tlpb['+id+'][subtitle]" type="hidden">');
				var sec_subtitle = $('[name="tlpb['+id+'][subtitle]"]').val();
			}
			if(typeof sec_subtitle_position == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-position" name="tlpb['+id+'][subtitle_position]" type="hidden">');
				var sec_subtitle_position = $('[name="tlpb['+id+'][subtitle_position]"]').val();
			}
			if(typeof sec_title_color == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-color" name="tlpb['+id+'][title_color]" type="hidden">');
				var sec_title_color = $('[name="tlpb['+id+'][title_color]"]').val();
			}
			if(typeof sec_subtitle_color == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-color" name="tlpb['+id+'][subtitle_color]" type="hidden">');
				var sec_subtitle_color = $('[name="tlpb['+id+'][subtitle_color]"]').val();
			}
			if(typeof sec_content_color == 'undefined'){
				$('#'+id).append('<input id="'+id+'-content-color" name="tlpb['+id+'][content_color]" type="hidden">');
				var sec_content_color = $('[name="tlpb['+id+'][content_color]"]').val();
			}
			if(typeof margin_top == 'undefined'){
				$('#'+id).append('<input id="'+id+'-margin-top" name="tlpb['+id+'][margin_top]" type="hidden" value="0">');
				var margin_top = $('[name="tlpb['+id+'][margin_top]"]').val();
			}
			if(typeof margin_bottom == 'undefined'){
				$('#'+id).append('<input id="'+id+'-margin-bottom" name="tlpb['+id+'][margin_bottom]" type="hidden" value="0">');
				var margin_bottom = $('[name="tlpb['+id+'][margin_bottom]"]').val();
			}
			if(typeof margin_left == 'undefined'){
				$('#'+id).append('<input id="'+id+'-margin-left" name="tlpb['+id+'][margin_left]" type="hidden" value="0">');
				var margin_left = $('[name="tlpb['+id+'][margin_left]"]').val();
			}
			if(typeof margin_right == 'undefined'){
				$('#'+id).append('<input id="'+id+'-margin-right" name="tlpb['+id+'][margin_right]" type="hidden" value="0">');
				var margin_right = $('[name="tlpb['+id+'][margin_right]"]').val();
			}
			if(typeof custom_margin_top == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-margin-top" name="tlpb['+id+'][custom_margin_top]" type="hidden" value="0">');
				var custom_margin_top = $('[name="tlpb['+id+'][custom_margin_top]"]').val();
			}
			if(typeof custom_margin_bottom == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-margin-bottom" name="tlpb['+id+'][custom_margin_bottom]" type="hidden" value="0">');
				var custom_margin_bottom = $('[name="tlpb['+id+'][custom_margin_bottom]"]').val();
			}
			if(typeof custom_margin_left == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-margin-left" name="tlpb['+id+'][custom_margin_left]" type="hidden" value="0">');
				var custom_margin_left = $('[name="tlpb['+id+'][custom_margin_left]"]').val();
			}
			if(typeof custom_margin_right == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-margin-right" name="tlpb['+id+'][custom_margin_right]" type="hidden" value="0">');
				var custom_margin_right = $('[name="tlpb['+id+'][custom_margin_right]"]').val();
			}
			if(typeof padding_top == 'undefined'){
				$('#'+id).append('<input id="'+id+'-padding-top" name="tlpb['+id+'][padding_top]" type="hidden" value="0">');
				var padding_top = $('[name="tlpb['+id+'][padding_top]"]').val();
			}
			if(typeof padding_bottom == 'undefined'){
				$('#'+id).append('<input id="'+id+'-padding-bottom" name="tlpb['+id+'][padding_bottom]" type="hidden" value="0">');
				var padding_bottom = $('[name="tlpb['+id+'][padding_bottom]"]').val();
			}
			if(typeof padding_left == 'undefined'){
				$('#'+id).append('<input id="'+id+'-padding-left" name="tlpb['+id+'][padding_left]" type="hidden" value="0">');
				var padding_left = $('[name="tlpb['+id+'][padding_left]"]').val();
			}
			if(typeof padding_right == 'undefined'){
				$('#'+id).append('<input id="'+id+'-padding-right" name="tlpb['+id+'][padding_right]" type="hidden" value="0">');
				var padding_right = $('[name="tlpb['+id+'][padding_right]"]').val();
			}
			if(typeof custom_padding_top == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-padding-top" name="tlpb['+id+'][custom_padding_top]" type="hidden" value="0">');
				var custom_padding_top = $('[name="tlpb['+id+'][custom_padding_top]"]').val();
			}
			if(typeof custom_padding_bottom == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-padding-bottom" name="tlpb['+id+'][custom_padding_bottom]" type="hidden" value="0">');
				var custom_padding_bottom = $('[name="tlpb['+id+'][custom_padding_bottom]"]').val();
			}
			if(typeof custom_padding_left == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-padding-left" name="tlpb['+id+'][custom_padding_left]" type="hidden" value="0">');
				var custom_padding_left = $('[name="tlpb['+id+'][custom_padding_left]"]').val();
			}
			if(typeof custom_padding_right == 'undefined'){
				$('#'+id).append('<input id="'+id+'-custom-padding-right" name="tlpb['+id+'][custom_padding_right]" type="hidden" value="0">');
				var custom_padding_right = $('[name="tlpb['+id+'][custom_padding_right]"]').val();
			}
			if(typeof title_tag == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-tag" name="tlpb['+id+'][title_tag]" type="hidden" value="h2">');
				var title_tag = $('[name="tlpb['+id+'][title_tag]"]').val();
			}
			if(typeof subtitle_tag == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-tag" name="tlpb['+id+'][subtitle_tag]" type="hidden" value="h3">');
				var subtitle_tag = $('[name="tlpb['+id+'][subtitle_tag]"]').val();
			}
			if(typeof title_color_class == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-color-class" name="tlpb['+id+'][title_color_class]" type="hidden" value="default">');
				var title_color_class = $('[name="tlpb['+id+'][title_color_class]"]').val();
			}
			if(typeof title_animation == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-animation" name="tlpb['+id+'][title_animation]" type="hidden" value="no">');
				var title_animation = $('[name="tlpb['+id+'][title_animation]"]').val();
			}
			if(typeof title_animation_delay == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-animation-delay" name="tlpb['+id+'][title_animation_delay]" type="hidden" value="200ms">');
				var title_animation_delay = $('[name="tlpb['+id+'][title_animation_delay]"]').val();
			}
			if(typeof title_animation_duration == 'undefined'){
				$('#'+id).append('<input id="'+id+'-title-animation-duration" name="tlpb['+id+'][title_animation_duration]" type="hidden" value="200ms">');
				var title_animation_duration = $('[name="tlpb['+id+'][title_animation_duration]"]').val();
			}
			if(typeof subtitle_color_class == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-color-class" name="tlpb['+id+'][subtitle_color_class]" type="hidden" value="default">');
				var subtitle_color_class = $('[name="tlpb['+id+'][subtitle_color_class]"]').val();
			}
			if(typeof subtitle_animation == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-animation" name="tlpb['+id+'][subtitle_animation]" type="hidden" value="no">');
				var subtitle_animation = $('[name="tlpb['+id+'][subtitle_animation]"]').val();
			}
			if(typeof subtitle_animation_delay == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-animation-delay" name="tlpb['+id+'][subtitle_animation_delay]" type="hidden" value="200ms">');
				var subtitle_animation_delay = $('[name="tlpb['+id+'][subtitle_animation_delay]"]').val();
			}
			if(typeof subtitle_animation_duration == 'undefined'){
				$('#'+id).append('<input id="'+id+'-subtitle-animation-duration" name="tlpb['+id+'][subtitle_animation_duration]" type="hidden" value="200ms">');
				var subtitle_animation_duration = $('[name="tlpb['+id+'][subtitle_animation_duration]"]').val();
			}
			if(typeof content_color_class == 'undefined'){
				$('#'+id).append('<input id="'+id+'-content-color-class" name="tlpb['+id+'][content_color_class]" type="hidden" value="default">');
				var content_color_class = $('[name="tlpb['+id+'][content_color_class]"]').val();
			}
			if(typeof content_animation == 'undefined'){
				$('#'+id).append('<input id="'+id+'-content-animation" name="tlpb['+id+'][content_animation]" type="hidden" value="no">');
				var content_animation = $('[name="tlpb['+id+'][content_animation]"]').val();
			}
			if(typeof content_animation_delay == 'undefined'){
				$('#'+id).append('<input id="'+id+'-content-animation-delay" name="tlpb['+id+'][content_animation_delay]" type="hidden" value="200ms">');
				var content_animation_delay = $('[name="tlpb['+id+'][content_animation_delay]"]').val();
			}
			if(typeof content_animation_duration == 'undefined'){
				$('#'+id).append('<input id="'+id+'-content-animation-duration" name="tlpb['+id+'][content_animation_duration]" type="hidden" value="200ms">');
				var content_animation_duration = $('[name="tlpb['+id+'][content_animation_duration]"]').val();
			}
			if(typeof background_color_class == 'undefined'){
				$('#'+id).append('<input id="'+id+'-background-color-class" name="tlpb['+id+'][background_color_class]" type="hidden" value="default">');
				var background_color_class = $('[name="tlpb['+id+'][background_color_class]"]').val();
			}
			if(typeof sec_classes == 'undefined'){
				$('#'+id).append('<input id="'+id+'-class" name="tlpb['+id+'][class]" type="hidden" value="">');
				var sec_classes = $('[name="tlpb['+id+'][class]"]').val();
			}
			if(typeof sec_id == 'undefined'){
				$('#'+id).append('<input id="'+id+'-id" name="tlpb['+id+'][id]" type="hidden" value="">');
				var sec_id = $('[name="tlpb['+id+'][id]"]').val();
			}
			if(typeof bgColor == 'undefined'){
				$('#'+id).append('<input id="'+id+'-background-color" name="tlpb['+id+'][background_color]" type="hidden" value="">');
				var bgColor = $('[name="tlpb['+id+'][background_color]"]').val();
			}
			if(typeof bgUrl == 'undefined'){
				$('#'+id).append('<input id="'+id+'-background-image" name="tlpb['+id+'][background_image]" type="hidden" value="">');
				var bgUrl = $('[name="tlpb['+id+'][background-image]"]').val();
			}
			
			
			var parallax = $(this).closest('.tl-pb-section').find('.tl-pb-section-content').attr('data-parallax');
			if(parallax=='1'){
				var selectie = 'checked="checked"';
			}else{
				var selectie = '';
			}

			var opt_htm='';

			if (typeof sec_id !== typeof undefined && sec_id !== false) {

				var section_id = sec_id;

			}else{

				var section_id = '';

			}
			
			var margin_padding_opt = '';
			for(var i=0; i<=40; i+=5){
				margin_padding_opt+= '<option value="'+i+'">'+i+'</option>'; 
			}
			margin_padding_opt+= '<option value="custom">Custom</option>';
			
			var header_opt = '';
			for(var i=1; i<=6; i++){
				header_opt += '<option value="h'+i+'">H'+i+'</option>';
			}

		var animatehtml = '<option value="no">No Animation</option>'
		+'<optgroup label="Attention Seekers">'
		  +'<option value="bounce">bounce</option>'
          +'<option value="flash">flash</option>'
          +'<option value="pulse">pulse</option>'
          +'<option value="rubberBand">rubberBand</option>'
          +'<option value="shake">shake</option>'
          +'<option value="swing">swing</option>'
          +'<option value="tada">tada</option>'
          +'<option value="wobble">wobble</option>'
          +'<option value="jello">jello</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Entrances">'
          +'<option value="bounceIn">bounceIn</option>'
          +'<option value="bounceInDown">bounceInDown</option>'
          +'<option value="bounceInLeft">bounceInLeft</option>'
          +'<option value="bounceInRight">bounceInRight</option>'
          +'<option value="bounceInUp">bounceInUp</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Exits">'
          +'<option value="bounceOut">bounceOut</option>'
          +'<option value="bounceOutDown">bounceOutDown</option>'
          +'<option value="bounceOutLeft">bounceOutLeft</option>'
          +'<option value="bounceOutRight">bounceOutRight</option>'
          +'<option value="bounceOutUp">bounceOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Entrances">'
          +'<option value="fadeIn">fadeIn</option>'
          +'<option value="fadeInDown">fadeInDown</option>'
          +'<option value="fadeInDownBig">fadeInDownBig</option>'
          +'<option value="fadeInLeft">fadeInLeft</option>'
          +'<option value="fadeInLeftBig">fadeInLeftBig</option>'
          +'<option value="fadeInRight">fadeInRight</option>'
          +'<option value="fadeInRightBig">fadeInRightBig</option>'
          +'<option value="fadeInUp">fadeInUp</option>'
          +'<option value="fadeInUpBig">fadeInUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Exits">'
          +'<option value="fadeOut">fadeOut</option>'
          +'<option value="fadeOutDown">fadeOutDown</option>'
          +'<option value="fadeOutDownBig">fadeOutDownBig</option>'
          +'<option value="fadeOutLeft">fadeOutLeft</option>'
          +'<option value="fadeOutLeftBig">fadeOutLeftBig</option>'
          +'<option value="fadeOutRight">fadeOutRight</option>'
          +'<option value="fadeOutRightBig">fadeOutRightBig</option>'
          +'<option value="fadeOutUp">fadeOutUp</option>'
          +'<option value="fadeOutUpBig">fadeOutUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Flippers">'
          +'<option value="flip">flip</option>'
          +'<option value="flipInX">flipInX</option>'
          +'<option value="flipInY">flipInY</option>'
          +'<option value="flipOutX">flipOutX</option>'
          +'<option value="flipOutY">flipOutY</option>'
        +'</optgroup>'

        +'<optgroup label="Lightspeed">'
          +'<option value="lightSpeedIn">lightSpeedIn</option>'
          +'<option value="lightSpeedOut">lightSpeedOut</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Entrances">'
          +'<option value="rotateIn">rotateIn</option>'
          +'<option value="rotateInDownLeft">rotateInDownLeft</option>'
          +'<option value="rotateInDownRight">rotateInDownRight</option>'
          +'<option value="rotateInUpLeft">rotateInUpLeft</option>'
          +'<option value="rotateInUpRight">rotateInUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Exits">'
          +'<option value="rotateOut">rotateOut</option>'
          +'<option value="rotateOutDownLeft">rotateOutDownLeft</option>'
          +'<option value="rotateOutDownRight">rotateOutDownRight</option>'
          +'<option value="rotateOutUpLeft">rotateOutUpLeft</option>'
          +'<option value="rotateOutUpRight">rotateOutUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Sliding Entrances">'
          +'<option value="slideInUp">slideInUp</option>'
          +'<option value="slideInDown">slideInDown</option>'
          +'<option value="slideInLeft">slideInLeft</option>'
          +'<option value="slideInRight">slideInRight</option>'
        +'</optgroup>'
		
        +'<optgroup label="Sliding Exits">'
          +'<option value="slideOutUp">slideOutUp</option>'
          +'<option value="slideOutDown">slideOutDown</option>'
          +'<option value="slideOutLeft">slideOutLeft</option>'
          +'<option value="slideOutRight">slideOutRight</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Entrances">'
          +'<option value="zoomIn">zoomIn</option>'
          +'<option value="zoomInDown">zoomInDown</option>'
          +'<option value="zoomInLeft">zoomInLeft</option>'
          +'<option value="zoomInRight">zoomInRight</option>'
          +'<option value="zoomInUp">zoomInUp</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Exits">'
          +'<option value="zoomOut">zoomOut</option>'
          +'<option value="zoomOutDown">zoomOutDown</option>'
          +'<option value="zoomOutLeft">zoomOutLeft</option>'
          +'<option value="zoomOutRight">zoomOutRight</option>'
          +'<option value="zoomOutUp">zoomOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Specials">'
          +'<option value="hinge">hinge</option>'
          +'<option value="rollIn">rollIn</option>'
          +'<option value="rollOut">rollOut</option>'
        +'</optgroup>';
		var delay_html = '';
		for(var i=200;i<=2000;i=i+200){
			delay_html += '<option value="'+i+'ms">'+i+'ms</option>';
		}
		var duration_html = '';
		for(var i=200;i<=2000;i=i+200){
			duration_html += '<option value="'+i+'ms">'+i+'ms</option>';
		}
		var colorscheme = '<option value="default">Default</option>'+
		'<option value="turquoise">Turquoise</option>'+
		'<option value="greensea">Green Sea</option>'+
		'<option value="sunflower">Sunflower</option>'+
		'<option value="orange">Orange</option>'+
		'<option value="emrald">Emrald</option>'+
		'<option value="nephritis">Nephritis</option>'+
		'<option value="carrot">Carrot</option>'+
		'<option value="pumpkin">Pumpkin</option>'+
		'<option value="peterriver">Peter River</option>'+
		'<option value="belizehole">Belize Hole</option>'+
		'<option value="alizarin">Alizarin</option>'+
		'<option value="pomegranate">Pomegranate</option>'+
		'<option value="amethyst">Amethyst</option>'+
		'<option value="wisteria">Wisteria</option>'+
		'<option value="wetasphalt">Wet Asphalt</option>'+
		'<option value="midnightblue">Midnight Blue</option>'+
		'<option value="brown">Brown</option>'+
		'<option value="cyan">Cyan</option>'+
		'<option value="teal">Teal</option>'+
		'<option value="grey">Grey</option>'+
		'<option value="green">Green</option>'+
		'<option value="lime">Lime</option>'+
		'<option value="yellow">Yellow</option>'+
		'<option value="amber">Amber</option>'+
		'<option value="darkgrey">Dark Grey</option>'+
		'<option value="mignightblack">Mignight Black</option>'+
		'<option value="black">Black</option>'+
		'<option value="white">White</option>'+
		'<option value="cloud">Cloud</option>'+
		'<option value="silver">Silver</option>'+
		'<option value="concrete">Concrete</option>'+
		'<option value="asbestos">Asbestos</option>';
		$('#tl-pb-popup-common').html(

			$('<div>').attr({'class':'tl-pb-sec-sett-pop','data-sid':id}).html(

				'<div class="row"><div class="tl-pb-pop-col"><label>Section Title</label><input type="text" name="sec_title" class="form-control" placeholder="Enter section title" value="'+sec_title+'"/></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Tag</label><select name="sec_title_tag" class="form-control">'+header_opt+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Position</label><select name="sec_title_position" class="form-control"><option value="">Default</option><option value="text-left">Left</option><option value="text-right">Right</option></select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Text Color Scheme</label> <select name="sec_title_color_scheme" class="form-control">'+colorscheme+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Text Color</label> <input type="text" name="sec_title_color" class="my-color-picker" value="'+sec_title_color+'"/> </div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Animation</label> <select name="sec_title_animation" class="form-control">'+animatehtml+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Animation Delay</label> <select name="sec_title_animation_delay" class="form-control">'+delay_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Title Animation Duration</label> <select name="sec_title_animation_duration" class="form-control">'+duration_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Section Subtitle</label><input type="text" name="sec_subtitle" class="form-control" placeholder="Enter section subtitle" value="'+sec_subtitle+'"/></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Tag</label><select name="sec_subtitle_tag" class="form-control">'+header_opt+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Position</label><select name="sec_subtitle_position" class="form-control"><option value="">Default</option><option value="text-left">Left</option><option value="text-right">Right</option></select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Text Color Scheme</label> <select name="sec_subtitle_color_scheme" class="form-control">'+colorscheme+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Text Color</label> <input type="text" name="sec_subtitle_color" class="my-color-picker" value="'+sec_subtitle_color+'"/> </div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Animation</label> <select name="sec_subtitle_animation" class="form-control">'+animatehtml+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Animation Delay</label> <select name="sec_subtitle_animation_delay" class="form-control">'+delay_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Subtitle Animation Duration</label> <select name="sec_subtitle_animation_duration" class="form-control">'+duration_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Content Area Primary Text Color Scheme</label> <select name="sec_color_scheme" class="form-control">'+colorscheme+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Content Area Primary Text Color</label> <input type="text" name="sec_content_color" class="my-color-picker" value="'+sec_content_color+'"/> </div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Content Area Animation</label> <select name="sec_animation" class="form-control">'+animatehtml+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Content Area Animation Delay</label> <select name="sec_animation_delay" class="form-control">'+delay_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Content Area Animation Duration</label> <select name="sec_animation_duration" class="form-control">'+duration_html+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><h3>Margin</h3></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col clearfix"><div class="margin-padding-selection"><label>Margin Top(px)</label><select name="section_margin_top" class="margin-padding" id="margin-top">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Margin Bottom(px)</label><select name="section_margin_bottom" class="margin-padding" id="margin-bottom">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Margin Left(px)</label><select name="section_margin_left" class="margin-padding" id="margin-left">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Margin Right(px)</label><select name="section_margin_right" class="margin-padding" id="margin-right">'+margin_padding_opt+'</select></div></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col clearfix"><div class="margin-padding-selection custom-margin-top" style="display:none;"><label>Margin Top(px)</label><input type="number" name="custom_margin_top" value="'+custom_margin_top+'"></div><div class="margin-padding-selection custom-margin-bottom" style="display:none;"><label>Margin Bottom(px)</label><input type="number" name="custom_margin_bottom" value="'+custom_margin_bottom+'"></div><div class="margin-padding-selection custom-margin-left" style="display:none;"><label>Margin Left(px)</label><input type="number" name="custom_margin_left" value="'+custom_margin_left+'"></div><div class="margin-padding-selection custom-margin-right" style="display:none;"><label>Margin Right(px)</label><input type="number" name="custom_margin_right" value="'+custom_margin_right+'"></div></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><h3>Padding</h3></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col clearfix"><div class="margin-padding-selection"><label>Padding Top(px)</label><select name="section_padding_top" class="margin-padding" id="padding-top">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Padding Bottom(px)</label><select name="section_padding_bottom" class="margin-padding" id="padding-bottom">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Padding Left(px)</label><select name="section_padding_left" class="margin-padding" id="padding-left">'+margin_padding_opt+'</select></div><div class="margin-padding-selection"><label>Padding Right(px)</label><select name="section_padding_right" class="margin-padding" id="padding-right">'+margin_padding_opt+'</select></div></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col clearfix"><div class="margin-padding-selection custom-padding-top" style="display:none;"><label>Padding Top(px)</label><input type="number" name="custom_padding_top" value="'+custom_padding_top+'"></div><div class="margin-padding-selection custom-padding-bottom" style="display:none;"><label>Padding Bottom(px)</label><input type="number" name="custom_padding_bottom" value="'+custom_padding_bottom+'"></div><div class="margin-padding-selection custom-padding-left" style="display:none;"><label>Padding Left(px)</label><input type="number" name="custom_padding_left" value="'+custom_padding_bottom+'"></div><div class="margin-padding-selection custom-padding-right" style="display:none;"><label>Padding Right(px)</label><input type="number" name="custom_padding_right" value="'+custom_padding_bottom+'"></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML classes</label><input type="text" name="sec_classes" class="form-control" value="'+sec_classes.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Section HTML ID</label><input type="text" name="sec_id" class="form-control" value="'+section_id.replace('tl-pb-section-content','')+'"/></div></div></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Color Scheme</label> <select name="sec_bgcolor_scheme" class="form-control">'+colorscheme+'</select></div></div>'+
				
				'<div class="row"><div class="tl-pb-pop-col"><label>Background Color</label> <input type="text" name="sec_bgcolor" class="my-color-picker" value="'+bgColor+'"/> </div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><label>Background Image</label> <div data-bgUrl="'+bgUrl+'" data-attachId="'+bgImageid+'" data-title="Set Image" class="tl-pb-section-post-background"><i class="fa fa-picture-o" aria-hidden="true"></i></div> </div></div>'+
								
				'<div class="row"><div class="tl-pb-pop-col"> <input type="checkbox" name="background_parallax" value="1" '+selectie+'>Use Background Image as Parallax </div></div>'+

				'<div class="row"><div class="tl-pb-pop-col"><button type="button" class="button button-primary button-large" id="btn_tl_pb_sec_sett">Change</button></div></div>'

			)

		);
		
		$('.margin-padding').change(function(){
			var custom_class = $(this).attr('id');
			var custom_opt = $(this).val();
			if(custom_opt=='custom'){
				$('.custom-'+custom_class).show();
			}else{
				$('.custom-'+custom_class).hide();
			}
		});
		
		$('#margin-top').val(margin_top).change();
		$('#margin-bottom').val(margin_bottom).change();
		$('#margin-left').val(margin_left).change();
		$('#margin-right').val(margin_right).change();
		
		$('#padding-top').val(padding_top).change();
		$('#padding-bottom').val(padding_bottom).change();
		$('#padding-left').val(padding_left).change();
		$('#padding-right').val(padding_right).change();
		
		$('[name="sec_title_tag"]').val(title_tag).change();
		$('[name="sec_subtitle_tag"]').val(subtitle_tag).change();
		
		$('[name="sec_title_position"]').val(sec_title_position).change();
		$('[name="sec_subtitle_position"]').val(sec_subtitle_position).change();
		
		$('[name="sec_title_color_scheme"]').val(title_color_class).change();
		$('[name="sec_title_animation"]').val(title_animation).change();
		$('[name="sec_title_animation_delay"]').val(title_animation_delay).change();
		$('[name="sec_title_animation_duration"]').val(title_animation_duration).change();
		$('[name="sec_subtitle_color_scheme"]').val(subtitle_color_class).change();
		$('[name="sec_subtitle_animation"]').val(subtitle_animation).change();
		$('[name="sec_subtitle_animation_delay"]').val(subtitle_animation_delay).change();
		$('[name="sec_subtitle_animation_duration"]').val(subtitle_animation_duration).change();
		$('[name="sec_color_scheme"]').val(content_color_class).change();
		$('[name="sec_animation"]').val(content_animation).change();
		$('[name="sec_animation_delay"]').val(content_animation_delay).change();
		$('[name="sec_animation_duration"]').val(content_animation_duration).change();
		$('[name="sec_bgcolor_scheme"]').val(background_color_class).change();
		
		var colno = $('.tl-pb-col-list').val();
		//$('.col-configure').load(ajaxurl,{col:colno,colconfig:col_len_config,action:'col_config'},function(m){});
		
		/*$( ".tl-pb-col-list" ).change(function() {
			var colno = $('.tl-pb-col-list').val();
			$('.col-configure').load(ajaxurl,{col:colno,colconfig:col_len_config,action:'col_config'},function(m){});
		});*/
		
		/*if($('.tl-pb-col-list').val()=='themelines_slider'){
			 $('.banner_type').load(ajaxurl,{action:'select_slider'},function(m){});
		  }else{
			  if($(this).val()=='other'){
			  		$('.banner_type').html('<label>Enter Shortcode</label><textarea name="shortcode" class="form-control" id="slidershortcode" name="shortcode"></textarea>');
			  }else{
				  	$('.banner_type').html('');
			  }
		  }*/
		
		if(bgImageid!=''){
			$('#tl-pb-popup-common').find('.tl-pb-section-post-background').css('background-image', 'url(' + bgUrl + ')').find('.fa').removeClass('fa-picture-o').addClass('fa-times-circle');

		}
		
		jQuery(document).ready(function(e) {
            jQuery('.my-color-picker').wpColorPicker();
        });
		
		
		tb_show("Configure Section", "#TB_inline?height=502&width=400&inlineId=tl-pb-popup-common");

		m7_resize_thickbox(); 

		jQuery(window).on( 'resize', m7_resize_thickbox );

	});
	
	$(document).on('click','.tl_pb_ban_sett',function(){
		
		var len = $(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').length;
		var $this = $(this);
		if(len==0){
			var animation = $(this).find('#this_animation').val();
			var animation_duration = $(this).find('#this_animation_duration').val();
			var animation_delay = $(this).find('#this_animation_delay').val();
			
			var animatehtml = '<option value="no">No Animation</option>'
		+'<optgroup label="Attention Seekers">'
		  +'<option value="bounce">bounce</option>'
          +'<option value="flash">flash</option>'
          +'<option value="pulse">pulse</option>'
          +'<option value="rubberBand">rubberBand</option>'
          +'<option value="shake">shake</option>'
          +'<option value="swing">swing</option>'
          +'<option value="tada">tada</option>'
          +'<option value="wobble">wobble</option>'
          +'<option value="jello">jello</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Entrances">'
          +'<option value="bounceIn">bounceIn</option>'
          +'<option value="bounceInDown">bounceInDown</option>'
          +'<option value="bounceInLeft">bounceInLeft</option>'
          +'<option value="bounceInRight">bounceInRight</option>'
          +'<option value="bounceInUp">bounceInUp</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Exits">'
          +'<option value="bounceOut">bounceOut</option>'
          +'<option value="bounceOutDown">bounceOutDown</option>'
          +'<option value="bounceOutLeft">bounceOutLeft</option>'
          +'<option value="bounceOutRight">bounceOutRight</option>'
          +'<option value="bounceOutUp">bounceOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Entrances">'
          +'<option value="fadeIn">fadeIn</option>'
          +'<option value="fadeInDown">fadeInDown</option>'
          +'<option value="fadeInDownBig">fadeInDownBig</option>'
          +'<option value="fadeInLeft">fadeInLeft</option>'
          +'<option value="fadeInLeftBig">fadeInLeftBig</option>'
          +'<option value="fadeInRight">fadeInRight</option>'
          +'<option value="fadeInRightBig">fadeInRightBig</option>'
          +'<option value="fadeInUp">fadeInUp</option>'
          +'<option value="fadeInUpBig">fadeInUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Exits">'
          +'<option value="fadeOut">fadeOut</option>'
          +'<option value="fadeOutDown">fadeOutDown</option>'
          +'<option value="fadeOutDownBig">fadeOutDownBig</option>'
          +'<option value="fadeOutLeft">fadeOutLeft</option>'
          +'<option value="fadeOutLeftBig">fadeOutLeftBig</option>'
          +'<option value="fadeOutRight">fadeOutRight</option>'
          +'<option value="fadeOutRightBig">fadeOutRightBig</option>'
          +'<option value="fadeOutUp">fadeOutUp</option>'
          +'<option value="fadeOutUpBig">fadeOutUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Flippers">'
          +'<option value="flip">flip</option>'
          +'<option value="flipInX">flipInX</option>'
          +'<option value="flipInY">flipInY</option>'
          +'<option value="flipOutX">flipOutX</option>'
          +'<option value="flipOutY">flipOutY</option>'
        +'</optgroup>'

        +'<optgroup label="Lightspeed">'
          +'<option value="lightSpeedIn">lightSpeedIn</option>'
          +'<option value="lightSpeedOut">lightSpeedOut</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Entrances">'
          +'<option value="rotateIn">rotateIn</option>'
          +'<option value="rotateInDownLeft">rotateInDownLeft</option>'
          +'<option value="rotateInDownRight">rotateInDownRight</option>'
          +'<option value="rotateInUpLeft">rotateInUpLeft</option>'
          +'<option value="rotateInUpRight">rotateInUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Exits">'
          +'<option value="rotateOut">rotateOut</option>'
          +'<option value="rotateOutDownLeft">rotateOutDownLeft</option>'
          +'<option value="rotateOutDownRight">rotateOutDownRight</option>'
          +'<option value="rotateOutUpLeft">rotateOutUpLeft</option>'
          +'<option value="rotateOutUpRight">rotateOutUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Sliding Entrances">'
          +'<option value="slideInUp">slideInUp</option>'
          +'<option value="slideInDown">slideInDown</option>'
          +'<option value="slideInLeft">slideInLeft</option>'
          +'<option value="slideInRight">slideInRight</option>'
        +'</optgroup>'
		
        +'<optgroup label="Sliding Exits">'
          +'<option value="slideOutUp">slideOutUp</option>'
          +'<option value="slideOutDown">slideOutDown</option>'
          +'<option value="slideOutLeft">slideOutLeft</option>'
          +'<option value="slideOutRight">slideOutRight</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Entrances">'
          +'<option value="zoomIn">zoomIn</option>'
          +'<option value="zoomInDown">zoomInDown</option>'
          +'<option value="zoomInLeft">zoomInLeft</option>'
          +'<option value="zoomInRight">zoomInRight</option>'
          +'<option value="zoomInUp">zoomInUp</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Exits">'
          +'<option value="zoomOut">zoomOut</option>'
          +'<option value="zoomOutDown">zoomOutDown</option>'
          +'<option value="zoomOutLeft">zoomOutLeft</option>'
          +'<option value="zoomOutRight">zoomOutRight</option>'
          +'<option value="zoomOutUp">zoomOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Specials">'
          +'<option value="hinge">hinge</option>'
          +'<option value="rollIn">rollIn</option>'
          +'<option value="rollOut">rollOut</option>'
        +'</optgroup>';
			var delay_html = '';
			for(var i=200;i<=2000;i=i+200){
				delay_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var duration_html = '';
			for(var i=200;i<=2000;i=i+200){
				duration_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var popHtml = '<div class="tl-pb-popover" style="display:none;"><div class="tl-pb-popover-head"><h4>Configure Banner <a href="javascript:;" class="tl-pb-popover-close pull-right"><i class="fa fa-times"></i></a></h4></div><div class="tl-pb-popover-body"><div class="tl-pb-popover-input">'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Class</label><input placeholder="Enter column class" type="text" name="column_class" value="'+$(this).find('#this_sec_class').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Id</label><input placeholder="Enter column id" type="text" name="column_id" value="'+$(this).find('#this_sec_id').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Select Animation (* Works only if animation is activated from Theme Options)</label><select name="sec_animation" class="form-control">'+animatehtml+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Delay</label><select name="sec_animation_delay" class="form-control">'+delay_html+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Duration</label><select name="sec_animation_duration" class="form-control">'+duration_html+'</select></div></div>'
			+'</div><div class="popup-change-btn"><button type="button" class="button button-primary button-large" id="btn_tl_pb_ban_col">Change</button></div></div></div>';
			

			$(this).closest('.tl-pb-section-content-buttons').after(popHtml);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation"]').val(animation);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_delay"]').val(animation_delay);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_duration"]').val(animation_duration);

			$(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').slideDown();

		}

	});
	
	$(document).on('click','#btn_tl_pb_ban_col',function(){
		var imgpos = $(this).closest('.tl-pb-popover').find('[name="image_position"]').val();
		var secani = $(this).closest('.tl-pb-popover').find('[name="sec_animation"]').val();
		var secani_delay = $(this).closest('.tl-pb-popover').find('[name="sec_animation_delay"]').val();
		var secani_duration = $(this).closest('.tl-pb-popover').find('[name="sec_animation_duration"]').val();
		var cls = $(this).closest('.tl-pb-col').attr('class');

		var cls = $(this).closest('.tl-pb-popover').find('[name="column_class"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_class').val(cls);
		var id = $(this).closest('.tl-pb-popover').find('[name="column_id"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_id').val(id);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation').val(secani);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_delay').val(secani_delay);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_duration').val(secani_duration);
		//$(this).closest('.tl-pb-section-content-uploader').find('.tl_pb_sec_con_sett').data('title',title);

		//$(this).closest('.tl-pb-col').find('.tl-pb-section-content-text-title').text(title);

		$(this).closest('.tl-pb-popover').slideUp('fast',function(){

			$(this).remove();

		});

	});

	$(document).on('click','#btn_tl_pb_sec_sett',function(){

		var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid');
			
			//var sec_column = parseInt($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_column"]').val());

		var col_len = $('#'+id).find('.tl-pb-col:visible').length;

			var col_len = parseInt(col_len);

			//sm = 12/sec_column;
			
			//var c = sec_column-col_len;
			//var d = $('.col_config').val();
			//$('#'+id).find('.tl-pb-row').html(TlPb.colColumnHTML(d,c,id));
			
			

			sec_clases = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_classes"]').val();

			var sec_id = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_id"]').val();
			
			var bgColor = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_bgcolor"]').val(),
			
			title_color = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_color"]').val(),
			
			subtitle_color = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_color"]').val(),
			
			content_color = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_content_color"]').val(),

	        bgImageid = $(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-section-post-background').attr('data-attachId'),

			bgUrl = $(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-section-post-background').attr('data-bgUrl'); 
			//var padding = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_padding"]:checked').val();
			
			var margin_top = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_margin_top"]').val();
			var margin_bottom = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_margin_bottom"]').val();
			var margin_left = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_margin_left"]').val();
			var margin_right = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_margin_right"]').val();
			
			var custom_margin_top = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_margin_top"]').val();
			var custom_margin_bottom = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_margin_bottom"]').val();
			var custom_margin_left = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_margin_left"]').val();
			var custom_margin_right = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_margin_right"]').val();
			
			var padding_top = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_padding_top"]').val();
			var padding_bottom = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_padding_bottom"]').val();
			var padding_left = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_padding_left"]').val();
			var padding_right = $(this).closest('.tl-pb-sec-sett-pop').find('[name="section_padding_right"]').val();
			
			var custom_padding_top = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_padding_top"]').val();
			var custom_padding_bottom = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_padding_bottom"]').val();
			var custom_padding_left = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_padding_left"]').val();
			var custom_padding_right = $(this).closest('.tl-pb-sec-sett-pop').find('[name="custom_padding_right"]').val();
			
			var title_tag = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_tag"]').val();
			var subtitle_tag = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_tag"]').val();
			
			var parallax = $(this).closest('.tl-pb-sec-sett-pop').find('[name="background_parallax"]:checked').val();
			//$('#'+id+'-column').val($(this).closest('.tl-pb-sec-sett-pop').find('.tl-pb-col-list').val());
			//$('#'+id+'-column-config').val($(this).closest('.tl-pb-sec-sett-pop').find('.col_config').val());
			
			var title_color_class = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_color_scheme"]').val();
			var title_animation = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_animation"]').val();
			var title_animation_delay = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_animation_delay"]').val();
			var title_animation_duration = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_animation_duration"]').val();
			var subtitle_color_class = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_color_scheme"]').val();
			var subtitle_animation = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_animation"]').val();
			var subtitle_animation_delay = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_animation_delay"]').val();
			var subtitle_animation_duration = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_animation_duration"]').val();
			var content_color_class = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_color_scheme"]').val();
			var content_animation = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_animation"]').val();
			var content_animation_delay = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_animation_delay"]').val();
			var content_animation_duration = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_animation_duration"]').val();
			var background_color_class = $(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_bgcolor_scheme"]').val();

			$('#'+id).find('.tl-pb-section-content').attr('data-backgoundcolor',bgColor);
	
			$('#'+id).find('.tl-pb-section-content').attr('data-backgoundimageid',bgImageid);
	
			$('#'+id).find('.tl-pb-section-content').attr('data-backgoundimageurl',bgUrl);
	
			$('#'+id).find('.tl-pb-section-black-bar-title').text($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val());
			
			
			$('#'+id+'-title').val($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title"]').val());
			$('#'+id+'-title-position').val($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_title_position"]').val());
			$('#'+id+'-subtitle').val($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle"]').val());
			$('#'+id+'-subtitle-position').val($(this).closest('.tl-pb-sec-sett-pop').find('[name="sec_subtitle_position"]').val());
			
			$('#'+id+'-class').val(sec_clases);
			$('#'+id+'-id').val(sec_id);
			//$('#'+id+'-column').val(sec_column);
			//$('#'+id+'-column-config').val(d);
			$('#'+id+'-title-color').val(title_color);
			$('#'+id+'-subtitle-color').val(subtitle_color);
			$('#'+id+'-content-color').val(content_color);
			$('#'+id+'-background-color').val(bgColor);
			$('#'+id+'-background-image').val(bgUrl);
			/*if(padding){
				$('#'+id+'-padding').val(padding);
			}else{
				$('#'+id+'-padding').val('0');
			}*/
			$('#'+id+'-margin-top').val(margin_top);
			$('#'+id+'-margin-bottom').val(margin_bottom);
			$('#'+id+'-margin-left').val(margin_left);
			$('#'+id+'-margin-right').val(margin_right);
			
			$('#'+id+'-custom-margin-top').val(custom_margin_top);
			$('#'+id+'-custom-margin-bottom').val(custom_margin_bottom);
			$('#'+id+'-custom-margin-left').val(custom_margin_left);
			$('#'+id+'-custom-margin-right').val(custom_margin_right);
			
			$('#'+id+'-padding-top').val(padding_top);
			$('#'+id+'-padding-bottom').val(padding_bottom);
			$('#'+id+'-padding-left').val(padding_left);
			$('#'+id+'-padding-right').val(padding_right);
			
			$('#'+id+'-custom-padding-top').val(custom_padding_top);
			$('#'+id+'-custom-padding-bottom').val(custom_padding_bottom);
			$('#'+id+'-custom-padding-left').val(custom_padding_left);
			$('#'+id+'-custom-padding-right').val(custom_padding_right);
			
			$('#'+id+'-title-tag').val(title_tag);
			$('#'+id+'-subtitle-tag').val(subtitle_tag);
			
			$('#'+id+'-title-color-class').val(title_color_class);
			$('#'+id+'-title-animation').val(title_animation);
			$('#'+id+'-title-animation-delay').val(title_animation_delay);
			$('#'+id+'-title-animation-duration').val(title_animation_duration);
			$('#'+id+'-subtitle-color-class').val(subtitle_color_class);
			$('#'+id+'-subtitle-animation').val(subtitle_animation);
			$('#'+id+'-subtitle-animation-delay').val(subtitle_animation_delay);
			$('#'+id+'-subtitle-animation-duration').val(subtitle_animation_duration);
			$('#'+id+'-content-color-class').val(content_color_class);
			$('#'+id+'-content-animation').val(content_animation);
			$('#'+id+'-content-animation-delay').val(content_animation_delay);
			$('#'+id+'-content-animation-duration').val(content_animation_duration);
			$('#'+id+'-background-color-class').val(background_color_class);
			
			if(parallax){
				$('#'+id).find('.tl-pb-section-content').attr('data-parallax',parallax);
				$('#'+id+'-parallax').val(parallax);
			}else{
				$('#'+id).find('.tl-pb-section-content').attr('data-parallax','0');
				$('#'+id+'-parallax').val('0');
			}
			
			
		/*if(sec_column>col_len){

			var c = sec_column-col_len;
			var d = $('.col_config').val();
			$('#'+id).find('.tl-pb-row').append(TlPb.colColumnHTML(d,c));

		}else{

			sec_column=sec_column-1;

			$('#'+id).find(".tl-pb-col:gt("+sec_column+")").remove();

		}

		$('#'+id).find(".tl-pb-col").attr('class', function(i, c) {

                return c.replace(/(^|\s)col-sm-\S+/g, ' col-sm-'+sm);

            });*/

            if(sec_clases!='') $('#'+id).find('.tl-pb-section-content').addClass(sec_clases);

			if(sec_id!='') $('#'+id).find('.tl-pb-section-content').attr('data-id',sec_id);   
			

        $('#tl-pb-popup-common').html('').hide();   

		tb_remove();

	});

	$(document).on('click','.tl_pb_sec_del',function(){
		$this = $(this);
		jConfirm('Are you sure you want to remove this area?', 'Confirmation', function(r){
			if(r==true){
				$this.closest('.tl-pb-section').remove();
			}
		});

	});

	$(document).on('click','.tl_pb_sec_dup',function(){
		var key = tl_pb_genarate();
		var id = $(this).closest('.tl-pb-section').attr('id');
		var fullhtml = $(this).closest('.tl-pb-section').wrap('<p/>').parent().html();
		$(this).closest('.tl-pb-section').unwrap();
		var newhtml = fullhtml.replace(new RegExp(id, 'g'), key); 
		//alert(newhtml);
		$('.tl-pb-stage').append(newhtml);
		jQuery('html, body').animate({
        scrollTop: jQuery('#'+key).offset().top-32
      }, 800);
		//$(this).closest('.tl-pb-section').clone().attr('id', tl_pb_genarate()).appendTo($('.tl-pb-stage'));

	});

	$(document).on('click','#btn_tl_pb_sec_sett',function(){

		var id = $(this).closest('.tl-pb-sec-sett-pop').data('sid');

	});	

	$(document).on('click','.tl_pb_sec_con_sett',function(){
		
		var len = $(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').length;
		var $this = $(this);
		if(len==0){
			var grid = $(this).find('#this_sec_grid').val();
			var position = $(this).find('#this_image_position').val();
			var animation = $(this).find('#this_animation').val();
			var animation_duration = $(this).find('#this_animation_duration').val();
			var animation_delay = $(this).find('#this_animation_delay').val();
			var opthtml ='';
			if(grid=='1'){
				opthtml += '<option value="1" selected="selected">1</option>';
			}else{
				opthtml += '<option value="1">1</option>';
			}
			if(grid=='2'){
				opthtml += '<option value="2" selected="selected">2</option>';
			}else{
				opthtml += '<option value="2">2</option>';
			}
			if(grid=='3'){
				opthtml += '<option value="3" selected="selected">3</option>';
			}else{
				opthtml += '<option value="3">3</option>';
			}
			if(grid=='4' || grid==''){
				opthtml += '<option value="4" selected="selected">4</option>';
			}else{
				opthtml += '<option value="4">4</option>';
			}
			if(grid=='5'){
				opthtml += '<option value="5" selected="selected">5</option>';
			}else{
				opthtml += '<option value="5">5</option>';
			}
			if(grid=='6'){
				opthtml += '<option value="6" selected="selected">6</option>';
			}else{
				opthtml += '<option value="6">6</option>';
			}
			if(grid=='7'){
				opthtml += '<option value="7" selected="selected">7</option>';
			}else{
				opthtml += '<option value="7">7</option>';
			}
			if(grid=='8'){
				opthtml += '<option value="8" selected="selected">8</option>';
			}else{
				opthtml += '<option value="8">8</option>';
			}
			if(grid=='9'){
				opthtml += '<option value="9" selected="selected">9</option>';
			}else{
				opthtml += '<option value="9">9</option>';
			}
			if(grid=='10'){
				opthtml += '<option value="10" selected="selected">10</option>';
			}else{
				opthtml += '<option value="10">10</option>';
			}
			if(grid=='11'){
				opthtml += '<option value="11" selected="selected">11</option>';
			}else{
				opthtml += '<option value="11">11</option>';
			}
			if(grid=='12'){
				opthtml += '<option value="12" selected="selected">12</option>';
			}else{
				opthtml += '<option value="12">12</option>';
			}
			var animatehtml = '<option value="no">No Animation</option>'
		+'<optgroup label="Attention Seekers">'
		  +'<option value="bounce">bounce</option>'
          +'<option value="flash">flash</option>'
          +'<option value="pulse">pulse</option>'
          +'<option value="rubberBand">rubberBand</option>'
          +'<option value="shake">shake</option>'
          +'<option value="swing">swing</option>'
          +'<option value="tada">tada</option>'
          +'<option value="wobble">wobble</option>'
          +'<option value="jello">jello</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Entrances">'
          +'<option value="bounceIn">bounceIn</option>'
          +'<option value="bounceInDown">bounceInDown</option>'
          +'<option value="bounceInLeft">bounceInLeft</option>'
          +'<option value="bounceInRight">bounceInRight</option>'
          +'<option value="bounceInUp">bounceInUp</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Exits">'
          +'<option value="bounceOut">bounceOut</option>'
          +'<option value="bounceOutDown">bounceOutDown</option>'
          +'<option value="bounceOutLeft">bounceOutLeft</option>'
          +'<option value="bounceOutRight">bounceOutRight</option>'
          +'<option value="bounceOutUp">bounceOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Entrances">'
          +'<option value="fadeIn">fadeIn</option>'
          +'<option value="fadeInDown">fadeInDown</option>'
          +'<option value="fadeInDownBig">fadeInDownBig</option>'
          +'<option value="fadeInLeft">fadeInLeft</option>'
          +'<option value="fadeInLeftBig">fadeInLeftBig</option>'
          +'<option value="fadeInRight">fadeInRight</option>'
          +'<option value="fadeInRightBig">fadeInRightBig</option>'
          +'<option value="fadeInUp">fadeInUp</option>'
          +'<option value="fadeInUpBig">fadeInUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Exits">'
          +'<option value="fadeOut">fadeOut</option>'
          +'<option value="fadeOutDown">fadeOutDown</option>'
          +'<option value="fadeOutDownBig">fadeOutDownBig</option>'
          +'<option value="fadeOutLeft">fadeOutLeft</option>'
          +'<option value="fadeOutLeftBig">fadeOutLeftBig</option>'
          +'<option value="fadeOutRight">fadeOutRight</option>'
          +'<option value="fadeOutRightBig">fadeOutRightBig</option>'
          +'<option value="fadeOutUp">fadeOutUp</option>'
          +'<option value="fadeOutUpBig">fadeOutUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Flippers">'
          +'<option value="flip">flip</option>'
          +'<option value="flipInX">flipInX</option>'
          +'<option value="flipInY">flipInY</option>'
          +'<option value="flipOutX">flipOutX</option>'
          +'<option value="flipOutY">flipOutY</option>'
        +'</optgroup>'

        +'<optgroup label="Lightspeed">'
          +'<option value="lightSpeedIn">lightSpeedIn</option>'
          +'<option value="lightSpeedOut">lightSpeedOut</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Entrances">'
          +'<option value="rotateIn">rotateIn</option>'
          +'<option value="rotateInDownLeft">rotateInDownLeft</option>'
          +'<option value="rotateInDownRight">rotateInDownRight</option>'
          +'<option value="rotateInUpLeft">rotateInUpLeft</option>'
          +'<option value="rotateInUpRight">rotateInUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Exits">'
          +'<option value="rotateOut">rotateOut</option>'
          +'<option value="rotateOutDownLeft">rotateOutDownLeft</option>'
          +'<option value="rotateOutDownRight">rotateOutDownRight</option>'
          +'<option value="rotateOutUpLeft">rotateOutUpLeft</option>'
          +'<option value="rotateOutUpRight">rotateOutUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Sliding Entrances">'
          +'<option value="slideInUp">slideInUp</option>'
          +'<option value="slideInDown">slideInDown</option>'
          +'<option value="slideInLeft">slideInLeft</option>'
          +'<option value="slideInRight">slideInRight</option>'
        +'</optgroup>'
		
        +'<optgroup label="Sliding Exits">'
          +'<option value="slideOutUp">slideOutUp</option>'
          +'<option value="slideOutDown">slideOutDown</option>'
          +'<option value="slideOutLeft">slideOutLeft</option>'
          +'<option value="slideOutRight">slideOutRight</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Entrances">'
          +'<option value="zoomIn">zoomIn</option>'
          +'<option value="zoomInDown">zoomInDown</option>'
          +'<option value="zoomInLeft">zoomInLeft</option>'
          +'<option value="zoomInRight">zoomInRight</option>'
          +'<option value="zoomInUp">zoomInUp</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Exits">'
          +'<option value="zoomOut">zoomOut</option>'
          +'<option value="zoomOutDown">zoomOutDown</option>'
          +'<option value="zoomOutLeft">zoomOutLeft</option>'
          +'<option value="zoomOutRight">zoomOutRight</option>'
          +'<option value="zoomOutUp">zoomOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Specials">'
          +'<option value="hinge">hinge</option>'
          +'<option value="rollIn">rollIn</option>'
          +'<option value="rollOut">rollOut</option>'
        +'</optgroup>';
			var delay_html = '';
			for(var i=200;i<=2000;i=i+200){
				delay_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var duration_html = '';
			for(var i=200;i<=2000;i=i+200){
				duration_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var popHtml = '<div class="tl-pb-popover" style="display:none;"><div class="tl-pb-popover-head"><h4>Configure Container <a href="javascript:;" class="tl-pb-popover-close pull-right"><i class="fa fa-times"></i></a></h4></div><div class="tl-pb-popover-body"><div class="tl-pb-popover-input">'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Class</label><input placeholder="Enter column class" type="text" name="column_class" value="'+$(this).find('#this_sec_class').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Id</label><input placeholder="Enter column id" type="text" name="column_id" value="'+$(this).find('#this_sec_id').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Select Grid</label><select name="grid" class="form-control">'+opthtml+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Select Image Position</label><select name="image_position" class="form-control"><option value="top">Top</option><option value="bottom">Bottom</option><option value="left">Left</option><option value="right">Right</option></select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Select Animation (* Works only if animation is activated from Theme Options)</label><select name="sec_animation" class="form-control">'+animatehtml+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Delay</label><select name="sec_animation_delay" class="form-control">'+delay_html+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Duration</label><select name="sec_animation_duration" class="form-control">'+duration_html+'</select></div></div>'
			+'</div><div class="popup-change-btn"><button type="button" class="button button-primary button-large" id="btn_tl_pb_conf_col">Change</button></div></div></div>';
			

			$(this).closest('.tl-pb-section-content-buttons').after(popHtml);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="image_position"]').val(position);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation"]').val(animation);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_delay"]').val(animation_delay);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_duration"]').val(animation_duration);

			$(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').slideDown();

		}

	});		

	$(document).on('click','#btn_tl_pb_conf_col',function(){
		var newcls = $(this).closest('.tl-pb-popover').find('[name="grid"]').val();
		var imgpos = $(this).closest('.tl-pb-popover').find('[name="image_position"]').val();
		var secani = $(this).closest('.tl-pb-popover').find('[name="sec_animation"]').val();
		var secani_delay = $(this).closest('.tl-pb-popover').find('[name="sec_animation_delay"]').val();
		var secani_duration = $(this).closest('.tl-pb-popover').find('[name="sec_animation_duration"]').val();
		var editcls = 'col-sm-'+newcls;
		var cls = $(this).closest('.tl-pb-col').attr('class');
		
		$(this).closest('.tl-pb-col').removeClass(cls).addClass('tl-pb-col '+editcls);

		var cls = $(this).closest('.tl-pb-popover').find('[name="column_class"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_class').val(cls);
		var id = $(this).closest('.tl-pb-popover').find('[name="column_id"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_id').val(id);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_grid').val(newcls);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_image_position').val(imgpos);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation').val(secani);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_delay').val(secani_delay);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_duration').val(secani_duration);
		//$(this).closest('.tl-pb-section-content-uploader').find('.tl_pb_sec_con_sett').data('title',title);

		//$(this).closest('.tl-pb-col').find('.tl-pb-section-content-text-title').text(title);

		$(this).closest('.tl-pb-popover').slideUp('fast',function(){

			$(this).remove();

		});

	});	
	
	$(document).on('click','.tl_pb_sec_gallery_sett',function(){
		var len = $(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').length;
		var $this = $(this);
		if(len==0){
			var animation = $(this).closest('.tl-pb-col').find('#this_animation').val();
			var animation_duration = $(this).closest('.tl-pb-col').find('#this_animation_duration').val();
			var animation_delay = $(this).closest('.tl-pb-col').find('#this_animation_delay').val();
			var animatehtml = '<option value="no">No Animation</option>'
		+'<optgroup label="Attention Seekers">'
		  +'<option value="bounce">bounce</option>'
          +'<option value="flash">flash</option>'
          +'<option value="pulse">pulse</option>'
          +'<option value="rubberBand">rubberBand</option>'
          +'<option value="shake">shake</option>'
          +'<option value="swing">swing</option>'
          +'<option value="tada">tada</option>'
          +'<option value="wobble">wobble</option>'
          +'<option value="jello">jello</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Entrances">'
          +'<option value="bounceIn">bounceIn</option>'
          +'<option value="bounceInDown">bounceInDown</option>'
          +'<option value="bounceInLeft">bounceInLeft</option>'
          +'<option value="bounceInRight">bounceInRight</option>'
          +'<option value="bounceInUp">bounceInUp</option>'
        +'</optgroup>'

        +'<optgroup label="Bouncing Exits">'
          +'<option value="bounceOut">bounceOut</option>'
          +'<option value="bounceOutDown">bounceOutDown</option>'
          +'<option value="bounceOutLeft">bounceOutLeft</option>'
          +'<option value="bounceOutRight">bounceOutRight</option>'
          +'<option value="bounceOutUp">bounceOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Entrances">'
          +'<option value="fadeIn">fadeIn</option>'
          +'<option value="fadeInDown">fadeInDown</option>'
          +'<option value="fadeInDownBig">fadeInDownBig</option>'

          +'<option value="fadeInLeft">fadeInLeft</option>'
          +'<option value="fadeInLeftBig">fadeInLeftBig</option>'
          +'<option value="fadeInRight">fadeInRight</option>'
          +'<option value="fadeInRightBig">fadeInRightBig</option>'
          +'<option value="fadeInUp">fadeInUp</option>'
          +'<option value="fadeInUpBig">fadeInUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Fading Exits">'
          +'<option value="fadeOut">fadeOut</option>'
          +'<option value="fadeOutDown">fadeOutDown</option>'
          +'<option value="fadeOutDownBig">fadeOutDownBig</option>'
          +'<option value="fadeOutLeft">fadeOutLeft</option>'
          +'<option value="fadeOutLeftBig">fadeOutLeftBig</option>'
          +'<option value="fadeOutRight">fadeOutRight</option>'
          +'<option value="fadeOutRightBig">fadeOutRightBig</option>'
          +'<option value="fadeOutUp">fadeOutUp</option>'
          +'<option value="fadeOutUpBig">fadeOutUpBig</option>'
        +'</optgroup>'

        +'<optgroup label="Flippers">'
          +'<option value="flip">flip</option>'
          +'<option value="flipInX">flipInX</option>'
          +'<option value="flipInY">flipInY</option>'
          +'<option value="flipOutX">flipOutX</option>'
          +'<option value="flipOutY">flipOutY</option>'
        +'</optgroup>'

        +'<optgroup label="Lightspeed">'
          +'<option value="lightSpeedIn">lightSpeedIn</option>'
          +'<option value="lightSpeedOut">lightSpeedOut</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Entrances">'
          +'<option value="rotateIn">rotateIn</option>'
          +'<option value="rotateInDownLeft">rotateInDownLeft</option>'
          +'<option value="rotateInDownRight">rotateInDownRight</option>'
          +'<option value="rotateInUpLeft">rotateInUpLeft</option>'
          +'<option value="rotateInUpRight">rotateInUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Rotating Exits">'
          +'<option value="rotateOut">rotateOut</option>'
          +'<option value="rotateOutDownLeft">rotateOutDownLeft</option>'
          +'<option value="rotateOutDownRight">rotateOutDownRight</option>'
          +'<option value="rotateOutUpLeft">rotateOutUpLeft</option>'
          +'<option value="rotateOutUpRight">rotateOutUpRight</option>'
        +'</optgroup>'

        +'<optgroup label="Sliding Entrances">'
          +'<option value="slideInUp">slideInUp</option>'
          +'<option value="slideInDown">slideInDown</option>'
          +'<option value="slideInLeft">slideInLeft</option>'
          +'<option value="slideInRight">slideInRight</option>'
        +'</optgroup>'
		
        +'<optgroup label="Sliding Exits">'
          +'<option value="slideOutUp">slideOutUp</option>'
          +'<option value="slideOutDown">slideOutDown</option>'
          +'<option value="slideOutLeft">slideOutLeft</option>'
          +'<option value="slideOutRight">slideOutRight</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Entrances">'
          +'<option value="zoomIn">zoomIn</option>'
          +'<option value="zoomInDown">zoomInDown</option>'
          +'<option value="zoomInLeft">zoomInLeft</option>'
          +'<option value="zoomInRight">zoomInRight</option>'
          +'<option value="zoomInUp">zoomInUp</option>'
        +'</optgroup>'
        
        +'<optgroup label="Zoom Exits">'
          +'<option value="zoomOut">zoomOut</option>'
          +'<option value="zoomOutDown">zoomOutDown</option>'
          +'<option value="zoomOutLeft">zoomOutLeft</option>'
          +'<option value="zoomOutRight">zoomOutRight</option>'
          +'<option value="zoomOutUp">zoomOutUp</option>'
        +'</optgroup>'

        +'<optgroup label="Specials">'
          +'<option value="hinge">hinge</option>'
          +'<option value="rollIn">rollIn</option>'
          +'<option value="rollOut">rollOut</option>'
        +'</optgroup>';
			var delay_html = '';
			for(var i=200;i<=2000;i=i+200){
				delay_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var duration_html = '';
			for(var i=200;i<=2000;i=i+200){
				duration_html += '<option value="'+i+'ms">'+i+'ms</option>'
			}
			var popHtml = '<div class="tl-pb-popover" style="display:none;"><div class="tl-pb-popover-head"><h4>Configure Gallery <a href="javascript:;" class="tl-pb-popover-close pull-right"><i class="fa fa-times"></i></a></h4></div><div class="tl-pb-popover-body"><div class="tl-pb-popover-input">'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Class</label><input placeholder="Enter image class" type="text" name="column_class" value="'+$(this).closest('.tl-pb-col').find('#this_sec_class').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Id</label><input placeholder="Enter image id" type="text" name="column_id" value="'+$(this).closest('.tl-pb-col').find('#this_sec_id').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Title</label><input placeholder="Enter image title" type="text" name="column_title" value="'+$(this).closest('.tl-pb-col').find('#this_sec_title').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Category</label><input placeholder="Enter image category" type="text" name="column_category" value="'+$(this).closest('.tl-pb-col').find('#this_sec_category').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>URL</label><input placeholder="Enter url" type="text" name="column_url" value="'+$(this).closest('.tl-pb-col').find('#this_sec_url').val()+'"/></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Select Animation (* Works only if animation is activated from Theme Options)</label><select name="sec_animation" class="form-control">'+animatehtml+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Delay</label><select name="sec_animation_delay" class="form-control">'+delay_html+'</select></div></div>'
			+'<div class="row"><div class="tl-pb-pop-col"><label>Animation Duration</label><select name="sec_animation_duration" class="form-control">'+duration_html+'</select></div></div>'
			+'</div><div class="popup-change-btn"><button type="button" class="button button-primary button-large" id="btn_tl_pb_conf_gal">Change</button></div></div></div>';
			

			$(this).closest('.tl-pb-section-content-buttons').after(popHtml);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation"]').val(animation);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_delay"]').val(animation_delay);
			$(this).closest('.tl-pb-section-content-uploader').find('[name="sec_animation_duration"]').val(animation_duration);

			$(this).closest('.tl-pb-section-content-uploader').find('.tl-pb-popover').slideDown();

		}

	});		

	$(document).on('click','#btn_tl_pb_conf_gal',function(){
		var secani = $(this).closest('.tl-pb-popover').find('[name="sec_animation"]').val();
		var secani_delay = $(this).closest('.tl-pb-popover').find('[name="sec_animation_delay"]').val();
		var secani_duration = $(this).closest('.tl-pb-popover').find('[name="sec_animation_duration"]').val();
		var sec_title = $(this).closest('.tl-pb-popover').find('[name="column_title"]').val();
		var sec_category = $(this).closest('.tl-pb-popover').find('[name="column_category"]').val();
		var sec_url = $(this).closest('.tl-pb-popover').find('[name="column_url"]').val();
		var cls = $(this).closest('.tl-pb-col').attr('class');

		var cls = $(this).closest('.tl-pb-popover').find('[name="column_class"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_class').val(cls);
		var id = $(this).closest('.tl-pb-popover').find('[name="column_id"]').val();
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_id').val(id);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_title').val(sec_title);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_url').val(sec_url);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_sec_category').val(sec_category	);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation').val(secani);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_delay').val(secani_delay);
		$(this).closest('.tl-pb-section-content-uploader').find('#this_animation_duration').val(secani_duration);
		//$(this).closest('.tl-pb-section-content-uploader').find('.tl_pb_sec_con_sett').data('title',title);

		//$(this).closest('.tl-pb-col').find('.tl-pb-section-content-text-title').text(title);

		$(this).closest('.tl-pb-popover').slideUp('fast',function(){

			$(this).remove();

		});

	});	


	$(document).on('click','.tl-pb-popover-close',function(){

		$(this).closest('.tl-pb-popover').slideUp('fast',function(){

			$(this).remove();

		});

	});	

	$(document).on('click','.tl-pb-gallery-add-item-link',function(){
		
		var key = $(this).parent('.tl-pb-section').attr('id');
		//alert(key);

		var len = $(this).closest('.tl-pb-section').find('.tl-pb-col').length,

			sm = 'col-sm-12';

		var $htm = $(TlPb.colGalleryHTML(1,key));

		if(len>0){

			sm =  $(this).closest('.tl-pb-section').find('.tl-pb-col:first').attr('class').replace('tl-pb-col',''); 

			$htm.filter(".tl-pb-col").attr('class', function(i, c) {

                return c.replace(/(^|\s)col-sm-\S+/g, sm);

       	 });

		}

		$(this).closest('.tl-pb-section').find('.tl-pb-gal-row').append($htm);

		var col_len = $(this).closest('.tl-pb-section').find('.tl-pb-col:visible').length,

			sm = 12/col_len;

	});	
	
	$(document).on('click','.tl-pb-banner-add-item-link',function(){
		
		var key = $(this).parent('.tl-pb-section').attr('id');
		
		$('#'+key).find('.tl-pb-row').append(TlPb.colBannerHTML(key));

	});	

	$(document).on('click','.tl_pb_sec_con_del',function(){
		var id = $(this).closest('.tl-pb-section').attr('id');
		$this = $(this);
		jConfirm('Are you sure you want to remove this section?', 'Confirmation', function(r){
			if(r==true){
				$this.closest('.tl-pb-col').remove();
			}
		});

	});	

	$(document).on('click','.tl_pb_sec_con_del_gal',function(){
		$this = $(this);
		jConfirm('Are you sure you want to remove this section?', 'Confirmation', function(r){
			if(r==true){
				$this.closest('.tl-pb-col').remove();	
			}
		});

	});	

	jQuery(".tl-pb-stage").sortable({

        handle: '.tl-pb-section-black-bar',

        cursor: 'move',

       // containment: "parent",

         items: "> div",

    });

    jQuery('.tl-pb-row').sortable({

		    handle: '.tl-pb-section-content-sortable',

			placeholder: 'sortable-placeholder',

			forcePlaceholderSizeType: true,

			distance: 2,

			tolerance: 'pointer',

			//zIndex: 99999,

			cursor: 'move'

    });

    
	jQuery(document).ready(function(e) {
		jQuery('.my-color-picker').wpColorPicker();
    });
       

});