jQuery(document).ready(function() {
	jQuery('#postdivrich').hide();
	jQuery('#tl_page_builder').hide();
	jQuery('#tl_page_builder_template').hide();
	jQuery('#postimagediv').hide();
	jQuery('#tl_contact_metabox').hide();
	//jQuery('#page_template option[value="page-builder/template-builder.php"]').remove();
	
	jQuery('#tlpb-btn').click(function(){
		jQuery('#postdivrich').hide();
		jQuery('#tl_page_builder').show();
		jQuery('#tl_page_builder_template').show();
		jQuery('#postimagediv').hide();
		jQuery('[name="tlpb_active"]').val('yes');
	});
	
	jQuery('#tlpb-cancel-btn').click(function(){
		jQuery('#postdivrich').show();
		jQuery('#tl_page_builder').hide();
		jQuery('#tl_page_builder_template').hide();
		jQuery('#postimagediv').show();
		jQuery('[name="tlpb_active"]').val('no');
	});
	
	if(jQuery('[name="tlpb_active"]').val()=='yes'){
		jQuery('#postdivrich').hide();
		jQuery('#tl_page_builder').show();
		jQuery('#tl_page_builder_template').show();
		jQuery('#postimagediv').hide();
	}else{
		jQuery('#postdivrich').show();
		jQuery('#tl_page_builder').hide();
		jQuery('#tl_page_builder_template').hide();
		jQuery('#postimagediv').show();
	}
	
	if(jQuery( "#page_template" ).val()=='template-contact.php'){
		jQuery('#tl_contact_metabox').show();
	}
	
	if(jQuery( "#page_template" ).val()=='template-left-sidebar-page.php' || jQuery( "#page_template" ).val()=='template-right-sidebar-page.php'){
		jQuery('.sidebar_selection_dropdown').show();
		jQuery('.no_sidebar_message').hide();
	}else{
		jQuery('.sidebar_selection_dropdown').hide();
		jQuery('.no_sidebar_message').show();
	}

    jQuery( "#page_template" ).change(function() {
		if(jQuery(this).val()=='template-contact.php'){
			jQuery('#tl_contact_metabox').show();
			jQuery('#postimagediv').hide();
		}else{
			if(jQuery('[name="tlpb_active"]').val()!='yes'){
				jQuery('#postdivrich').show();
			}
			jQuery('#tl_contact_metabox').hide();
		}
		if(jQuery( this ).val()=='template-left-sidebar-page.php' || jQuery( this ).val()=='template-right-sidebar-page.php'){
			jQuery('.sidebar_selection_dropdown').show();
			jQuery('.no_sidebar_message').hide();
		}else{
			jQuery('.sidebar_selection_dropdown').hide();
			jQuery('.no_sidebar_message').show();
		}
	});
	//jQuery( document ).tooltip();
});
function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 45 || charCode == 95 || charCode == 0 || charCode == 8 || (charCode >= 48 && charCode <= 57))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
jQuery(window).load(function(){
	jQuery( document ).tooltip();
});	