<?php
add_shortcode( 'tlpb_developer', 'tlpb_developer_shortcode' );
function tlpb_developer_shortcode( $atts, $content ) {
    ob_start();
	echo apply_filters('tl_content',wpautop(do_shortcode($content)));
 	$myvariable = ob_get_clean();
    return $myvariable;
}