<?php
add_shortcode( 'tlpb_container', 'tlpb_container_shortcode' );
function tlpb_container_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
       'grid'=>'',
	   'sec_class'=>'',
	   'sec_id'=>'',
	   'image_position'=>'',
	   'animation'=>'',
	   'animation_delay'=>'',
	   'animation_duration'=>'',
	   'image'=>'',
    ), $atts ) );
?>
	<div class="col-lg-<?php echo isset($grid) && $grid?$grid:'6';?> col-md-<?php echo isset($grid) && $grid?$grid:'6';?> col-sm-<?php echo isset($grid) && $grid?$grid:'6';?> col-xs-12<?php echo (!empty($sec_class))?' '.$sec_class:'';?>"<?php echo (!empty($sec_id))?' id="'.$sec_id.'"':'';?>>
		<?php if(!empty($animation) && $animation!='no'):?>
        <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
        <?php endif;?>
            <?php if(!empty($image)):?>
            <?php
			$image_id = tl_get_image_id($image);
			/*if($grid>9 && $grid<=12){
				$size = array(1140,'');
			}
			if($grid==9){
				$size = array(848,'');
			}
			if($grid>6 && $grid<=8){
				$size = array(750,'');
			}
			if($grid>4 && $grid<=6){
				$size = array(555,'');
			}
			if($grid==4){
				$size = array(360,'');
			}
			if($grid==3){
				$size = array(263,'');
			}
			if($grid<3){
				$size = 'thumbnail';
			}*/
			$size = 'full';
			?>
            <?php if($image_position=='top'):?>
            <figure>
            <?php echo wp_get_attachment_image( $image_id, $size, "", array( "class" => "img-responsive" ) );  ?>
            </figure>
			<?php echo do_shortcode($content);?>
            <?php endif;?>
            <?php if($image_position=='left'):?>
            <figure>
            <?php echo wp_get_attachment_image( $image_id, $size, "", array( "class" => "size-full wp-image-".$image_id." alignleft" ) );  ?>
            </figure>
            <?php echo do_shortcode($content);?>
            <?php endif;?>
            <?php if($image_position=='right'):?>
            <figure>
            <?php echo wp_get_attachment_image( $image_id, $size, "", array( "class" => "size-full wp-image-".$image_id." alignright" ) );  ?>
            </figure>
            <?php echo do_shortcode($content);?>
            <?php endif;?>
            <?php if($image_position=='bottom'):?>
            <?php echo do_shortcode($content);?>
            <figure>
            <?php echo wp_get_attachment_image( $image_id, $size, "", array( "class" => "img-responsive" ) );  ?>
            </figure>
            <?php endif;?>
            <?php else:?>
            <?php echo do_shortcode($content);?>
            <?php endif;?>
        <?php if($animation!='no'):?>
        </div>
        <?php endif;?> 
    </div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}