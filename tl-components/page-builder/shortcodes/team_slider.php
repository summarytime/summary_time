<?php
add_shortcode( 'tlpb_team_slider', 'tlpb_team_slider_shortcode' );
function tlpb_team_slider_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'post_type'=>'team',
	   'post_order'=>'',
	   'post_number'=>'',
	   'post_offset'=>'',
	   'post_column'=>'',
	   'post_element'=>'',
	   'key'=>'',
	   'container'=>'yes',
	   'animation'=>'no',
	   'animation_duration'=>'200ms',
	   'animation_delay'=>'200ms',
    ), $atts ) );
?>
<?php
			$order = explode('-',$post_order);
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $post_number,
					'offset'           => $post_offset,
				);
				
			$wp_query = new WP_Query($args);
			if ($wp_query->have_posts() ) :
			$element = explode(',',$post_element);
?>
<?php if($container=='yes'):?>
<div class="container">
<?php endif;?>
	<div class="team-div-col-<?php echo $post_column;?> team-div-description clearfix">
             <div class="carousel-wrap">
                  <div class="owl-carousel owl-carousel-<?php echo $key;?>">
                  	<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <?php if($animation && $animation!='no'):?>
                    <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
                    <?php endif;?>
                    <div class="item">
                      <div class="team-height team-div">
                         <div class="photo">
                            <div class="overlays">
                                <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
                                <a href="<?php the_permalink();?>">
                                <?php endif;?>
                                  <div class="content-overlay"></div>
                                  <?php if(isset($element) && in_array('show_image',$element)):?>
                                  <?php if(get_post_meta( get_the_ID(), 'team_member_image', true )):
									$image_id = tl_get_image_id(get_post_meta( get_the_ID(), 'team_member_image', true ));
									/*if($post_column==2){
										$image_thumb = wp_get_attachment_image_src($image_id, 'team-slid-two');
									}
									if($post_column==3){
										$image_thumb = wp_get_attachment_image_src($image_id, 'team-slid-three');
									}
									if($post_column==4){
										$image_thumb = wp_get_attachment_image_src($image_id, 'team-slid-four');
									}*/
								  ?>
                                  <?php echo wp_get_attachment_image( $image_id, 'large', "", array( "class" => "content-image" ) );?>
								  <!--<img src="<?php //echo $image_thumb[0]; ?>" alt="" class="content-image" />-->
                                  <?php endif;?>
                                  <?php endif;?>
                                  <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
                                  <div class="content-details">
                                    <span><i class="fa fa-link" aria-hidden="true"></i></span>
                                  </div>
                                  <?php endif;?>
                                <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
                                </a>
                                <?php endif;?>
                            </div>
                         </div>
                         <div class="info">
                           <?php if(isset($element) && in_array('show_name',$element)):?>
                           <h5 class="name"><?php the_title();?></h5>
                           <?php endif;?>
                           <?php if(isset($element) && in_array('show_designation',$element)):?>
                           <p class="position"><?php echo get_post_meta( get_the_ID(), 'team_member_designation', true );?></p>
                           <?php endif;?>
                         </div>
                         <?php if(isset($element) && in_array('show_social_icons',$element)):?>
                         <div class="social-media-div">
                          <ul class="socials unstyled">
                            <?php if(get_post_meta( get_the_ID(), 'team_member_facebook_url', true )):?>
                            <li>
                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_facebook_url', true );?>" class="fb"target="_blank">
                             <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            </li>
                            <?php endif;?>
                            <?php if(get_post_meta( get_the_ID(), 'team_member_twitter_url', true )):?>
                            <li>
                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_twitter_url', true );?>" class="tw" target="_blank">
                             <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            </li>
                            <?php endif;?>
                            <?php if(get_post_meta( get_the_ID(), 'team_member_linkedin_url', true )):?>
                            <li>
                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_linkedin_url', true );?>" class="dri" target="_blank">
                             <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                            </li>
                            <?php endif;?>
                            <?php if(get_post_meta( get_the_ID(), 'team_member_google_plus_url', true )):?>
                            <li>
                            <a href="<?php echo get_post_meta( get_the_ID(), 'team_member_google_plus_url', true );?>" class="beh" target="_blank">
                             <i class="fa fa-google-plus" aria-hidden="true"></i>
                            </a>
                            </li>
                            <?php endif;?>
                          </ul>
                         </div> 
                    	 <?php endif;?>
                       </div>
                    </div>
                    <?php if($animation && $animation!='no'):?>
                    </div>
                    <?php endif;?>
                    <?php endwhile;?>
                  </div>
                </div>
         
         
         </div>
<?php if($container=='yes'):?>
</div>
<?php endif;?>
<script>
jQuery('.owl-carousel-<?php echo $key;?>').owlCarousel({
	loop: true,
	margin: 10,
	nav: true,
	navText: [
	'<i class="fa fa-angle-left"></i>',
	'<i class="fa fa-angle-right"></i>'
	],
	autoplay: true,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1
		},
		360: {
			items: 1,
			nav: false
		},
		480: {
			items: 2,
			nav: false
		},
		768: {
			items: 2,
			nav: false
		},
		1000: {
			items: <?php echo $post_column;?>,
			nav: false
		}
	}
});
</script>
<?php wp_reset_query(); endif;?>
	
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}