<?php

add_shortcode( 'tlpb_post_slider', 'tlpb_post_slider_shortcode' );

function tlpb_post_slider_shortcode( $atts, $content ) {

    ob_start();

	extract( shortcode_atts( array (

	   'layout'=>'',

	   'category'=>'',

	   'key'=>'',

	   'container'=>'yes'

    ), $atts ) );

?>

	<?php if($layout=='box'):?>

    <div class="container">

    <?php endif;?>

    <?php $args = array(

			'posts_per_page'   => -1,

			'category'		   => $category,

			'category_name'    => get_cat_name($category),

			'post_type'        => 'post',

			'post_status'      => 'publish',

			'suppress_filters' => true 

		);

		//print_r($args); die();

		$loop = new WP_Query($args);

		if($loop->have_posts()): ?>

	<div class="tl-paralax-inner parallax21" <?php //echo $style;?>>

    			<?php if($container=='yes'):?>

				<div class="container">

                <?php endif;?>

						<div id="caraousel-<?php echo $key;?>" class="carousel slide" data-ride="carousel">

						  <!-- Wrapper for slides -->

						  <div class="carousel-inner" role="listbox">

                          	<?php $i=0;?>

            				<?php while($loop->have_posts()) : $loop->the_post();?>

                            <?php //$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>

							<div class="item <?php echo ($i==0)?'active':'';?>">

							<div class="inner-slides">

								<div class="col-sm-6 col-md-6 col-lg-6">

								<div class="tl-carosual-post-image-box">

                                	<?php if(has_post_thumbnail()):?>

                                	<a href="<?php the_permalink();?>">

										<?php the_post_thumbnail('post-slid', array('class'=>'caraousel-img'));?>

                                    </a>

                                    <?php endif;?>

								</div>

								</div>

								<div class="col-sm-6 col-md-6 col-lg-6">

								  <div class="carosual-db-content">

                                  	<a href="<?php the_permalink();?>">

                                  	<h3><?php the_title();?></h3>

                                    </a>

                                    <?php echo do_shortcode(html_cut(get_the_content(),500));?>

                                    <p><a href="<?php the_permalink();?>" class="tl-btn-shutter-out-horizontal">Read More</a></p>

								  </div>

								</div>

							</div>

							</div>

                            <?php $i++;?>

                            <?php endwhile;?>

							

							

						  </div>



						  <!-- Controls -->

						 

						  <a class="left carousel-control" href="#caraousel-<?php echo $key;?>" role="button" data-slide="prev">

							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

							<span class="sr-only">Previous</span>

						  </a>

						  <a class="right carousel-control" href="#caraousel-<?php echo $key;?>" role="button" data-slide="next">

							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

							<span class="sr-only">Next</span>

						  </a>

						 

						</div>	

                    <?php if($container=='yes'):?>

					</div>

                    <?php endif;?>

				</div>

                <?php if($layout=='box'):?>

                </div>

                <?php endif;?>

                <?php wp_reset_query();?>

                <script>

				jQuery('#caraousel-<?php echo $key;?>').carousel({

				  interval: 2000

				});
				var car_width = jQuery('#caraousel-<?php echo $key;?>').find('.tl-carosual-post-image-box').first().width();
				var car_height = jQuery('#caraousel-<?php echo $key;?>').find('.tl-carosual-post-image-box').first().height();
				jQuery('#caraousel-<?php echo $key;?>').find('.tl-carosual-post-image-box').each(function(){
					jQuery(this).find('img.wp-post-image').attr('sizes','(max-width: '+car_width+'px) 100vw, '+car_width+'px');
					jQuery(this).find('img.wp-post-image').attr('width',car_width);
					jQuery(this).find('img.wp-post-image').attr('height',car_height);
					//jQuery(this).attr('height',jQuery(this).find('img.wp-post-image').height());
				});

				</script>

                <?php endif;?>

<?php

 	$myvariable = ob_get_clean();

    return $myvariable;

}