<?php
add_shortcode( 'tl_block', 'tl_block_shortcode' );
function tl_block_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'layout'=>'layout-1',
	   'icon'=>'',
	   'title'=>'',
	   'button'=>'no',
	   'button_text'=>'View Details',
	   'button_url'=>'javascript:void(0);'
    ), $atts ) );
 ?>
<?php if($layout=='layout-1'):?>
<div class="tl-feature-block-s1 tl-no-border">
	<?php if($icon){?><i class="<?php echo $icon;?>"></i><?php }?>
    <?php if($title):?>
    <h3><?php echo $title;?></h3>
    <?php endif;?>
    <?php echo do_shortcode($content);?>
    <?php if($button=='yes'):?>
    <div class="clearfix"></div>
    <a class="btn btn-default btn-raised" href="<?php echo $button_url;?>"><?php echo $button_text;?></a>
    <?php endif;?>
</div>	
<?php endif;?>
<?php if($layout=='layout-2'):?>
<div class="tl-feature-block-s2 service-nopad-btop service-nopad tl-no-border">
	<?php if($icon){?><i class="<?php echo $icon;?>"></i><?php }?>
    <?php if($title):?>
    <h3><?php echo $title;?></h3>
    <?php endif;?>
    <?php echo do_shortcode($content);?>
    <?php if($button=='yes'):?>
    <div class="clearfix"></div>
    <a class="btn btn-md btn-link" href="<?php echo $button_url;?>"><?php echo $button_text;?></a>
    <?php endif;?>
</div> 
<?php endif;?>
<?php if($layout=='layout-3'):?>
<div class="tl-feature-block-s3">
	<?php if($icon){?><i class="<?php echo $icon;?>"></i><?php }?>
    <div class="feature-text-s3">
    	<?php if($button=='yes'):?>        
        <a class="tl-feature-title-s3" href="<?php echo $button_url;?>"><?php echo $title?$title:$button_text;?></a>
        <?php else:?>
        <?php if($title):?>
        <h2><?php echo $title;?></h2>
        <?php endif;?>
        <?php endif;?>
        <?php echo do_shortcode($content);?>
        <?php if($button=='yes'):?>
        <div class="feature-blocks-btn-s3"><a type="button" class="btn btn-default btn-raised" href="<?php echo $button_url;?>"><?php echo $button_text;?></a></div>
        <?php endif;?>
    </div>
</div>
<?php endif;?>
<?php if($layout=='layout-4'):?>
<div class="tl-feature-block-s4">
    <?php if($icon){?><div class="tl-we-do-icon"><i class="<?php echo $icon;?>"></i></div><?php }?>
    <?php if($title):?>
    <h3><?php echo $title;?></h3>
    <?php endif;?>
    <h5><?php echo do_shortcode($content);?></h5>
	<?php if($button=='yes'):?>
    <div class="clearfix"></div>
    <a class="btn btn-default btn-raised" href="<?php echo $button_url;?>"><?php echo $button_text;?></a>
    <?php endif;?>
</div>
<?php endif;?>
 <?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}