<?php
add_shortcode( 'tlpb_post_masonry', 'tlpb_post_masonry_shortcode' );
function tlpb_post_masonry_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'post_type'=>'',
	   'post_order'=>'',
	   'post_from'=>'',
	   'page_from'=>'',
	   'posts_per_page'=>'',
	   'offset'=>'',
	   'pagination'=>'',
	   'column'=>'',
	   'feature'=>'',
	   'post_excerpt'=>'',
	   'post_length'=>'',
	   'post_element'=>'',
	   'container'=>'yes',
	   'animation'=>'no',
	   'animation_duration'=>'200ms',
	   'animation_delay'=>'200ms',
    ), $atts ) );
?>
<?php
			$order = explode('-',$post_order);
			if($post_type=='post' && $post_from=='all'){
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $posts_per_page,
					'offset'           => $offset,
				);
			}else{
				if($post_type=='post' && $post_from!='all'){
				$tax_query = array();
				$tax_query[] = array(
									'taxonomy' => 'category',
									'field' => 'slug',
									'terms' => $post_from
								);
				$args = array(
						'tax_query'		   => $tax_query,
						'orderby'          => $order[0],
						'order'            => $order[1],
						'post_type'        => $post_type,
						'post_status'      => 'publish',
						'posts_per_page'   => $posts_per_page,
						'offset'           => $offset,
					);
				}
			}

			if($post_type=='page' && $page_from=='all'){
			
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $posts_per_page,
					'offset'           => $offset,
				);
			}else{
				if($post_type=='page' && $page_from!='all'){
				$args = array(
						'post_parent'	   => $page_from,
						'orderby'          => $order[0],
						'order'            => $order[1],
						'post_type'        => $post_type,
						'post_status'      => 'publish',
						'posts_per_page'   => $posts_per_page,
						'offset'           => $offset,
					);
				}
			}
			if(isset($pagination) && $pagination=='on'){
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args['offset'] = (($paged-1)*$posts_per_page)+$offset;
			}
			
			$wp_query = new WP_Query($args);
			if ($wp_query->have_posts() ) :
			$element = explode(',',$post_element);
?>
<?php if($container=='yes'):?>
<div class="container">
    <div class="row">
<?php endif;?>
<article>
<div class="masonry-grid-<?php echo $column;?>">
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <?php if($animation && $animation!='no'):?>
    <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
    <?php endif;?>
    <div class="item">
      <div class="well"> 
        <div class="tl-blog-img">
            <?php if(isset($feature) && $feature=='on'):?>
            <?php if(has_post_thumbnail()):?>
            <figure>
            <?php /*if($column=='2'):
            the_post_thumbnail('post-mas-two');
            endif;
            if($column=='3'):
            the_post_thumbnail('post-mas-three');
            endif;
            if($column=='4'):
            the_post_thumbnail('post-mas-four');
            endif;*/
			the_post_thumbnail('full');
            ?>
            </figure>
            <div class="post-image-hover"> </div>
            <p class="post_hover">
            <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
            <a class="icon readmore_link" href="<?php the_permalink();?>">
            <i class="fa fa-link"></i>
            </a>
            <?php endif;?>
            </p>
            <?php endif;?>
            <?php endif;?>
              
        </div>
        
        <div class="tl-blog-text">
         <div class="entry-meta">
            <?php if(isset($element) && in_array('show_date',$element)):?>
            <span class="entry-date"> <i class="fa fa-calendar"></i><?php echo tl_posted_on();?></span>
            <?php endif;?>
            
            <?php if(isset($element) && in_array('show_author',$element)):?>
            <span class="entry-author"><i class="fa fa-user"></i><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
            </span> 
            <?php endif;?>
            
            <?php if(isset($element) && in_array('show_category',$element)):?>
            <?php $terms = get_the_terms(get_the_ID(), 'category');?>
            <?php if(!empty($terms)){?>
            <span class="entry-category"><i class="fa fa-paper-plane"></i>
            <?php foreach($terms as $term):?>
            <a href="<?php echo get_term_link( $term->term_id );?>"><?php echo $term->name.' ';?></a>
            <?php endforeach;?>
            </span> 
            <?php }?>
            
            <?php endif;?>
            
            <?php if(isset($element) && in_array('show_tag',$element)):?>
            
            <?php $terms = get_the_terms(get_the_ID(), 'post_tag');?>
            <?php if(!empty($terms)){?>
            <span class="entry-category"><i class="fa fa-paper-plane"></i>
            <?php foreach($terms as $term):?>
            <a href="<?php echo get_term_link( $term->term_id );?>"><?php echo $term->name.' ';?></a>
            <?php endforeach;?>
            </span> 
            <?php }?>
            <?php endif;?>
            <?php if(isset($element) && in_array('show_comment_count',$element)):?>
            <span class="entry-comment">
             <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
                <a rel="category tag" href="<?php the_permalink();?>#comments"><i class="fa fa-comment"></i> <?php comments_number(); ?></a>
             <?php else:?>
                <a rel="category tag" href="javacript:void(0);"><i class="fa fa-comment"></i><?php comments_number(); ?></a>
             <?php endif;?>
             
            </span>
            <?php endif;?>
           </div>
         <?php if(isset($element) && in_array('show_title',$element)):?>
         <h4 class="tl-masonry-blog-title">
         <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
         <a href="<?php the_permalink();?>">
         <?php endif;?>
        <?php the_title();?>
        <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
         </a>
        <?php endif;?>
         </h4>
         <?php endif;?>
         <?php if(isset($post_excerpt) && $post_excerpt=='on'):?>
            <div class="post-content">
                <?php echo substr(get_the_excerpt(), 0,$post_length); ?>
            </div>
            <?php endif;?>
          <?php if(isset($element) && in_array('show_details_page_link',$element)):?>
            <a class="btn btn-raised more-link" href="<?php the_permalink();?>">Read More <i class="fa fa-angle-double-right"></i></a>
          <?php endif;?>
        </div>
      </div>
    </div>
    <?php if($animation && $animation!='no'):?>
    </div>
    <?php endif;?>
    <?php endwhile;?>
</div>
</article>
<?php if(isset($pagination) && $pagination=='on'){?>
<?php tl_pagination($wp_query->max_num_pages);?>
<?php }?>
<?php if($container=='yes'):?>
</div>
    </div>
<?php endif;?>    
<?php endif; wp_reset_query(); ?>
	
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}