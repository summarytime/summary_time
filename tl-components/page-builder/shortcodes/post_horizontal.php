<?php
add_shortcode( 'tlpb_post_horizontal', 'tlpb_post_horizontal_shortcode' );
function tlpb_post_horizontal_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'post_type'=>'',
	   'post_order'=>'',
	   'post_from'=>'',
	   'page_from'=>'',
	   'posts_per_page'=>'',
	   'offset'=>'',
	   'pagination'=>'',
	   'column'=>'',
	   'feature'=>'',
	   'post_excerpt'=>'',
	   'post_length'=>'',
	   'post_element'=>'',
	   'container'=>'yes',
	   'animation'=>'no',
	   'animation_duration'=>'200ms',
	   'animation_delay'=>'200ms',
    ), $atts ) );
?>
<?php
			$order = explode('-',$post_order);
			if($post_type=='post' && $post_from=='all'){
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $posts_per_page,
					'offset'           => $offset,
				);
			}else{
				if($post_type=='post' && $post_from!='all'){
				$tax_query = array();
				$tax_query[] = array(
									'taxonomy' => 'category',
									'field' => 'slug',
									'terms' => $post_from
								);
				$args = array(
						'tax_query'		   => $tax_query,
						'orderby'          => $order[0],
						'order'            => $order[1],
						'post_type'        => $post_type,
						'post_status'      => 'publish',
						'posts_per_page'   => $posts_per_page,
						'offset'           => $offset,
					);
				}
			}

			if($post_type=='page' && $page_from=='all'){
			
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $posts_per_page,
					'offset'           => $offset,
				);
			}else{
				if($post_type=='page' && $page_from!='all'){
				$args = array(
						'post_parent'	   => $page_from,
						'orderby'          => $order[0],
						'order'            => $order[1],
						'post_type'        => $post_type,
						'post_status'      => 'publish',
						'posts_per_page'   => $posts_per_page,
						'offset'           => $offset,
					);
				}
			}
			if(isset($pagination) && $pagination=='on'){
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args['offset'] = (($paged-1)*$posts_per_page)+$offset;
			}
			
			$wp_query = new WP_Query($args);
			if ($wp_query->have_posts() ) :
			$element = explode(',',$post_element);
?>
<?php if($container=='yes'):?>
<div class="container">
    <div class="row">
<?php endif;?>
<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
<div class="tl-bloglisting-block clearfix">
		<article>
        <?php if($animation && $animation!='no'):?>
        <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
        <?php endif;?>
		<div class="tl-blog-leftcolumn tl-float-column">
			<div class="tl-postimage-box">
			<?php if(isset($feature) && $feature=='on'):?>
			<?php if(has_post_thumbnail()):?>
            <figure>
			<?php the_post_thumbnail('full');?>
            </figure>
			<?php endif;?>
			<?php endif;?>
			</div>
		</div>
		<div class="tl-blog-rightcolumn tl-float-column">
			<div class="tl-blog-caption">
            <?php if(isset($element) && in_array('show_title',$element)):?>
            <h4 class="tl-postblog-title">
			<?php if(isset($element) && in_array('show_details_page_link',$element)):?>
			<a href="<?php the_permalink();?>">
			<?php endif;?>
			<?php the_title();?>
			<?php if(isset($element) && in_array('show_details_page_link',$element)):?>
			</a>
			<?php endif;?>
            </h4>
            <?php endif;?>
			</div>
			<div class="tl-postmeta-box">
				<?php if(isset($element) && in_array('show_author',$element)):?>
				<i class="icon-profile-male"></i>
				<span class="tl-admin"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
				</span> | 
				<?php endif;?>
				
				<?php if(isset($element) && in_array('show_date',$element)):?>
				<span class="tl-date"> <?php echo tl_posted_on();?></span> | 
				<?php endif;?>
				
				<?php if(isset($element) && in_array('show_category',$element)):?>
				<?php $terms = get_the_terms(get_the_ID(), 'category');?>
				<?php if(!empty($terms)){?>
				<i class="icon-pricetags"></i><span class="tl-post-tag">
				<?php foreach($terms as $term):?>
				<a href="<?php echo get_term_link( $term->term_id );?>"><?php echo $term->name.' ';?></a>
				<?php endforeach;?>
				</span> 
				<?php }?>
				<?php endif;?>
				
				<?php if(isset($element) && in_array('show_tag',$element)):?>
				<?php $terms = get_the_terms(get_the_ID(), 'post_tag');?>
				<?php if(!empty($terms)){?>
				<i class="icon-pricetags"></i><span class="tl-post-tag">
				<?php foreach($terms as $term):?>
				<a href="<?php echo get_term_link( $term->term_id );?>"><?php echo $term->name.' ';?></a>
				<?php endforeach;?>
				</span> 
				<?php }?>
				<?php endif;?>
			</div>
			<?php if(isset($post_excerpt) && $post_excerpt=='on'):?>
			<div class="post-content">
				<?php echo substr(get_the_excerpt(), 0,$post_length); ?>
			</div>
			<?php endif;?>
			<p class="tl-post-footer">
			<?php if(isset($element) && in_array('show_details_page_link',$element)):?>
			<a class="readmore-blog" href="<?php the_permalink();?>">Read More</a>
			<?php endif;?>
			<?php if(isset($element) && in_array('show_comment_count',$element)):?>
			<?php if(isset($element) && in_array('show_details_page_link',$element)):?>
			<a class="tl-post-comment pull-right" href="<?php the_permalink();?>#comments"><i class="icon-chat"></i> <?php comments_number(); ?></a>
			<?php else:?>
			<div class="tl-post-comment pull-right"><i class="icon-chat"></i> <?php comments_number(); ?></div>
			<?php endif;?>
			<?php endif;?>
			</p>
		</div>
        <?php if($animation && $animation!='no'):?>
        </div>
        <?php endif;?>
		</article>
</div><!--tl-bloglisting-block end--->
<?php endwhile;?>
<?php if(isset($pagination) && $pagination=='on'){?>
<?php tl_pagination($wp_query->max_num_pages);?>
<?php }?>
<?php if($container=='yes'):?>
</div>
    </div>
<?php endif;?>
         
<?php endif; wp_reset_query(); ?>
	
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}