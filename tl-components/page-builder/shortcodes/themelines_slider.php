<?php
add_shortcode( 'tlpb_themelines_slider', 'tlpb_themelines_slider_shortcode' );
function tlpb_themelines_slider_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'layout'=>'',
	   'category'=>'',
	   'interval'=>'',
	   'key'=>'',
	   'container'=>'yes'
    ), $atts ) );
?>
	<?php if($layout=='box'):?>
    <div class="container">
    <div class="row">
    <?php endif;?>
    <?php $cat = explode(',',$category);?>
    <?php 
			$tax_query = array();
			$tax_query[] = array(
								'taxonomy' => 'slider_category',
								'field' => 'term_id',
								'terms' => $cat
							);
			$args = array(
						'post_type' => 'slider',
						'tax_query' => $tax_query,
						'post_status' => 'publish',
				 );
				 $loop = new WP_Query($args);
				 if($loop->have_posts()) {
	?>
	<div class="tl-slider-slide tl-full-slider">
		
		<!-- Carousel -->
    	<div id="caraousel-<?php echo $key;?>" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
            	<?php $i=0;?>
            	<?php while($loop->have_posts()) : $loop->the_post();?>
			  	<li data-target="#caraousel-<?php echo $key;?>" data-slide-to="<?php echo $i;?>" <?php echo ($i==0)?'class="active"':'';?>></li>
				<?php $i++;?>
				<?php endwhile;?>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
            	<?php $i=0;?>
            	<?php while($loop->have_posts()) : $loop->the_post();?>
			    <div class="item <?php echo ($i==0)?'active':'';?>">
                	<?php //if($layout=='box'):?>
			    	<?php //the_post_thumbnail('banner-inside-container');?>
                    <?php //else:?>
                    <?php the_post_thumbnail('full', array('class'=>'caraousel-img'));?>
                    <?php //endif;?>
                    <!-- Static Header -->
                    <div class="tl-header-text">
                    <?php if(!get_post_meta(get_the_ID(), 'slider_layer_active', true) || get_post_meta(get_the_ID(), 'slider_layer_active', true)=='no'):?>
                    
                        <div class="col-md-12 text-center">
                            <?php the_content();?>
                        </div>
                    <!-- /header-text -->
                    <?php else:?>
                    <div class="tl-text-position">
                    <?php $slider_layer = get_post_meta(get_the_ID(), 'slider_layer', true);?>
                    <?php if($slider_layer):?>
                    <div class="<?php echo get_post_meta(get_the_ID(), 'layer_text_position', true)?get_post_meta(get_the_ID(), 'layer_text_position', true):'text-center';?>">
                    <?php foreach($slider_layer as $key1=>$value):?>
                    <?php if(isset($value['sec_animation']) && $value['sec_animation']!='no'):?>
                    <div data-animation="animated <?php echo $value['sec_animation'];?>" style="animation-duration: <?php echo $value['sec_animation_duration'];?>; animation-delay: <?php echo $value['sec_animation_delay'];?>;">
                    <?php endif;?>
                       
                        	<?php if(isset($value['layer_text']) && $value['layer_text']):?>
                        	<div class="tl_slider_content">
                                <<?php echo isset($value['heading'])?$value['heading']:'h1';?> class="<?php echo isset($value['text_colour']) && $value['text_colour']!='default'?$value['text_colour']:'';?>" <?php echo isset($value['text_custom_colour']) && $value['text_custom_colour']?'style="color:'.$value['text_custom_colour'].';"':'';?>>
                                    <?php echo isset($value['layer_text'])?$value['layer_text']:'';?>
                                </<?php echo isset($value['heading'])?$value['heading']:'h1';?>>
                            </div>
                            <?php endif;?>
                            <?php if(isset($value['show_button']) && $value['show_button']=='1'):?>
                            <?php $btn_style = '';
							if($value['button_text_custom_color'] || $value['button_background_custom_color']){
								$btn_style .= ' style="';
							}
							if(isset($value['button_text_custom_color']) && $value['button_text_custom_color']){
								$btn_style .='color:'.$value['button_text_custom_color'].';';
							}
							if(isset($value['button_background_custom_color']) && $value['button_background_custom_color']){
								$btn_style .='background-color:'.$value['button_background_custom_color'].';';
							}
							if($value['button_text_custom_color'] || $value['button_background_custom_color']){
								$btn_style .= '"';
							}
							?>
                            <div class="tl_slider_btn">
                            	<div class="<?php echo isset($value['button_text_colour']) && $value['button_text_colour']!='default'?$value['button_text_colour']:'';?> <?php echo isset($value['button_background_colour']) && $value['button_background_colour']!='default'?$value['button_background_colour']:'';?>"<?php echo $btn_style;?>>
                                    <a href="<?php echo isset($value['button_url']) && $value['button_url']?$value['button_url']:'#';?>">
                                        <?php echo isset($value['button_text']) && $value['button_text']?$value['button_text']:'';?>
                                    </a>
                                </div>
                             </div>
                             <?php endif;?>
                        
                    <?php if(isset($value['sec_animation']) && $value['sec_animation']!='no'):?>
                    </div>
                    <?php endif;?>
                    <?php endforeach;?>
                    </div>
                    <?php endif;?>
                    <?php endif;?>
                    </div>
                    </div>
			    </div>
                <?php $i++;?>
                <?php endwhile;?>
                <?php wp_reset_query();?>
			    
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#caraousel-<?php echo $key;?>" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#caraousel-<?php echo $key;?>" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div><!-- /carousel -->
	
    </div>
    <script>
	jQuery('#caraousel-<?php echo $key;?>').carousel({
	  interval: <?php echo $interval;?>,
	  //afterInit : attachEvent
	});
	var car_width = jQuery('#caraousel-<?php echo $key;?>').find('.item').first().width();
	var car_height = jQuery('#caraousel-<?php echo $key;?>').find('.item img.wp-post-image').first().height();
	jQuery('#caraousel-<?php echo $key;?>').find('.item').each(function(){
		jQuery(this).find('img.wp-post-image').attr('sizes','(max-width: '+car_width+'px) 100vw, '+car_width+'px');
		jQuery(this).find('img.wp-post-image').attr('width',car_width);
		jQuery(this).find('img.wp-post-image').attr('height',car_height);
		//jQuery(this).attr('height',jQuery(this).find('img.wp-post-image').height());
	});		
	
	/* Demo Scripts for Bootstrap Carousel and Animate.css article
	* on SitePoint by Maria Antonietta Perna
	*/
	(function( $ ) {
	
		//Function to animate slider captions 
		function doAnimations( elems ) {
			//Cache the animationend event in a variable
			var animEndEv = 'webkitAnimationEnd animationend';
			
			elems.each(function () {
				var $this = $(this),
					$animationType = $this.data('animation');
				$this.addClass($animationType).one(animEndEv, function () {
					$this.removeClass($animationType);
				});
			});
		}
		
		//Variables on page load 
		var $myCarousel = $('#caraousel-<?php echo $key;?>'),
			$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
			
		//Initialize carousel 
		$myCarousel.carousel();
		
		//Animate captions in first slide on page load 
		doAnimations($firstAnimatingElems);
		
		//Pause carousel  
		$myCarousel.carousel('pause');
		
		
		//Other slides to be animated on carousel slide event 
		$myCarousel.on('slide.bs.carousel', function (e) {
			var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
			doAnimations($animatingElems);
		});  
		
	})(jQuery);
	</script>
<?php }?>
<?php if($layout=='box'):?>
</div>
</div>
<?php endif;?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}