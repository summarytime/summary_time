<?php
add_shortcode( 'tl_cta', 'tl_cta_shortcode' );
function tl_cta_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'button_text'=>'',
	   'button_url'=>'javascript:void(0);',
	   'button_icon'=>'',
	   'position'=>'left',
	   'background_color'=>'',
	   'background_image'=>'',
    ), $atts ) );
	if($background_color || $background_image){
		$style = ' style="';
		if($background_color){
			$style .= 'background-color:'.$background_color.';';
		}
		if($background_image){
			$style .= "background:url('".$background_image."');";
		}
		
		$style .= '"';
	}
 ?>
<?php if($position=='left' || $position=='right'):?>
<div class="tl-call-2action-1 dark-1 clearfix"<?php echo isset($style) && $style?$style:'';?>>
<?php if($position=='left'):?>        
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo do_shortcode($content);?></div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a class="tl-btn-shutter-out-horizontal" href="<?php echo $button_url;?>"><?php echo $button_icon?'<i class="'.$button_icon.'"></i>':'';?> <?php echo $button_text;?></a></div>
<?php endif;?>
<?php if($position=='right'):?>        
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a class="tl-btn-shutter-out-horizontal" href="<?php echo $button_url;?>"><?php echo $button_icon?'<i class="'.$button_icon.'"></i>':'';?> <?php echo $button_text;?></a></div>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><?php echo do_shortcode($content);?></div>
<?php endif;?>
</div>
<?php endif;?>
<?php if($position=='below' || $position=='above'):?>
<div class="tl-business-solutions-light clearfix"<?php echo isset($style) && $style?$style:'';?>>           
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<?php if($position=='above'):?>
<?php echo do_shortcode($content);?>
<a class="tl-btn-shutter-out-horizontal" href="<?php echo $button_url;?>"><?php echo $button_text;?> <?php echo $button_icon?'<i class="'.$button_icon.'"></i>':'';?></a>
<?php endif;?>
<?php if($position=='below'):?>
<a class="tl-btn-shutter-out-horizontal" href="<?php echo $button_url;?>"><?php echo $button_text;?> <?php echo $button_icon?'<i class="'.$button_icon.'"></i>':'';?></a>
<?php echo do_shortcode($content);?> 
<?php endif;?>
</div>
</div>
<?php endif;?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}