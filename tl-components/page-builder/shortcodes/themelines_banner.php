<?php
add_shortcode( 'tlpb_themelines_banner', 'tlpb_themelines_banner_shortcode' );
function tlpb_themelines_banner_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'layout'=>'',
	   'banner_parallax'=>'',
       'slider'=>'',
	   'container'=>'yes'
    ), $atts ) );
?>
	<?php if($banner_parallax==0){ $stl = "background-attachment:scroll;";}else{$stl = "";}?>
    
   <?php if($layout=='box'):?>
    <div class="container">
    <div class="row">
    <?php endif;?>
    <?php $banner = get_post($slider);?>
    <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($slider) );?>
    <?php if(isset($feat_image) && $feat_image):?>
	<div id="tl-slider-bg" style="<?php echo $stl;?>background-image:url('<?php echo $feat_image;?>');">
    <?php else:?>
    <?php if(get_post_meta( $slider, 'slider_video', true )):?>
    <div class="tl-video-one">
        <figure class="video-inner">
          <video loop muted="muted" autoplay>
            <source src="<?php echo get_post_meta( $slider, 'slider_video', true );?>" type="video/mp4">
          </video>
          <div class="video-ovaly-border"></div>
        </figure>
        <div class="video-wrapper-content">
        	<div class="tl-video-content-inner">
            <?php echo wpautop(do_shortcode($banner->post_content));?>
            </div>
        </div>
    </div>
    <?php //endif;?>
    <?php else:?>
    <?php if(get_post_meta( $slider, 'slider_video_iframe', true )):?>
    <?php //if($banner_parallax==1):?>
	<!--<div id="youtube" class="bg_parallax" data-type="background" data-speed="10">
         <div id="down_video_bg" class="tl-iframe-videoBg">
             <?php //echo get_post_meta( $slider, 'slider_video_iframe', true );?>
         </div>
        
        <div class="youtube-video-content">
            <?php //echo wpautop(do_shortcode($banner->post_content));?>
        </div>
        
        <div class="video-ovaly-border"></div>
        <div class="clearfix"></div>
    </div>-->
    <?php //else:?>
    <div class="tl-video-one">
        <div class="video-container">
            <div class="video-foreground">
            <?php echo get_post_meta( $slider, 'slider_video_iframe', true );?>
            </div>
            <div class="video-ovaly-border"></div>
        </div>
        <div class="video-wrapper-content">
            <?php echo wpautop(do_shortcode($banner->post_content));?>
        </div>
    </div>
    <?php //endif;?>
    <?php endif;?>
    <?php endif;?>
    <?php endif;?>
    		<?php if((isset($feat_image) && $feat_image) || (!get_post_meta( $slider, 'slider_video', true ) && !get_post_meta( $slider, 'slider_video_iframe', true ))):?>
            <?php if($container=='yes'):?>
            <div class="container">
                <div class="row">
                <?php endif;?>
                <div class="col-sm-12">	
                     <div class="tl-slider-fix-back2 clearfix">
                        <div class="col-lg-12">
                              <?php echo wpautop(do_shortcode($banner->post_content));?>
                        </div> 
                    </div>
                </div>
            <?php if($container=='yes'):?>
            </div>
        </div>
        <?php endif;?>
        	<?php endif;?>
    <?php if(isset($feat_image) && $feat_image):?>  
	</div>
    <?php endif;?>
    <?php if($layout=='box'):?>
    </div>
    </div>
    <?php endif;?>
    <?php wp_reset_query();?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}