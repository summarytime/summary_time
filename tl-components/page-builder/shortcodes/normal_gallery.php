<?php
add_shortcode( 'tlpb_normal_gallery', 'tlpb_normal_shortcode' );
function tlpb_normal_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'key'=>'',
	   'items'=>'', 
	   'categories'=>'',
	   'filter'=>'',
	   'gal_grid'=>'',
	   'onclick'=>'',
	   'load_more'=>'0',
	   'container'=>'yes'
    ), $atts ) );
$cats = explode(',',$categories);
$tax_query = array();
$tax_query[] = array(
	'taxonomy' => 'gallery_category',
	'field' => 'id',
	'terms' => $cats
);
$args = array(
	'post_type' => 'gallery',
	'tax_query' => $tax_query,
	'posts_per_page' => $items,
	'post_status' => 'publish',
);
$loop = new WP_Query($args);
if($loop->have_posts()):
$gal_cat_slug = array();
$gal_cat = array();
foreach($cats as $cat){
	$gal_cat_details = get_term_by('id', $cat, 'gallery_category');
	$gal_cat_slug[] = $gal_cat_details->slug;
	$gal_cat[] = $gal_cat_details->name;
}
$style = '';
$col = 12/$gal_grid;
?>
<?php if($container=='yes'):?>
<div class="container">
<?php endif;?>
<div class="filter-container isotopeFilters">
	<div class="row">
    <div class="col-sm-12">
    <ul id="filters" class="clearfix" style="<?php echo $filter!=1?'display:none;':'';?>">
    	<?php $all = implode(' ',$gal_cat_slug);?>
        <li><span class="filter<?php echo $key;?> active" data-filter="<?php echo $all;?>">All</span></li>
        <?php $j=0;?>
        <?php foreach($gal_cat as $cate):?>
        <?php if($j<count($gal_cat)-1):?>
        <li><span class="filter<?php echo $key;?>" data-filter="<?php echo $gal_cat_slug[$j];?>"><?php echo $gal_cat[$j];?></span></li>
        <?php endif;?>
        <?php $j++;?>
        <?php endforeach;?>
    </ul>
    </div>
    </div>
</div>
<div id="portfoliolist<?php echo $key;?>" class="portfoliolist">
	<div class="row">
    <div class="port-container<?php echo $key;?>">
	<?php 
		$count = 1;
		while($loop->have_posts()):
		$loop->the_post();
		if(has_post_thumbnail()):
		$cats = '';
		$cats_class = '';
		$gallery_terms = array();
		$term_list = wp_get_post_terms(get_the_ID(), 'gallery_category', array("fields" => "all"));
		foreach($term_list as $terms){
			$gallery_terms[] = $terms->slug;
		}
		$j=1;
		$cats = implode(' ',$gallery_terms);
		$fancy_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
		//if ($count%$gal_grid == 1){echo '<div class="row">';}
	?>
    <div class="col-lg-<?php echo $col;?> col-sm-6 col-xs-12 portfolio <?php echo $cats;?>" data-cat="<?php echo $cats;?>">
            <div class="portfolio-wrapper port<?php echo $key;?>">	
                <div class="image-block-folio">		
                    <?php the_post_thumbnail('large', array('class'=>'gal-img'));?>
                    <div class="hover-icon-folio">
                       <?php if($onclick=='fancybox'):?>
                        <a class="fancybox-<?php echo $key;?> gal-icon" rel="<?php echo $key;?>" href="<?php echo $fancy_image[0];?>" title="<?php the_title();?>">
                            <i class="fa fa-search"></i>
                        </a>
                        <?php elseif($onclick=='new_page'):?>
                        <a class="gal-icon" href="<?php the_permalink();?>">
                            <i class="fa fa-link"></i>
                        </a>
                        <?php else:?>
                        <i class="fa fa-picture-o"></i>
                        <?php endif;?>
                    </div>
                </div>
                <div class="label">
                    <div class="label-text">
                        <?php if($onclick!='fancybox' || $onclick!='new_page'):?>
                        <div class="text-title"><?php the_title();?></div>
                        <?php else:?>
                        <a class="text-title" href="javascript:void(0);"><?php the_title();?></a>
                        <?php endif;?>
                    </div>
                    <div class="label-bg"></div>
                </div>
            </div>
        </div>
        
    <?php //if ($count%$gal_grid == 0) echo '<div class="clearfix"></div>';?>
	<?php
        $count++;
        endif;
        endwhile;
    ?>
    </div>
    <?php if($items!=-1 && $load_more==1):?>
    <div class="col-sm-12 text-center">
    <a href="javascript:void(0);" class="btn btn-primary" id="load<?php echo $key;?>">Load More</a>
    </div>
    <?php endif;?>
	</div>
</div>
<?php if($container=='yes'):?>
</div>
<?php endif;?>
    <script>
	jQuery(window).load(function() {
		equal_height<?php echo $key;?>();
	});
	jQuery(window).resize(function(){
		equal_height<?php echo $key;?>();
	});
	function equal_height<?php echo $key;?>(){
		equalheight('.port<?php echo $key;?>');
	}
	function filtering<?php echo $key;?>(){
		jQuery(function () {
			var filterList = {
				init: function () {
					jQuery('#portfoliolist<?php echo $key;?>').mixitup({
						targetSelector: '.portfolio',
						filterSelector: '.filter<?php echo $key;?>',
						effects: ['fade'],
						easing: 'snap',
						onMixEnd: filterList.hoverEffect()
					});				
				},
				hoverEffect: function () {
					jQuery('#portfoliolist<?php echo $key;?> .portfolio').hover(
						function () {
							jQuery(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
							jQuery(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
						},
						function () {
							jQuery(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
							jQuery(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
						}		
					);				
				}
			};
			filterList.init();
		});
		jQuery('a.text-title').click(function(){
			jQuery(this).closest('.portfolio').find('.gal-icon').trigger('click');
		});
		 jQuery(document).ready(function() {
			<?php if($onclick=='fancybox'):?>
			jQuery(".fancybox-<?php echo $key;?>").fancybox({
				openEffect	: 'none',
				closeEffect	: 'none',
				helpers: {
				  title : {
					  type : 'float'
				  }
			  }
			});
			<?php endif;?>
			jQuery('.portfolio-wrapper').each(function(index, element) {
				jQuery(this).find('.image-block-folio').css('width',jQuery(this).width());
			});
		});
	}
	filtering<?php echo $key;?>();
	var offset<?php echo $key;?> = 0;
	jQuery('#load<?php echo $key;?>').on('click', function(){
		var $this = jQuery(this);
		$this.html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
		var key = '<?php echo $key;?>';
		var items = '<?php echo $items;?>';
		var categories = '<?php echo $categories;?>';
		var filter = '<?php echo $filter;?>';
		var gal_grid = '<?php echo $gal_grid;?>';
		var onclick = '<?php echo $onclick;?>';
		var load_more = '<?php echo $load_more;?>';
		var count = '<?php echo $count-1;?>';
		offset<?php echo $key;?> = parseInt(offset<?php echo $key;?>) + parseInt(count);
		jQuery.ajax({
			type: 'POST',
			url: '<?php echo admin_url( 'admin-ajax.php' );?>',
			data: {
				action: 'load_gallery',
				key: key,
				items: items,
				categories: categories,
				filter: filter,
				gal_grid: gal_grid,
				onclick: onclick,
				load_more: load_more,
				count: count,
				offset: offset<?php echo $key;?>,
				type: 'column'
			},
			success: function(response){
				if(response!='no'){
					jQuery('.port-container<?php echo $key;?>').append(response);
					$this.html('Load More');
				}else{
					$this.remove();
				}
				jQuery('.filter<?php echo $key;?>.active').removeClass('active');
				jQuery('.filter<?php echo $key;?>').first().addClass('active');
				filtering<?php echo $key;?>();
				equal_height<?php echo $key;?>();
			}
		});
	});
	</script>
	<?php wp_reset_query();?>
<?php
	endif;
 	$myvariable = ob_get_clean();
    return $myvariable;
}