<?php
add_shortcode( 'tlpb_masonry_gallery', 'tlpb_masonry_gallery_shortcode' );
function tlpb_masonry_gallery_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'key'=>'',
	   'items'=>'', 
	   'categories'=>'',
	   'filter'=>'',
	   'gal_grid'=>'',
	   'onclick'=>'',
	   'load_more'=>'0',
	   'container'=>'yes'
    ), $atts ) );
$cats = explode(',',$categories);
$tax_query = array();
$tax_query[] = array(
	'taxonomy' => 'gallery_category',
	'field' => 'id',
	'terms' => $cats
);
$args = array(
	'post_type' => 'gallery',
	'tax_query' => $tax_query,
	'posts_per_page' => $items,
	'post_status' => 'publish',
);
$cats = explode(',',$categories);
$tax_query = array();
$tax_query[] = array(
	'taxonomy' => 'gallery_category',
	'field' => 'id',
	'terms' => $cats
);
$args = array(
	'post_type' => 'gallery',
	'tax_query' => $tax_query,
	'posts_per_page' => $items,
	'post_status' => 'publish',
);
$loop = new WP_Query($args);
if($loop->have_posts()):
$gal_cat_slug = array();
$gal_cat = array();
foreach($cats as $cat){
	$gal_cat_details = get_term_by('id', $cat, 'gallery_category');
	$gal_cat_slug[] = $gal_cat_details->slug;
	$gal_cat[] = $gal_cat_details->name;
}
$style = '';
$col = 12/$gal_grid;
?>
<?php if($container=='yes'):?>
<div class="container">
<?php endif;?>
<div class="filter-section">
	<div class="filter-container isotopeFilters">
        <ul class="list-inline filter">
            <li class="active"><a href="#" data-filter="*">All </a><span>/</span></li>
            <?php $j=0;?>
            <?php foreach($gal_cat as $cate):?>
            <?php if($j<count($gal_cat)-1):?>
            <li><a href="#" data-filter=".<?php echo $gal_cat_slug[$j];?>"><?php echo $gal_cat[$j];?></a><span>/</span></li>
            <?php else:?>
            <li><a href="#" data-filter=".<?php echo $gal_cat_slug[$j];?>"><?php echo $gal_cat[$j];?></a></li>
            <?php endif;?>
            <?php $j++;?>
            <?php endforeach;?>
        </ul>
    </div>
</div> 
<div class="portfolio-section port-col">
    <div class="isotopeContainer">
        <?php 
            $count = 1;
            while($loop->have_posts()):
            $loop->the_post();
            if(has_post_thumbnail()):
            $cats = '';
            $cats_class = '';
            $gallery_terms = array();
            $term_list = wp_get_post_terms(get_the_ID(), 'gallery_category', array("fields" => "all"));
            foreach($term_list as $terms){
                $gallery_terms[] = $terms->slug;
            }
            $j=1;
            $cats = implode(' ',$gallery_terms);
            $fancy_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
        ?>
        <div class="col-sm-<?php echo $col;?> isotopeSelector no-padding <?php echo $cats;?>">
            <article class="">
                <figure>
                    <?php the_post_thumbnail('full', array('class'=>'img-responsive'));?>
                    <div class="overlay-background">
                        <div class="inner"></div>
                    </div>
                    <div class="overlay">
                        <div class="inner-overlay">
                            <div class="inner-overlay-content with-icons">
                                <?php if($onclick=='fancybox'):?>
                                <a class="fancybox-<?php echo $key;?> gal-icon" rel="<?php echo $key;?>" href="<?php echo $fancy_image[0];?>" title="<?php the_title();?>">
                                    <i class="fa fa-search"></i>
                                </a>
                                <?php elseif($onclick=='new_page'):?>
                                <a class="gal-icon" href="<?php the_permalink();?>">
                                    <i class="fa fa-link"></i>
                                </a>
                                <?php else:?>
                                <i class="fa fa-picture-o"></i>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </figure>
                <div class="article-title">
                    <?php if($onclick!='fancybox' && $onclick!='new_page'):?>
                    <div class="text-title"><?php the_title();?></div>
                    <?php else:?>
                    <a class="text-title" href="javascript:void(0);"><?php the_title();?></a>
                    <?php endif;?>
                </div>
            </article>
        </div>
        <?php
            $count++;
            endif;
            endwhile;
            wp_reset_query();
        ?>
    </div>
</div>
<?php if($container=='yes'):?>
</div>
<?php endif;?>

   <script>
	jQuery('a.text-title').click(function(){
		jQuery(this).closest('.isotopeSelector').find('.gal-icon').trigger('click');
	});
	</script>
   
    <?php if($onclick=='fancybox'):?>
    <script>
     jQuery(document).ready(function() {
        jQuery(".fancybox-<?php echo $key;?>").fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
			helpers: {
              title : {
                  type : 'float'
              }
          }
        });
		jQuery('.portfolio-wrapper').each(function(index, element) {
            jQuery(this).find('.image-block-folio').css('width',jQuery(this).width());
        });
    });
	</script>
    <?php endif;?>
	<?php wp_reset_query();?>
<?php
	endif;
 	$myvariable = ob_get_clean();
    return $myvariable;
}