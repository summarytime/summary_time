<?php
add_shortcode( 'tlpb_section', 'tlpb_section_shortcode' );
function tlpb_section_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'class'=>'',
	   'id'=>'',
	   'title'=>'',
	   'title_tag'=>'h2',
	   'title_position'=>'',
	   'title_color_scheme'=>'default',
	   'title_animation'=>'no',
	   'title_animation_delay'=>'200ms',
	   'title_animation_duration'=>'200ms',
	   'subtitle'=>'',
	   'subtitle_position'=>'',
	   'subtitle_tag'=>'h3',
	   'subtitle_color_scheme'=>'default',
	   'subtitle_animation'=>'no',
	   'subtitle_animation_delay'=>'200ms',
	   'subtitle_animation_duration'=>'200ms',
	   'content_color_scheme'=>'default',
	   'content_animation'=>'no',
	   'content_animation_delay'=>'200ms',
	   'content_animation_duration'=>'200ms',
	   'title_color'=>'',
	   'subtitle_color'=>'',
	   'content_color'=>'',
	   'padding'=>'0',
	   'background_color'=>'',
	   'background_color_scheme'=>'default',
	   'background_image'=>'',
	   'image_opacity'=>'',
	   'parallax'=>'0',
	   'type'=>'',
	   'margin_top'=>'normal-0',
	   'margin_bottom'=>'normal-0',
	   'margin_left'=>'normal-0',
	   'margin_right'=>'normal-0',
	   'padding_top'=>'normal-0',
	   'padding_bottom'=>'normal-0',
	   'padding_left'=>'normal-0',
	   'padding_right'=>'normal-0'
    ), $atts ) );
	$margin_t = explode('-',$margin_top);
	$margin_b = explode('-',$margin_bottom);
	$margin_l = explode('-',$margin_left);
	$margin_r = explode('-',$margin_right);
	
	$padding_t = explode('-',$padding_top);
	$padding_b = explode('-',$padding_bottom);
	$padding_l = explode('-',$padding_left);
	$padding_r = explode('-',$padding_right);
	
	if(!empty($background_image) || !empty($background_color)){
		$style	=	' style="';
	}
	else{
		$style	=	'';
	}
	if(!empty($background_image)){
		$style .= "background-image: url('".$background_image."');";
		$tlpb_class	=	'tlpb_bg';
		if($parallax=='1'){
			$tlpb_class	.=	'_fixed';
		}
	}
	else{
		$tlpb_class	=	'';
	}
	if(!empty($background_color)){
		$style .= "background-color:".$background_color.";";
	}
	if(!empty($background_image) || !empty($background_color)){
		$style	.=	'"';
	}
	$margin_padding = '';
	if($padding==1){
		$pad = ' tl-common-gap';
	}else{
		$pad = '';
	}
	if($margin_t[0]=='normal' && $margin_t[1]>0){
		$margin_padding .= ' tl-margin-top'.$margin_t[1];
	}
	if($margin_b[0]=='normal' && $margin_b[1]>0){
		$margin_padding .= ' tl-margin-bottom'.$margin_b[1];
	}
	if($margin_l[0]=='normal' && $margin_l[1]>0){
		$margin_padding .= ' tl-margin-left'.$margin_l[1];
	}
	if($margin_r[0]=='normal' && $margin_r[1]>0){
		$margin_padding .= ' tl-margin-right'.$margin_r[1];
	}
	if($padding_t[0]=='normal' && $padding_t[1]>0){
		$margin_padding .= ' tl-padding-top'.$padding_t[1];
	}
	if($padding_b[0]=='normal' && $padding_b[1]>0){
		$margin_padding .= ' tl-padding-bottom'.$padding_b[1];
	}
	if($padding_l[0]=='normal' && $padding_l[1]>0){
		$margin_padding .= ' tl-padding-left'.$padding_l[1];
	}
	if($padding_r[0]=='normal' && $padding_r[1]>0){
		$margin_padding .= ' tl-padding-right'.$padding_r[1];
	}
	$background_color_class = ($background_color_scheme && $background_color_scheme!='default')?' color-'.$background_color_scheme:'';
 ?>
<section class="tlpb<?php echo (!empty($tlpb_class))?' '.$tlpb_class:'';?><?php echo (!empty($class))?' '.$class:'';?><?php echo $background_color_class;?><?php echo (!empty($class))?' '.$class:'';?><?php echo $pad;?><?php echo $margin_padding;?>"<?php echo (!empty($id))?' id="'.$id.'"':'';?> <?php echo $style;?>>
	<?php
	if($title || $subtitle || $type=='column'):?>
	<div class="container">
		<div class="row">
        	<?php if((isset($title) && $title) || (isset($subtitle) && $subtitle)):?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php if(isset($title) && $title):?>
                <?php if($title_color){
					$title_style = ' style="color:'.$title_color.';"';
				}else{
					$title_style = '';
				}
				$title_color_class = ($title_color_scheme && $title_color_scheme!='default')?' font_'.$title_color_scheme:'';
				?>
                <?php if($title_animation && $title_animation!='no'):?>
                <div class="wow <?php echo $title_animation;?>" data-wow-delay="<?php echo $title_animation_delay;?>" data-wow-duration="<?php echo $title_animation_duration;?>">
        		<?php endif;?>
                <<?php echo $title_tag?$title_tag:'h2';?> class="tl-section-heading <?php echo $title_position;?><?php echo $title_color_class;?>"<?php echo $title_style;?>><?php echo $title;?></<?php echo $title_tag?$title_tag:'h2';?>>
                <?php if($title_animation && $title_animation!='no'):?>
                </div>
                <?php endif;?>
                <?php endif;?>
                <?php if(isset($subtitle) && $subtitle):?>
                <?php if($subtitle_color){
					$subtitle_style = ' style="color:'.$subtitle_color.';"';
				}else{
					$subtitle_style = '';
				}
				$subtitle_color_class = ($subtitle_color_scheme && $subtitle_color_scheme!='default')?' font_'.$subtitle_color_scheme:'';
				?>
                <?php if($subtitle_animation && $subtitle_animation!='no'):?>
                <div class="wow <?php echo $subtitle_animation;?>" data-wow-delay="<?php echo $subtitle_animation_delay;?>" data-wow-duration="<?php echo $subtitle_animation_duration;?>">
        		<?php endif;?>
                <<?php echo $subtitle_tag?$subtitle_tag:'h3';?> class="tl-section-subheading <?php echo $subtitle_position;?><?php echo $subtitle_color_class;?>"<?php echo $subtitle_style;?>><?php echo $subtitle;?></<?php echo $subtitle_tag?$subtitle_tag:'h3';?>>
                <?php if($subtitle_animation && $subtitle_animation!='no'):?>
                </div>
                <?php endif;?>
                <?php endif;?>
            </div>
            <?php endif;?>
        <?php if($type!='column'):?>
        </div>
        <?php endif;?>
     <?php if($type!='column'):?>
     </div>
     <?php endif;?>
     <?php endif;?>
     <?php if($content_color){
		 		$content_style = ' style="color:'.$content_color.';"';
	 		}else{
				$content_style = '';
			}
			$content_color_class = ($content_color_scheme && $content_color_scheme!='default')?' font_'.$content_color_scheme:'';
	 ?>
     <?php if($content_style || $content_color_class){echo '<div class="'.$content_color_class.'"'.$content_style.'>';}?>
     <?php if($content_animation && $content_animation!='no'):?>
     <div class="wow <?php echo $content_animation;?>" data-wow-delay="<?php echo $content_animation_delay;?>" data-wow-duration="<?php echo $content_animation_duration;?>">
     <?php endif;?>
     <?php echo do_shortcode($content);?>
     <?php if($content_animation && $content_animation!='no'):?>
     </div>
     <?php endif;?>
     <?php if($content_style || $content_color_class){echo '</div>';}?>
     <?php if($type=='column'){echo '</div>';}?>
     <?php if($type=='column'){ echo '</div>';}?>
</section>
 <?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}