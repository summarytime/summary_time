<?php
add_shortcode( 'tlpb_other', 'tlpb_other_shortcode' );
function tlpb_other_shortcode( $atts, $content ) {
    ob_start();
	 extract( shortcode_atts( array (
    ), $atts ) );
?>
<?php
 	echo do_shortcode($content);
?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}