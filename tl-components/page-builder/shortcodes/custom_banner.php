<?php
add_shortcode( 'tlpb_custom_banner', 'tlpb_custom_banner_shortcode' );
function tlpb_custom_banner_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
       'layout'=>'',
	   'banner_parallax'=>'',
	   'sec_class'=>'',
	   'sec_id'=>'',
	   'animation'=>'',
	   'animation_delay'=>'',
	   'animation_duration'=>'',
	   'img'=>''
    ), $atts ) );
?>
	<?php if($banner_parallax==0){ $stl = "background-attachment:scroll;";}else{$stl = "";}?>
    
    <?php if($layout=='box'):?>
    <div class="container">
    <div class="row">
    <?php endif;?>
	<div id="tl-slider-bg" style="<?php echo $stl;?>background-image:url('<?php echo $img;?>');"><!--tl-slider-->
    <div class="<?php echo (!empty($sec_class))?' '.$sec_class:'';?>"<?php echo (!empty($sec_id))?' id="'.$sec_id.'"':'';?>>
		<?php if($animation!='no'):?>
        <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
        <?php endif;?>
		<div class="container">
			<div class="row">
			<div class="col-sm-12">	
				 <div class="tl-slider-fix-back2 clearfix">
					<div class="col-lg-12">
						<?php echo wpautop(do_shortcode($content));?>
					</div> 
				</div>
			</div>
		</div>
    </div>
    	<?php if($animation!='no'):?>
        </div>
        <?php endif;?>
      </div>
	</div>
    <?php if($layout=='box'):?>
    </div>
    </div>
    <?php endif;?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}