<?php
add_shortcode( 'tlpb_testimonial_chat', 'tlpb_testimonial_chat_shortcode' );
function tlpb_testimonial_chat_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'post_type'=>'testimonial',
	   'post_order'=>'',
	   'post_number'=>'',
	   'post_offset'=>'',
	   'post_column'=>'',
	   'post_element'=>'',
	   'key'=>'',
	   'container'=>'yes',
	   'animation'=>'no',
	   'animation_duration'=>'200ms',
	   'animation_delay'=>'200ms',
    ), $atts ) );
?>
<?php
			$order = explode('-',$post_order);
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $post_number,
					'offset'           => $post_offset,
				);
				
			$wp_query = new WP_Query($args);
			if ($wp_query->have_posts() ) :
			$element = explode(',',$post_element);
?>
<?php if(isset($element) && !in_array('show_image',$element)):?>
<style>
.tl-chatview1-content::after {
border-left: 0 solid rgba(0, 0, 0, 0);
border-top: 0 solid #cccccc;
}
</style>
<?php endif;?>
<div class="tl-testimonial-chatview-s1">
<div class="tl-view-team">
	<?php if($container=='yes'):?>
	<div class="container">
	<div class="row">
    <?php endif;?>
		<div class="tl-testimonials-chatview">
			<div id="chatview1-testimonials" class="owl-carousel owl-carousel-<?php echo $key;?> owl-theme">
              <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php if($animation && $animation!='no'):?>
              <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
              <?php endif;?>
			  <div class="item tl-chatview1">
			   <div class="tl-chatview1-content">
				<?php echo wpautop(get_post_meta( get_the_ID(), 'testimonial_testimonial', true ));?>
				<p><cite>&mdash;
                <?php if(isset($element) && in_array('show_name',$element)):?>
                <span class="accent"><?php echo get_post_meta( get_the_ID(), 'testimonial_author_name', true );?></span>
                <?php endif;?>
                <?php if(isset($element) && in_array('show_designation',$element)):?>	
                , <?php echo get_post_meta( get_the_ID(), 'testimonial_author_designation', true );?>
                <?php endif;?>
                </cite>
                </p>
			  </div>
              <?php if(isset($element) && in_array('show_image',$element)):?>
			  <?php if(get_post_meta( get_the_ID(), 'testimonial_author_image', true )):
				$image_id = tl_get_image_id(get_post_meta( get_the_ID(), 'testimonial_author_image', true ));
				?>
			  <div class="tl-chatview1-imgs">
				<?php echo wp_get_attachment_image( $image_id, array('88','88'), "", array( "class" => "testi-image caraousel-img" ) );?>
			  </div>
              <?php endif;?>
              <?php endif;?>
			  </div>
              <?php if($animation && $animation!='no'):?>
              </div>
              <?php endif;?>
			  <?php endwhile;?>
			</div>
            <div class="tl-chatview1 customNavigation top-button-arrow">
              <a class="btn prev"><i class="fa fa-angle-left"></i></a>
              <a class="btn next"><i class="fa fa-angle-right"></i></a>
            </div>
		</div>
    <?php if($container=='yes'):?>
	</div> 
	</div>
    <?php endif;?>
</div>
</div>
<script>
jQuery('.owl-carousel-<?php echo $key;?>').owlCarousel({
	  loop: true,
	  margin: 10,
	  nav: false,
	  autoplay: true,
	  autoplayHoverPause: true,
	  responsive: {
		0: {
		  items: 1
		},
		600: {
		  items: 1
		},
		1000: {
		  items: <?php echo $post_column;?>
		}
	  }
	});
	// Custom Navigation Events
      jQuery(".next").click(function(){
        jQuery('.owl-next').trigger('click');
      })
      jQuery(".prev").click(function(){
        jQuery('.owl-prev').trigger('click');
      })
</script>
<?php endif; wp_reset_query();?>
	
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}