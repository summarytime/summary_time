<?php
add_shortcode( 'tl_widget', 'tl_widget_shortcode' );
function tl_widget_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'id'=>'',
	   'sidebar'=>'',
    ), $atts ) );
?>
<?php if($sidebar==1):?>
<div class="sidebar-padder tl-cp-sidebar1 tl-cp-sidebar2 sidebar">
<?php endif;?>
<?php do_action( 'before_sidebar' ); ?>
<?php if ( ! dynamic_sidebar( $id ) ) : ?>
<?php endif;?>
<?php if($sidebar==1):?>
</div>
<?php endif;?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}