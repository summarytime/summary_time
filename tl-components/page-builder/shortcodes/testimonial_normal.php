<?php
add_shortcode( 'tlpb_testimonial_normal', 'tlpb_testimonial_normal_shortcode' );
function tlpb_testimonial_normal_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'post_type'=>'testimonial',
	   'post_order'=>'',
	   'post_number'=>'',
	   'post_offset'=>'',
	   'post_column'=>'',
	   'post_element'=>'',
	   'key'=>'',
	   'container'=>'yes',
	   'animation'=>'no',
	   'animation_duration'=>'200ms',
	   'animation_delay'=>'200ms',
    ), $atts ) );
?>
<?php
			$order = explode('-',$post_order);
			$args = array(
					'orderby'          => $order[0],
					'order'            => $order[1],
					'post_type'        => $post_type,
					'post_status'      => 'publish',
					'posts_per_page'   => $post_number,
					'offset'           => $post_offset,
				);
				
			$wp_query = new WP_Query($args);
			if ($wp_query->have_posts() ) :
			$element = explode(',',$post_element);
?>
<div class="tl-client dark-bg">
<div class="tl-fullwith-testimonials">
	<?php if($container=='yes'):?>
    <div class="container">
        <div class="row">
    <?php endif;?>
            <div class="tl-testimonials-section tl-slide-testimonials tl-tesimonials-bg">				      
                <div id="carousel-<?php echo $key;?>" class="carousel slide" data-ride="carousel">
                
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <?php $i=0;?>
                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <?php if($animation && $animation!='no'):?>
                    <div class="wow <?php echo $animation;?>" data-wow-delay="<?php echo $animation_delay;?>" data-wow-duration="<?php echo $animation_duration;?>">
                    <?php endif;?>
                    <div class="item <?php echo ($i==0)?'active':'';?>">
                    <?php if(isset($element) && in_array('show_image',$element)):?>
                     <div class="tl-db-testi"> 
                            <?php if(get_post_meta( get_the_ID(), 'testimonial_author_image', true )):
                            $image_id = tl_get_image_id(get_post_meta( get_the_ID(), 'testimonial_author_image', true ));
							//$image_thumb = wp_get_attachment_image_src($image_id, 'testi-normal');
							?>
                            <?php echo wp_get_attachment_image( $image_id, array('114','114'), "", array( "class" => "testi-normal caraousel-img" ) );?>
                            <?php endif;?>
                        </div>
                       <?php endif;?>
                      <div class="carosual-db-content">
                        <h5><?php echo get_post_meta( get_the_ID(), 'testimonial_testimonial', true );?></h5>
                        <p>
                        <cite>&mdash;
                        <?php if(isset($element) && in_array('show_name',$element)):?>
                        <span class="accent">
                        <?php echo get_post_meta( get_the_ID(), 'testimonial_author_name', true );?></span>,
                        <?php endif;?> 
                        <?php if(isset($element) && in_array('show_designation',$element)):?>	
                        <?php echo get_post_meta( get_the_ID(), 'testimonial_author_designation', true );?>
                        <?php endif;?>
                        </cite></p>
                        
                      </div>
                    </div>
                    <?php if($animation && $animation!='no'):?>
              		</div>
              		<?php endif;?>
                    <?php $i++;endwhile;?>
                    <?php wp_reset_query();?>
                  </div>
    
                  <!-- Controls -->
                 
                  <a class="left carousel-control" href="#carousel-<?php echo $key;?>" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-<?php echo $key;?>" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                 
                
                </div>	
    
            </div>
    <?php if($container=='yes'):?>  
        </div>
    </div>
    <?php endif;?>
</div>
</div>
<?php endif; wp_reset_query();?>	
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}