<?php
add_action( 'add_meta_boxes', 'tl_page_builder_add',1 );

function tl_page_builder_add(){
		if(get_option( 'tlpb_status') && get_option( 'tlpb_status')=='true'){
			add_meta_box( 'tl_page_builder', 'Themelines Page Builder', 'tl_page_builder_creation', 'page', 'normal', 'high' );
			add_meta_box( 'tl_page_builder_template', 'Save Template', 'tl_page_builder_template_creation', 'page', 'side', 'low' );
		}

}

function tl_page_builder_template_creation(){
?>
<p><strong>Load TL Page Builder Template</strong></p>
<label for="load_template" class="screen-reader-text">Load TL Page Builder Template</label>
<?php $tlpbtemplate = get_option('page_builder_template');
	  if($tlpbtemplate){
	  	$template = explode(',',$tlpbtemplate);
	  }
?>
<div class="tpl_sec">
<select name="template_name" class="form-control">
	<option value="">--Select Template--</option>
    <?php if($tlpbtemplate):?>
	<?php foreach($template as $tpl):?>
    <option value="<?php echo $tpl;?>"><?php echo $tpl;?></option>
    <?php endforeach;?>
    <?php endif;?>
</select>
</div>
<p>
<button id="load_template" class="button button-primary button-large" type="button">Load</button>
<button id="delete_template" class="button button-delete button-large" type="button">Delete</button>
</p>
<script>
jQuery('#load_template').click(function(){
	var loadval = jQuery('[name="template_name"]').val();
	var postid = jQuery('#post_ID').attr('value');
	jQuery.ajax({
		type: 'POST',
		url: '<?php echo admin_url('admin-ajax.php');?>',
		data: {
			action: 'load_tpl_html',
			load_template: loadval,
			id: postid
		},
		success: function(response){
			jQuery('.tl-pb-stage').html(response);
		},
		error: function(){
			jAlert('Error! Try again later.','Notice');
		}
	});
});
jQuery('#delete_template').click(function(){
		var template = jQuery('[name="template_name"]').val();
		if(template==''){
			jAlert('Please select a template to delete.','Notice');
		}else{
			jConfirm('Are you sure you want to delete this template?', 'Confirmation', function(r){
				if(r==true){
					jQuery.ajax({
						type: 'POST',
						url: '<?php echo admin_url('admin-ajax.php');?>',
						data: {
							action: 'delete_template',
							template: template,
						},
						success: function(response){
							if(response=='no'){
								jQuery('.tpl_msg').html('This template does not exists. Please select a valid template to delete.');
							}else{
								jQuery('.tpl_msg').html('');
								jQuery('.tpl_sec').html(response);
							}
						},
						error: function(){
							jAlert('Error! Try again later.','Notice');
						}
					});
				}
			});
		}
});
</script>
<p><strong>Save Template</strong></p>
<p>
<label for="save_template" class="screen-reader-text">Save Template</label>
<input type="text" id="save_template" name="save_template" class="form-control" placeholder="Save Template" onkeypress="return onlyAlphabets(event,this);">
</p>
<p>
<button id="template_save_btn" class="button button-primary button-large" type="button">Save</button>
</p>
<p class="tpl_msg" style="color:red;"></p>

<script>
jQuery('#save_template').val(jQuery('#editable-post-name').text());
jQuery('#template_save_btn').click(function(){
	var template = jQuery('#save_template').val();
	var tplval = jQuery('.tl-pb-stage').html();
	if(template==''){
		jAlert('Please enter a template name.','Notice');
	}else{
		jQuery.ajax({
            type: 'POST',
            url: '<?php echo admin_url('admin-ajax.php');?>',
            data: {
                action: 'template_save',
                template: template,
				template_val: tplval
            },
            success: function(response){
                if(response=='no'){
					jQuery('.tpl_msg').html('Template with the same name already exists. Please save with a different template name.');
				}else{
					jQuery('.tpl_msg').html('');
					jQuery('.tpl_sec').html(response);
				}
            },
            error: function(){
				jAlert('Error! Try again later.','Notice');
            }
        });
	}
});
</script>

<?php
}

function tlpb_admin_script(){
	global $typenow;
	if($typenow=='page'){
		wp_enqueue_style( 'wp-color-picker' );        
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script('jquery-tl-custom', get_template_directory_uri(). '/page-builder/js/custom.js');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script("jquery-ui-tooltip");
		wp_enqueue_script('editor');
		add_thickbox();
		wp_enqueue_style( 'builder-admin-css', get_template_directory_uri(). '/page-builder/css/admin.css', null);
		wp_enqueue_script('jquery-tl-pb', get_template_directory_uri(). '/page-builder/js/jquery.tl.pb.js');
		wp_enqueue_style( 'tlpb-font-awesome', get_template_directory_uri(). '/page-builder/font-awesome/css/font-awesome.min.css', null);
		wp_enqueue_script( 'jquery-ui-draggable');
		//wp_enqueue_script( 'tl-pb-uidraggable', get_template_directory_uri(). '/page-builder/js/jquery.ui.draggable.js', null, false);
		wp_enqueue_script( 'tl-pb-alert', get_template_directory_uri(). '/page-builder/js/jquery.alert.js', null, false);
		wp_enqueue_style( 'tl-pb-alert', get_template_directory_uri(). '/page-builder/css/jquery.alert.css', null, false);
		wp_enqueue_style( 'tl-pb-thickbox', get_template_directory_uri(). '/page-builder/css/thickbox.css', null, false);
	}
}
add_action( 'admin_print_styles', 'tlpb_admin_script' );
function tl_page_builder_creation(){
	wp_nonce_field( 'tl_pagebuilder_nonce', 'pagebuilder_nonce' );
?>
<a href="javascript:void(0);" id="tlpb-cancel-btn" class="button"><i class="fa fa-minus-circle"></i>  Use Default Editor</a>
<div id="page-builder" class="tl-page-builder">
	<div class="tl-pb-menu-panel">
		<ul class="tl-pb-menu-list">

			<li class="tl-pb-menu-item" id="tl-pb-menu-item-column">

				<div class="tl-pb-menu">
					<i class="fa fa-columns" aria-hidden="true"></i>
					<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Container</h4>

			</li>
            
            <li class="tl-pb-menu-item" id="tl-pb-menu-item-full-column">

				<div class="tl-pb-menu">
					<i class="fa fa-arrows-h" aria-hidden="true"></i>
					<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Full Width</h4>

			</li>

			<li class="tl-pb-menu-item" id="tl-pb-menu-item-banner">

				<div class="tl-pb-menu">
                <i class="fa fa-picture-o" aria-hidden="true"></i>

				<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Banner / Slider</h4>

			</li>

			<li class="tl-pb-menu-item" id="tl-pb-menu-item-gallery">

				<div class="tl-pb-menu">
				<i class="fa fa-camera" aria-hidden="true"></i>

				<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Gallery</h4>

			</li>

            <li class="tl-pb-menu-item" id="tl-pb-menu-item-post">

				<div class="tl-pb-menu">
				<i class="fa fa-tags" aria-hidden="true"></i>
				<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Post Types</h4>

			</li>
            
            <li class="tl-pb-menu-item" id="tl-pb-menu-item-developer">

				<div class="tl-pb-menu">
				<i class="fa fa-terminal" aria-hidden="true"></i>

				<span class="tl-pb-menu-plus fa fa-plus-circle"></span>

				</div>

				<h4>Developer Mode</h4>

			</li>

		</ul>

	</div>
    <?php global $post;
		  if(isset($_GET['post']) && $_GET['post']){
		  	$id=$_GET['post'];
		  }else{
			$id=0;
		  }

	?>

	<div class="tl-pb-stage">

		<?php echo (get_post_meta( $id, 'tl_pb_full_html', true ))?get_post_meta( $id, 'tl_pb_full_html', true ):'';?>

		

	</div>

</div>

<input type="hidden" name="tlpb_active" value="<?php echo (get_post_meta( $id, 'tlpb_active', true ))?get_post_meta( $id, 'tlpb_active', true ):'';?>" />

<!-------------------- PoopUP----------------------->

<div id="tl-pb-popup-common" style="display: none;">

	

</div>

<!-------------------- PoopUP----------------------->



<!-------------------- Content Area PopUp----------------------->

<?php add_thickbox(); ?>

<div class="modalDialog">
    <div>	<a href="javascript:void(0);" title="Close" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>

        	<?php $settings = array(
								'wpautop'=>true,
								'media_buttons'	=> true,
								'textarea_name' => 'tlcontent',
								'textarea_rows'	=> 10,
								'tinymce'       => array(
									'wp_autoresize_on' => false,
									'resize'           => false,
								),
								'editor_height' => 300,
							);

			?>

			<?php wp_editor( '', 'tlcontent', $settings);
				/*\_WP_Editors::enqueue_scripts();
				print_footer_scripts();
				\_WP_Editors::editor_js();*/
			?>
            <div class="row">

            <div class="tl-pb-pop-col">
    
                <div id="data-area"></div>
    
                <a href="javascript:void(0);" class="button button-primary button-large" id="btn_tl_pb_content_save">Save</a>
                <input type="hidden" name="section_detect" id="section_detect" value="" />
    
            </div>

   		</div>
    </div>
</div>
<div class="post_slider_div" style="display:none;">
<?php
$terms = get_terms( 'category',array('hide_empty' => false)  );
		if ( $terms ):
?>
		<label>Select Category</label>
<?php
		echo '<select name="slider" id="slider" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>"><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add post category to use post banner.';
		endif;
?>
</div>
<div class="single_slider_div" style="display:none;">
<?php
	$args = array();
	$args=array(
	  'post_type' => 'slider',
	  'post_status' => 'publish',
	  'posts_per_page' => -1,
	);
	$query = new WP_Query($args);
	if( $query->have_posts() ) {
	echo '
	<label>Select Slide as Banner</label>
	<select name="select_banner_slide" class="form-control">';
  	while ($query->have_posts()) : $query->the_post();
?>
	<option value="<?php echo get_the_ID();?>" rel="<?php the_title();?>"><?php the_title();?></option>
<?php
	endwhile; 
	echo '</select>';
	}else{
		echo 'Please add slides under slider post type to use this option.';
	}
	wp_reset_query();
?>
<script>
jQuery("[name='select_banner_slide']").change( function(){
	var val = jQuery(this).val();
	jQuery("[name='select_banner_slide'] option").each(function(){
		jQuery(this).removeAttr("selected");
	  if (jQuery(this).val() == val){
		jQuery(this).attr("selected","selected");
	  }

	});

});
</script>
</div>
<div class="slider_div" style="display:none;">
<?php
$terms = get_terms( 'slider_category',array('hide_empty' => false)  );
		if ( $terms ):
?>
		<label>Select Category</label>
<?php
		echo '<select name="slider" id="slider" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>"><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add slider category to use slider.';
		endif;
?>
</div>
<div class="gallery_category_div" style="display:none;">
<?php
$terms = get_terms( 'gallery_category',array('hide_empty' => false)  );
?>
		<div class="row">
        <div class="tl-pb-pop-col">
<?php
		if ( $terms ):
?>
		<label>Select Gallery Category</label>
<?php
		echo '<select name="gallery_category" id="gallery_category" class="form-control" multiple="multiple">';
		foreach($terms as $term):
?>
		<option value="<?php echo $term->term_id;?>" rel="<?php echo $term->name;?>"><?php echo $term->name;?></option>
<?php
		endforeach;
		echo '</select>';
		else:
		echo 'Please add gallery category to use gallery.';
		endif;
?>
		</div>
        </div>
</div>
<script>
function duplicate_post_value(){
	jQuery('.post-num-control').bind( "keyup mouseover mouseenter mouseleave change", function() {
		jQuery(this).closest('li').find('.post-num-control-length').val(jQuery(this).val());
		jQuery(this).attr('value',jQuery(this).closest('li').find('.post-num-control-length').val());
	});
	jQuery('.tlpb_check').click(function(){
		if(jQuery(this).prop('checked')){
			jQuery(this).attr('checked','checked');
		}else{
			jQuery(this).removeAttr('checked');
		}
	});
}
function post_type_action(){
	jQuery(".opt").change( function(){
		var val = jQuery(this).val();
		var post_type = jQuery(this).val();
		if(post_type=='page'){
			jQuery(this).closest('.tl-pb-post').find('.page-from').show();
			jQuery(this).closest('.tl-pb-post').find('.post-from').hide();
			jQuery(this).closest('.tl-pb-post').find('.only-post').hide();
			jQuery(this).closest('.tl-pb-post').find('.only-post-page').show();
			jQuery(this).closest('.tl-pb-post').find('.only-post-page-team').show();
			jQuery(this).closest('.tl-pb-post').find('.only-team').hide();
			jQuery(this).closest('.tl-pb-post').find('.only-testimonial').hide();
			jQuery(this).closest('.tl-pb-post').find('.only-team-testimonial').hide();
		}else{
			if(post_type=='post'){
				jQuery(this).closest('.tl-pb-post').find('.page-from').hide();
				jQuery(this).closest('.tl-pb-post').find('.post-from').show();
				jQuery(this).closest('.tl-pb-post').find('.only-post').show();
				jQuery(this).closest('.tl-pb-post').find('.only-post-page').show();
				jQuery(this).closest('.tl-pb-post').find('.only-post-page-team').show();
				jQuery(this).closest('.tl-pb-post').find('.only-team').hide();
				jQuery(this).closest('.tl-pb-post').find('.only-testimonial').hide();
				jQuery(this).closest('.tl-pb-post').find('.only-team-testimonial').hide();
			}else{
				if(post_type=='team'){
					jQuery(this).closest('.tl-pb-post').find('.page-from').hide();
					jQuery(this).closest('.tl-pb-post').find('.post-from').hide();
					jQuery(this).closest('.tl-pb-post').find('.only-post').hide();
					jQuery(this).closest('.tl-pb-post').find('.only-post-page').hide();
					jQuery(this).closest('.tl-pb-post').find('.only-post-page-team').show();
					jQuery(this).closest('.tl-pb-post').find('.only-team').show();
					jQuery(this).closest('.tl-pb-post').find('.only-testimonial').hide();
					jQuery(this).closest('.tl-pb-post').find('.only-team-testimonial').show();
				}else{
					if(post_type=='testimonial'){
						jQuery(this).closest('.tl-pb-post').find('.page-from').hide();
						jQuery(this).closest('.tl-pb-post').find('.post-from').hide();
						jQuery(this).closest('.tl-pb-post').find('.only-post').hide();
						jQuery(this).closest('.tl-pb-post').find('.only-post-page').hide();
						jQuery(this).closest('.tl-pb-post').find('.only-post-page-team').hide();
						jQuery(this).closest('.tl-pb-post').find('.only-team').hide();
						jQuery(this).closest('.tl-pb-post').find('.only-testimonial').show();
						jQuery(this).closest('.tl-pb-post').find('.only-team-testimonial').show();
					}
				}
			}
		}
		jQuery(this).find('option').each(function(){
			jQuery(this).removeAttr("selected");
		  if (jQuery(this).val() == val && jQuery(this).closest('select').hasClass('opt')){
			jQuery(this).attr("selected","selected");
	
		  }
		});
		duplicate_post_value();
	});
}
jQuery(window).load(function(){
	duplicate_post_value();
	post_type_action();
});
</script>
<div class="post_div" style="display:none;">
<?php $key='123tlpbkey123';?>
<div class="tl-pb-post">

<div class="row">

	<div class="tl-post-select-part">

		<ul>
            <li>
				<label>Post Types:</label>
				<select name="tlpb[<?php echo $key;?>][post_type]" class="post-text opt" id="post-type">
					<option value="post">Post</option>
                    <option value="page">Page</option>
                    <option value="team">Team</option>
                    <option value="testimonial">Testimonials</option>
				</select>
			</li>
            
            <li>
				<label>Order:</label>
				<select name="tlpb[<?php echo $key;?>][post_order]" id="post-order" class="post-text opt">
					<option value="date-desc">Date: Newest First</option>
                    <option value="date-asc">Date: Oldest First</option>
                    <option value="title-asc">Title: A - Z</option>
                    <option value="title-desc">Title: Z - A</option>
                    <option value="comment_count-desc">Most Comments</option>
                    <option value="menu_order-asc">Page Order</option>
                    <option value="rand-asc">Random</option>
				</select>
			</li>
            
            <li class="post-from">
				<label>From:</label>
				<select name="tlpb[<?php echo $key;?>][post_from]" class="post-text opt" id="post-from">
                	<option value="all">All Posts</option>
                    <?php $category = get_terms( 'category',array('hide_empty' => false)  );?>
                    <?php if(isset($category) && $category):?>
					<?php foreach($category as $cat):?>
					<option value="<?php echo $cat->slug;?>"><?php echo $cat->name;?></option>
					<?php endforeach;?>
                    <?php endif;?>
				</select>
			</li>
            
            <li class="page-from" style="display:none;">
            	<label>From:</label>
                <select name="tlpb[<?php echo $key;?>][page_from]" class="post-text opt" id="page-from">
					<?php $args = array(
                        'sort_order' => 'asc',
                        'sort_column' => 'post_title',
                        'hierarchical' => 1,
                        'exclude' => '',
                        'include' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'authors' => '',
                        'child_of' => 0,
                        'parent' => -1,
                        'exclude_tree' => '',
                        'number' => '',
                        'offset' => 0,
                        'post_type' => 'page',
                        'post_status' => 'publish'
                    ); 
                    $pages = get_pages($args);
                    ?>
                    <option value="all">All Pages</option>
                    <optgroup label="--Child Of--">
						<?php if(isset($pages) && $pages):?>
                       
                        <?php foreach($pages as $page):?>
                        <option value="<?php echo $page->ID;?>"><?php echo $page->post_title;?></option>
                        <?php endforeach;?>
                        <?php endif;?>
                    </optgroup>
                </select>
            </li>
            
			<li>
				<label>Number of Items ( -1 For all Items ):</label>
                <input type="number" name="tlpb[<?php echo $key;?>][post_number]" value="-1" id="post-number" class="post-num-control post-text"/>
				<input type="hidden" name="tlpb[<?php echo $key;?>][post_number]" value="-1" class="post-num-control-length" id="post-number-length" />
			</li>
            
            <li>
				<label>Item Offset:</label>
                <input type="number" name="tlpb[<?php echo $key;?>][post_offset]" value="0" id="post-offset" class="post-text post-num-control"/>
				<input type="hidden" name="tlpb[<?php echo $key;?>][post_offset]" value="0" class="post-num-control-length" id="post-offset-length" />
			</li>

			<li>
				<label>Columns:</label>
				<select name="tlpb[<?php echo $key;?>][post_column]" class="post-text opt" id="post-column">
					<?php for($i=2;$i<=4;$i++):?>
					<option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php endfor;?>
				</select>
			</li>
            
            <li class="only-post-page">
				<label>Featured Image:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_feature]" value="on" checked />Show Featured Image
			</li>
            
            <li class="only-post-page">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][post_style]" class="post-text opt" id="post-style">
					<option value="column">Column View</option>
                    <option value="horizontal">Horizontal View</option>
                    <option value="masonry">Masonry</option>
				</select>
			</li>
            
            <li class="only-team" style="display:none;">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][team_style]" class="post-text opt" id="team-style">
					<option value="column">Column View</option>
                    <option value="slder">Slider</option>
				</select>
			</li>
            
            <li class="only-testimonial" style="display:none;">
				<label>Style:</label>
				<select name="tlpb[<?php echo $key;?>][testimonial_style]" class="post-text opt" id="testimonial-style">
					<option value="chat">Chat View</option>
                    <option value="normal">Normal Slider</option>
				</select>
			</li>
            
            <li>
				<label>Elements:</label>
				<p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_title" />Show Title</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_date" />Show Date</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_author" />Show Author</p>
                <p class="only-post"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_category" />Show Categories</p>
                <p class="only-post"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_tag" />Show Tags</p>
                <p class="only-post-page"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_comment_count" />Show Comment Count</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_name" />Show Name</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_designation" />Show Designation</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_image" />Show Image</p>
                <p class="only-team-testimonial" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_contact_details" />Show Contact Details</p>
                <p class="only-team" style="display:none;"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_social_icons" />Show Social Icons</p>
                <p class="only-post-page-team"><input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_element][]" value="show_details_page_link" />Enable Link To Details Page</p>
			</li>
            
            <li class="only-post-page">
				<label>Excerpt:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_excerpt]" value="on" checked/>Show Excerpt (Applicable for Posts and also Pages without Themelines Page Builder)
			</li>
            
            <li class="only-post-page">
				<label>Excerpt Length (Characters):</label>
				<input type="number" name="tlpb[<?php echo $key;?>][post_length]" value="50" class="post-num-control" id="post-character"/>
                <input type="hidden" name="tlpb[<?php echo $key;?>][post_length]" value="50" class="post-num-control-length" id="post-character-length" />
			</li>
            
            <li class="only-post-page">
				<label>Pagination:</label>
				<input class="tlpb_check" type="checkbox" name="tlpb[<?php echo $key;?>][post_pagination]" value="on"/>Pagination
			</li>
       	<?php
				$animatehtml = '<option value="no">No Animation</option>
		<optgroup label="Attention Seekers">
		  <option value="bounce">bounce</option>
          <option value="flash">flash</option>
          <option value="pulse">pulse</option>
          <option value="rubberBand">rubberBand</option>
          <option value="shake">shake</option>
          <option value="swing">swing</option>
          <option value="tada">tada</option>
          <option value="wobble">wobble</option>
          <option value="jello">jello</option>
        </optgroup>
        <optgroup label="Bouncing Entrances">
          <option value="bounceIn">bounceIn</option>
          <option value="bounceInDown">bounceInDown</option>
          <option value="bounceInLeft">bounceInLeft</option>
          <option value="bounceInRight">bounceInRight</option>
          <option value="bounceInUp">bounceInUp</option>
        </optgroup>
        <optgroup label="Bouncing Exits">
          <option value="bounceOut">bounceOut</option>
          <option value="bounceOutDown">bounceOutDown</option>
          <option value="bounceOutLeft">bounceOutLeft</option>
          <option value="bounceOutRight">bounceOutRight</option>
          <option value="bounceOutUp">bounceOutUp</option>
        </optgroup>
        <optgroup label="Fading Entrances">
          <option value="fadeIn">fadeIn</option>
          <option value="fadeInDown">fadeInDown</option>
          <option value="fadeInDownBig">fadeInDownBig</option>
          <option value="fadeInLeft">fadeInLeft</option>
          <option value="fadeInLeftBig">fadeInLeftBig</option>
          <option value="fadeInRight">fadeInRight</option>
          <option value="fadeInRightBig">fadeInRightBig</option>
          <option value="fadeInUp">fadeInUp</option>
          <option value="fadeInUpBig">fadeInUpBig</option>
        </optgroup>
        <optgroup label="Fading Exits">
          <option value="fadeOut">fadeOut</option>
          <option value="fadeOutDown">fadeOutDown</option>
          <option value="fadeOutDownBig">fadeOutDownBig</option>
          <option value="fadeOutLeft">fadeOutLeft</option>
          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
          <option value="fadeOutRight">fadeOutRight</option>
          <option value="fadeOutRightBig">fadeOutRightBig</option>
          <option value="fadeOutUp">fadeOutUp</option>
          <option value="fadeOutUpBig">fadeOutUpBig</option>
        </optgroup>
		<optgroup label="Flippers">
          <option value="flip">flip</option>
          <option value="flipInX">flipInX</option>
          <option value="flipInY">flipInY</option>
          <option value="flipOutX">flipOutX</option>
          <option value="flipOutY">flipOutY</option>
        </optgroup>
        <optgroup label="Lightspeed">
          <option value="lightSpeedIn">lightSpeedIn</option>
          <option value="lightSpeedOut">lightSpeedOut</option>
        </optgroup>
        <optgroup label="Rotating Entrances">
          <option value="rotateIn">rotateIn</option>
          <option value="rotateInDownLeft">rotateInDownLeft</option>
          <option value="rotateInDownRight">rotateInDownRight</option>
          <option value="rotateInUpLeft">rotateInUpLeft</option>
          <option value="rotateInUpRight">rotateInUpRight</option>
        </optgroup>
        <optgroup label="Rotating Exits">
          <option value="rotateOut">rotateOut</option>
          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
          <option value="rotateOutDownRight">rotateOutDownRight</option>
          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
          <option value="rotateOutUpRight">rotateOutUpRight</option>
        </optgroup>

        <optgroup label="Sliding Entrances">
          <option value="slideInUp">slideInUp</option>
          <option value="slideInDown">slideInDown</option>
          <option value="slideInLeft">slideInLeft</option>
          <option value="slideInRight">slideInRight</option>
        </optgroup>
        <optgroup label="Sliding Exits">
          <option value="slideOutUp">slideOutUp</option>
          <option value="slideOutDown">slideOutDown</option>
          <option value="slideOutLeft">slideOutLeft</option>
          <option value="slideOutRight">slideOutRight</option>
        </optgroup>
        <optgroup label="Zoom Entrances">
          <option value="zoomIn">zoomIn</option>
          <option value="zoomInDown">zoomInDown</option>
          <option value="zoomInLeft">zoomInLeft</option>
          <option value="zoomInRight">zoomInRight</option>
          <option value="zoomInUp">zoomInUp</option>
        </optgroup>
        <optgroup label="Zoom Exits">
          <option value="zoomOut">zoomOut</option>
          <option value="zoomOutDown">zoomOutDown</option>
          <option value="zoomOutLeft">zoomOutLeft</option>
          <option value="zoomOutRight">zoomOutRight</option>
          <option value="zoomOutUp">zoomOutUp</option>
        </optgroup>
        <optgroup label="Specials">
          <option value="hinge">hinge</option>
          <option value="rollIn">rollIn</option>
          <option value="rollOut">rollOut</option>
        </optgroup>';
		$delay_html = '';
		for($i=200;$i<=2000;$i=$i+200){
			$delay_html .= '<option value="'.$i.'ms">'.$i.'ms</option>';
		}
		$duration_html = '';
		for($i=200;$i<=2000;$i=$i+200){
			$duration_html .= '<option value="'.$i.'ms">'.$i.'ms</option>';
		}
			?>
			<li>
				<label>Items Animation:</label>
				<select name="tlpb[<?php echo $key;?>][animation]" class="post-text opt" id="animation">
					<?php echo $animatehtml;?>
				</select>
            </li>
            <li>
				<label>Items Animation Delay:</label>
				<select name="tlpb[<?php echo $key;?>][animation_delay]" class="post-text opt" id="animation_delay">
					<?php echo $delay_html;?>
				</select>
            </li>
            <li>
				<label>Items Animation Duration:</label>
				<select name="tlpb[<?php echo $key;?>][animation_duration]" class="post-text opt" id="animation_duration">
					<?php echo $duration_html;?>
				</select>
            </li>

		</ul>

	</div>  

</div>

</div>
</div>

<div class="modalDialog-developer">
    <div>	<a href="javascript:void(0);" title="Close" class="close-developer"><i class="fa fa-times" aria-hidden="true"></i></a>

        	<div class="developer_div" style="display:none;">
			<?php
                $settings = array(
					'wpautop'=> true,
					'media_buttons'	=> false,
					'textarea_name' => 'tlpb[123tlpbkey123][content]',
					'textarea_rows'	=> 10,
					'tinymce'       => array(
						'wp_autoresize_on' => false,
						'resize'           => false,
					),
					'editor_height' => 300,
					'tinymce'=> false,
				);
                wp_editor('', '123tlpbkey123-developer-content', $settings);
            
                /*\_WP_Editors::enqueue_scripts();
                print_footer_scripts();
               \_WP_Editors::editor_js()*/
            ?>
            <script>
                    jQuery('#<?php echo $key;?>-developer-content').bind( "keyup mouseover mouseenter mouseleave change", function() {
                        jQuery(this).html(jQuery(this).val());
                    });
                </script>
            </div>
            <div class="row"><div class="tl-pb-pop-col pull-right"><div class="developer-button-area"></div></div></div>
    </div>
</div>
<div class="widget" style="display:none;">
<?php
	$sidebars = $GLOBALS['wp_registered_sidebars'];
	if(isset($sidebars) && $sidebars){
?>
		<select name="select_sidebar" class="form-control">
<?php
		foreach($sidebars as $sidebar):
?>
        	<option value="<?php echo ucwords( $sidebar['id'] );?>"><?php echo ucwords( $sidebar['name'] );?></option>      
<?php
		endforeach;
?>
		</select>
<?php
	}else{
		echo '<div class="no-widget">No Custom Widget Found. Go to <a href="themes.php" target="_blank">Appearance</a> > <a href="widgets.php" target="_blank">Widgets</a> to create one.</div>';
	}
?>
</div>
<div class="con-form-area" style="display:none;">
<?php
	$args = array ( 'post_type' => 'wpcf7_contact_form','orderby' => 'post_title', 'order' => 'ASC');
	query_posts( $args );
	if ( have_posts() ):
	?>
	<select name="contact_form" id="contact_form" class="form-control">
	<?php
		while ( have_posts() ) :the_post(); 
	?>
	<option value='[contact-form-7 id="<?php echo get_the_ID();?>" title="<?php the_title();?>"]'><?php the_title();?></option>
	<?php
	endwhile;
	?>
	</select>
	<?php
	else:
	echo 'Please add atleast one contact form. Add <a href="admin.php?page=wpcf7" target="_blank">Contact Form 7</a> now.';
	endif;
?>
</div>


<textarea name="full_admin_html" id="full_admin_html" style="display:none;"/></textarea>



<?php

}

add_action( 'admin_footer-post-new.php', 'wpse_80215_script' );

add_action( 'admin_footer-post.php', 'wpse_80215_script' );



function wpse_80215_script()

{

    if ( 'page' !== $GLOBALS['post_type'] )

        return;



    ?>

<script>
jQuery(window).load(function(){
	//jQuery('.post_slider_div').load(ajaxurl,{action:'select_post_slider'},function(m){});
	//jQuery('.single_slider_div').load(ajaxurl,{action:'select_single_slider'},function(m){});
	//jQuery('.slider_div').load(ajaxurl,{action:'select_slider'},function(m){});
	//jQuery('.gallery_category_div').load(ajaxurl,{action:'select_gallery_category'},function(m){});
	//jQuery('.post_div').load(ajaxurl,{action:'create_pb_post_html',key:'123tlpbkey123'},function(m){});
});
jQuery(document).ready(function($){
	
	/*$.ajax({
            type: 'GET',
            url: ajaxurl,
            data: {
                action: 'create_content_area',
				key:'123tlpbkey123'
            },
            success: function(response){
                $('.developer_div').html(response);
            }
        });*/

	if(window.location.href=='#openModal' || location.hash=='#close'){
		window.location.href = "";
	}

	$('#publish').one('click', function( event ) {

        event.preventDefault();

		//$('#full_admin_html').val($('.tl-pb-stage').html());

		//$('#publish').trigger( "click" );

        var postID=$('#post_ID').attr('value'), dataj = TlPb.CreateFJSON(), full_html = $('.tl-pb-stage').html();
		$('#full_admin_html').val($('.tl-pb-stage').html());
		$('#publish').trigger( "click" );
		/*
        $.ajax({

                url:ajaxurl,

                method: "POST",

                data: {action: 'update_tl_page_builder_pg',postID:postID,full_html:full_html},

                success: function(data) {

					$('.tl-pb-stage').html(data);

					$('#full_admin_html').val($('.tl-pb-stage').html());

					$('#publish').trigger( "click" );

                    //$('#post').submit();

                }

        });*/

    });

});

(function($) {
	$(window).load(function(){
		$('.tl-pb-section .tl-post-select-part').each(function(index, element) {
			var $this = $(this);
			var secid = $this.closest('.tl-pb-section').attr('id');
			var post_from = $this.closest('.tl-pb-section').find('.post-from').val();
			var page_from = $this.closest('.tl-pb-section').find('.page-from').val();
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'post_from_change',
					key: secid,
					post_from: post_from
				},
				success: function(response){
					//alert($this.find('.post_from').html());
					$this.find('.post-from').html(response);
					duplicate_post_value();
					post_type_action();
				}
			});
			
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'page_from_change',
					key: secid,
					page_from: page_from
				},
				success: function(response){
					$this.find('.page-from').html(response);
					duplicate_post_value();
					post_type_action();
				}
			});
			
			var animation = $this.closest('.tl-pb-section').find('#animation').val();
			if(typeof animation == 'undefined'){
				var animatehtml = '<option value="no">No Animation</option>'
				+'<optgroup label="Attention Seekers">'
				  +'<option value="bounce">bounce</option>'
				  +'<option value="flash">flash</option>'
				  +'<option value="pulse">pulse</option>'
				  +'<option value="rubberBand">rubberBand</option>'
				  +'<option value="shake">shake</option>'
				  +'<option value="swing">swing</option>'
				  +'<option value="tada">tada</option>'
				  +'<option value="wobble">wobble</option>'
				  +'<option value="jello">jello</option>'
				+'</optgroup>'
		
				+'<optgroup label="Bouncing Entrances">'
				  +'<option value="bounceIn">bounceIn</option>'
				  +'<option value="bounceInDown">bounceInDown</option>'
				  +'<option value="bounceInLeft">bounceInLeft</option>'
				  +'<option value="bounceInRight">bounceInRight</option>'
				  +'<option value="bounceInUp">bounceInUp</option>'
				+'</optgroup>'
		
				+'<optgroup label="Bouncing Exits">'
				  +'<option value="bounceOut">bounceOut</option>'
				  +'<option value="bounceOutDown">bounceOutDown</option>'
				  +'<option value="bounceOutLeft">bounceOutLeft</option>'
				  +'<option value="bounceOutRight">bounceOutRight</option>'
				  +'<option value="bounceOutUp">bounceOutUp</option>'
				+'</optgroup>'
		
				+'<optgroup label="Fading Entrances">'
				  +'<option value="fadeIn">fadeIn</option>'
				  +'<option value="fadeInDown">fadeInDown</option>'
				  +'<option value="fadeInDownBig">fadeInDownBig</option>'
				  +'<option value="fadeInLeft">fadeInLeft</option>'
				  +'<option value="fadeInLeftBig">fadeInLeftBig</option>'
				  +'<option value="fadeInRight">fadeInRight</option>'
				  +'<option value="fadeInRightBig">fadeInRightBig</option>'
				  +'<option value="fadeInUp">fadeInUp</option>'
				  +'<option value="fadeInUpBig">fadeInUpBig</option>'
				+'</optgroup>'
		
				+'<optgroup label="Fading Exits">'
				  +'<option value="fadeOut">fadeOut</option>'
				  +'<option value="fadeOutDown">fadeOutDown</option>'
				  +'<option value="fadeOutDownBig">fadeOutDownBig</option>'
				  +'<option value="fadeOutLeft">fadeOutLeft</option>'
				  +'<option value="fadeOutLeftBig">fadeOutLeftBig</option>'
				  +'<option value="fadeOutRight">fadeOutRight</option>'
				  +'<option value="fadeOutRightBig">fadeOutRightBig</option>'
				  +'<option value="fadeOutUp">fadeOutUp</option>'
				  +'<option value="fadeOutUpBig">fadeOutUpBig</option>'
				+'</optgroup>'
		
				+'<optgroup label="Flippers">'
				  +'<option value="flip">flip</option>'
				  +'<option value="flipInX">flipInX</option>'
				  +'<option value="flipInY">flipInY</option>'
				  +'<option value="flipOutX">flipOutX</option>'
				  +'<option value="flipOutY">flipOutY</option>'
				+'</optgroup>'
		
				+'<optgroup label="Lightspeed">'
				  +'<option value="lightSpeedIn">lightSpeedIn</option>'
				  +'<option value="lightSpeedOut">lightSpeedOut</option>'
				+'</optgroup>'
		
				+'<optgroup label="Rotating Entrances">'
				  +'<option value="rotateIn">rotateIn</option>'
				  +'<option value="rotateInDownLeft">rotateInDownLeft</option>'
				  +'<option value="rotateInDownRight">rotateInDownRight</option>'
				  +'<option value="rotateInUpLeft">rotateInUpLeft</option>'
				  +'<option value="rotateInUpRight">rotateInUpRight</option>'
				+'</optgroup>'
		
				+'<optgroup label="Rotating Exits">'
				  +'<option value="rotateOut">rotateOut</option>'
				  +'<option value="rotateOutDownLeft">rotateOutDownLeft</option>'
				  +'<option value="rotateOutDownRight">rotateOutDownRight</option>'
				  +'<option value="rotateOutUpLeft">rotateOutUpLeft</option>'
				  +'<option value="rotateOutUpRight">rotateOutUpRight</option>'
				+'</optgroup>'
		
				+'<optgroup label="Sliding Entrances">'
				  +'<option value="slideInUp">slideInUp</option>'
				  +'<option value="slideInDown">slideInDown</option>'
				  +'<option value="slideInLeft">slideInLeft</option>'
				  +'<option value="slideInRight">slideInRight</option>'
				+'</optgroup>'
				
				+'<optgroup label="Sliding Exits">'
				  +'<option value="slideOutUp">slideOutUp</option>'
				  +'<option value="slideOutDown">slideOutDown</option>'
				  +'<option value="slideOutLeft">slideOutLeft</option>'
				  +'<option value="slideOutRight">slideOutRight</option>'
				+'</optgroup>'
				
				+'<optgroup label="Zoom Entrances">'
				  +'<option value="zoomIn">zoomIn</option>'
				  +'<option value="zoomInDown">zoomInDown</option>'
				  +'<option value="zoomInLeft">zoomInLeft</option>'
				  +'<option value="zoomInRight">zoomInRight</option>'
				  +'<option value="zoomInUp">zoomInUp</option>'
				+'</optgroup>'
				
				+'<optgroup label="Zoom Exits">'
				  +'<option value="zoomOut">zoomOut</option>'
				  +'<option value="zoomOutDown">zoomOutDown</option>'
				  +'<option value="zoomOutLeft">zoomOutLeft</option>'
				  +'<option value="zoomOutRight">zoomOutRight</option>'
				  +'<option value="zoomOutUp">zoomOutUp</option>'
				+'</optgroup>'
		
				+'<optgroup label="Specials">'
				  +'<option value="hinge">hinge</option>'
				  +'<option value="rollIn">rollIn</option>'
				  +'<option value="rollOut">rollOut</option>'
				+'</optgroup>';
				var delay_html = '';
				for(var i=200;i<=2000;i=i+200){
					delay_html += '<option value="'+i+'ms">'+i+'ms</option>';
				}
				var duration_html = '';
				for(var i=200;i<=2000;i=i+200){
					duration_html += '<option value="'+i+'ms">'+i+'ms</option>';
				}
				$this.closest('.tl-pb-section').find('ul').append('<li><label>Items Animation:</label><select name="tlpb['+secid+'][animation]" class="post-text opt" id="animation">'+animatehtml+'</select></li><li><label>Items Animation Delay:</label><select name="tlpb['+secid+'][animation_delay]" class="post-text opt" id="animation_delay">'+delay_html+'</select></li><li><label>Items Animation Duration:</label><select name="tlpb['+secid+'][animation_duration]" class="post-text opt" id="animation_duration">'+duration_html+'</select></li>');
			}
		});
		$('.widget').load(ajaxurl,{action:'fetch_widget'});
		$('.con-form-area').load(ajaxurl,{action:'contact_form_list'},function(m){});
	});
}(jQuery));

</script>

    <?php

}



add_action( 'save_post', 'tl_pb_save', 10,2 );

function tl_pb_save( $post_id ){
	if(isset($_POST['tlpb_active']) && $_POST['tlpb_active']){
		update_post_meta($post_id, 'tlpb_active', $_POST['tlpb_active']);
	}
	if(isset($_POST['tlpb_active']) && $_POST['tlpb_active']=='yes'):
	if(isset($_POST['full_admin_html']) && $_POST['full_admin_html']){
		update_post_meta($post_id, 'tl_pb_full_html', $_POST['full_admin_html']);
	}
	/*if(isset($_POST['page_template']) && $_POST['page_template']=='default'){
		update_post_meta($post_id, '_wp_page_template', 'page-builder/template-builder.php');
	}*/
	if(isset($_POST['tlpb']) && $_POST['tlpb']){
		//update_post_meta($post_id, 'tlpb_struct', $_POST['tlpb']);
		$grid = $_POST['tlpb'];
		/******* SEO Starts *******/
		$content = false;
		$image = false;
		foreach($grid['key'] as $key){
			
			if($grid[$key]['type']=='column' || $grid[$key]['type']=='full_column'){
				$i=0;
            	foreach($grid[$key]['sec_grid'] as $grid_struct){
					if(!$content){
						if($grid[$key]['content'][$i]){
							$content = $grid[$key]['content'][$i];
							update_post_meta($post_id, 'tlpb_seo_content', $content);
						}
					}
					if(!$image){
						if($grid[$key]['image'][$i]){
							$image = $grid[$key]['image'][$i];
							update_post_meta($post_id, 'tlpb_seo_image', $image);
						}
					}
					if($content && $image){
						break;
					}
					$i++;
				}
			}
			if($content && $image){
				break;
			}
		}
		/******* SEO Ends *******/
		$shortcode = '';
		remove_action( 'save_post',  'tl_pb_save', 10, 2 );
		foreach($grid['key'] as $key){
			$class = isset($grid[$key]['class']) && $grid[$key]['class']?$grid[$key]['class']:'';
			$id = isset($grid[$key]['id']) && $grid[$key]['id']?$grid[$key]['id']:'';
			$title = isset($grid[$key]['title']) && $grid[$key]['title']?$grid[$key]['title']:'';
			$title_position = isset($grid[$key]['title_position']) && $grid[$key]['title_position']?$grid[$key]['title_position']:'';
			$subtitle = isset($grid[$key]['subtitle']) && $grid[$key]['subtitle']?$grid[$key]['subtitle']:'';
			$subtitle_position = isset($grid[$key]['subtitle_position']) && $grid[$key]['subtitle_position']?$grid[$key]['subtitle_position']:'';
			$title_color = isset($grid[$key]['title_color']) && $grid[$key]['title_color']?$grid[$key]['title_color']:'';
			$subtitle_color = isset($grid[$key]['subtitle_color']) && $grid[$key]['subtitle_color']?$grid[$key]['subtitle_color']:'';
			$content_color = isset($grid[$key]['content_color']) && $grid[$key]['content_color']?$grid[$key]['content_color']:'';
			$padding = isset($grid[$key]['padding']) && $grid[$key]['padding']?$grid[$key]['padding']:'';
			$background_color = isset($grid[$key]['background_color']) && $grid[$key]['background_color']?$grid[$key]['background_color']:'';
			$background_image = isset($grid[$key]['background_image']) && $grid[$key]['background_image']?$grid[$key]['background_image']:'';
			$parallax = isset($grid[$key]['parallax']) && $grid[$key]['parallax']?$grid[$key]['parallax']:'';
			
			$margin_top = isset($grid[$key]['margin_top']) && $grid[$key]['margin_top']?$grid[$key]['margin_top']:'';
			if($margin_top=='custom'){
				$m = isset($grid[$key]['custom_margin_top']) && $grid[$key]['custom_margin_top']?$grid[$key]['custom_margin_top']:'0';
				$margin_top = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['margin_top']) && $grid[$key]['margin_top']?$grid[$key]['margin_top']:'0';
				$margin_top = 'normal-'.$m;
			}
			$margin_bottom = isset($grid[$key]['margin_bottom']) && $grid[$key]['margin_bottom']?$grid[$key]['margin_bottom']:'';
			if($margin_bottom=='custom'){
				$m = isset($grid[$key]['custom_margin_bottom']) && $grid[$key]['custom_margin_bottom']?$grid[$key]['custom_margin_bottom']:'0';
				$margin_bottom = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['margin_bottom']) && $grid[$key]['margin_bottom']?$grid[$key]['margin_bottom']:'0';
				$margin_bottom = 'normal-'.$m;
			}
			$margin_left = isset($grid[$key]['margin_left']) && $grid[$key]['margin_left']?$grid[$key]['margin_left']:'0';
			if($margin_left=='custom'){
				$m = isset($grid[$key]['custom_margin_left']) && $grid[$key]['custom_margin_left']?$grid[$key]['custom_margin_left']:'0';
				$margin_left = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['margin_left']) && $grid[$key]['margin_left']?$grid[$key]['margin_left']:'0';
				$margin_left = 'normal-'.$m;
			}
			$margin_right = $grid[$key]['margin_right'];
			if($margin_right=='custom'){
				$m = isset($grid[$key]['custom_margin_right']) && $grid[$key]['custom_margin_right']?$grid[$key]['custom_margin_right']:'0';
				$margin_right = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['margin_right']) && $grid[$key]['margin_right']?$grid[$key]['margin_right']:'0';
				$margin_right = 'normal-'.$m;
			}
			
			$padding_top = $grid[$key]['padding_top'];
			if($padding_top=='custom'){
				$m = isset($grid[$key]['custom_padding_top']) && $grid[$key]['custom_padding_top']?$grid[$key]['custom_padding_top']:'0';
				$padding_top = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['padding_top']) && $grid[$key]['padding_top']?$grid[$key]['padding_top']:'0';
				$padding_top = 'normal-'.$grid[$key]['padding_top'];
			}
			$padding_bottom = $grid[$key]['padding_bottom'];
			if($padding_bottom=='custom'){
				$m = isset($grid[$key]['custom_padding_bottom']) && $grid[$key]['custom_padding_bottom']?$grid[$key]['custom_padding_bottom']:'0';
				$padding_bottom = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['padding_bottom']) && $grid[$key]['padding_bottom']?$grid[$key]['padding_bottom']:'0';
				$padding_bottom = 'normal-'.$m;
			}
			$padding_left = $grid[$key]['padding_left'];
			if($padding_left=='custom'){
				$m = isset($grid[$key]['custom_padding_left']) && $grid[$key]['custom_padding_left']?$grid[$key]['custom_padding_left']:'0';
				$padding_left = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['padding_left']) && $grid[$key]['padding_left']?$grid[$key]['padding_left']:'0';
				$padding_left = 'normal-'.$m;
			}
			$padding_right = $grid[$key]['padding_right'];
			if($padding_right=='custom'){
				$m = isset($grid[$key]['custom_padding_right']) && $grid[$key]['custom_padding_right']?$grid[$key]['custom_padding_right']:'0';
				$padding_right = 'custom-'.$m;
			}else{
				$m = isset($grid[$key]['padding_right']) && $grid[$key]['padding_right']?$grid[$key]['padding_right']:'0';
				$padding_right = 'normal-'.$grid[$key]['padding_right'];
			}
			
			$title_tag = isset($grid[$key]['title_tag']) && $grid[$key]['title_tag']?$grid[$key]['title_tag']:'h2';
			$subtitle_tag = isset($grid[$key]['subtitle_tag']) && $grid[$key]['subtitle_tag']?$grid[$key]['subtitle_tag']:'h3';
			
			$title_color_class = isset($grid[$key]['title_color_class']) && $grid[$key]['title_color_class']?$grid[$key]['title_color_class']:'default';
			$title_animation = isset($grid[$key]['title_animation']) && $grid[$key]['title_animation']?$grid[$key]['title_animation']:'no';
			$title_animation_delay = isset($grid[$key]['title_animation_delay']) && $grid[$key]['title_animation_delay']?$grid[$key]['title_animation_delay']:'200ms';
			$title_animation_duration = isset($grid[$key]['title_animation_duration']) && $grid[$key]['title_animation_duration']?$grid[$key]['title_animation_duration']:'200ms';
			$subtitle_color_class = isset($grid[$key]['subtitle_color_class']) && $grid[$key]['subtitle_color_class']?$grid[$key]['subtitle_color_class']:'default';
			$subtitle_animation = isset($grid[$key]['subtitle_animation']) && $grid[$key]['subtitle_animation']?$grid[$key]['subtitle_animation']:'no';
			$subtitle_animation_delay = isset($grid[$key]['subtitle_animation_delay']) && $grid[$key]['subtitle_animation_delay']?$grid[$key]['subtitle_animation_delay']:'200ms';
			$subtitle_animation_duration = isset($grid[$key]['subtitle_animation_duration']) && $grid[$key]['subtitle_animation_duration']?$grid[$key]['subtitle_animation_duration']:'200ms';
			$content_color_class = isset($grid[$key]['content_color_class']) && $grid[$key]['content_color_class']?$grid[$key]['content_color_class']:'default';
			$content_animation = isset($grid[$key]['content_animation']) && $grid[$key]['content_animation']?$grid[$key]['content_animation']:'no';
			$content_animation_delay = isset($grid[$key]['content_animation_delay']) && $grid[$key]['content_animation_delay']?$grid[$key]['content_animation_delay']:'200ms';
			$content_animation_duration = isset($grid[$key]['content_animation_duration']) && $grid[$key]['content_animation_duration']?$grid[$key]['content_animation_duration']:'200ms';
			$background_color_class = isset($grid[$key]['background_color_class']) && $grid[$key]['background_color_class']?$grid[$key]['background_color_class']:'default';
			
			$type = isset($grid[$key]['type']) && $grid[$key]['type']?$grid[$key]['type']:'';
			
			if($grid[$key]['type']=='column' || $grid[$key]['type']=='banner' || $grid[$key]['type']=='slider' || $grid[$key]['type']=='other' || $grid[$key]['type']=='gallery' || $grid[$key]['type']=='post_type'){
				$shortcode .= '[tlpb_section class="'.$class.'" id="'.$id.'" title="'.$title.'" title_position="'.$title_position.'" subtitle="'.$subtitle.'" subtitle_position="'.$subtitle_position.'" title_color="'.$title_color.'" subtitle_color="'.$subtitle_color.'" content_color="'.$content_color.'" padding="'.$padding.'" background_color="'.$background_color.'" background_image="'.$background_image.'" parallax="'.$parallax.'" type="'.$type.'" margin_left="'.$margin_left.'" margin_right="'.$margin_right.'" margin_top="'.$margin_top.'" margin_bottom="'.$margin_bottom.'" padding_left="'.$padding_left.'" padding_right="'.$padding_right.'" padding_top="'.$padding_top.'" padding_bottom="'.$padding_bottom.'" title_tag="'.$title_tag.'" subtitle_tag="'.$subtitle_tag.'" title_color_scheme="'.$title_color_class.'" title_animation="'.$title_animation.'" title_animation_delay="'.$title_animation_delay.'" title_animation_duration="'.$title_animation_duration.'" subtitle_color_scheme="'.$subtitle_color_class.'" subtitle_animation="'.$subtitle_animation.'" subtitle_animation_delay="'.$subtitle_animation_delay.'" subtitle_animation_duration="'.$subtitle_animation_duration.'" content_color_scheme="'.$content_color_class.'" content_animation="'.$content_animation.'" content_animation_delay="'.$content_animation_delay.'" content_animation_duration="'.$content_animation_duration.'" background_color_scheme="'.$background_color_class.'"]';
			}
			if($grid[$key]['type']=='full_column' || $grid[$key]['type']=='developer'){
				$shortcode .= '[tlpb_section_full_width class="'.$class.'" id="'.$id.'" title="'.$title.'" title_position="'.$title_position.'" subtitle="'.$subtitle.'" subtitle_position="'.$subtitle_position.'" title_color="'.$title_color.'" subtitle_color="'.$subtitle_color.'" content_color="'.$content_color.'" padding="'.$padding.'" background_color="'.$background_color.'" background_image="'.$background_image.'" parallax="'.$parallax.'" margin_left="'.$margin_left.'" margin_right="'.$margin_right.'" margin_top="'.$margin_top.'" margin_bottom="'.$margin_bottom.'" padding_left="'.$padding_left.'" padding_right="'.$padding_right.'" padding_top="'.$padding_top.'" padding_bottom="'.$padding_bottom.'" title_tag="'.$title_tag.'" subtitle_tag="'.$subtitle_tag.'" title_color_scheme="'.$title_color_class.'" title_animation="'.$title_animation.'" title_animation_delay="'.$title_animation_delay.'" title_animation_duration="'.$title_animation_duration.'" subtitle_color_scheme="'.$subtitle_color_class.'" subtitle_animation="'.$subtitle_animation.'" subtitle_animation_delay="'.$subtitle_animation_delay.'" subtitle_animation_duration="'.$subtitle_animation_duration.'" content_color_scheme="'.$content_color_class.'" content_animation="'.$content_animation.'" content_animation_delay="'.$content_animation_delay.'" content_animation_duration="'.$content_animation_duration.'" background_color_scheme="'.$background_color_class.'"]';
			}
			
			if($grid[$key]['type']=='column' || $grid[$key]['type']=='full_column'){
				$i=0;
            	foreach($grid[$key]['sec_grid'] as $grid_struct):
					$sec_class = isset($grid[$key]['sec_class'][$i]) && $grid[$key]['sec_class'][$i]?$grid[$key]['sec_class'][$i]:'';
					$sec_id = isset($grid[$key]['sec_id'][$i]) && $grid[$key]['sec_id'][$i]?$grid[$key]['sec_id'][$i]:'';
					$image_position = isset($grid[$key]['image_position'][$i]) && $grid[$key]['image_position'][$i]?$grid[$key]['image_position'][$i]:'top';
					$animation = isset($grid[$key]['animation'][$i]) && $grid[$key]['animation'][$i]?$grid[$key]['animation'][$i]:'no';
					$animation_delay = isset($grid[$key]['animation_delay'][$i]) && $grid[$key]['animation_delay'][$i]?$grid[$key]['animation_delay'][$i]:'200ms';
					$animation_duration = isset($grid[$key]['animation_duration'][$i]) && $grid[$key]['animation_duration'][$i]?$grid[$key]['animation_duration'][$i]:'200ms';
					$image = isset($grid[$key]['image'][$i]) && $grid[$key]['image'][$i]?$grid[$key]['image'][$i]:'';
					$content = isset($grid[$key]['content'][$i]) && $grid[$key]['content'][$i]?$grid[$key]['content'][$i]:'';
					
					$shortcode .= '[tlpb_container grid="'.$grid_struct.'" sec_class="'.$sec_class.'" sec_id="'.$sec_id.'" image_position="'.$image_position.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'" image="'.$image.'"]'.$content.'[/tlpb_container]';
					$i++;
				endforeach;
			}
			if($grid[$key]['type']=='developer'){
				$content = isset($grid[$key]['content']) && $grid[$key]['content']?$grid[$key]['content']:'';
				$shortcode .= '[tlpb_developer]'.$content.'[/tlpb_developer]';
			}
			if($grid[$key]['type']=='banner'){
				$layout = isset($grid[$key]['layout']) && $grid[$key]['layout']?$grid[$key]['layout']:'banner';
				$subtype = isset($grid[$key]['subtype']) && $grid[$key]['subtype']?$grid[$key]['subtype']:'themelines_banner';
				$banner_parallax = isset($grid[$key]['banner_parallax']) && $grid[$key]['banner_parallax']?$grid[$key]['banner_parallax']:'0';
				if($subtype=='themelines_banner'){
					$slider = isset($grid[$key]['slider']) && $grid[$key]['slider']?$grid[$key]['slider']:'0';
					$shortcode .= '[tlpb_themelines_banner layout="'.$layout.'" banner_parallax="'.$banner_parallax.'" slider="'.$slider.'"]';
				}
			}
			if($grid[$key]['type']=='slider'){
				$layout = isset($grid[$key]['layout']) && $grid[$key]['layout']?$grid[$key]['layout']:'full_width';
				$subtype = isset($grid[$key]['subtype']) && $grid[$key]['subtype']?$grid[$key]['subtype']:'post_slider';
				$category = isset($grid[$key]['category']) && $grid[$key]['category']?$grid[$key]['category']:'0';
				$interval = isset($grid[$key]['interval']) && $grid[$key]['interval']?$grid[$key]['interval']:'500';
				if($subtype=='post_slider'){
					$shortcode .= '[tlpb_post_slider layout="'.$layout.'" category="'.$category.'" interval="'.$interval.'" key="'.$key.'"]';
				}
				if($subtype=='themelines_slider'){
					$shortcode .= '[tlpb_themelines_slider layout="'.$layout.'" category="'.$category.'" interval="'.$interval.'" key="'.$key.'"]';
				}
			}
			if($grid[$key]['type']=='other'){
				$content = isset($grid[$key]['shortcode']) && $grid[$key]['shortcode']?$grid[$key]['shortcode']:'';
				$shortcode .= '[tlpb_other]'.$content.'[/tlpb_other]';
			}
			
			if($grid[$key]['type']=='gallery'){
				$subtype = isset($grid[$key]['subtype']) && $grid[$key]['subtype']?$grid[$key]['subtype']:'themelines_gallery';
				if($subtype=='themelines_gallery'){
					$gal_items = isset($grid[$key]['items']) && $grid[$key]['items']?$grid[$key]['items']:'-1';
					$gal_cat = isset($grid[$key]['category']) && $grid[$key]['category']?$grid[$key]['category']:'0';
					$gal_grid = isset($grid[$key]['grid']) && $grid[$key]['grid']?$grid[$key]['grid']:'3';
					$gal_style = isset($grid[$key]['style']) && $grid[$key]['style']?$grid[$key]['style']:'normal';
					$filter = isset($grid[$key]['filter']) && $grid[$key]['filter']?$grid[$key]['filter']:'0';
					$onclick = isset($grid[$key]['onclick']) && $grid[$key]['onclick']?$grid[$key]['onclick']:'do_nothing';
					$load = isset($grid[$key]['load']) && $grid[$key]['load']?$grid[$key]['load']:'0';
					if($gal_style=='no_padding'){
						$shortcode .= '[tlpb_no_padding_gallery key="'.$key.'" items="'.$gal_items.'" categories="'.$gal_cat.'" filter="'.$filter.'" gal_grid="'.$gal_grid.'" onclick="'.$onclick.'" load_more="'.$load.'"]';
					}
					if($gal_style=='masonry'){
						$shortcode .= '[tlpb_masonry_gallery key="'.$key.'" items="'.$gal_items.'" categories="'.$gal_cat.'" filter="'.$filter.'" gal_grid="'.$gal_grid.'" onclick="'.$onclick.'" load_more="'.$load.'"]';
					}
					
					if($gal_style=='normal'){
						$shortcode .= '[tlpb_normal_gallery key="'.$key.'" items="'.$gal_items.'" categories="'.$gal_cat.'" filter="'.$filter.'" gal_grid="'.$gal_grid.'" onclick="'.$onclick.'" load_more="'.$load.'"]';
					}
				}else{
					if($subtype=='others'){
						$content = isset($grid[$key]['shortcode']) && $grid[$key]['shortcode']?$grid[$key]['shortcode']:'';
						$shortcode .= '[tlpb_other]'.$content.'[/tlpb_other]';
					}
				}
			}
			
			if($grid[$key]['type']=='post_type'){
				$animation = isset($grid[$key]['animation']) && $grid[$key]['animation']?$grid[$key]['animation']:'no';
				$animation_delay = isset($grid[$key]['animation_delay']) && $grid[$key]['animation_delay']?$grid[$key]['animation_delay']:'200ms';
				$animation_duration = isset($grid[$key]['animation_duration']) && $grid[$key]['animation_duration']?$grid[$key]['animation_duration']:'200ms';
				$post_type = isset($grid[$key]['post_type']) && $grid[$key]['post_type']?$grid[$key]['post_type']:'post';
				$post_order = isset($grid[$key]['post_order']) && $grid[$key]['post_order']?$grid[$key]['post_order']:'date-desc';
				$post_from = isset($grid[$key]['post_from']) && $grid[$key]['post_from']?$grid[$key]['post_from']:'all';
				$page_from = isset($grid[$key]['page_from']) && $grid[$key]['page_from']?$grid[$key]['page_from']:'all';
				$posts_per_page = isset($grid[$key]['post_number']) && $grid[$key]['post_number']?$grid[$key]['post_number']:'-1';
				$post_offset = isset($grid[$key]['post_offset']) && $grid[$key]['post_offset']?$grid[$key]['post_offset']:'0';
				$post_column = isset($grid[$key]['post_column']) && $grid[$key]['post_column']?$grid[$key]['post_column']:'3';
				$post_length = isset($grid[$key]['post_length']) && $grid[$key]['post_length']?$grid[$key]['post_length']:'50';
				if($grid[$key]['post_type']=='post' || $grid[$key]['post_type']=='page'){
					$post_element = array();
					if($grid[$key]['post_style']=='column'){
						$pagination = isset($grid[$key]['post_pagination']) && $grid[$key]['post_pagination']=='on'?$grid[$key]['post_pagination']:'';
						$feature = isset($grid[$key]['post_feature']) && $grid[$key]['post_feature']=='on'?$grid[$key]['post_feature']:'';
						$post_excerpt = isset($grid[$key]['post_excerpt']) && $grid[$key]['post_excerpt']=='on'?$grid[$key]['post_excerpt']:'';
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_post_column post_type="'.$post_type.'" post_order="'.$post_order.'" post_from="'.$post_from.'" page_from="'.$page_from.'" posts_per_page="'.$posts_per_page.'" offset="'.$post_offset.'" pagination="'.$pagination.'" column="'.$post_column.'" feature="'.$feature.'" post_excerpt="'.$post_excerpt.'" post_length="'.$post_length.'" post_element="'.$post_element.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
						
					}
					if($grid[$key]['post_style']=='horizontal'){
						$pagination = isset($grid[$key]['post_pagination']) && $grid[$key]['post_pagination']=='on'?$grid[$key]['post_pagination']:'';
						$feature = isset($grid[$key]['post_feature']) && $grid[$key]['post_feature']=='on'?$grid[$key]['post_feature']:'';
						$post_excerpt = isset($grid[$key]['post_excerpt']) && $grid[$key]['post_excerpt']=='on'?$grid[$key]['post_excerpt']:'';
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_post_horizontal post_type="'.$post_type.'" post_order="'.$post_order.'" post_from="'.$post_from.'" page_from="'.$page_from.'" posts_per_page="'.$posts_per_page.'" offset="'.$post_offset.'" pagination="'.$pagination.'" column="'.$post_column.'" feature="'.$feature.'" post_excerpt="'.$post_excerpt.'" post_length="'.$post_length.'" post_element="'.$post_element.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
					if($grid[$key]['post_style']=='masonry'){
						$pagination = isset($grid[$key]['post_pagination']) && $grid[$key]['post_pagination']=='on'?$grid[$key]['post_pagination']:'';
						$feature = isset($grid[$key]['post_feature']) && $grid[$key]['post_feature']=='on'?$grid[$key]['post_feature']:'';
						$post_excerpt = isset($grid[$key]['post_excerpt']) && $grid[$key]['post_excerpt']=='on'?$grid[$key]['post_excerpt']:'';
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_post_masonry post_type="'.$post_type.'" post_order="'.$post_order.'" post_from="'.$post_from.'" page_from="'.$page_from.'" posts_per_page="'.$posts_per_page.'" offset="'.$post_offset.'" pagination="'.$pagination.'" column="'.$post_column.'" feature="'.$feature.'" post_excerpt="'.$post_excerpt.'" post_length="'.$post_length.'" post_element="'.$post_element.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
				}
				if($grid[$key]['post_type']=='team'){
					if($grid[$key]['team_style']=='column'){
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_team_column post_type="'.$post_type.'" post_order="'.$post_order.'" post_number="'.$posts_per_page.'" post_offset="'.$post_offset.'" post_column="'.$post_column.'" post_element="'.$post_element.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
					
					if($grid[$key]['team_style']=='slder'){
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_team_slider post_type="'.$post_type.'" post_order="'.$post_order.'" post_number="'.$posts_per_page.'" post_offset="'.$post_offset.'" post_column="'.$post_column.'" post_element="'.$post_element.'" key="'.$key.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
				}
				if($grid[$key]['post_type']=='testimonial'){
					if($grid[$key]['testimonial_style']=='normal'){
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_testimonial_normal post_type="'.$post_type.'" post_order="'.$post_order.'" post_number="'.$posts_per_page.'" post_offset="'.$post_offset.'" post_column="'.$post_column.'" post_element="'.$post_element.'" key="'.$key.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
					if($grid[$key]['testimonial_style']=='chat'){
						$post_element = isset($grid[$key]['post_element']) && $grid[$key]['post_element']?implode(',',$grid[$key]['post_element']):'';
						$shortcode .= '[tlpb_testimonial_chat post_type="'.$post_type.'" post_order="'.$post_order.'" post_number="'.$posts_per_page.'" post_offset="'.$post_offset.'" post_column="'.$post_column.'" post_element="'.$post_element.'" key="'.$key.'" animation="'.$animation.'" animation_delay="'.$animation_delay.'" animation_duration="'.$animation_duration.'"]';
					}
				}
			}
			
			if($grid[$key]['type']=='column' || $grid[$key]['type']=='banner' || $grid[$key]['type']=='slider' || $grid[$key]['type']=='other' || $grid[$key]['type']=='gallery' || $grid[$key]['type']=='post_type'){
				$shortcode .= '[/tlpb_section]';
			}
			if($grid[$key]['type']=='full_column' || $grid[$key]['type']=='developer'){
				$shortcode .= '[/tlpb_section_full_width]';
			}
		}
		$tlpb_post = array(
			  'ID'           => $post_id,
			  'post_content' => $shortcode,
		  );
		wp_update_post( $tlpb_post );
		add_action( 'save_post',  'tl_pb_save', 10, 2 );
	
	}
	endif;
}

function tlpb_allow_php_script($content) {
    if (strpos($content, '<' . '?') !== false) {
        ob_start();
        eval('?' . '>' . $content);
        $content = ob_get_clean();
    }
    return $content;
}
add_filter('tl_content', 'tlpb_allow_php_script', 99);