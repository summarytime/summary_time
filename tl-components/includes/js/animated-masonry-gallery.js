jQuery(window).load(function () {

var size = 1;
var button = 1;
var button_class = "gallery-header-center-right-links-current";
var normal_size_class = "gallery-content-center-normal";
var full_size_class = "gallery-content-center-full";
var $container = jQuery('#gallery-content-center');
    
$container.isotope({itemSelector : '.tl-ms-grid'});


function check_button(){
	jQuery('.gallery-header-center-right-links').removeClass(button_class);
	if(button==1){
		jQuery("#filter-all").addClass(button_class);
		jQuery("#gallery-header-center-left-title").html('All Galleries');
	}
	/*if(button==2){
		jQuery("#filter-studio").addClass(button_class);
		jQuery("#gallery-header-center-left-title").html('Studio Gallery');
		}
	if(button==3){
		jQuery("#filter-landscape").addClass(button_class);
		jQuery("#gallery-header-center-left-title").html('Landscape Gallery');
		}*/	
}
	
function check_size(){
	jQuery("#gallery-content-center").removeClass(normal_size_class).removeClass(full_size_class);
	if(size==0){
		jQuery("#gallery-content-center").addClass(normal_size_class); 
		jQuery("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23a;"></span>');
		}
	if(size==1){
		jQuery("#gallery-content-center").addClass(full_size_class); 
		jQuery("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23b;"></span>');
		}
	$container.isotope({itemSelector : '.tl-ms-grid'});
}


	
/*jQuery("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(); });
jQuery("#filter-studio").click(function() {  $container.isotope({ filter: '.studio' }); button = 2; check_button();  });
jQuery("#filter-landscape").click(function() {  $container.isotope({ filter: '.landscape' }); button = 3; check_button();  });*/
jQuery('.tl-mas-gal-full').click(function(){
	var id = jQuery(this).attr('id');
	var result = id.split('-');
	var res = result.shift();
	var final_res = result.join();
	var fres = final_res.replace(res, ''); 
	var fres = fres.replace(',', '-');
	$container.isotope({ filter: '.'+fres }); 
	button = 0;
	check_button();
	jQuery(this).addClass(button_class);
});
jQuery("#gallery-header-center-left-icon").click(function() { if(size==0){size=1;}else if(size==1){size=0;} check_size(); });


check_button();
check_size();
});