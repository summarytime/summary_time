jQuery(document).ready(function(e) {
    var wow = new WOW(
	  {
		boxClass:     'wow',      // animated element css class (default is wow)
		animateClass: 'animated', // animation css class (default is animated)
		offset:       0,          // distance to the element when triggering the animation (default is 0)
		mobile:       true,       // trigger animations on mobile devices (default is true)
		live:         true,       // act on asynchronously loaded content (default is true)
		callback:     function(box) {
		  // the callback is fired every time an animation is started
		  // the argument that is passed in is the DOM node being animated
		},
		scrollContainer: null // optional scroll container selector, otherwise use window
	  }
	);
	wow.init();
	jQuery(window).resize(function(){
		resize_images();
	});
	function resize_images(){
		jQuery('img').each(function(index, element) {
			var srcset = jQuery(this).attr('srcset');
			if(typeof srcset !== typeof undefined && srcset !== false){
				if(jQuery(this).hasClass('gal-img')){
					var width = jQuery(this).closest('.portfolio').width();
					jQuery(this).attr('sizes','(max-width: '+width+'px) 100vw, '+width+'px');
					jQuery(this).attr('width',width);
					jQuery(this).attr('height',jQuery(this).closest('.portfolio-wrapper').height());
				}else{
					if(!jQuery(this).hasClass('caraousel-img')){
						var width = jQuery(this).closest('div').width();
						jQuery(this).attr('sizes','(max-width: '+width+'px) 100vw, '+width+'px');
						jQuery(this).attr('width',width);
						jQuery(this).attr('height',jQuery(this).height());
					}
				}
			}
			/*var srcset = jQuery(this).attr('srcset');
			if(typeof srcset !== typeof undefined && srcset !== false){
				var width = jQuery( window ).width();
				var img_size = jQuery(this).attr('sizes');
				var img_height = jQuery(this).attr('height');
				alert(jQuery(this).closest('figure').width());
				if(jQuery(this).attr('width')<767){
					var img_width = jQuery(this).attr('width');
					var fixed_img_size = jQuery(this).attr('sizes');
				}
				if(width<768 && img_width<768){
					jQuery(this).attr('sizes','(max-width: '+width+'px) 100vw, '+width+'px');
					jQuery(this).attr('width',width);
				}else{
					jQuery(this).attr('sizes',fixed_img_size);
					jQuery(this).attr('width',img_width);
				}
			}*/
        });
	}
	resize_images();
});

