

/** jquery Footer Reval js **/

(function(e){e.fn.footerReveal=function(t){var n=e(this),r=n.prev(),i=e(window),s=e.extend({shadow:true,shadowOpacity:.8,zIndex:-100},t),o=e.extend(true,{},s,t);if(n.outerHeight()<=i.outerHeight()){n.css({"z-index":s.zIndex,position:"fixed",bottom:0});if(s.shadow){r.css({"-moz-box-shadow":"0 20px 30px -20px rgba(0,0,0,"+s.shadowOpacity+")","-webkit-box-shadow":"0 20px 30px -20px rgba(0,0,0,"+s.shadowOpacity+")","box-shadow":"0 20px 30px -20px rgba(0,0,0,"+s.shadowOpacity+")"})}i.on("load resize",function(){n.css({width:r.outerWidth()});r.css({"margin-bottom":n.outerHeight()})})}return this}})(jQuery);

	

/*global jQuery */

/*!

* FitText.js 1.2

*

* Copyright 2011, Dave Rupert http://daverupert.com

* Released under the WTFPL license

* http://sam.zoy.org/wtfpl/

*

* Date: Thu May 05 14:23:00 2011 -0600

*/



/*(function( $ ){



  $.fn.fitText = function( kompressor, options ) {



    // Setup options

    var compressor = kompressor || 1,

        settings = $.extend({

          'minFontSize' : Number.NEGATIVE_INFINITY,

          'maxFontSize' : Number.POSITIVE_INFINITY

        }, options);



    return this.each(function(){



      // Store the object

      var $this = $(this);



      // Resizer() resizes items based on the object width divided by the compressor * 10

      var resizer = function () {

        $this.css('font-size', Math.max(Math.min($this.width() / (compressor*10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));

      };



      // Call once to set.

      resizer();



      // Call on resize. Opera debounces their resize by default.

      $(window).on('resize.fittext orientationchange.fittext', resizer);



    });



  };



})( jQuery );*/