<?php
/**
 * Init Session
 */
function ses_init() {
if (!session_id())
  session_start();
}
add_action('init','ses_init');
/**
 * Theme Options
 */
define( 'TL_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/tl-framework/' );
require_once get_template_directory() . '/tl-framework/tl-framework.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );


add_action( 'tlframework_custom_scripts', 'tlframework_custom_scripts' );

function tlframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});

	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}

});
</script>
<?php
}
/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function tl_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'tl_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		//wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		  $post_type = ($_GET['post_type'])?'&post_type='.$_GET['post_type']:'';
		  wp_redirect( admin_url( 'edit.php?'.$post_type ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_tl_duplicate_post_as_draft', 'tl_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function tl_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$post_type = (isset($_GET['post_type']) && $_GET['post_type'])?'&post_type='.$_GET['post_type']:'';
		$actions['duplicate'] = '<a href="admin.php?action=tl_duplicate_post_as_draft&amp;post=' . $post->ID .$post_type.'" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'tl_duplicate_post_link', 10, 2 );
add_filter( 'page_row_actions', 'tl_duplicate_post_link', 10, 2 );

function html_cut($text, $max_length)
{
    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i};
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }

    while ($tags)
        $result .= "</".array_pop($tags).">";

    return $result;
}

if(tlf_get_option( 'tl_defer_script', '' ) && tlf_get_option( 'tl_defer_script', '' )!='off'){
	
	/****************Defer Script Load Start*****************/
	/*function to add defer to all scripts*/
	function tl_js_async_attr($tag){
	
	# Do not add defer to these scripts
	$xclude_defer = preg_replace('/\s*,\s*/', ',',tlf_get_option( 'tl_exclude_defer_script', '' ));
	$scripts_to_exclude = explode(',',$xclude_defer);
	 
	if(!empty($scripts_to_exclude)){
		foreach($scripts_to_exclude as $exclude_script){
			if($exclude_script && true == strpos($tag, $exclude_script ) )
			return $tag;	
		}
	}
	
	if(tlf_get_option( 'tl_defer_script', '' )=='defer'){
		$defer = ' defer="defer" src';
	}elseif(tlf_get_option( 'tl_async_script', '' )=='async'){
		$defer = ' async="async" src';
	}
	
	# Add defer to all remaining scripts
	return str_replace( ' src', $defer, $tag );
	}
	
	if(!is_admin()){
		add_filter( 'script_loader_tag', 'tl_js_async_attr', 10 );
	}
	/****************Defer Script Load End*****************/
	
}


if(tlf_get_option( 'tl_place_script_footer', '' ) && tlf_get_option( 'tl_place_script_footer', '' )!='on'){
	// Start Custom Scripting to Move JavaScript to footer
	function tl_remove_head_scripts() {
	   remove_action('wp_head', 'wp_print_scripts');
	   remove_action('wp_head', 'wp_print_head_scripts', 9);
	   remove_action('wp_head', 'wp_enqueue_scripts', 1);
	 
	   add_action('wp_footer', 'wp_print_scripts', 5);
	   add_action('wp_footer', 'wp_enqueue_scripts', 5);
	   add_action('wp_footer', 'wp_print_head_scripts', 5);
	
	}
	if(!is_admin()){
		add_action( 'wp_enqueue_scripts', 'tl_remove_head_scripts' );
	}
	
	if(tlf_get_option( 'tl_exclude_footer_script', '' ) && tlf_get_option( 'tl_exclude_footer_script', '' )){
		if(!is_admin()){
			add_action( 'wp_print_styles', 'tl_exclude_scripts', 100 );
			add_action( 'wp_head', 'tl_include_header_scripts' );
		}
		
		function tl_exclude_scripts() {
			$exclude_scripts = tlf_get_option( 'tl_exclude_footer_script', '' );
			$exclude_scripts_array = explode(',',$exclude_scripts);
			if(isset($exclude_scripts_array) && $exclude_scripts_array){
				foreach($exclude_scripts_array as $exs){
					$exs_name = explode('|',$exs);
					if(isset($exs_name) && $exs_name){
						wp_deregister_script( $exs_name[0] ); // The slug or name used to enqueue the script
					}
				}
			}
		}
		
		function tl_include_header_scripts() {
			$exclude_scripts = tlf_get_option( 'tl_exclude_footer_script', '' );
			$exclude_scripts_array = explode(',',$exclude_scripts);
			if(isset($exclude_scripts_array) && $exclude_scripts_array){
				foreach($exclude_scripts_array as $exs){
					$exs_name = explode('|',$exs);
					if(isset($exs_name) && $exs_name){
						echo '<script type="text/javascript" src="'.$exs_name[1].'"></script>';
					}
				}
			}
		}
	}
}

function tl_remove_cssjs_ver( $src ) {	
    $parts = explode( '?', $src );
	return $parts[0];
}

if(tlf_get_option( 'remove_version_stylesheet', '' ) && tlf_get_option( 'remove_version_stylesheet', '' )!='on'){
	add_filter( 'style_loader_src', 'tl_remove_cssjs_ver', 10, 2 );
}

if(tlf_get_option( 'remove_version_script', '' ) && tlf_get_option( 'remove_version_script', '' )!='on'){
	add_filter( 'script_loader_src', 'tl_remove_cssjs_ver', 10, 2 );
}

if(tlf_get_option('custom_css','')){
function tl_custom_css(){
	wp_enqueue_style( 'tl-custom-css',get_template_directory_uri() . '/includes/css/tl_custom.css' );
}
add_action( 'wp_head', 'tl_custom_css',1);
}

$theme = wp_get_theme();
$author = $theme->get( 'Author' );
$author_uri = $theme->get( 'AuthorURI' );
$theme_name = $theme->get( 'Name' );
if(md5($author)=='e1ee198732a51ecf7bfe397aa333f82e' && strpos($author_uri, 'themelines.com') && md5($theme_name)=='358cebe1645e9401399a8b9c293010f0'){
	update_option( 'tlpb_status', 'true', 'yes' );
}else{
	update_option( 'tlpb_status', 'false', 'yes' );
}


function tl_breadcrumbs() {
	if(tlf_get_option('show_breadcrumb','')):
	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page
	$text['page']     = 'Page %s'; // text 'Page N'
	$text['cpage']    = 'Comment Page %s'; // text 'Comment Page N'
	if(tlf_get_option('breadcrumb_position','')=='right'){
		$wrap_first = '<div class="pull-right">';
		$wrap_last = '</div>';
	}elseif(tlf_get_option('breadcrumb_position','')=='center'){
		$wrap_first = '<div class="text-center">';
		$wrap_last = '</div>';
	}else{
		$wrap_first = '';
		$wrap_last = '';
	}
	$wrap_before    = $wrap_first.'<div class="tl-page-breadcrumbs"><div class="container"><div class="row"><div class="breadcrumbs">'; // the opening wrapper tag
	$wrap_after     = $wrap_last.'</div></div></div></div><!-- .breadcrumbs -->'; // the closing wrapper tag
	$sep            = tlf_get_option('breadcrumb_seperator','')?tlf_get_option('breadcrumb_seperator',''):'›'; // separator between crumbs
	$sep_before     = '<span class="sep">'; // tag before separator
	$sep_after      = '</span>'; // tag after separator
	$show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
	$show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$show_current   = 1; // 1 - show current page title, 0 - don't show
	$before         = '<span class="current">'; // tag before the current crumb
	$after          = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */
	global $post;
	$home_link      = home_url('/');
	$link_before    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
	$link_after     = '</span>';
	$link_attr      = ' itemprop="url"';
	$link_in_before = '<span itemprop="title">';
	$link_in_after  = '</span>';
	$link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
	$frontpage_id   = get_option('page_on_front');
	$parent_id      = $post->post_parent;
	$sep            = ' ' . $sep_before . $sep . $sep_after . ' ';
	if (is_home() || is_front_page()) {
		if ($show_on_home) echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;
	} else {
		echo $wrap_before;
		if ($show_home_link) echo sprintf($link, $home_link, $text['home']);
		if ( is_category() ) {
			$cat = get_category(get_query_var('cat'), false);
			if ($cat->parent != 0) {
				$cats = get_category_parents($cat->parent, TRUE, $sep);
				$cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				if ($show_home_link) echo $sep;
				echo $cats;
			}
			if ( get_query_var('paged') ) {
				$cat = $cat->cat_ID;
				echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
			}
		} elseif ( is_search() ) {
			if (have_posts()) {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
			} else {
				if ($show_home_link) echo $sep;
				echo $before . sprintf($text['search'], get_search_query()) . $after;
			}
		} elseif ( is_day() ) {
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
			echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
			if ($show_current) echo $sep . $before . get_the_time('d') . $after;
		} elseif ( is_month() ) {
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
			if ($show_current) echo $sep . $before . get_the_time('F') . $after;
		} elseif ( is_year() ) {
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . get_the_time('Y') . $after;
		} elseif ( is_single() && !is_attachment() ) {
			if ($show_home_link) echo $sep;
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($show_current) echo $sep . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $sep);
				if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
				if ( get_query_var('cpage') ) {
					echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
				} else {
					if ($show_current) echo $before . get_the_title() . $after;
				}
			}
		// custom post type
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			if ( get_query_var('paged') ) {
				echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . $post_type->label . $after;
			}
		} elseif ( is_attachment() ) {
			if ($show_home_link) echo $sep;
			$parent = get_post($parent_id);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			if ($cat) {
				$cats = get_category_parents($cat, TRUE, $sep);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
			}
			printf($link, get_permalink($parent), $parent->post_title);
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && !$parent_id ) {
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && $parent_id ) {
			if ($show_home_link) echo $sep;
			if ($parent_id != $frontpage_id) {
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					if ($parent_id != $frontpage_id) {
						$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ($i != count($breadcrumbs)-1) echo $sep;
				}
			}
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_tag() ) {
			if ( get_query_var('paged') ) {
				$tag_id = get_queried_object_id();
				$tag = get_tag($tag_id);
				echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
			}
		} elseif ( is_author() ) {
			global $author;
			$author = get_userdata($author);
			if ( get_query_var('paged') ) {
				if ($show_home_link) echo $sep;
				echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
			}
		} elseif ( is_404() ) {
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . $text['404'] . $after;
		} elseif ( has_post_format() && !is_singular() ) {
			if ($show_home_link) echo $sep;
			echo get_post_format_string( get_post_format() );
		}
		echo $wrap_after;
	}
	endif;
} // end of tl_breadcrumbs()

add_filter( 'post_row_actions', 'tl_duplicate_post_link', 10, 2 );
add_filter( 'page_row_actions', 'tl_duplicate_post_link', 10, 2 );

function tl_login_logo() {
	$logo = tlf_get_option( 'admin_login_image', '' )?tlf_get_option( 'admin_login_image', '' ):get_stylesheet_directory_uri()."/includes/images/logo.png";
	$color = tlf_get_option( 'logo_background_color', '' )?tlf_get_option( 'logo_background_color', '' ):'#f1f1f1';
    echo "<style type='text/css'>
        #login h1 a, .login h1 a {
           background-image: url(".$logo.");
			background-size: 200px auto;
    		width: 200px;
			outline: none;
			height: 55px;
        }
		#login h1, .login h1{
			background-color: ".$color.";
		}
    </style>";
}
add_action( 'login_enqueue_scripts', 'tl_login_logo' );
function tl_login_logo_url() {
    return tlf_get_option( 'logo_url', '' )?tlf_get_option( 'logo_url', '' ):'https://www.themelines.com/';
}
add_filter( 'login_headerurl', 'tl_login_logo_url' );

function tl_login_logo_url_title() {
    return tlf_get_option( 'logo_title', '' )?tlf_get_option( 'logo_title', '' ):'Themelines';
}
add_filter( 'login_headertitle', 'tl_login_logo_url_title' );

add_filter('comment_form_default_fields', 'tl_custom_fields');
function tl_custom_fields($fields) {
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );

    $fields[ 'author' ] = '<div class="tl-comment-fields"><div class="form-group comment-name">'.
      '<label for="author">' . __( 'Name: ', 'tl' ) . '</label>'.
      ( $req ? '<span class="required">*</span>' : '' ).
      '<input id="author" class="form-control" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) .
      '" size="30" tabindex="1"' . $aria_req . ' required/></div>';

    $fields[ 'email' ] = '<div class="form-group comment-email">'.
      '<label for="email">' . __( 'Email: ', 'tl' ) . '</label>'.
      ( $req ? '<span class="required">*</span>' : '' ).
      '<input id="email" class="form-control" name="email" type="email" value="'. esc_attr( $commenter['comment_author_email'] ) .
      '" size="30"  tabindex="2"' . $aria_req . ' required/></div>';

    $fields[ 'url' ] = '<div class="form-group comment-website">'.
      '<label for="url">' . __( 'Website', 'tl' ) . '</label>'.
      '<input id="url" class="form-control" name="url" type="text" value="'. esc_attr( $commenter['comment_author_url'] ) .
      '" tabindex="3" /></div>';

    $fields[ 'phone' ] = '<div class="form-group comment-phone">'.
      '<label for="phone">' . __( 'Contact Number', 'tl' ) . '</label>'.
      '<input id="phone" class="form-control" name="phone" type="tel" size="30"  tabindex="4" /></div></div>';
	  
  return $fields;
}

add_filter( 'preprocess_comment', 'tl_verify_comment_meta_data' );
function tl_verify_comment_meta_data( $commentdata ) {
	if(tlf_get_option( 'validate_comment', '' )){
		if ( ! isset( $_POST['math'] ) || $_POST['math']=='' ){
			wp_die( __( 'Error: You did not enter the calculation result. Hit the BACK button of your Web browser and resubmit your comment with the correct result.', 'tl' ) );
		}else{
			if ( $_POST['math']!=$_POST['answer'] ){
				wp_die( __( 'Error: Incorrect calculation result. Hit the BACK button of your Web browser and resubmit your comment with the correct result.', 'tl' ) );
			}
		}
	}
	return $commentdata;
}
add_action( 'after_setup_theme', 'tl_resize' );
function tl_resize() {
	add_image_size( 'post-col-two', 565, 282, true );
	add_image_size( 'post-col-three', 370, 296, true );
	add_image_size( 'post-col-four', 273, 173, true );
	add_image_size( 'post-horizontal', 285, 190, true );
	add_image_size( 'post-mas-two', 574, '', false );
	add_image_size( 'post-mas-three', 378, '', false );
	add_image_size( 'post-mas-four', 275, '', false );
	add_image_size( 'gal-normal-two', 547, 300, true );
	add_image_size( 'gal-normal-three', 356, 225, true );
	add_image_size( 'gal-normal-four', 262, 173, true );
	add_image_size( 'team-col-two', 555, 555, true );
	add_image_size( 'team-col-three', 360, 360, true );
	add_image_size( 'team-col-four', 263, 263, true );
	add_image_size( 'team-slid-two', 499, 499, true );
	add_image_size( 'team-slid-three', 330, 330, true );
	add_image_size( 'team-slid-four', 245, 245, true );
	add_image_size( 'testi-normal', 114, 114, true );
	add_image_size( 'testi-chat', 88, 88, true );
	add_image_size( 'post-slid', 420, 420, true );
	add_image_size( 'banner-inside-container', 1170, '', false );
	add_image_size( 'logo-img', 160, '', false );
	add_image_size( 'single-img', 690, '', false );
}
function tl_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
    return $attachment[0];
}
/**
 * Import Update Checker
 */
require_once get_template_directory() . '/includes/theme-update-checker.php';
$example_update_checker = new ThemeUpdateChecker(
	'cleanify',                                            //Theme folder name, AKA "slug". 
	'https://www.themelines.com/demo/update/cleanify.json' //URL of the metadata file.
);