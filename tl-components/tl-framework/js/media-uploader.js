jQuery(document).ready(function($){

	var tlframework_upload;
	var tlframework_selector;

	function tlframework_add_file(event, selector) {

		var upload = $(".uploaded-file"), frame;
		var $el = $(this);
		tlframework_selector = selector;

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( tlframework_upload ) {
			tlframework_upload.open();
		} else {
			// Create the media frame.
			tlframework_upload = wp.media.frames.tlframework_upload =  wp.media({
				// Set the title of the modal.
				title: $el.data('choose'),

				// Customize the submit button.
				button: {
					// Set the text of the button.
					text: $el.data('update'),
					// Tell the button not to close the modal, since we're
					// going to refresh the page when the image is selected.
					close: false
				}
			});

			// When an image is selected, run a callback.
			tlframework_upload.on( 'select', function() {
				// Grab the selected attachment.
				var attachment = tlframework_upload.state().get('selection').first();
				tlframework_upload.close();
				tlframework_selector.find('.upload').val(attachment.attributes.url);
				if ( attachment.attributes.type == 'image' ) {
					tlframework_selector.find('.screenshot').empty().hide().append('<img src="' + attachment.attributes.url + '"><a class="remove-image">Remove</a>').slideDown('fast');
				}
				tlframework_selector.find('.upload-button').unbind().addClass('remove-file').removeClass('upload-button').val(tlframework_l10n.remove);
				tlframework_selector.find('.tlf-background-properties').slideDown();
				tlframework_selector.find('.remove-image, .remove-file').on('click', function() {
					tlframework_remove_file( $(this).parents('.section') );
				});
			});

		}

		// Finally, open the modal.
		tlframework_upload.open();
	}

	function tlframework_remove_file(selector) {
		selector.find('.remove-image').hide();
		selector.find('.upload').val('');
		selector.find('.tlf-background-properties').hide();
		selector.find('.screenshot').slideUp();
		selector.find('.remove-file').unbind().addClass('upload-button').removeClass('remove-file').val(tlframework_l10n.upload);
		// We don't display the upload button if .upload-notice is present
		// This means the user doesn't have the WordPress 3.5 Media Library Support
		if ( $('.section-upload .upload-notice').length > 0 ) {
			$('.upload-button').remove();
		}
		selector.find('.upload-button').on('click', function(event) {
			tlframework_add_file(event, $(this).parents('.section'));
		});
	}

	$('.remove-image, .remove-file').on('click', function() {
		tlframework_remove_file( $(this).parents('.section') );
    });

    $('.upload-button').click( function( event ) {
    	tlframework_add_file(event, $(this).parents('.section'));
    });

});