/**
 * Custom scripts needed for the colorpicker, image button selectors,
 * and navigation tabs.
 */

jQuery(document).ready(function($) {

	// Loads the color pickers
	$('.tlf-color').wpColorPicker();

	// Image Options
	$('.tlf-radio-img-img').click(function(){
		$(this).parent().parent().find('.tlf-radio-img-img').removeClass('tlf-radio-img-selected');
		$(this).addClass('tlf-radio-img-selected');
	});

	$('.tlf-radio-img-label').hide();
	$('.tlf-radio-img-img').show();
	$('.tlf-radio-img-radio').hide();

	// Loads tabbed sections if they exist
	if ( $('.nav-tab-wrapper').length > 0 ) {
		tl_framework_tabs();
	}

	function tl_framework_tabs() {

		var $group = $('.group'),
			$navtabs = $('.nav-tab-wrapper a'),
			active_tab = '';

		// Hides all the .group sections to start
		$group.hide();

		// Find if a selected tab is saved in localStorage
		if ( typeof(localStorage) != 'undefined' ) {
			active_tab = localStorage.getItem('active_tab');
		}

		// If active tab is saved and exists, load it's .group
		if ( active_tab != '' && $(active_tab).length ) {
			$(active_tab).fadeIn();
			$(active_tab + '-tab').addClass('nav-tab-active');
		} else {
			$('.group:first').fadeIn();
			$('.nav-tab-wrapper a:first').addClass('nav-tab-active');
		}

		// Bind tabs clicks
		$navtabs.click(function(e) {

			e.preventDefault();

			// Remove active class from all tabs
			$navtabs.removeClass('nav-tab-active');

			$(this).addClass('nav-tab-active').blur();

			if (typeof(localStorage) != 'undefined' ) {
				localStorage.setItem('active_tab', $(this).attr('href') );
			}

			var selected = $(this).attr('href');

			$group.hide();
			$(selected).fadeIn();

		});
	}
	$(document).on('click','.btn_add_repeat',function(){
		 //var content = $(this).closest('.tl_repeat').html().not('.btn_add_repeat').clone();
		var content = $(this).closest('.tl_repeat').clone().find('.btn_add_repeat').remove().end().html();
		content+='<button type="button" class="btn_remove_repeat">Remove</button>';
		 $('.repeat_main').append('<div class="tl_repeat">'+content+'</div>');
	});
	$(document).on('click','.btn_remove_repeat',function(){
		$(this).closest('.tl_repeat').remove();
	});
	$('#options-group-1-tab').prepend( '<i class="option-icon fa fa-paper-plane"></i>' );
	$('#options-group-2-tab').prepend( '<i class="option-icon fa fa-header"></i>' );
	$('#options-group-3-tab').prepend( '<i class="option-icon fa fa-gavel"></i>' );
	$('#options-group-4-tab').prepend( '<i class="option-icon fa fa-users"></i>' );
	$('#options-group-5-tab').prepend( '<i class="option-icon fa fa-code"></i>' );
	$('#options-group-6-tab').prepend( '<i class="option-icon fa fa-globe"></i>' );
	$('#options-group-7-tab').prepend( '<i class="option-icon fa fa-pagelines"></i>' );
	$('#options-group-8-tab').prepend( '<i class="option-icon fa fa-pencil"></i>' );
	$('#options-group-9-tab').prepend( '<i class="option-icon fa fa-exclamation-triangle"></i>' );
	$('#options-group-10-tab').prepend( '<i class="option-icon fa fa-sort"></i>' );
	$('#options-group-11-tab').prepend( '<i class="option-icon fa fa-css3"></i>' );
	$('#options-group-12-tab').prepend( '<i class="option-icon fa fa-user"></i>' );
});