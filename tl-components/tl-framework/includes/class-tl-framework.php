<?php
/**
 * @package   Tl_Framework
 * @author    Themelines <info@themelines.com>
 * @license   GPL-2.0+
 * @link      https://www.themelines.com
 * @copyright 2015-2016 Themelines
 */

class Tl_Framework {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since 1.7.0
	 * @type string
	 */
	const VERSION = '1.9.1';

	/**
	 * Gets option name
	 *
	 * @since 1.9.0
	 */
	function get_option_name() {

		$name = '';

		// Gets option name as defined in the theme
		if ( function_exists( 'tlframework_option_name' ) ) {
			$name = tlframework_option_name();
		}

		// Fallback
		if ( '' == $name ) {
			$name = get_option( 'stylesheet' );
			$name = preg_replace( "/\W/", "_", strtolower( $name ) );
		}

		return apply_filters( 'tl_framework_option_name', $name );

	}

	/**
	 * Wrapper for tlframework_options()
	 *
	 * Allows for manipulating or setting options via 'tlf_options' filter
	 * For example:
	 *
	 * <code>
	 * add_filter( 'tlf_options', function( $options ) {
	 *     $options[] = array(
	 *         'name' => 'Input Text Mini',
	 *         'desc' => 'A mini text input field.',
	 *         'id' => 'example_text_mini',
	 *         'std' => 'Default',
	 *         'class' => 'mini',
	 *         'type' => 'text'
	 *     );
	 *
	 *     return $options;
	 * });
	 * </code>
	 *
	 * Also allows for setting options via a return statement in the
	 * options.php file.  For example (in options.php):
	 *
	 * <code>
	 * return array(...);
	 * </code>
	 *
	 * @return array (by reference)
	 */
	static function &_tlframework_options() {
		static $options = null;

		if ( !$options ) {
	        // Load options from options.php file (if it exists)
	        $location = apply_filters( 'tl_framework_location', array( 'options.php' ) );
	        if ( $optionsfile = locate_template( $location ) ) {
	            $maybe_options = load_template( $optionsfile );
	            if ( is_array( $maybe_options ) ) {
					$options = $maybe_options;
	            } else if ( function_exists( 'tlframework_options' ) ) {
					$options = tlframework_options();
				}
	        }

	        // Allow setting/manipulating options via filters
	        $options = apply_filters( 'tlf_options', $options );
		}

		return $options;
	}

}