<?php
function seo_title(){
	global $wp_query;
	global $wp_locale;
	global $post;
 
    $m        = get_query_var( 'm' );
    $year     = get_query_var( 'year' );
    $monthnum = get_query_var( 'monthnum' );
    $day      = get_query_var( 'day' );
    $search   = get_query_var( 's' );
    $title    = '';
	
	$sitename=get_bloginfo( 'name' );
	$seperator=(tlf_get_option( 'seo_title_seperator', '' ))?tlf_get_option( 'seo_title_seperator', '' ):'|';
	if(isset($post->ID) && $post->ID){
		$pageTitle = get_the_title($post->ID);
	}
	
	$t_sep = ' '.$seperator.' ';
	
	if(is_page() || is_single()){
		if(get_post_meta( $post->ID, 'seo_title', true )){
			$title = get_post_meta( $post->ID, 'seo_title', true );
		}else{
			$pageTitle = get_the_title($post->ID);
			$title = $pageTitle.$t_sep.$sitename;
		}
	}
	
	if(is_category() || is_tag() || is_tax()){
		$term = $wp_query->get_queried_object();
		$t_id = $term->term_id;
		$term_meta = get_option( "taxonomy_$t_id" ); 
		
		if($term_meta['seo_title']){
			$pageTitle = $term_meta['seo_title'];
			$title = $pageTitle.$t_sep.$sitename;
		}else{
			$pageTitle = $term->name;
			$title = $pageTitle.$t_sep.$sitename;
		}
		
	}
	
	if( is_home() ) {
		$title = 'Home'.$t_sep.$sitename;
    }
	
	// If there's a post type archive
    if ( is_post_type_archive() ) {
        $post_type = get_query_var( 'post_type' );
        if ( is_array( $post_type ) ) {
            $post_type = reset( $post_type );
        }
        $post_type_object = get_post_type_object( $post_type );
        if ( ! $post_type_object->has_archive ) {
            $title = post_type_archive_title( '', false ).$t_sep.$sitename;;
        }
    }
	
	// If there's an author
    if ( is_author() && ! is_post_type_archive() ) {
        $author = get_queried_object();
        if ( $author ) {
            $title = $author->display_name.$t_sep.$sitename;
        }
    }
 
    // Post type archives with has_archive should override terms.
    if ( is_post_type_archive() && $post_type_object->has_archive ) {
        $title = post_type_archive_title( '', false ).$t_sep.$sitename;;
    }
	
	// If there's a month
    if ( is_archive() && ! empty( $m ) ) {
        $my_year  = substr( $m, 0, 4 );
        $my_month = $wp_locale->get_month( substr( $m, 4, 2 ) );
        $my_day   = intval( substr( $m, 6, 2 ) );
        $title    = $my_year . ( $my_month ? $t_sep . $my_month : '' ) . ( $my_day ? $t_sep . $my_day : '' );
    }
 
    // If there's a year
    if ( is_archive() && ! empty( $year ) ) {
        $title = $year;
        if ( ! empty( $monthnum ) ) {
            $title .= $t_sep . $wp_locale->get_month( $monthnum );
        }
        if ( ! empty( $day ) ) {
            $title .= $t_sep . zeroise( $day, 2 );
        }
		$title .= $t_sep.$sitename;
    }
	
	// If it's a search
    if ( is_search() ) {
        /* translators: 1: separator, 2: search phrase */
        $title = sprintf( __( 'Search Results %1$s %2$s', 'tl' ), $t_sep, strip_tags( $search ) ).$t_sep.$sitename;
    }
 
    // If it's a 404 page
    if ( is_404() ) {
        $title = __( 'Page not found', 'tl' ).$t_sep.$sitename;
    }
	
	return $title;
}
add_filter( 'wp_title', 'seo_title', 15, 3 );

function seo_description(){
	global $wp_query;
	global $post;
	$term = $wp_query->get_queried_object();
	/*$t_id = $term->term_id;
	$term_meta = get_option( "taxonomy_$t_id" );*/
	
	$seo = "
<!-- / TL-SEO STARTS / -->";
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_meta_robots_index', true )==1){
			$robot = 'noindex';
			if(get_post_meta( $post->ID, 'seo_meta_robots_follow', true )){
				$robot .= ','.get_post_meta( $post->ID, 'seo_meta_robots_follow', true );
			}
			if(get_post_meta( $post->ID, 'seo_meta_robots_advanced', true ) && get_post_meta( $post->ID, 'seo_meta_robots_advanced', true )!='-' && get_post_meta( $post->ID, 'seo_meta_robots_advanced', true )!='none'){
				$robot .= ','.get_post_meta( $post->ID, 'seo_meta_robots_advanced', true );
			}
			$seo .= '
<meta name="robots" content="'.$robot.'"/>';
		}
	}
	if(is_category() || is_tag() || is_tax()){
		$term = $wp_query->get_queried_object();
		$t_id = $term->term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		if($term_meta['noindex_this_category'] && $term_meta['noindex_this_category']!='default' && $term_meta['noindex_this_category']!='index'){
			$seo .= '
<meta name="robots" content="noindex,follow"/>';
		}
	}
	
	$tagline = get_bloginfo ( 'description' );
	$description = '
<meta name="description" content="'.$tagline.'"/>';
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_description', true )){
			$description = '
<meta name="description" content="'.get_post_meta( $post->ID, 'seo_description', true ).'"/>';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
					$description = '
<meta name="description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'"/>';
				}
			}else{
				$post_data = get_post($post->ID); 
				$excerpt = preg_replace( "/\r|\n\s+/", "", do_shortcode($post_data->post_content));
				if($excerpt){
					$description = '
<meta name="description" content="'.substr(trim(strip_tags(strip_shortcodes($excerpt))),0,160).'"/>';
				}
			}
		}
	}
	if(is_category() || is_tag() || is_tax()){
		if($term_meta['seo_description']){
			$description = '
<meta name="description" content="'.$term_meta['seo_description'].'"/>';
		}else{
			$description = '
<meta name="description" content="'.trim(strip_tags(term_description())).'"/>';
		}
	}
	$seo .= $description;
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_keywords', true )){
			$seo .= '
<meta name="keywords" content="'.get_post_meta( $post->ID, 'seo_keywords', true ).'"/>';
		}
	}
	
	$canonical = '
<link rel="canonical" href="'.get_permalink().'" />';
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_canonical_url', true )){
			$canonical = '
<link rel="canonical" href="'.get_post_meta( $post->ID, 'seo_canonical_url', true ).'" />';
		}
	}
	
	if(is_category() || is_tag() || is_tax()){
		if($term_meta['canonical']){
			$canonical = '
<link rel="canonical" href="'.$term_meta['canonical'].'" />';
		}
	}
	
	$seo .= $canonical;
	
	$locale = '
<meta property="og:locale" content="'.get_locale().'" />';
	
	if(is_home() || is_front_page()){
		$type = '
<meta property="og:type" content="website" />';
	}else{
		if(is_single() || is_page()){
			$type = '
<meta property="og:type" content="article" />';
		}else{
			$type = '
<meta property="og:type" content="object" />';
		}
	}
	
	$title = '
<meta property="og:title" content="'.seo_title().'" />';
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_title', '' )){
			$title = '
<meta property="og:title" content="'.tlf_get_option( 'seo_facebook_title', '' ).'" />';
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_facebook_title', true )){
			$title = '
<meta property="og:title" content="'.get_post_meta( $post->ID, 'seo_facebook_title', true ).'" />';
		}
	}
	
	$desc = '
<meta property="og:description" content="'.$tagline.'" />';
			
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_description', '' )){
			$desc = '
<meta property="og:description" content="'.tlf_get_option( 'seo_facebook_description', '' ).'" />';
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_facebook_description', true )){
			$desc = '
<meta property="og:description" content="'.get_post_meta( $post->ID, 'seo_facebook_description', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
					if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
						$desc = '
<meta property="og:description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'" />';
					}
			}else{
				$post_data = get_post($post->ID);
				$excerpt = preg_replace( "/\r|\n\s+/", "", do_shortcode($post_data->post_content));
				if($excerpt){
					$desc = '
<meta property="og:description" content="'.substr(trim(strip_tags(strip_shortcodes($excerpt))),0,160).'" />';
				}
			}
		}
	}
	
	$url = '
<meta property="og:url" content="'.get_permalink().'" />';
			
	$site_name = '
<meta property="og:site_name" content="'.get_bloginfo( 'name' ).'" />';
			
	$pub_url = '';
			
	if(is_single() || is_page()){
		if(tlf_get_option('seo_facebook_page_url')){
			$pub_url = '
<meta property="article:publisher" content="'.tlf_get_option('seo_facebook_page_url').'" />';
		}
	}
	
	$section = '';
	if(is_single()){
		$terms = wp_get_post_terms( $post->ID, 'category' );
		if(isset($terms) && $terms){
		$section .= '
<meta property="article:section" content="'.$terms[0]->name.'" />';
		}
	}
	
	if(is_single() || is_page()){
		$section .= '
<meta property="article:published_time" content="'.get_the_date( DATE_ATOM, $post->ID ).'" />';
		$section .= '
<meta property="article:modified_time" content="'.get_post_modified_time(DATE_ATOM).'" />';
		$section .= '
<meta property="og:updated_time" content="'.get_post_modified_time(DATE_ATOM).'" />';
	}
	
	if(tlf_get_option( 'logo_uploader', '' )){
		$fb_image= '
<meta property="og:image" content="'.tlf_get_option( 'logo_uploader', '' ).'" />';
	}else{
		$fb_image='';
	}
	
	if(tlf_get_option( 'seo_facebook_default_image', '' )){
			$fb_image= '
<meta property="og:image" content="'.tlf_get_option( 'seo_facebook_default_image', '' ).'" />';
	}
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_front_image', '' )){
			$fb_image= '
<meta property="og:image" content="'.tlf_get_option( 'seo_facebook_front_image', '' ).'" />';
		}else{
				if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
					if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
						$fb_image= '
<meta property="og:image" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
					}
				}
			}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_facebook_image', true )){
			$fb_image= '
<meta property="og:image" content="'.get_post_meta( $post->ID, 'seo_facebook_image', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
					$fb_image= '
<meta property="og:image" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
				}
			}else{
				if(has_post_thumbnail( $post->ID )){
					$fb_image= '
<meta property="og:image" content="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID) ).'" />';
				}else{
					$post_data = get_post($post->ID); 
					$excerpt = strip_shortcodes($post_data->post_content);
					if($excerpt){
						$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $excerpt, $matches);
						if(isset($matches[1][0]) && $matches[1][0]){
							$first_img = $matches[1][0];
						}else{
							$first_img = false;
						}
						
						if($first_img){
							$fb_image = '
<meta property="og:image" content="'.$first_img.'" />';
						}
					}
				}
			}
		}
	}
	
	$facebook = $locale.$type.$title.$desc.$site_name.$pub_url.$section.$fb_image;
	$seo .= $facebook;
	
	$card = '
<meta name="twitter:card" content="'.tlf_get_option( 'seo_twitter_default_card_type', '' ).'"/>';
			
	$twit_title = '
<meta name="twitter:title" content="'.seo_title().'" />';
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_title', '' )){
			$twit_title = '
<meta name="twitter:title" content="'.tlf_get_option( 'seo_facebook_title', '' ).'" />';
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_twitter_title', true )){
			$twit_title = '
<meta name="twitter:title" content="'.get_post_meta( $post->ID, 'seo_twitter_title', true ).'" />';
		}
	}
	
	$twit_desc = '
<meta name="twitter:description" content="'.$tagline.'" />';
			
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_description', '' )){
			$twit_desc = '
<meta name="twitter:description" content="'.tlf_get_option( 'seo_facebook_description', '' ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
					$twit_desc = '
<meta name="twitter:description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'" />';
				}
			}
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_twitter_description', true )){
			$twit_desc = '
<meta name="twitter:description" content="'.get_post_meta( $post->ID, 'seo_twitter_description', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
					$twit_desc = '
<meta name="twitter:description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'" />';
				}
			}else{
				$post_data = get_post($post->ID);
				$excerpt = preg_replace( "/\r|\n\s+/", "", do_shortcode($post_data->post_content));
				if($excerpt){
					$twit_desc = '
<meta name="twitter:description" content="'.substr(trim(strip_tags(strip_shortcodes($excerpt))),0,160).'" />';
				}
			}
		}
	}
	
	$twit_site ='';
	if(tlf_get_option( 'seo_twitter_username', '' )){
		$twit_site = '
<meta name="twitter:site" content="'.tlf_get_option( 'seo_twitter_username', '' ).'"/>';
	}
	
	$domain = '
<meta name="twitter:domain" content="'.get_bloginfo( 'name' ).'" />';
			
	if(tlf_get_option( 'logo_uploader', '' )){
		$twit_image= '
<meta name="twitter:image:src" content="'.tlf_get_option( 'logo_uploader', '' ).'" />';
	}else{
		$twit_image='';
	}
	
	if(tlf_get_option( 'seo_facebook_default_image', '' )){
			$twit_image= '
<meta name="twitter:image:src" content="'.tlf_get_option( 'seo_facebook_default_image', '' ).'" />';
	}
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_front_image', '' )){
			$twit_image= '
<meta name="twitter:image:src" content="'.tlf_get_option( 'seo_facebook_front_image', '' ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
					$twit_image = '
<meta name="twitter:image:src" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
				}
			}
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_twitter_image', true )){
			$twit_image= '
<meta name="twitter:image:src" content="'.get_post_meta( $post->ID, 'seo_twitter_image', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
					$twit_image = '
<meta name="twitter:image:src" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
				}
			}else{
				if(has_post_thumbnail( $post->ID )){
					$twit_image= '
<meta name="twitter:image:src" content="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID) ).'" />';
				}else{
					$post_data = get_post($post->ID); 
					$excerpt = strip_shortcodes($post_data->post_content);
					if($excerpt){
						$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $excerpt, $matches);
						if(isset($matches[1][0]) && $matches[1][0]){
							$first_img = $matches[1][0];
						}else{
							$first_img = false;
						}
						if($first_img){
							$twit_image = '
<meta name="twitter:image:src" content="'.$first_img.'" />';
						}
					}
				}
			}
		}
	}
	
	$creator = '';
			
	if(is_single() || is_page()){
		if(tlf_get_option('seo_twitter_username')){
			$creator = '
<meta name="twitter:creator" content="'.tlf_get_option('seo_twitter_username').'" />';
		}
	}
			
	$twitter = $card.$twit_title.$twit_desc.$twit_site.$domain.$twit_image.$creator;
	$seo .= $twitter;
	
	$googlep_title = '
<meta itemprop="name" content="'.seo_title().'" />';
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_title', '' )){
			$googlep_title = '
<meta itemprop="name" content="'.tlf_get_option( 'seo_facebook_title', '' ).'" />';
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_google_title', true )){
			$googlep_title = '
<meta itemprop="name" content="'.get_post_meta( $post->ID, 'seo_google_title', true ).'" />';
		}
	}
	
	$googlep_desc = '
<meta itemprop="description" content="'.$tagline.'" />';
			
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_description', '' )){
			$googlep_desc = '
<meta itemprop="description" content="'.tlf_get_option( 'seo_facebook_description', '' ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
					$googlep_desc = '
<meta itemprop="description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'" />';
				}
			}
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_google_description', true )){
			$googlep_desc = '
<meta itemprop="description" content="'.get_post_meta( $post->ID, 'seo_google_description', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_content', true )){
					$googlep_desc = '
<meta itemprop="description" content="'.substr(trim(strip_tags(strip_shortcodes(do_shortcode(get_post_meta( $post->ID, 'tlpb_seo_content', true))))),0,160).'" />';
				}
			}else{
				$post_data = get_post($post->ID);
				$excerpt = preg_replace( "/\r|\n\s+/", "", do_shortcode($post_data->post_content));
				if($excerpt){
					$googlep_desc = '
<meta itemprop="description" content="'.substr(trim(strip_tags(strip_shortcodes($excerpt))),0,160).'" />';
				}
			}
		}
	}
	
	if(tlf_get_option( 'logo_uploader', '' )){
		$googlep_image= '
<meta itemprop="image" content="'.tlf_get_option( 'logo_uploader', '' ).'" />';
	}else{
		$googlep_image='';
	}
	
	if(tlf_get_option( 'seo_facebook_default_image', '' )){
			$googlep_image= '
<meta itemprop="image" content="'.tlf_get_option( 'seo_facebook_default_image', '' ).'" />';
	}
	
	if(is_home() || is_front_page()){
		if(tlf_get_option( 'seo_facebook_front_image', '' )){
			$googlep_image= '
<meta itemprop="image" content="'.tlf_get_option( 'seo_facebook_front_image', '' ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
					$googlep_image = '
<meta itemprop="image" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
				}
			}
		}
	}
	
	if(is_single() || is_page()){
		if(get_post_meta( $post->ID, 'seo_google_image', true )){
			$googlep_image= '
<meta itemprop="image" content="'.get_post_meta( $post->ID, 'seo_google_image', true ).'" />';
		}else{
			if(get_post_meta( $post->ID, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $post->ID, 'tlpb_seo_image', true )){
					$googlep_image = '
<meta itemprop="image" content="'.get_post_meta( $post->ID, 'tlpb_seo_image', true ).'" />';
				}
			}else{
				if(has_post_thumbnail( $post->ID )){
					$googlep_image= '
<meta itemprop="image" content="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID) ).'" />';
				}else{
					$post_data = get_post($post->ID); 
					$excerpt = strip_shortcodes($post_data->post_content);
					if($excerpt){
						$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $excerpt, $matches);
						if(isset($matches[1][0]) && $matches[1][0]){
							$first_img = $matches[1][0];
						}else{
							$first_img = false;
						}
						if($first_img){
							$googlep_image = '
<meta itemprop="image" content="'.$first_img.'" />';
						}
					}
				}
			}
		}
	}
	
	$google_plus = $googlep_title.$googlep_desc.$googlep_image;
	$seo .= $google_plus;
	
	if(tlf_get_option( 'seo_google_publisher_page', '' )){
			$seo .= '
<link rel="publisher" href="'.tlf_get_option( 'seo_google_publisher_page', '' ).'" />';
	}
	
	if(is_home() || is_front_page()){
		$site_url=site_url();
		$site=str_replace('/','\/',$site_url);
		$site_name = get_bloginfo ( 'name' );
		$seo .='
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","url":"'.$site.'","name":"'.$site_name.'","potentialAction":{"@type":"SearchAction","target":"'.$site.'?s={search_term_string}","query-input":"required name=search_term_string"}}</script>';
	}
	$seo .= "
<!-- / TL-SEO ENDS / -->
";
	echo $seo;
}
add_filter('wp_head','seo_description',1);