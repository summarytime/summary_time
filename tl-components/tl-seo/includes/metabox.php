<?php
	$seo_check = array();
	add_action( 'add_meta_boxes', 'tl_seo_metabox_add',3 );
	function tl_seo_metabox_add()
	{
			$screens = get_post_types( '', 'names' );			
			foreach ( $screens as $screen ) {
				add_meta_box( 'tl_seo_metabox', 'Themelines SEO Options', 'tl_seo_metabox_fun', $screen, 'normal', 'high' );
			}
			
			wp_enqueue_style( 'seo-style', get_template_directory_uri(). '/tl-seo/admin/assets/css/tl-seo.css' );
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_style('thickbox');
	}
	
	function tl_seo_metabox_fun()
	{
		global $post;
		if(isset($_GET['post']) && $_GET['post']){
			$id = $_GET['post'];
		}else{
			$id = -1;
		}
		$values = get_post_custom( $id );
		$title = isset( $values['seo_title'][0] ) && $values['seo_title'][0] ? $values['seo_title'][0] : '';
		$title_character = isset( $values['title_character'][0] ) && $values['title_character'][0] ? $values['title_character'][0] : '';
		$description = isset( $values['seo_description'][0] ) && $values['seo_description'][0] ? $values['seo_description'][0] : '';
		$description_character = isset( $values['description_character'][0] ) && $values['description_character'][0] ? $values['description_character'][0] : '';
		if(!$description){
			if(get_post_meta( $id, 'tlpb_active', true )=='yes'){
				if(get_post_meta( $id, 'tlpb_seo_content', true )){
					$desc = substr(trim(strip_tags(get_post_meta( $id, 'tlpb_seo_content', true ))),0,160);
				}
			}else{
				$post_data = get_post($id); 
				if(isset($post_data->post_content) && $post_data->post_content){
					$excerpt = preg_replace( "/\r|\n\s+/", "", do_shortcode($post_data->post_content));
				}else{
					$excerpt = '';
				}
				if($excerpt){
					$desc = substr(trim(strip_tags(strip_shortcodes($excerpt))),0,160);
				}else{
					$desc = get_bloginfo ( 'description' );
				}
			}
		}else{
			$desc = $description;
		}
		$keywords = isset( $values['seo_keywords'][0] ) && $values['seo_keywords'][0] ? $values['seo_keywords'][0] : '';
		$meta_robots_index = isset( $values['seo_meta_robots_index'][0] ) && $values['seo_meta_robots_index'][0] ? $values['seo_meta_robots_index'][0] : '';
		$meta_robots_follow = isset( $values['seo_meta_robots_follow'][0] ) && $values['seo_meta_robots_follow'][0] ? $values['seo_meta_robots_follow'][0] : '';
		$meta_robots_advanced = isset( $values['seo_meta_robots_advanced'][0] ) && $values['seo_meta_robots_advanced'][0] ? $values['seo_meta_robots_advanced'][0] : '';
		$canonical_url = isset( $values['seo_canonical_url'][0] ) && $values['seo_canonical_url'][0] ? $values['seo_canonical_url'][0] : '';
		$facebook_title = isset( $values['seo_facebook_title'][0] ) && $values['seo_facebook_title'][0] ? $values['seo_facebook_title'][0] : '';
		$facebook_description = isset( $values['seo_facebook_description'][0] ) && $values['seo_facebook_description'][0] ? $values['seo_facebook_description'][0] : '';
		$facebook_image = isset( $values['seo_facebook_image'][0] ) && $values['seo_facebook_image'][0] ? $values['seo_facebook_image'][0] : '';
		$twitter_title = isset( $values['seo_twitter_title'][0] ) && $values['seo_twitter_title'][0] ? $values['seo_twitter_title'][0] : '';
		$twitter_description = isset( $values['seo_twitter_description'][0] ) && $values['seo_twitter_description'][0] ? $values['seo_twitter_description'][0] : '';
		$twitter_image = isset( $values['seo_twitter_image'][0] ) && $values['seo_twitter_image'][0] ? $values['seo_twitter_image'][0] : '';
		$google_title = isset( $values['seo_google_title'][0] ) && $values['seo_google_title'][0] ? $values['seo_google_title'][0] : '';
		$google_description = isset( $values['seo_google_description'][0] ) && $values['seo_google_description'][0] ? $values['seo_google_description'][0] : '';
		$google_image = isset( $values['seo_google_image'][0] ) && $values['seo_google_image'][0] ? $values['seo_google_image'][0] : '';
		
		wp_nonce_field( 'seo_metabox_nonce', 'metabox_nonce' );
?>                                 
             <div id="tl-seo-navigation">

                <h2 class="nav-tab-wrapper current">
                    <a class="seo-tab nav-tab nav-tab-active" rel="general">General</a>
                    <a class="seo-tab nav-tab" rel="advanced">Advanced</a>
                    <a class="seo-tab nav-tab" rel="social">Social</a>
                </h2>
                
                <div class="tl-seo-content">
                	<div class="general">
                	<table class="form-table tl-seo-table">
                    	<tr>
                        	<th>Snipnet Preview</th>
                            <td> 
                            	<div class="tlseosnippet">
                                    <div id="snipnet">
                                    	<?php 
										$sitename=get_bloginfo( 'name' );
                                        $seperator=(tlf_get_option( 'seo_title_seperator', '' ))?tlf_get_option( 'seo_title_seperator', '' ):'|';
										$pageTitle=(get_post_meta( $id, 'seo_title', true ))?get_post_meta( $id, 'seo_title', true ):get_the_title($id);
										?>
                                    	<a id="tlseosnippet_title" class="title" href="#"><?php echo (get_post_meta( $id, 'seo_title', true ))?get_post_meta( $id, 'seo_title', true ):$pageTitle.' '.$seperator.' '.$sitename;?></a>
                                    </div>
                                    <span class="url"><?php echo ($id)?get_permalink($id):site_url();?></span>
                                   	<?php if(!isset($desc) || !$desc):?>
                                    <?php if(!$id):?>
                                    <p>This is a rendering of what this post might look like in Google's search results.</p>
                                    <?php else:?>
                                    <?php 
									?>
                                    <p class="description"><?php echo $description;?></p>
                                    <?php
                                    endif;?>
                                    <?php else:?>
                                    <p class="description"><?php echo $desc;?></p>
                                    <?php endif;?>
                                    
                                </div>
                            </td>
                        </tr>
                        <tr>
                        	<th>SEO Title</th>
                            <td>
                            	<input type="text" name="seo_title" id="seo_title" class="seo-input" value="<?php if(!empty($title)) echo $title;?>" placeholder="<?php echo (get_post_meta( $id, 'seo_title', true ))?get_post_meta( $id, 'seo_title', true ):$pageTitle.' '.$seperator.' '.$sitename;?>" />
                                <p>The SEO title defaults to what is generated based on this sites title template for this posttype.</p>
                                <h5><?php echo seo_character_check($title_character, 55, 'titleCount');?></h5>
                                <input type="hidden" name="title_character" value="<?php echo $title_character;?>" />
                            </td>
                        </tr>
                        <tr>
                        	<th>Meta Description</th>
                            <td>
                            	<textarea name="seo_description" id="seo_description" placeholder="<?php echo isset($desc) && $desc?$desc:$description;?>"><?php echo $description;?></textarea>
                                <p>The meta description is often shown as the black text under the title in a search result. For this to work it has to contain the keyword that was searched for.</p>
                                <h5><?php echo seo_character_check($description_character, 160, 'charCount');?></h5>
                                <input type="hidden" name="description_character" value="<?php echo $description_character;?>" />
                            </td>
                        </tr>
                        <tr>
                        	<th>Meta Keywords</th>
                            <td>
                            	<input type="text" name="seo_keywords" id="seo_keywords" class="seo-input" value="<?php echo $keywords;?>">
                                <p>Meta Keywords are a specific type of meta tag that appear in the HTML code of a Web page and help tell search engines what the topic of the page is. Keywords should be comma seperated.</p>
                            </td>
                        </tr>
                        
                     </table>
				</div>
                
                    <div class="advanced">
                        <table class="form-table tl-seo-table">
                            <tr>
                                <th>Meta Robots Index</th>
                                <td>
                                    <select name="seo_meta_robots_index" id="seo_meta_robots_index"/>
                                        <option value="0">Default for post type, currently: index</option>
                                        <option value="2" <?php if($meta_robots_index=='2') echo 'selected="selected"';?>>index</option>
                                        <option value="1" <?php if($meta_robots_index=='1') echo 'selected="selected"';?>>noindex</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Meta Robots Follow</th>
                                <td>
                                    <label>Follow</label>
                                    <input name="seo_meta_robots_follow" id="seo_meta_robots_follow" type="radio" value="follow" <?php if($meta_robots_follow=='follow' || $meta_robots_follow=='') echo 'checked="checked"';?>/>
                                    <label>No Follow</label>
                                    <input name="seo_meta_robots_follow" id="seo_meta_robots_follow" type="radio" value="nofollow" <?php if($meta_robots_follow=='nofollow') echo 'checked="checked"';?>>
                                </td>
                            </tr>
                            <tr>
                                <th>Meta Robots Advanced</th>
                                <td>
                                    <select id="seo_meta_robots_advanced" name="seo_meta_robots_advanced" style="height: 144px;" size="7" multiple="multiple">
                                        <option value="-" <?php if($meta_robots_advanced=='-') echo 'selected="selected"';?>>Site-wide default: None</option>
                                        <option value="none" <?php if($meta_robots_advanced=='none') echo 'selected="selected"';?>>None</option>
                                        <option value="noodp" <?php if($meta_robots_advanced=='noodp') echo 'selected="selected"';?>>NO ODP</option>
                                        <option value="noydir" <?php if($meta_robots_advanced=='noydir') echo 'selected="selected"';?>>NO YDIR</option>
                                        <option value="noimageindex" <?php if($meta_robots_advanced=='noimageindex') echo 'selected="selected"';?>>No Image Index</option>
                                        <option value="noarchive" <?php if($meta_robots_advanced=='noarchive') echo 'selected="selected"';?>>No Archive</option>
                                        <option value="nosnippet" <?php if($meta_robots_advanced=='nosnippet') echo 'selected="selected"';?>>No Snippet</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Canonical URL</th>
                                <td>
                                    <input type="text" name="seo_canonical_url" id="seo_canonical_url" class="seo-input" value="<?php echo $canonical_url;?>">
                                    <p>The canonical URL that this page should point to, leave empty to default to permalink. Cross domain canonical supported too.</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="social" style="display:none;">
                        <table class="form-table tl-seo-table">
                            <tr>
                                <th>Facebook Title</th>
                                <td>
                                    <input type="text" name="seo_facebook_title" id="seo_facebook_title" class="seo-input" value="<?php echo $facebook_title;?>">
                                    <p>If you don't want to use the post title for sharing the post on Facebook but instead want another title there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Facebook Description</th>
                                <td>
                                    <textarea name="seo_facebook_description" id="seo_facebook_description" ><?php echo $facebook_description;?></textarea>
                                    <p>If you don't want to use the meta description for sharing the post on Facebook but want another description there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Facebook Image</th>
                                <td>
                                    <input type="text" name="seo_facebook_image" id="seo_facebook_image" value="<?php echo $facebook_image;?>" />
                                    <input id="upload_facebook_image_button" class="button button-primary button-large" type="button" value="Upload">
                                    <p>If you want to override the image used on Facebook for this post, upload / choose an image or add the URL here. The recommended image size for Facebook is 1200 x 628px.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Twitter Title</th>
                                <td>
                                    <input type="text" name="seo_twitter_title" id="seo_twitter_title" class="seo-input" value="<?php echo $twitter_title;?>">
                                    <p>If you don't want to use the post title for sharing the post on Twitter but instead want another title there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Twitter Description</th>
                                <td>
                                    <textarea name="seo_twitter_description" id="seo_twitter_description" ><?php echo $twitter_description;?></textarea>
                                    <p>If you don't want to use the meta description for sharing the post on Twitter but want another description there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Twitter Image</th>
                                <td>
                                    <input type="text" name="seo_twitter_image" id="seo_twitter_image" value="<?php echo $twitter_image;?>" />
                                    <input id="upload_twitter_image_button" class="button button-primary button-large" type="button" value="Upload">
                                    <p>If you want to override the image used on Twitter for this post, upload / choose an image or add the URL here. The recommended image size for Twitter is 1024 x 512px.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Google+ Title</th>
                                <td>
                                    <input type="text" name="seo_google_title" id="seo_google_title" class="seo-input" value="<?php echo $google_title;?>">
                                    <p>If you don't want to use the post title for sharing the post on Google+ but instead want another title there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Google+ Description</th>
                                <td>
                                    <textarea name="seo_google_description" id="seo_google_description" ><?php echo $google_description;?></textarea>
                                    <p>If you don't want to use the meta description for sharing the post on Google+ but want another description there, write it here.</p>
                                </td>
                            </tr>
                            <tr>
                                <th>Google+ Image</th>
                                <td>
                                    <input type="text" name="seo_google_image" id="seo_google_image" value="<?php echo $google_image;?>" />
                                    <input id="upload_google_image_button" class="button button-primary button-large" type="button" value="Upload">
                                    <p>If you want to override the image used on Google+ for this post, upload / choose an image or add the URL here. The recommended image size for Google+ is 800 x 1200px.</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
              </div>
        
        <script>
		jQuery( '#title' ).keyup(function() {
			jQuery('#seo_title').attr("placeholder", jQuery('#title').val()+'<?php echo (get_post_meta( $id, 'seo_title', true ))?get_post_meta( $id, 'seo_title', true ):' '.html_entity_decode($seperator).' '.$sitename;?>');
			var snip = jQuery('#seo_title').attr('placeholder');
			jQuery('#snipnet').html('<a id="tlseosnippet_title" class="title" href="#">'+snip+'</a>');
			
		});
		jQuery( '#seo_title' ).keyup(function() {
			var site = '<?php echo get_bloginfo( 'name' ); ?>';
			if(jQuery('#seo_title').val()){
				jQuery('#snipnet').html('<a id="tlseosnippet_title" class="title" href="#">'+jQuery('#seo_title').val()+'</a>');
			}else{
				jQuery('#seo_title').attr("placeholder", jQuery('#title').val()+'<?php echo (get_post_meta( $id, 'seo_title', true ))?get_post_meta( $id, 'seo_title', true ):' '.html_entity_decode($seperator).' '.$sitename;?>');
				var snip = jQuery('#seo_title').attr('placeholder');
				jQuery('#snipnet').html('<a id="tlseosnippet_title" class="title" href="#">'+snip+'</a>');
			}
			
		});
		
		jQuery( '#seo_description' ).keyup(function() {
			if(jQuery(this).val()){
				jQuery('.description').text(jQuery(this).val());
			}else{
				jQuery('.description').html('<?php echo (isset($desc) && $desc)?trim(strip_tags($desc)):trim(strip_tags($description));?>');
			}
		});
		
		jQuery('.seo-tab').on('click', function(event){
		   event.preventDefault();
		   var target = jQuery(this).attr('rel');
		   jQuery(this).addClass('nav-tab-active').siblings('a').removeClass('nav-tab-active');
		   jQuery("."+target).show().siblings("div").hide();
		});
		
		
		jQuery(document).ready(function(){
		
		
			jQuery('#upload_facebook_image_button').click(function(e){
				var $this = e,
				frame = frame || {}, props, image;
				if ('function' === typeof frame.open) {
					frame.open();
					return;
				}
				// Create the media frame.
				frame = wp.media.frames.frame = wp.media({
					title: 'Select Facebook Image',
					className: 'media-frame tl-pb-builder-uploader',
					button: {
						text: 'Insert Facebook Image'
					},
					multiple: false
				});
				// When an image is selected, run a callback.
				frame.on('select', function () {
					// We set multiple to false so only get one image from the uploader
					var attachment = frame.state().get('selection').first().toJSON();
					// Remove the attachment caption
					attachment.caption = '';
					// Build the image
					props = wp.media.string.props(
						{},
						attachment
					);
					jQuery('#seo_facebook_image').val(attachment.url);
				});
				frame.open();
			});
			
				
			jQuery('#upload_twitter_image_button').click(function(e){
				var $this = e,
				frame = frame || {}, props, image;
				if ('function' === typeof frame.open) {
					frame.open();
					return;
				}
				// Create the media frame.
				frame = wp.media.frames.frame = wp.media({
					title: 'Select Twitter Image',
					className: 'media-frame tl-pb-builder-uploader',
					button: {
						text: 'Insert Twitter Image'
					},
					multiple: false
				});
				// When an image is selected, run a callback.
				frame.on('select', function () {
					// We set multiple to false so only get one image from the uploader
					var attachment = frame.state().get('selection').first().toJSON();
					// Remove the attachment caption
					attachment.caption = '';
					// Build the image
					props = wp.media.string.props(
						{},
						attachment
					);
					jQuery('#seo_twitter_image').val(attachment.url);
				});
				frame.open();
			
			});
		
			jQuery('#upload_google_image_button').click(function(e){
				var $this = e,
				frame = frame || {}, props, image;
				if ('function' === typeof frame.open) {
					frame.open();
					return;
				}
				// Create the media frame.
				frame = wp.media.frames.frame = wp.media({
					title: 'Select Google+ Image',
					className: 'media-frame tl-pb-builder-uploader',
					button: {
						text: 'Insert Google+ Image'
					},
					multiple: false
				});
				// When an image is selected, run a callback.
				frame.on('select', function () {
					// We set multiple to false so only get one image from the uploader
					var attachment = frame.state().get('selection').first().toJSON();
					// Remove the attachment caption
					attachment.caption = '';
					// Build the image
					props = wp.media.string.props(
						{},
						attachment
					);
					jQuery('#seo_google_image').val(attachment.url);
				});
				frame.open();
			});
			
		});

(function($) {
	var descCounter = {
		init: function() {
			this.DOM();
			this.events();
		},
		DOM: function() {
			this.textbox = $("#seo_description");
			this.charCount = $("#charCount");
		},
		events: function() {
			this.textbox.on("input", this.count.bind(this));
		},
		count: function() {
			var chars = this.textbox.val();
			$('[name="description_character"]').val(chars.length);
			//COUNT CHARACTERS
			if (chars.length <= 0) {
				this.charCount.text(chars.length + " Character (Recommended 160 Characters)");
				$("#charCount").removeAttr('style');
			} else if (chars.length === 1) {
				this.charCount.text(chars.length + " Character (Recommended 160 Characters)");
				$("#charCount").css('color','#f0ad4e');
			} else if (chars.length < 160) {
				this.charCount.text(chars.length + " Characters (Recommended 160 Characters)");
				$("#charCount").css('color','#f0ad4e');
			} else if (chars.length === 160) {
				this.charCount.text(chars.length + " Characters (Recommended 160 Characters)");
				$("#charCount").css('color','#5cb85c');
			} else if (chars.length > 160){
				this.charCount.text(chars.length + " Characters (Recommended 160 Characters)");
				$("#charCount").css('color','#d9534f');
			} else if (chars.length === 0){
				this.charCount.text(chars.length + " Character (Recommended 160 Characters)");
				$("#charCount").removeAttr('style');
			}
		}
	}
	descCounter.init();
}(jQuery));

(function($) {
	var titleCounter = {
		init: function() {
			this.DOM();
			this.events();
		},
		DOM: function() {
			this.textbox = $("#seo_title");
			this.charCount = $("#titleCount");
		},
		events: function() {
			this.textbox.on("input", this.count.bind(this));
		},
		count: function() {
			var chars = this.textbox.val();
			$('[name="title_character"]').val(chars.length);
			//COUNT CHARACTERS
			if (chars.length <= 0) {
				this.charCount.text(chars.length + " Character (Recommended 55 Characters)");
				$("#titleCount").removeAttr('style');
			} else if (chars.length === 1) {
				this.charCount.text(chars.length + " Character (Recommended 55 Characters)");
				$("#titleCount").css('color','#f0ad4e');
			} else if (chars.length < 55) {
				this.charCount.text(chars.length + " Characters (Recommended 55 Characters)");
				$("#titleCount").css('color','#f0ad4e');
			} else if (chars.length === 55) {
				this.charCount.text(chars.length + " Characters (Recommended 55 Characters)");
				$("#titleCount").css('color','#5cb85c');
			} else if (chars.length > 55){
				this.charCount.text(chars.length + " Characters (Recommended 55 Characters)");
				$("#titleCount").css('color','#d9534f');
			} else if (chars.length === 0){
				this.charCount.text(chars.length + " Character (Recommended 55 Characters)");
				$("#titleCount").removeAttr('style');
			}
		}
	}
	titleCounter.init();
}(jQuery));	
</script>	
		
<?php    
	}
	
	
	add_action( 'save_post', 'seo_metabox_save' );
	function seo_metabox_save( $post_id )
	{
		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	
		if( !isset( $_POST['metabox_nonce'] ) || !wp_verify_nonce( $_POST['metabox_nonce'], 'seo_metabox_nonce' ) ) return;
		 
		if( !current_user_can( 'edit_post' ) ) return;
		 
		$allowed = array( 
			'a' => array(
				'href' => array()
			)
		);
		 
		if( isset( $_POST['seo_title'] ) ){
			update_post_meta( $post_id, 'seo_title', wp_kses( $_POST['seo_title'], $allowed ) );
		}
		
		if( isset( $_POST['seo_description'] ) ){
			update_post_meta( $post_id, 'seo_description', wp_kses( $_POST['seo_description'], $allowed ) );
		}
		
		if( isset( $_POST['seo_keywords'] ) ){
			update_post_meta( $post_id, 'seo_keywords', wp_kses( $_POST['seo_keywords'], $allowed ) );
		}
		
		if( isset( $_POST['seo_meta_robots_index'] ) ){
			update_post_meta( $post_id, 'seo_meta_robots_index', wp_kses( $_POST['seo_meta_robots_index'], $allowed ) );
		}
		
		if( isset( $_POST['seo_meta_robots_follow'] ) ){
			update_post_meta( $post_id, 'seo_meta_robots_follow', wp_kses( $_POST['seo_meta_robots_follow'], $allowed ) );
		}
		
		if( isset( $_POST['seo_meta_robots_advanced'] ) ){
			update_post_meta( $post_id, 'seo_meta_robots_advanced', wp_kses( $_POST['seo_meta_robots_advanced'], $allowed ) );
		}
		
		if( isset( $_POST['seo_canonical_url'] ) ){
			update_post_meta( $post_id, 'seo_canonical_url', wp_kses( $_POST['seo_canonical_url'], $allowed ) );
		}	
		
		if( isset( $_POST['seo_facebook_title'] ) ){
			update_post_meta( $post_id, 'seo_facebook_title', wp_kses( $_POST['seo_facebook_title'], $allowed ) );
		}
		
		if( isset( $_POST['seo_facebook_description'] ) ){
			update_post_meta( $post_id, 'seo_facebook_description', wp_kses( $_POST['seo_facebook_description'], $allowed ) );
		}
		
		if( isset( $_POST['seo_facebook_image'] ) ){
			update_post_meta( $post_id, 'seo_facebook_image', wp_kses( $_POST['seo_facebook_image'], $allowed ) );
		}	
		
		if( isset( $_POST['seo_twitter_title'] ) ){
			update_post_meta( $post_id, 'seo_twitter_title', wp_kses( $_POST['seo_twitter_title'], $allowed ) );
		}
		
		if( isset( $_POST['seo_twitter_description'] ) ){
			update_post_meta( $post_id, 'seo_twitter_description', wp_kses( $_POST['seo_twitter_description'], $allowed ) );
		}
		
		if( isset( $_POST['seo_twitter_image'] ) ){
			update_post_meta( $post_id, 'seo_twitter_image', wp_kses( $_POST['seo_twitter_image'], $allowed ) );
		}	  
		
		if( isset( $_POST['seo_google_title'] ) ){
			update_post_meta( $post_id, 'seo_google_title', wp_kses( $_POST['seo_google_title'], $allowed ) );
		}
		
		if( isset( $_POST['seo_google_description'] ) ){
			update_post_meta( $post_id, 'seo_google_description', wp_kses( $_POST['seo_google_description'], $allowed ) );
		}
		
		if( isset( $_POST['seo_google_image'] ) ){
			update_post_meta( $post_id, 'seo_google_image', wp_kses( $_POST['seo_google_image'], $allowed ) );
		}	
		
		if( isset( $_POST['title_character'] ) ){
			update_post_meta( $post_id, 'title_character', wp_kses( $_POST['title_character'], $allowed ) );
		}
		
		if( isset( $_POST['description_character'] ) ){
			update_post_meta( $post_id, 'description_character', wp_kses( $_POST['description_character'], $allowed ) );
		}	  
	}
	
function seo_character_check($length=false, $limit, $id){
	if(!$length || $length<0){
		return '<span id="'.$id.'">0 Character (Recommended '.$limit.' Characters)</span>';
	}elseif($length == 1){
		return '<span id="'.$id.'" style="color:#f0ad4e;">'.$length.' Character (Recommended '.$limit.' Characters)</span>';
	}elseif($length < $limit){
		return '<span id="'.$id.'" style="color:#f0ad4e;">'.$length.' Character (Recommended '.$limit.' Characters)</span>';
	}elseif($length == 55){
		return '<span id="'.$id.'" style="color:#5cb85c;">'.$length.' Character (Recommended '.$limit.' Characters)</span>';
	}elseif($length > 55){
		return '<span id="'.$id.'" style="color:#d9534f;">'.$length.' Character (Recommended '.$limit.' Characters)</span>';
	}
}