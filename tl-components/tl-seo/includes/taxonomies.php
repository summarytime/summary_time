<?php 
    // A callback function to add a custom field to all taxonomies  
    // Add term page
function tl_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[seo_title]"><?php _e( 'SEO Title', 'tl' ); ?></label>
		<input type="text" name="term_meta[seo_title]" id="term_meta[seo_title]" value="">
		<p class="description"><?php _e( 'The SEO title is used on the archive page for this term.','tl' ); ?></p>
	</div>
    
    <div class="form-field">
		<label for="term_meta[seo_description]"><?php _e( 'SEO Description', 'tl' ); ?></label>
		<textarea name="term_meta[seo_description]" id="term_meta[seo_description]"></textarea>
		<p class="description"><?php _e( 'The SEO description is used for the meta description on the archive page for this term.','tl' ); ?></p>
	</div>
    
    <div class="form-field">
		<label for="term_meta[canonical]"><?php _e( 'Canonical', 'tl' ); ?></label>
		<input type="text" name="term_meta[canonical]" id="term_meta[canonical]" value="">
		<p class="description"><?php _e( 'The canonical link is shown on the archive page for this term.','tl' ); ?></p>
	</div>
    
    <div class="form-field">
		<label for="term_meta[noindex_this_category]"><?php _e( 'Noindex this category', 'tl' ); ?></label>
        <select name="term_meta[noindex_this_category]" id="term_meta[noindex_this_category]">
        	<option value="default">Use category default (Currently: index)</option>
			<option value="index">Always index</option>
			<option value="noindex">Always noindex</option>
        </select>
		<p class="description"><?php _e( 'This category follows the indexation rules set under Metas and Titles, you can override it here.','tl' ); ?></p>
	</div>
    
<?php
}



// Edit term page
function tl_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); 
	$screens = get_post_types( '', 'names' );
	$i=0;
	foreach ( $screens as $screen ) {
		$post_name[$i] = $screen;
		$i++;
	}
$taxonomy_names = get_object_taxonomies( $post_name );
$taxonomy_names = array_diff($taxonomy_names, array("nav_menu", "post_format"));
$taxonomies = get_taxonomies( array( 'public' => true ), 'names' );
//print_r($taxonomies);
	?>
    <tr class="form-field">
		<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><h3>TL SEO Settings</h3></label></th>
	</tr>
    
	<tr class="form-field">
		<th scope="row" valign="top">
    		<label for="term_meta[seo_title]"><?php _e( 'SEO Title', 'tl' ); ?></label>
        </th>
		<td>
			<input type="text" name="term_meta[seo_title]" id="term_meta[seo_title]" value="<?php echo esc_attr( $term_meta['seo_title'] ) ? esc_attr( $term_meta['seo_title'] ) : ''; ?>">
			<p class="description"><?php _e( 'The SEO title is used on the archive page for this term.','tl' ); ?></p>
		</td>
	</tr>
    
    <tr class="form-field">
		<th scope="row" valign="top">
    		<label for="term_meta[seo_description]"><?php _e( 'SEO Description', 'tl' ); ?></label>
        </th>
		<td>
			<textarea name="term_meta[seo_description]" id="term_meta[seo_description]"><?php echo esc_attr( $term_meta['seo_description'] ) ? esc_attr( $term_meta['seo_description'] ) : ''; ?></textarea>
			<p class="description"><?php _e( 'The SEO description is used for the meta description on the archive page for this term.','tl' ); ?></p>
		</td>
	</tr>
    
    <tr class="form-field">
		<th scope="row" valign="top">
    		<label for="term_meta[canonical]"><?php _e( 'Canonical', 'tl' ); ?></label>
        </th>
		<td>
			<input type="text" name="term_meta[canonical]" id="term_meta[canonical]" value="<?php echo esc_attr( $term_meta['canonical'] ) ? esc_attr( $term_meta['canonical'] ) : ''; ?>">
			<p class="description"><?php _e( 'The canonical link is shown on the archive page for this term.','tl' ); ?></p>
		</td>
	</tr>
    
    <tr class="form-field">
		<th scope="row" valign="top">
    		<label for="term_meta[noindex_this_category]"><?php _e( 'Noindex this category', 'tl' ); ?></label>
        </th>
		<td>
			<select name="term_meta[noindex_this_category]" id="term_meta[noindex_this_category]">
                <option value="default" <?php echo esc_attr( $term_meta['noindex_this_category']=='default' ) ? 'selected="selected"' : ''; ?>>Use category default (Currently: index)</option>
                <option value="index" <?php echo esc_attr( $term_meta['noindex_this_category']=='index' ) ? 'selected="selected"' : ''; ?>>Always index</option>
                <option value="noindex" <?php echo esc_attr( $term_meta['noindex_this_category']=='noindex' ) ? 'selected="selected"' : ''; ?>>Always noindex</option>
        	</select>
			<p class="description"><?php _e( 'This category follows the indexation rules set under Metas and Titles, you can override it here.','tl' ); ?></p>
		</td>
	</tr>
    
    <tr class="form-field">
		<th scope="row" valign="top">
    		<label for="term_meta[include_in_sitemap]"><?php _e( 'Include in sitemap?', 'tl' ); ?></label>
        </th>
		<td>
            <select name="term_meta[include_in_sitemap]" id="term_meta[include_in_sitemap]">
                <option value="-" <?php echo esc_attr( $term_meta['include_in_sitemap']=='-' ) ? 'selected="selected"' : ''; ?>>Auto detect</option>
                <option value="always" <?php echo esc_attr( $term_meta['include_in_sitemap']=='always' ) ? 'selected="selected"' : ''; ?>>Always include</option>
                <option value="never" <?php echo esc_attr( $term_meta['include_in_sitemap']=='never' ) ? 'selected="selected"' : ''; ?>>Never include</option>
            </select>
            <p class="description"><?php _e( '','tl' ); ?></p>

		</td>
	</tr>
    
<?php
}


// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
$screens = get_post_types( '', 'names' );
	$i=0;
	foreach ( $screens as $screen ) {
		$post_name[$i] = $screen;
		$i++;
	}
$taxonomy_names = get_object_taxonomies( $post_name );
do_action('dudu');
$taxonomies = get_taxonomies( array( 'public' => true ), 'names' );
foreach ( $taxonomies as $taxa=>$key ) {
	add_action( '$key_add_form_fields', 'tl_taxonomy_add_new_meta_field', 10, 2 );
	add_action( '$key_edit_form_fields', 'tl_taxonomy_edit_meta_field', 10, 2 );
	add_action( 'edited_$key', 'save_taxonomy_custom_meta', 10, 2 );  
	add_action( 'create_$key', 'save_taxonomy_custom_meta', 10, 2 );
}

function taxonomy_metadata_init() {
  // Require the Taxonomy Metadata plugin
  if( !function_exists('save_taxonomy_custom_meta') || !function_exists('tl_taxonomy_add_new_meta_field') || !function_exists('tl_taxonomy_edit_meta_field') ) return false;

  // Get a list of all public custom taxonomies
  $taxonomies = get_taxonomies( array( 'public' => true ), 'names' );
  // Attach additional fields onto all custom, public taxonomies
  if ( $taxonomies ) {
    foreach ( $taxonomies  as $taxonomy ) {
      // Add fields to "add" and "edit" term pages
      add_action("{$taxonomy}_add_form_fields", 'tl_taxonomy_add_new_meta_field', 10, 1);
      add_action("{$taxonomy}_edit_form_fields", 'tl_taxonomy_edit_meta_field', 10, 1);
      // Process and save the data
      add_action("created_{$taxonomy}", 'save_taxonomy_custom_meta', 10, 1);
      add_action("edited_{$taxonomy}", 'save_taxonomy_custom_meta', 10, 1);
    }
  }
}
add_action('admin_init', 'taxonomy_metadata_init');