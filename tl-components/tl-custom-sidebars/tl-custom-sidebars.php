<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class TL_Custom_Sidebars {
	protected static $_instance = null;
	public $version = '1.0.0';
	public $stored;
	public $sidebars = array();
	protected $title;

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	function __construct() {

		$this->title  = __( 'Themelines Custom Widget Area', 'tl' );
		$this->stored = 'tl_custom_sidebars';
		
		add_action( 'admin_footer', array( &$this, 'template_custom_widget_area' ), 200 );
		add_action( 'load-widgets.php', array( &$this, 'load_scripts_styles' ) , 5 );

		add_action( 'widgets_init', array( &$this, 'register_custom_sidebars' ), 1000 );
		add_action( 'wp_ajax_tl_ajax_delete_custom_sidebar', array( &$this, 'delete_sidebar_area' ) , 1000 );

		add_shortcode( 'tl_sidebar', array( &$this, 'tl_sidebar_shortcode' ) );

		add_filter( 'wie_unencoded_export_data', array( &$this, 'export_data' ) );
		add_filter( 'wie_import_results', array( &$this, 'reset_custom_key' ) );
		add_action( 'wie_import_data', array( &$this, 'before_wie_import' ) );

		add_action( 'customize_controls_print_scripts', array( &$this, 'customize_controls_print_scripts' ) );
	}

	public function load_scripts_styles() {

		global $wp_version;

		add_action( 'load-widgets.php', array( $this, 'add_sidebar_area' ), 100 );

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'tl-custom-sidebars', get_template_directory_uri() . '/tl-custom-sidebars/assets/js/tl-custom-sidebars.js', array( 'jquery' ), $this->version, true );

		wp_localize_script( 'tl-custom-sidebars', 'objectL10n', array(
			'shortcode'           => __( 'Shortcode', 'tl' ),
			'delete_sidebar_area' => __( 'Are you sure you want to delete this sidebar?', 'tl' )
			) );
			
		wp_enqueue_style( 'tl-custom-sidebars', get_template_directory_uri() .  '/tl-custom-sidebars/assets/css/tl-custom-sidebars.css', '', $this->version, 'screen' );	

		if ( true === version_compare( $wp_version, '3.7.9', '>' ) ) {
			wp_enqueue_style( 'tl-custom-sidebars-wp38plus', get_template_directory_uri() .  '/tl-custom-sidebars/assets/css/tl-custom-sidebars-wp38plus.css', '', $this->version, 'screen' );
		} else {
		wp_enqueue_style( 'tl-custom-sidebars', get_template_directory_uri() .  '/tl-custom-sidebars/assets/css/tl-custom-sidebars.css', '', $this->version, 'screen' );
		}
	}

	public function template_custom_widget_area() {
		global $wp_version;
		?>
		<script type="text/html" id="tmpl-tl-add-widget">
			<div class="tl-widgets-holder-wrap">
							

			<form class="tl-add-widget" method="post">
				
				<div class="sidebar-name">
					<h3><?php echo $this->title ?></h3>
				</div>
			
			<input type="text" name="tl-add-widget" value="" placeholder="<?php _e( 'Enter name of the new widget area here', 'tl' ); ?>" required />
			<input type="checkbox" name="advance_settings" id="advance_settings"> <?php _e( 'Open advance settings', 'tl' ); ?>
			<div class="advance_settings_area">
				<label>Before Widget</label>
				<input type="text" name="tl-before-widget" value="" placeholder="<?php _e( '<aside id=%s class=%s>', 'tl' ); ?>" />
				<label>After Widget</label>
				<input type="text" name="tl-after-widget" value="" placeholder="<?php _e( '</aside>', 'tl' ); ?>" />
				<label>Before Title</label>
				<input type="text" name="tl-before-title" value="" placeholder="<?php _e( '<h3 class=%s>', 'tl' ); ?>" />
				<label>After Title</label>
				<input type="text" name="tl-after-title" value="" placeholder="<?php _e( '</h3>', 'tl' ); ?>" />
			</div>
			<?php submit_button( __( 'Add Widget Area', 'tl' ), 'secondary large', $name = 'tl-custom-sidebar-submit' ); ?>
			<input type='hidden' name='tlcs-delete-nonce' value="<?php echo wp_create_nonce( 'tlcs-delete-nonce' ) ?>">
		</form>
	</div>
</script>
<?php
}

	public function add_sidebar_area() {
		if ( ! empty( $_POST['tl-add-widget'] ) ) {
			$this->sidebars = get_option( $this->stored );
			$name           = $this->get_name( $_POST['tl-add-widget'] );
			$this->sidebars[ sanitize_title_with_dashes( $name ) ] = $name;
			update_option( $this->stored, $this->sidebars );
			if(!empty($_POST['tl-before-widget'])){
				update_option( sanitize_title_with_dashes( $name ).'_before_widget',$_POST['tl-before-widget']);
			}
			if(!empty($_POST['tl-after-widget'])){
				update_option( sanitize_title_with_dashes( $name ).'_after_widget',$_POST['tl-after-widget']);
			}
			if(!empty($_POST['tl-before-title'])){
				update_option( sanitize_title_with_dashes( $name ).'_before_title',$_POST['tl-before-title']);
			}
			if(!empty($_POST['tl-after-title'])){
				update_option( sanitize_title_with_dashes( $name ).'_after_title',$_POST['tl-after-title']);
			}
			wp_redirect( admin_url( 'widgets.php' ) );
			die();
		}
	}

	public function delete_sidebar_area() {
		check_ajax_referer( 'tlcs-delete-nonce' );
		if ( ! empty( $_POST['name'] ) ) {
			$name           = sanitize_title_with_dashes( stripslashes( $_POST['name'] ) );
			$this->sidebars = get_option( $this->stored );

			if ( array_key_exists( $name, $this->sidebars ) ) {
				unset( $this->sidebars[ $name ] );
				update_option( $this->stored, $this->sidebars );
				unregister_sidebar( $name );
				echo 'sidebar-deleted';
			}
		}
		die();
	}

	public function get_name( $name ) {
		if ( empty( $GLOBALS['wp_registered_sidebars'] ) ) {
			return $name;
		}

		$taken = array();

		foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
			$taken[] = $sidebar['name'];
		}

		if ( empty($this->sidebars) ) $this->sidebars = array();
		$taken = array_merge( $taken, $this->sidebars );

		if ( in_array( $name, $taken ) ) {
			$counter  = substr( $name, -1 );
			$new_name = '';

			if ( ! is_numeric( $counter ) ) {
				$new_name = $name . ' 1';
			} else {
				$new_name = substr( $name, 0, -1 ) . ( (int) $counter + 1 );
			}

			$name = $this->get_name( $new_name );
		}

		return $name;
	}

	/**
	 * Register sidebars.
	 *
	 * @access public
	 * @return void
	 */
	public function register_custom_sidebars() {

		$sidebars = get_option( $this->stored );
		
		/*$args = apply_filters( 'tl_custom_sidebars_widget_args', array(
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widgettitle">',
			'after_title'   => '</h3>',
			)
		);*/
		
		if ( is_array( $sidebars ) ) {
			foreach ( $sidebars as $sidebar ) {
				
				$name = $sidebar;

				$sidebar = sanitize_title_with_dashes( $sidebar );
				
				$args = apply_filters( 'tl_custom_sidebars_widget_args', array(
					'name' => $name,
					'before_widget' => get_option($sidebar.'_before_widget')?get_option($sidebar.'_before_widget'):'<aside id="'.stripslashes('.%1$s.').'" class="widget %2$s">',
					'after_widget'  => get_option($sidebar.'_after_widget')?get_option($sidebar.'_after_widget'):'</aside>',
					'before_title'  => get_option($sidebar.'_before_title')?get_option($sidebar.'_before_title'):'<h3 class="widgettitle">',
					'after_title'   => get_option($sidebar.'_after_title')?get_option($sidebar.'_after_title'):'</h3>',
					)
				);

				
				$args['id']    = $sidebar;
				$args['class'] = 'tl-custom';
				
				foreach ($args as $key=>$value) {
					$args[$key] = stripslashes($value);
				}

				register_sidebar( apply_filters( 'scs_widget_args_' . $sidebar,$args ) );
			}
		}
	}

	public function tl_sidebar_shortcode( $atts ) {
		$atts = shortcode_atts( array(
			'id' => '1',
			'class' => '',
		), $atts );

		$output = '';

		if ( is_active_sidebar( $atts['id'] ) ) {
			ob_start();

			do_action( 'tl_custom_sidebars_before', $atts['id'] );

			echo "<section id='{$atts['id']}' class='tl-custom-widget-area {$atts['class']}'>";
			dynamic_sidebar( $atts['id'] );
			echo '</section>';

			do_action( 'tl_custom_sidebars_after' );

			$output = ob_get_clean();
		}

		return $output;
	}

	public function export_data( $sidebars ) {

		if ( empty( $this->sidebars ) ) $this->sidebars = get_option( $this->stored );

		$sidebars['tl-custom-sidebars-areas'] = $this->sidebars;

		return $sidebars;
	}

	/**
	 * Delete custom array key before 'Widget Importer & Exporter' import.
	 *
	 * @uses Widget_Importer_Exporter
	 * @link https://wordpress.org/plugins/widget-importer-exporter
	 *
	 * @since 1.0.6
	 * @param  array $results An array containing sidebars' widget data.
	 * @return array $results Modified array, deletes custom array key set during export.
	 */
	public function reset_custom_key( $results ) {
		unset($results['tl-custom-sidebars-areas']);

		return $results;
	}

	function before_wie_import( $data ) {
		global $wp_registered_sidebars;

		$data = (array) $data;

		$key             = 'tl-custom-sidebars-areas';
		$sidebars        = get_option( 'tl_custom_sidebars' );
		$custom_sidebars = (array) $data[ $key ];

		unset( $data[ $key ] );

		// Loop through each imported custom sidebar area and prepare it
		// to be added in new custom sidebar areas.
		foreach ( $custom_sidebars as $sidebar_id => $title ) {
			if ( ! isset( $wp_registered_sidebars[ $sidebar_id ] ) ) {
				$sidebars[ $sidebar_id ] = $title;
			}
		}

		update_option( 'tl_custom_sidebars', $sidebars );

		TLCS()->register_custom_sidebars();

		return $data;
	}

	/**
	 * Tweak style for Widget customizer.
	 *
	 * @since 1.0.7.
	 * @return void
	 */
	function customize_controls_print_scripts() {

		if ( false === ( $sidebars = get_option( 'tl_custom_sidebars' ) ) ) return;

		// Get custom sidebar keys
		$sidebars = array_keys( $sidebars );

		if ( ! is_array( $sidebars ) ) return;

		echo "<style type='text/css'>\n";
		foreach ( $sidebars as $sidebar_id ) :
			echo "#accordion-section-sidebar-widgets-{$sidebar_id} { display: list-item !important; height: auto !important; }\n";
			echo "#accordion-section-sidebar-widgets-{$sidebar_id} .widget-top { opacity: 1 !important; }\n";
		endforeach;
		echo "</style>\n";
	}
}

function TLCS() {
	return TL_Custom_Sidebars::instance();
}

$GLOBALS['tl_custom_sidebars'] = TLCS();