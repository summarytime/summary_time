<?php
add_action( 'add_meta_boxes', 'tl_custom_metabox_add', 2 );
function tl_custom_metabox_add()
{
		$screens = get_post_types( '', 'names' );			
		foreach ( $screens as $screen ) {
			if($screen=='page' || $screen=='post'){
				add_meta_box( 'tl_custom_metabox', 'Themelines Page Settings', 'tl_custom_metabox_fun', $screen, 'normal', 'high' );
			}
			if($screen=='page'){
				add_meta_box( 'tl_contact_metabox', 'Themelines Contact Settings', 'tl_contact_metabox_fun', $screen, 'normal', 'high' );
			}
			if($screen=='team'){
				add_meta_box( 'tl_team_metabox', 'Themelines Team Settings', 'tl_team_fun', $screen, 'normal', 'high' );
			}
			if($screen=='testimonial'){
				add_meta_box( 'tl_testimonial_metabox', 'Themelines Testimonial Settings', 'tl_testimonial_fun', $screen, 'normal', 'high' );
			}
			if($screen=='slider'){
				add_meta_box( 'tl_slider_metabox', 'Themelines Banner Settings', 'tl_slider_fun', $screen, 'normal', 'high' );
				add_meta_box( 'tl_layer_metabox', 'Themelines Layers', 'tl_layer_fun', $screen, 'normal', 'high' );
			}
			add_meta_box( 'tl_sidebar_metabox', 'Themelines Sidebar Settings', 'tl_sidebar_fun', $screen, 'side', 'default' );
		}
		wp_enqueue_script('media-upload');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script( 'jquery-ui-draggable');
		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');
		wp_enqueue_style( 'wp-color-picker' );        
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'tl_custom_metabox_style', get_template_directory_uri(). '/tl-custom-metabox/css/tl-custom-metabox.css');
		wp_enqueue_script( 'tl_custom_metabox_script', get_template_directory_uri(). '/tl-custom-metabox/js/tl-custom-metabox.js', array(), '1.0.0', true );
		wp_enqueue_style( 'builder-admin-css', get_template_directory_uri(). '/page-builder/css/admin.css', null);
		wp_enqueue_style( 'tlpb-font-awesome', get_template_directory_uri(). '/page-builder/font-awesome/css/font-awesome.min.css', null);
}

function tl_sidebar_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$themelines_sidebar = isset( $values['themelines_sidebar'][0] ) ? $values['themelines_sidebar'][0] : '';
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	<tr class="sidebar_selection_dropdown">
            	<th>Select Sidebar</th>
                <td>
                <?php
				$sidebars = $GLOBALS['wp_registered_sidebars'];
				if(isset($sidebars) && $sidebars):?>
                <select name="themelines_sidebar">
                	<?php foreach($sidebars as $sidebar):?>
                    <option value="<?php echo ucwords( $sidebar['id'] );?>"<?php echo $themelines_sidebar==ucwords( $sidebar['id'] )?' selected':'';?>><?php echo ucwords( $sidebar['name'] );?></option>
                    <?php endforeach;?>
                </select>
                <?php endif;?>
                </td>
            </tr>
            <tr class="no_sidebar_message" style="display:none;">
            	<td colspan="2">Please select a sidebar template to choose sidebar.</td>
            </tr>
        </table>
    </div>
<?php
}
function tl_custom_metabox_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$hide_title = isset( $values['tl_hide_title'][0] ) ? $values['tl_hide_title'][0] : '';
	$title_position = isset( $values['tl_title_position'][0] ) ? $values['tl_title_position'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	<tr>
            	<th>Hide Title</th>
                <td><input type="checkbox" value="on" name="tl_hide_title" <?php echo (isset($hide_title) && $hide_title=='on')?'checked':'';?>/></td>
            </tr>
            <tr>
            	<th>Title Position</th>
                <td>
                	<select name="tl_title_position">
                    	<option value="">Default</option>
                    	<option value="pull-left" <?php echo (isset($title_position) && $title_position=="pull-left")?'selected':'';?>>Left</option>
                        <option value="pull-right" <?php echo (isset($title_position) && $title_position=="pull-right")?'selected':'';?>>Right</option>
                        <option value="text-center" <?php echo (isset($title_position) && $title_position=="text-center")?'selected':'';?>>Center</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>
<?php
}

function tl_contact_metabox_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$contact_title = isset( $values['contact_title'][0] ) ? $values['contact_title'][0] : '';
	$contact_subtitle = isset( $values['contact_subtitle'][0] ) ? $values['contact_subtitle'][0] : '';
	$contact_address_title = isset( $values['contact_address_title'][0] ) ? $values['contact_address_title'][0] : '';
	$contact_address = isset( $values['contact_address'][0] ) ? $values['contact_address'][0] : '';
	$contact_email = isset( $values['contact_email'][0] ) ? $values['contact_email'][0] : '';
	$contact_phone = isset( $values['contact_phone'][0] ) ? $values['contact_phone'][0] : '';
	$contact_website = isset( $values['contact_website'][0] ) ? $values['contact_website'][0] : '';
	$contact_form = isset( $values['contact_form'][0] ) ? $values['contact_form'][0] : '';
	$show_map = isset( $values['show_map'][0] ) ? $values['show_map'][0] : '';
	$map_position = isset( $values['map_position'][0] ) ? $values['map_position'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	 <tr>
            	<th>Contact Title</th>
                <td><input type="text" value="<?php echo $contact_title;?>" name="contact_title" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Contact Subitle</th>
                <td><input type="text" value="<?php echo $contact_subtitle;?>" name="contact_subtitle" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Address Block Title</th>
                <td><input type="text" value="<?php echo $contact_address_title;?>" name="contact_address_title" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Address</th>
                <td><input type="text" name="contact_address" value="<?php echo $contact_address;?>" class="tl_textbox"></td>
            </tr>
            <tr>
            	<th>Email</th>
                <td><input type="text" name="contact_email" value="<?php echo $contact_email;?>" class="tl_textbox"></td>
            </tr>
            <tr>
            	<th>Phone</th>
                <td><input type="text" name="contact_phone" value="<?php echo $contact_phone;?>" class="tl_textbox"></td>
            </tr>
            <tr>
            	<th>Website</th>
                <td><input type="text" name="contact_website" value="<?php echo $contact_website;?>" class="tl_textbox"></td>
            </tr>
            <tr>
            	<th>Contact Form Shortcode</th>
                <?php
                $settings = array(
								'wpautop'=>true,
								'media_buttons'	=> true,
								'textarea_name' => 'contact_form',
								'textarea_rows'	=> 10,
								'tinymce'       => array(
									'wp_autoresize_on' => false,
									'resize'           => false,
								),
								'editor_height' => 300,
							);
				?>
                <td><?php wp_editor( $contact_form, 'contact_form', $settings);?></td>
            </tr>
            <tr>
            	<th>Show Map</th>
                <td><input type="checkbox" value="on" name="show_map" <?php echo (isset($show_map) && $show_map=='on')?'checked':'';?>/></td>
            </tr>
            <tr>
            	<th>Map Position</th>
                <td>
                	<select name="map_position">
                    	<option value="top" <?php echo (isset($map_position) && $map_position=="top")?'selected':'';?>>Top</option>
                        <option value="bottom" <?php echo (isset($map_position) && $map_position=="bottom")?'selected':'';?>>Bottom</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>
<?php
}


function tl_team_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$member_designation = isset( $values['team_member_designation'][0] ) ? $values['team_member_designation'][0] : '';
	$member_image = isset( $values['team_member_image'][0] ) ? $values['team_member_image'][0] : '';
	$member_address = isset( $values['team_member_address'][0] ) ? $values['team_member_address'][0] : '';
	$member_office_phone_number = isset( $values['team_member_office_phone'][0] ) ? $values['team_member_office_phone'][0] : '';
	$member_mobile_phone = isset( $values['team_member_mobile_phone'][0] ) ? $values['team_member_mobile_phone'][0] : '';
	$member_fax = isset( $values['team_member_fax'][0] ) ? $values['team_member_fax'][0] : '';
	$member_email = isset( $values['team_member_email'][0] ) ? $values['team_member_email'][0] : '';
	$member_facebook_url = isset( $values['team_member_facebook_url'][0] ) ? $values['team_member_facebook_url'][0] : '';
	$member_twitter_url = isset( $values['team_member_twitter_url'][0] ) ? $values['team_member_twitter_url'][0] : '';
	$member_linkedin_url = isset( $values['team_member_linkedin_url'][0] ) ? $values['team_member_linkedin_url'][0] : '';
	$member_google_plus_url = isset( $values['team_member_google_plus_url'][0] ) ? $values['team_member_google_plus_url'][0] : '';
	$member_description = isset( $values['team_member_description'][0] ) ? $values['team_member_description'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	<tr>
            	<th>Member Designation</th>
                <td><input type="text" value="<?php echo $member_designation;?>" name="team_member_designation" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Image</th>
                <td>
                <input type="text" value="<?php echo $member_image;?>" name="team_member_image" class="tl_textbox" id="team_member_image"/>
                <p><input type="button" name="member_image" class="button button-primary button-large" id="member_image" value="Upload" /></p>
                <?php if(isset($member_image) && $member_image):?>
                <p>
                <img src="<?php echo $member_image;?>" style="width:100px;" />
                </p>
                <?php endif;?>
                </td>
            </tr>
            <tr>
            	<th>Member Address</th>
                <td><textarea name="team_member_address" class="tl_textbox"><?php echo $member_address;?></textarea></td>
            </tr>
            <tr>
            	<th>Member Office Phone Number</th>
                <td><input type="text" name="team_member_office_phone" value="<?php echo $member_office_phone_number;?>" class="tl_textbox" /></td>
            </tr>
            <tr>
            	<th>Member Mobile Number</th>
                <td><input type="text" name="team_member_mobile_phone" value="<?php echo $member_mobile_phone;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Fax Number</th>
                <td><input type="text" name="team_member_fax" value="<?php echo $member_fax;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Email</th>
                <td><input type="text" name="team_member_email" value="<?php echo $member_email;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Facebook URL</th>
                <td><input type="text" name="team_member_facebook_url" value="<?php echo $member_facebook_url;?>" /></td>
            </tr>
            <tr>
            	<th>Member Twitter URL</th>
                <td><input type="text" name="team_member_twitter_url" value="<?php echo $member_twitter_url;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Linkedin URL</th>
                <td><input type="text" name="team_member_linkedin_url" value="<?php echo $member_linkedin_url;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Google Plus URL</th>
                <td><input type="text" name="team_member_google_plus_url" value="<?php echo $member_google_plus_url;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Member Description</th>
                <?php
                $settings = array(
								'wpautop'=>true,
								'media_buttons'	=> true,
								'textarea_name' => 'team_member_description',
								'textarea_rows'	=> 10,
								'tinymce'       => array(
									'wp_autoresize_on' => false,
									'resize'           => false,
								),
								'editor_height' => 300,
							);
				?>
                <td><?php wp_editor( $member_description, 'team_member_description', $settings);?></td>
            </tr>
        </table>
    </div>
<?php
}

function tl_testimonial_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$author_name = isset( $values['testimonial_author_name'][0] ) ? $values['testimonial_author_name'][0] : '';
	$author_image = isset( $values['testimonial_author_image'][0] ) ? $values['testimonial_author_image'][0] : '';
	$author_designation = isset( $values['testimonial_author_designation'][0] ) ? $values['testimonial_author_designation'][0] : '';
	$author_short_address = isset( $values['testimonial_author_short_address'][0] ) ? $values['testimonial_author_short_address'][0] : '';
	$testimonial = isset( $values['testimonial_testimonial'][0] ) ? $values['testimonial_testimonial'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	<tr>
            	<th>Author Name</th>
                <td><input type="text" value="<?php echo $author_name;?>" name="testimonial_author_name" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Author Image</th>
                <td>
                <input type="text" value="<?php echo $author_image;?>" name="testimonial_author_image" class="tl_textbox" id="testimonial_author_image"/>
                <p><input type="button" name="author_image" class="button button-primary button-large" id="author_image" value="Upload" /></p>
                <?php if(isset($author_image) && $author_image):?>
                <p>
                <img src="<?php echo $author_image;?>" style="width:100px;" />
                </p>
                <?php endif;?>
                </td>
            </tr>
            <tr>
            	<th>Author Designation</th>
                <td><input type="text" name="testimonial_author_designation" value="<?php echo $author_designation;?>" class="tl_textbox" /></td>
            </tr>
            <tr>
            	<th>Author Short Address</th>
                <td><input type="text" name="testimonial_author_short_address" value="<?php echo $author_short_address;?>" class="tl_textbox"/></td>
            </tr>
            <tr>
            	<th>Testimonial</th>
                <?php
                $settings = array(
								'wpautop'=>true,
								'media_buttons'	=> true,
								'textarea_name' => 'testimonial_testimonial',
								'textarea_rows'	=> 10,
								'tinymce'       => array(
									'wp_autoresize_on' => false,
									'resize'           => false,
								),
								'editor_height' => 300,
							);
				?>
                <td><?php wp_editor( $testimonial, 'testimonial_testimonial', $settings);?></td>
            </tr>
        </table>
    </div>
<?php
}

function tl_slider_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$slider_video = isset( $values['slider_video'][0] ) ? $values['slider_video'][0] : '';
	$slider_video_iframe = isset( $values['slider_video_iframe'][0] ) ? $values['slider_video_iframe'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
	<div id="tl-custom-metabox">
    	<table class="form-table tl-custom-table">
        	<tr>
            	<th>Upload Video (Optional)</th>
                <td>
                <input type="text" value="<?php echo $slider_video;?>" name="slider_video" class="tl_textbox"/>
                <input type="button" id="slider_video" value="Upload Video" class="button button-primary button-large" />
                <?php if(isset($slider_video) && $slider_video):?>
                <div style="width:300px;">
                <iframe width="300" height="200" src="<?php echo $slider_video;?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <?php endif;?>
                </td>
            </tr>
            <tr>
            	<th>Video Iframe (Optional):</th>
                <td>
                <textarea name="slider_video_iframe" class="tl_textarea"><?php echo $slider_video_iframe;?></textarea>
                <?php if(isset($slider_video_iframe) && $slider_video_iframe):?>
                <div style="width:300px;">
                <?php echo $slider_video_iframe;?>
                </div>
                <?php endif;?>
                </td>
            </tr>
        </table>
    </div>
<?php
}

function tl_layer_fun()
{
	global $post;
	if(isset($_GET['post']) && $_GET['post']){
		$id=$_GET['post'];
	}else{
		$id=0;
	}
	$values = get_post_custom( $id );
	$slider_video = isset( $values['slider_video'][0] ) ? $values['slider_video'][0] : '';
	$slider_video_iframe = isset( $values['slider_video_iframe'][0] ) ? $values['slider_video_iframe'][0] : '';
	$slider_layer		=	isset($values['slider_layer'][0])?unserialize($values['slider_layer'][0]) : '';
	$slider_layer_active =	isset($values['slider_layer_active'][0])?$values['slider_layer_active'][0] : '';
	$layer_text_position =  isset($values['layer_text_position'][0])?$values['layer_text_position'][0] : '';
	
	wp_nonce_field( 'custom_metabox_nonce', 'custom_metabox_nonce' );
?>
<?php $text_position_arr	=	array(
							'top-left'		=>	'Top Left',
							'top-center'	=>	'Top Center',
							'top-right'		=>	'Top Right',
							'center-left'	=>	'Center Left',
							'center-center'	=>	'Center Center',
							'center-right'	=>	'Center Right',
							'bottom-left'	=>	'Bottom Left',
							'bottom-center'	=>	'Bottom Center',
							'bottom-right'	=>	'Bottom Right',
						 );
	$text_position_str	=	'<select name="layer_text_position">';
	foreach($text_position_arr as $txt_pos_key=>$text_position)
	{
		$selected = $txt_pos_key==$layer_text_position?' selected="selected"':'';
		$text_position_str	.=	'<option value="'.$txt_pos_key.'"'.$selected.'>'.$text_position.'</option>';
	}	
	$text_position_str		.=	'</select>';
?>
<table class="form-table">
    <tr>
        <th>Text Position</th>
        <td><?php echo $text_position_str;?></td>
    </tr>
</table>
<div id="tl-layer-content">
	
	<?php
						 
	$heading_arr		=	array(
									'h1'	=>	'H1',
									'h2'	=>	'H2',
									'h3'	=>	'H3',
									'h4'	=>	'h4',
									'h5'	=>	'h5',
									'h6'	=>	'h6',
								 );
			 
		$sec_animation_arr	=	array(
									'att_seeker'	=>	array(
																'name'          =>	'Attention Seekers',
																'values'		=> array(
																		'bounce'		=>	'bounce',
																		'flash'			=>	'flash',
																		'pulse'			=>	'pulse',
																		'rubberBand'	=>	'rubberBand',
																		'shake'			=>	'shake',
																		'swing'			=>	'swing',
																		'tada'			=>	'tada',
																		'wobble'		=>	'wobble',
																		'jello'			=>	'jello',
																	),
															),
											
									'bounce_in'	=>	array(
															'name'          =>	'Bouncing Entrances',
															'values'		=> array(
																'bounceIn'		=>	'bounceIn',
																'bounceInDown'	=>	'bounceInDown',
																'bounceInLeft'	=>	'bounceInLeft',
																'bounceInRight'	=>	'bounceInRight',
																'bounceInUp'	=>	'bounceInUp',
															),
														),
									
									'bounce_out'	=>	array(
																'name'          =>	'Bouncing Exits',
																'values'		=> array(
																	'bounceOut'		=>	'bounceOut',
																	'bounceOutDown'	=>	'bounceOutDown',
																	'bounceOutLeft'	=>	'bounceOutLeft',
																	'bounceOutRight'=>	'bounceOutRight',
																	'bounceOutUp'	=>	'bounceOutUp',
															),
														),
															
									'fade_in'		=>	array(
																'name'          =>	'Fading Entrances',
																'values'		=> array(
																	'fadeIn'		=>	'fadeIn',
																	'fadeInDown'	=>	'fadeInDown',
																	'fadeInDownBig'	=>	'fadeInDownBig',
																	'fadeInLeft'	=>	'fadeInLeft',
																	'fadeInLeftBig'	=>	'fadeInLeftBig',
																	'fadeInRight'	=>	'fadeInRight',
																	'fadeInRightBig'=>	'fadeInRightBig',
																	'fadeInUp'		=>	'fadeInUp',
																	'fadeInUpBig'	=>	'fadeInUpBig',
																),
															),
									
									'fade_out'		=>	array(
																'name'          =>	'Fading Exits',
																'values'		=> array(
																	'fadeOut'		=>	'fadeOut',
																	'fadeOutDown'	=>	'fadeOutDown',
																	'fadeOutDownBig'=>	'fadeOutDownBig',
																	'fadeOutLeft'	=>	'fadeOutLeft',
																	'fadeOutLeftBig'=>	'fadeOutLeftBig',
																	'fadeOutRight'	=>	'fadeOutRight',
																	'fadeOutRightBig'=>	'fadeOutRightBig',
																	'fadeOutUp'		=>	'fadeOutUp',
																	'fadeOutUpBig'	=>	'fadeOutUpBig',
																	'fadeOutUpBig'	=>	'fadeOutUpBig',
															),
														),
									
									'flip'			=>	array(
																'name'          =>	'Flippers',
																'values'		=> array(
																	'flip'			=>	'flip',
																	'flipInX'		=>	'flipInX',
																	'flipInY'		=>	'flipInY',
																	'flipOutX'		=>	'flipOutX',
																	'flipOutY'		=>	'flipOutY',
															),
														),
									
									'light_speed'		=>	array(
																'name'          =>	'Lightspeed',
																'values'		=> array(
																	'lightSpeedIn'	=>	'lightSpeedIn',
																	'lightSpeedOut'	=>	'lightSpeedOut',
															),
														),
															
									'rotate_in'			=>	array(
																'name'          =>	'Rotating Entrances',
																'values'		=> array(
																	'rotateIn'			=>	'rotateIn',
																	'rotateInDownLeft'	=>	'rotateInDownLeft',
																	'rotateInDownRight'	=>	'rotateInDownRight',
																	'rotateInUpLeft'	=>	'rotateInUpLeft',
																	'rotateInUpRight'	=>	'rotateInUpRight',
															),
														),
															
									'rotate_out'		=>	array(
																'name'          =>	'Rotating Exits',
																'values'		=> array(
																	'rotateOut'			=>	'rotateOut',
																	'rotateOutDownLeft'	=>	'rotateOutDownLeft',
																	'rotateOutDownRight'=>	'rotateOutDownRight',
																	'rotateOutUpLeft'	=>	'rotateOutUpLeft',
																	'rotateOutUpRight'	=>	'rotateOutUpRight',
															),
														),
															
									'slide_in'			=>	array(
																'name'          =>	'Sliding Entrances',
																'values'		=> array(
																	'slideInUp'			=>	'slideInUp',
																	'slideInDown'		=>	'slideInDown',
																	'slideInLeft'		=>	'slideInLeft',
																	'slideInRight'		=>	'slideInRight',
															),
														),
															
									'slide_out'			=>	array(
																'name'          =>	'Sliding Exits',
																'values'		=> array(
																	'slideOutUp'		=>	'slideOutUp',
																	'slideOutDown'		=>	'slideOutDown',
																	'slideOutLeft'		=>	'slideOutLeft',
																	'slideOutRight'		=>	'slideOutRight',
															),
														),
															
									'zoom_in'			=>	array(
																'name'          =>	'Zoom Entrances',
																'values'		=> array(
																	'zoomIn'			=>	'zoomIn',
																	'zoomInDown'		=>	'zoomInDown',
																	'zoomInLeft'		=>	'zoomInLeft',
																	'zoomInRight'		=>	'zoomInRight',
																	'zoomInUp'			=>	'zoomInUp',
															),
														),
															
									'zoom_out'			=>	array(
																'name'          =>	'Zoom Exits',
																'values'		=> array(
																	'zoomOut'			=>	'zoomOut',
																	'zoomOutDown'		=>	'zoomOutDown',
																	'zoomOutLeft'		=>	'zoomOutLeft',
																	'zoomOutRight'		=>	'zoomOutRight',
																	'zoomOutUp'			=>	'zoomOutUp',
															),
														),
															
									'special'			=>	array(
																'name'          =>	'Specials',
																'values'		=> array(
																	'hinge'				=>	'hinge',
																	'rollIn'			=>	'rollIn',
																	'rollOut'			=>	'rollOut',
															),
														),
									
								 );
								 
		$sec_animation_delay_arr	=	array(
											'200ms'		=>	'200ms',
											'400ms'		=>	'400ms',
											'600ms'		=>	'600ms',
											'800ms'		=>	'800ms',
											'1000ms'	=>	'1000ms',
											'1200ms'	=>	'1200ms',
											'1400ms'	=>	'1400ms',
											'1600ms'	=>	'1600ms',
											'1800ms'	=>	'1800ms',
											'2000ms'	=>	'2000ms',
								);
								
		$sec_animation_duration_arr	=	array(
											'200ms'		=>	'200ms',
											'400ms'		=>	'400ms',
											'600ms'		=>	'600ms',
											'800ms'		=>	'800ms',
											'1000ms'	=>	'1000ms',
											'1200ms'	=>	'1200ms',
											'1400ms'	=>	'1400ms',
											'1600ms'	=>	'1600ms',
											'1800ms'	=>	'1800ms',
											'2000ms'	=>	'2000ms',
								);
								
		$background_colour_arr	=	array(
											'default'				=>	'Default',
											'color-turquoise'		=>	'Turquoise',
											'color-greensea'		=>	'Green Sea',
											'color-sunflower'		=>	'Sunflower',
											'color-orange'			=>	'Orange',
											'color-emrald'			=>	'Emrald',
											'color-nephritis'		=>	'Nephritis',
											'color-carrot'			=>	'Carrot',
											'color-pumpkin'			=>	'Pumpkin',
											'color-peterriver'		=>	'Peter River',
											'color-belizehole'		=>	'Belize Hole',
											'color-alizarin'		=>	'Alizarin',
											'color-pomegranate'		=>	'Pomegranate',
											'color-amethyst'		=>	'Amethyst',
											'color-wisteria'		=>	'Wisteria',
											'color-wetasphalt'		=>	'Wet Asphalt',
											'color-midnightblue'	=>	'Midnight Blue',
											'color-brown'			=>	'Brown',
											'color-cyan'			=>	'Cyan',
											'color-teal'			=>	'Teal',
											'color-grey'			=>	'Grey',
											'color-lightgreen'		=>	'Light Green',
											'color-lime'			=>	'Lime',
											'color-yellow'			=>	'Yellow',
											'color-amber'			=>	'Amber',
											'color-grey'			=>  'Grey',
											'color-darkgrey'		=>	'Dark Grey',
											'color-mignightblack'	=>	'Mignight Black',
											'color-black'			=>	'Black',
											'color-white'			=>	'White',
											'color-cloud'			=>	'Cloud',
											'color-silver'			=>	'Silver',
											'color-concrete'		=>	'Concrete',
											'color-asbestos'		=>	'Asbestos',
			
							);
							
			$text_colour_arr	=	array(
										'default'				=>	'Default',
										'font_turquoise'		=>	'Turquoise',
										'font_greensea'			=>	'Green Sea',
										'font_sunflower'		=>	'Sunflower',
										'font_orange'			=>	'Orange',
										'font_emrald'			=>	'Emrald',
										'font_nephritis'		=>	'Nephritis',
										'font_carrot'			=>	'Carrot',
										'font_pumpkin'			=>	'Pumpkin',
										'font_peterriver'		=>	'Peter River',
										'font_belizehole'		=>	'Belize Hole',
										'font_alizarin'			=>	'Alizarin',
										'font_pomegranate'		=>	'Pomegranate',
										'font_amethyst'			=>	'Amethyst',
										'font_wisteria'			=>	'Wisteria',
										'font_wetasphalt'		=>	'Wet Asphalt',
										'font_midnightblue'		=>	'Midnight Blue',
										'font_brown'			=>	'Brown',
										'font_cyan'				=>	'Cyan',
										'font_teal'				=>	'Teal',
										'font_green'			=>	'Grey',
										'font_lightgreen'		=>	'Light Green',
										'font_lime'				=>	'Lime',
										'font_yellow'			=>	'Yellow',
										'font_amber'			=>	'Amber',
										'font_grey'				=>  'Grey',
										'font_darkgrey'			=>	'Dark Grey',
										'font_mignightblack'	=>	'Mignight Black',
										'font_black'			=>	'Black',
										'font_white'			=>	'White',
										'font_cloud'			=>	'Cloud',
										'font_silver'			=>	'Silver',
										'font_concrete'			=>	'Concrete',
										'font_asbestos'			=>	'Asbestos',
		
						);
	
	
	
	if(!empty($slider_layer))
	{
		$count = count($slider_layer);
		foreach($slider_layer as $key=>$value)
		{	
			
			
			// Heading Dropdown
			$heading_str	=	'<select name="layer['.$key.'][heading]">';
			foreach($heading_arr as $heading_key=>$heading)
			{
				$selected = $heading_key==$value['heading']?' selected="selected"':'';
				$heading_str	.=	'<option value="'.$heading_key.'"'.$selected.'>'.$heading_arr[$heading_key].'</option>';
			}	
			$heading_str		.=	'</select>';	
			
			// Animation Dropdown
			$animation_str	=	'<select name="layer['.$key.'][sec_animation]"><option value="no">No Animation</option>';
			foreach($sec_animation_arr as $ani_key=>$option_group)
			{
				$animation_str		.=	'<optgroup label="'.$option_group['name'].'">';
				foreach($option_group['values'] as $group)
				{
					$selected = $group==$value['sec_animation']?' selected="selected"':'';
					$animation_str	.=	'<option value="'.$group.'"'.$selected.'>'.$group.'</option>';
				}
				$animation_str		.=	'</optgroup>';
				
			}	
			$animation_str		.=	'</select>';
			// Animation Delay Dropdown
			$animation_delay_str	=	'<select name="layer['.$key.'][sec_animation_delay]">';
			foreach($sec_animation_delay_arr as $ani_delay_key=>$sec_animation_delay)
			{
				$selected = $ani_delay_key==$value['sec_animation_delay']?' selected="selected"':'';
				$animation_delay_str	.=	'<option value="'.$ani_delay_key.'"'.$selected.'>'.$sec_animation_delay_arr[$ani_delay_key].'</option>';
			}	
			$animation_delay_str		.=	'</select>';
			
			// Animation Duration Dropdown
			$animation_duration_str	=	'<select name="layer['.$key.'][sec_animation_duration]">';
			foreach($sec_animation_duration_arr as $ani_dur_key=>$sec_animation_duration)
			{
				$selected = $ani_dur_key==$value['sec_animation_duration']?' selected="selected"':'';
				$animation_duration_str	.=	'<option value="'.$ani_dur_key.'"'.$selected.'>'.$sec_animation_duration_arr[$ani_dur_key].'</option>';
			}	
			$animation_duration_str		.=	'</select>';
			
			// Text Colour Dropdown
			$text_colour_str	=	'<select name="layer['.$key.'][text_colour]">';
			foreach($text_colour_arr as $txt_col_key=>$text_colour)
			{
				$selected = $txt_col_key==$value['text_colour']?' selected="selected"':'';
				$text_colour_str	.=	'<option value="'.$txt_col_key.'"'.$selected.'>'.$text_colour_arr[$txt_col_key].'</option>';
			}	
			$text_colour_str		.=	'</select>';
			
			
			// Button Text Colour Dropdown
			$btn_text_colour_str	=	'<select name="layer['.$key.'][button_text_colour]">';
			foreach($text_colour_arr as $btn_txt_col_key=>$btn_text_colour)
			{	
				$selected = isset($value['button_text_colour']) && $value['button_text_colour'] && $btn_txt_col_key==$value['button_text_colour']?' selected="selected"':'';
				$btn_text_colour_str	.=	'<option value="'.$btn_txt_col_key.'"'.$selected.'>'.$text_colour_arr[$btn_txt_col_key].'</option>';
			}	
			$btn_text_colour_str		.=	'</select>';
			
			// Button Background Colour Dropdown
			$btn_background_colour_str	=	'<select name="layer['.$key.'][button_background_colour]">';
			foreach($background_colour_arr as $btn_back_col_key=>$btn_back_colour)
			{	
				$selected = isset($value['button_background_colour']) && $value['button_background_colour'] && $btn_back_col_key==$value['button_background_colour']?' selected="selected"':'';
				$btn_background_colour_str	.=	'<option value="'.$btn_back_col_key.'"'.$selected.'>'.$background_colour_arr[$btn_back_col_key].'</option>';
			}	
			$btn_background_colour_str		.=	'</select>';
			?>
			<div class="tl-layer-wrapper">
				<table class="form-table">
					<tr>
						<th>Heading</th>
						<td><?php echo $heading_str;?></td>
					</tr>
					<tr>
						<th>Animation</th>
						<td><?php echo $animation_str;?></td>
					</tr>
					<tr>
						<th>Animation Delay</th>
						<td><?php echo $animation_delay_str;?></td>
					</tr>
					<tr>
						<th>Animation Duration</th>
						<td><?php echo $animation_duration_str;?></td>
					</tr>
					<tr>
						<th>Layer Text</th>
						<td><textarea class="form-control" name="layer[<?php echo $key;?>][layer_text]"><?php echo $value['layer_text'];?></textarea></td>
					</tr>
					<tr>
						<th>Text Colour</th>
						<td><?php echo $text_colour_str;?></td>
					</tr>
                    <tr>
						<th>Text Custom Colour</th>
						<td><input type="text" name="layer[<?php echo $key;?>][text_custom_color]" class="my-color-picker" value="<?php echo $value['text_custom_color'];?>"/></td>
					</tr>
					<tr>
						<th>Show Button</th>
						<td><input name="layer[<?php echo $key;?>][show_button]" <?php echo isset($value['show_button'])?' checked="checked"':''?> value="1" class="show_button" type="checkbox">
						<input class="show_button_hidden" name="layer[<?php echo $key;?>][show_button_hidden]" value="0" type="hidden"></td>
					</tr>
					<tr>
					  <td colspan="2"><table class="button-element" style="<?php echo isset($value['show_button'])?'display: block':'display: none';?>;">
						  <tbody>
							<tr>
							  <th>Button URL</th>
							  <td><input name="layer[<?php echo $key;?>][button_url]" value="<?php echo $value['button_url'];?>" type="text"></td>
							</tr>
							<tr>
							  <th>Button Text</th>
							  <td><input name="layer[<?php echo $key;?>][button_text]" value="<?php echo $value['button_text'];?>" type="text"></td>
							</tr>
							<tr>
							  <th>Button Text Colour</th>
							  <td><?php echo $btn_text_colour_str;?></td>
							</tr>
                            <tr>
                            	<th>Button Text Custom Colour</th>
                                <td><input type="text" name="layer[<?php echo $key;?>][button_text_custom_color]" class="my-color-picker" value="<?php echo $value['button_text_custom_color'];?>"/></td>
                            </tr>
                            <tr>
							  <th>Button Background Colour</th>
							  <td><?php echo $btn_background_colour_str;?></td>
							</tr>
                            <tr>
                            	<th>Button Background Custom Colour</th>
                                <td><input type="text" name="layer[<?php echo $key;?>][button_background_custom_color]" class="my-color-picker" value="<?php echo $value['button_background_custom_color'];?>"/></td>
                            </tr>
						  </tbody>
						</table></td>
					</tr>
					<tr><td><input type="button" class="button button-warning remove_layer" value="Remove Layer"></td></tr>
                    <tr>
                    <td><button type="button" class="order button button-warning" value="Up"><i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>
<button type="button" class="order button button-warning" value="Down"><i class="fa fa-arrow-down" aria-hidden="true"></td>
                    </tr>
                    
				</table>
             </div>
			<?php
		}
	}
	?>
</div>

<table class="form-table tl-custom-table">
    <tr>
      <td>
      	<input type="button" id="add_layer" value="Add Layer" class="button button-primary button-large" />
        <input type="hidden" name="slider_layer_active" value="<?php echo isset($slider_layer_active) && $slider_layer_active?$slider_layer_active:'no';?>" />
      </td>
    </tr>
  </table>
  <table>
    <tr>
    <td><a href="javascript:void(0);" id="layer-deactivate" class="button"><i class="fa fa-minus-circle"></i>  Use Default Option</a></td>
    </tr>
    </table>
<script type="text/javascript">
	<?php if(isset($count) && $count):?>
	var count = <?php echo $count;?>;
	<?php else:?>
	var count =0;
	<?php endif;?>
	
	jQuery(document).on('click','.show_button',function(){
		if(jQuery(this).is(':checked'))
		{
			jQuery(this).closest('table').find('.button-element').css( "display", "block" );
			jQuery(this).next('.show_button_hidden').val(1);
		}
		else
		{
			jQuery(this).closest('table').find('.button-element').css( "display", "none" );
			jQuery(this).next('.show_button_hidden').val(0);
		}
	});
	jQuery('#remove_layer').click(function(){
		jQuery('.tl-layer-wrapper').last().remove();
	});
	jQuery(document).on('click','.remove_layer',function(){
		jQuery(this).closest('.tl-layer-wrapper').remove();
		count--;
	});
	jQuery('#add_layer').click(function(){
		var layerHTML	=	'<div class="tl-layer-wrapper"><table class="form-table">';
		
		layerHTML	+=	'<tr><th>Heading</th><td><select name="layer['+count+'][heading]"><option value="h1">H1</option><option value="h2">H2</option><option value="h3">H3</option><option value="h4">H4</option><option value="h5">H5</option><option value="h6">H6</option></select></td></tr>';
		
		layerHTML	+=	'<tr><th>Animation</th><td>'+
		'<select class="form-control" name="layer['+count+'][sec_animation]">'+
            '<option value="no">No Animation</option>'+
            '<optgroup label="Attention Seekers">'+
            '<option value="bounce">bounce</option>'+
            '<option value="flash">flash</option>'+
            '<option value="pulse">pulse</option>'+
            '<option value="rubberBand">rubberBand</option>'+
            '<option value="shake">shake</option>'+
            '<option value="swing">swing</option>'+
            '<option value="tada">tada</option>'+
            '<option value="wobble">wobble</option>'+
            '<option value="jello">jello</option>'+
            '</optgroup>'+
            '<optgroup label="Bouncing Entrances">'+
            '<option value="bounceIn">bounceIn</option>'+
            '<option value="bounceInDown">bounceInDown</option>'+
            '<option value="bounceInLeft">bounceInLeft</option>'+
            '<option value="bounceInRight">bounceInRight</option>'+
            '<option value="bounceInUp">bounceInUp</option>'+
            '</optgroup>'+
            '<optgroup label="Bouncing Exits">'+
            '<option value="bounceOut">bounceOut</option>'+
            '<option value="bounceOutDown">bounceOutDown</option>'+
            '<option value="bounceOutLeft">bounceOutLeft</option>'+
            '<option value="bounceOutRight">bounceOutRight</option>'+
            '<option value="bounceOutUp">bounceOutUp</option>'+
            '</optgroup>'+
            '<optgroup label="Fading Entrances">'+
            '<option value="fadeIn">fadeIn</option>'+
            '<option value="fadeInDown">fadeInDown</option>'+
            '<option value="fadeInDownBig">fadeInDownBig</option>'+
            '<option value="fadeInLeft">fadeInLeft</option>'+
            '<option value="fadeInLeftBig">fadeInLeftBig</option>'+
            '<option value="fadeInRight">fadeInRight</option>'+
            '<option value="fadeInRightBig">fadeInRightBig</option>'+
            '<option value="fadeInUp">fadeInUp</option>'+
            '<option value="fadeInUpBig">fadeInUpBig</option>'+
            '</optgroup>'+
            '<optgroup label="Fading Exits">'+
            '<option value="fadeOut">fadeOut</option>'+
            '<option value="fadeOutDown">fadeOutDown</option>'+
            '<option value="fadeOutDownBig">fadeOutDownBig</option>'+
            '<option value="fadeOutLeft">fadeOutLeft</option>'+
            '<option value="fadeOutLeftBig">fadeOutLeftBig</option>'+
            '<option value="fadeOutRight">fadeOutRight</option>'+
            '<option value="fadeOutRightBig">fadeOutRightBig</option>'+
            '<option value="fadeOutUp">fadeOutUp</option>'+
            '<option value="fadeOutUpBig">fadeOutUpBig</option>'+
            '</optgroup>'+
            '<optgroup label="Flippers">'+
            '<option value="flip">flip</option>'+
            '<option value="flipInX">flipInX</option>'+
            '<option value="flipInY">flipInY</option>'+
            '<option value="flipOutX">flipOutX</option>'+
            '<option value="flipOutY">flipOutY</option>'+
            '</optgroup>'+
            '<optgroup label="Lightspeed">'+
            '<option value="lightSpeedIn">lightSpeedIn</option>'+
            '<option value="lightSpeedOut">lightSpeedOut</option>'+
            '</optgroup>'+
            '<optgroup label="Rotating Entrances">'+
            '<option value="rotateIn">rotateIn</option>'+
            '<option value="rotateInDownLeft">rotateInDownLeft</option>'+
            '<option value="rotateInDownRight">rotateInDownRight</option>'+
            '<option value="rotateInUpLeft">rotateInUpLeft</option>'+
            '<option value="rotateInUpRight">rotateInUpRight</option>'+
            '</optgroup>'+
            '<optgroup label="Rotating Exits">'+
            '<option value="rotateOut">rotateOut</option>'+
            '<option value="rotateOutDownLeft">rotateOutDownLeft</option>'+
            '<option value="rotateOutDownRight">rotateOutDownRight</option>'+
            '<option value="rotateOutUpLeft">rotateOutUpLeft</option>'+
            '<option value="rotateOutUpRight">rotateOutUpRight</option>'+
            '</optgroup>'+
            '<optgroup label="Sliding Entrances">'+
            '<option value="slideInUp">slideInUp</option>'+
            '<option value="slideInDown">slideInDown</option>'+
            '<option value="slideInLeft">slideInLeft</option>'+
            '<option value="slideInRight">slideInRight</option>'+
            '</optgroup>'+
            '<optgroup label="Sliding Exits">'+
            '<option value="slideOutUp">slideOutUp</option>'+
            '<option value="slideOutDown">slideOutDown</option>'+
            '<option value="slideOutLeft">slideOutLeft</option>'+
            '<option value="slideOutRight">slideOutRight</option>'+
            '</optgroup>'+
            '<optgroup label="Zoom Entrances">'+
            '<option value="zoomIn">zoomIn</option>'+
            '<option value="zoomInDown">zoomInDown</option>'+
            '<option value="zoomInLeft">zoomInLeft</option>'+
            '<option value="zoomInRight">zoomInRight</option>'+
            '<option value="zoomInUp">zoomInUp</option>'+
            '</optgroup>'+
            '<optgroup label="Zoom Exits">'+
            '<option value="zoomOut">zoomOut</option>'+
            '<option value="zoomOutDown">zoomOutDown</option>'+
            '<option value="zoomOutLeft">zoomOutLeft</option>'+
            '<option value="zoomOutRight">zoomOutRight</option>'+
            '<option value="zoomOutUp">zoomOutUp</option>'+
            '</optgroup>'+
            '<optgroup label="Specials">'+
            '<option value="hinge">hinge</option>'+
            '<option value="rollIn">rollIn</option>'+
            '<option value="rollOut">rollOut</option>'+
            '</optgroup>'+
          '</select></td></tr>';
		
		layerHTML	+=	'<tr><th>Animation Delay</th><td>'+
		  '<select class="form-control" name="layer['+count+'][sec_animation_delay]">'+
            '<option value="200ms">200ms</option>'+
            '<option value="400ms">400ms</option>'+
            '<option value="600ms">600ms</option>'+
            '<option value="800ms">800ms</option>'+
            '<option value="1000ms">1000ms</option>'+
            '<option value="1200ms">1200ms</option>'+
            '<option value="1400ms">1400ms</option>'+
            '<option value="1600ms">1600ms</option>'+
            '<option value="1800ms">1800ms</option>'+
            '<option value="2000ms">2000ms</option>'+
          '</select></td></tr>'; 
		  
		layerHTML	+=	'<tr><th>Animation Duration</th><td>'+
		  '<select class="form-control" name="layer['+count+'][sec_animation_duration]">'+
            '<option value="200ms">200ms</option>'+
            '<option value="400ms">400ms</option>'+
            '<option value="600ms">600ms</option>'+
            '<option value="800ms">800ms</option>'+
            '<option value="1000ms">1000ms</option>'+
            '<option value="1200ms">1200ms</option>'+
            '<option value="1400ms">1400ms</option>'+
            '<option value="1600ms">1600ms</option>'+
            '<option value="1800ms">1800ms</option>'+
            '<option value="2000ms">2000ms</option>'+
          '</select></td></tr>';
		  
		layerHTML	+=	'<tr><th>Layer Text</th><td>'+
						'<textarea class="form-control" name="layer['+count+'][layer_text]"></textarea>'+
						'</td></tr>';
						
		layerHTML	+=	'<tr><th>Text Colour</th><td>'+
		 '<select id="text_colour" name="layer['+count+'][text_colour]" class="form-control">'+
          	'<option value="default">Default</option>'+
			'<option value="font_turquoise">Turquoise</option>'+
			'<option value="font_greensea">Green Sea</option>'+
			'<option value="font_sunflower">Sunflower</option>'+
			'<option value="font_orange">Orange</option>'+
			'<option value="font_emrald">Emrald</option>'+
			'<option value="font_nephritis">Nephritis</option>'+
			'<option value="font_carrot">Carrot</option>'+
			'<option value="font_pumpkin">Pumpkin</option>'+
			'<option value="font_peterriver">Peter River</option>'+
			'<option value="font_belizehole">Belize Hole</option>'+
			'<option value="font_alizarin">Alizarin</option>'+
			'<option value="font_pomegranate">Pomegranate</option>'+
			'<option value="font_amethyst">Amethyst</option>'+
			'<option value="font_wisteria">Wisteria</option>'+
			'<option value="font_wetasphalt">Wet Asphalt</option>'+
			'<option value="font_midnightblue">Midnight Blue</option>'+
			'<option value="font_brown">Brown</option>'+
			'<option value="font_cyan">Cyan</option>'+
			'<option value="font_teal">Teal</option>'+
			'<option value="font_green">Green</option>'+
			'<option value="font_lightgreen">Light Green</option>'+
			'<option value="font_lime">Lime</option>'+
			'<option value="font_yellow">Yellow</option>'+
			'<option value="font_amber">Amber</option>'+
			'<option value="font_grey">Grey</option>'+
			'<option value="font_darkgrey">Dark Grey</option>'+
			'<option value="font_mignightblack">Mignight Black</option>'+
			'<option value="font_black">Black</option>'+
			'<option value="font_white">White</option>'+
			'<option value="font_cloud">Cloud</option>'+
			'<option value="font_silver">Silver</option>'+
			'<option value="font_concrete">Concrete</option>'+
			'<option value="font_asbestos">Asbestos</option>'+
          '</select></td></tr>';
		  
		  layerHTML	+=	'<tr><th>Text Custom Colour</th>'+
		  				'<td><input type="text" name="layer['+count+'][text_custom_color]" class="my-color-picker" value=""/></td></tr>';
		  
		 layerHTML	+=	'<tr><th>Show Button</th>'+
        '<td><input type="checkbox" name="layer['+count+'][show_button]" value="1" class="show_button" /></td>'+
      	'</tr>';
		
		layerHTML	+=	'<tr><td colspan="2">'+
							'<table class="button-element" style="display:none;"><tr>'+
								'<th>Button URL</th>'+
								'<td><input type="text" name="layer['+count+'][button_url]" /></td></tr>'+
								'<tr><th>Button Text</th>'+
								'<td><input type="text" name="layer['+count+'][button_text]" /></td></tr>'+
								'<tr><th>Button Text Colour</th>'+
								'<td><select id="button_text_colour" name="layer['+count+'][button_text_colour]" class="form-control">'+
										'<option value="default">Default</option>'+
										'<option value="font_turquoise">Turquoise</option>'+
										'<option value="font_greensea">Green Sea</option>'+
										'<option value="font_sunflower">Sunflower</option>'+
										'<option value="font_orange">Orange</option>'+
										'<option value="font_emrald">Emrald</option>'+
										'<option value="font_nephritis">Nephritis</option>'+
										'<option value="font_carrot">Carrot</option>'+
										'<option value="font_pumpkin">Pumpkin</option>'+
										'<option value="font_peterriver">Peter River</option>'+
										'<option value="font_belizehole">Belize Hole</option>'+
										'<option value="font_alizarin">Alizarin</option>'+
										'<option value="font_pomegranate">Pomegranate</option>'+
										'<option value="font_amethyst">Amethyst</option>'+
										'<option value="font_wisteria">Wisteria</option>'+
										'<option value="font_wetasphalt">Wet Asphalt</option>'+
										'<option value="font_midnightblue">Midnight Blue</option>'+
										'<option value="font_brown">Brown</option>'+
										'<option value="font_cyan">Cyan</option>'+
										'<option value="font_teal">Teal</option>'+
										'<option value="font_green">Green</option>'+
										'<option value="font_lightgreen">Light Green</option>'+
										'<option value="font_lime">Lime</option>'+
										'<option value="font_yellow">Yellow</option>'+
										'<option value="font_amber">Amber</option>'+
										'<option value="font_grey">Grey</option>'+
										'<option value="font_darkgrey">Dark Grey</option>'+
										'<option value="font_mignightblack">Mignight Black</option>'+
										'<option value="font_black">Black</option>'+
										'<option value="font_white">White</option>'+
										'<option value="font_cloud">Cloud</option>'+
										'<option value="font_silver">Silver</option>'+
										'<option value="font_concrete">Concrete</option>'+
										'<option value="font_asbestos">Asbestos</option>'+
									'</select></td></tr>'+
									'<tr><th>Button Text Custom Colour</th>'+
									'<td><input type="text" name="layer['+count+'][button_text_custom_color]" class="my-color-picker" value=""/></td></tr>'+
									'<tr><th>Button Background Colour</th>'+
									'<td><select id="button_background_colour" name="layer['+count+'][button_background_colour]" class="form-control">'+
										'<option value="default" selected="selected">Default</option>'+
										'<option value="color-turquoise">Turquoise</option>'+
										'<option value="color-greensea">Green Sea</option>'+
										'<option value="color-sunflower">Sunflower</option>'+
										'<option value="color-orange">Orange</option>'+
										'<option value="color-emrald">Emrald</option>'+
										'<option value="color-nephritis">Nephritis</option>'+
										'<option value="color-carrot">Carrot</option>'+
										'<option value="color-pumpkin">Pumpkin</option>'+
										'<option value="color-peterriver">Peter River</option>'+
										'<option value="color-belizehole">Belize Hole</option>'+
										'<option value="color-alizarin">Alizarin</option>'+
										'<option value="color-pomegranate">Pomegranate</option>'+
										'<option value="color-amethyst">Amethyst</option>'+
										'<option value="color-wisteria">Wisteria</option>'+
										'<option value="color-wetasphalt">Wet Asphalt</option>'+
										'<option value="color-midnightblue">Midnight Blue</option>'+
										'<option value="color-brown">Brown</option>'+
										'<option value="color-cyan">Cyan</option>'+
										'<option value="color-teal">Teal</option>'+
										'<option value="color-grey">Grey</option>'+
										'<option value="color-green">Green</option>'+
										'<option value="color-lightgreen">Light Green</option>'+
										'<option value="color-lime">Lime</option>'+
										'<option value="color-yellow">Yellow</option>'+
										'<option value="color-amber">Amber</option>'+
										'<option value="color-darkgrey">Dark Grey</option>'+
										'<option value="color-mignightblack">Mignight Black</option>'+
										'<option value="color-black">Black</option>'+
										'<option value="color-white">White</option>'+
										'<option value="color-cloud">Cloud</option>'+
										'<option value="color-silver">Silver</option>'+
										'<option value="color-concrete">Concrete</option>'+
										'<option value="color-asbestos">Asbestos</option>'+
									'</select></td>'+
									'<tr><th>Button Background Custom Colour</th>'+
									'<td><input type="text" name="layer['+count+'][button_background_custom_color]" class="my-color-picker" value=""/></td></tr></tr></table>'+
									'</td></tr>';
									
					
					layerHTML	+=	'<tr><td><input type="button" class="button button-warning remove_layer" value="Remove Layer"></td></tr><tr><td><button type="button" class="order button button-warning" value="Up"><i class="fa fa-arrow-up" aria-hidden="true"></i></button><button type="button" class="order button button-warning" value="Down"><i class="fa fa-arrow-down" aria-hidden="true"></i></button></td></tr></table></div>';
					count++;
					
		if(jQuery('.tl-layer-wrapper').length == 0)
		{
			jQuery('#tl-layer-content').html(layerHTML);
		}	
		else
		{						
			jQuery('.tl-layer-wrapper:last').after(layerHTML);
		}
		jQuery('.my-color-picker').wpColorPicker();
	});
function moveUp(item) {
    var prev = item.prev();
    if (prev.length == 0)
        return;
    prev.css('z-index', 999).css('position','relative').animate({ top: item.height() }, 1000);
    item.css('z-index', 1000).css('position', 'relative').animate({ top: '-' + prev.height() }, 1000, function () {
        prev.css('z-index', '').css('top', '').css('position', '');
        item.css('z-index', '').css('top', '').css('position', '');
        item.insertBefore(prev);
    });
}
function moveDown(item) {
    var next = item.next();
    if (next.length == 0)
        return;
    next.css('z-index', 999).css('position', 'relative').animate({ top: '-' + item.height() }, 1000);
    item.css('z-index', 1000).css('position', 'relative').animate({ top: next.height() }, 1000, function () {
        next.css('z-index', '').css('top', '').css('position', '');
        item.css('z-index', '').css('top', '').css('position', '');
        item.insertAfter(next);
    });
}


jQuery(document).on('click', '.order', function(e) { 
	e.preventDefault();
    var btn = jQuery(this);
    var val = btn.val();
	
    if (val == 'Up')
        moveUp(btn.parents('.tl-layer-wrapper'));
    else
        moveDown(btn.parents('.tl-layer-wrapper'));
});
	</script>
    
<?php
}

add_action( 'save_post', 'custom_metabox_save' );
function custom_metabox_save( $post_id )
{
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	if( !isset( $_POST['custom_metabox_nonce'] ) || !wp_verify_nonce( $_POST['custom_metabox_nonce'], 'custom_metabox_nonce' ) ) return;
	 
	if( !current_user_can( 'edit_post' ) ) return;
	
	$allowed = array( 
			'a' => array(
				'href' => array()
			),
			'iframe' => array(
				'id' => array(),
				'src' => array(),
				'class' => array(),
				'width' => array(),
				'height' => array(),
				'frameborder' => array(),
				'allowfullscreen' => array(),
				'style'	=> array()
			),
		);
	if( isset( $_POST['themelines_sidebar'] ) ){
		update_post_meta( $post_id, 'themelines_sidebar', $_POST['themelines_sidebar'] );
	}
	
	if( isset( $_POST['tl_hide_title'] ) ){
		update_post_meta( $post_id, 'tl_hide_title', wp_kses( $_POST['tl_hide_title'], $allowed ) );
	}else{
		update_post_meta( $post_id, 'tl_hide_title', 'off' );
	}
	
	if( isset( $_POST['tl_title_position'] ) ){
		update_post_meta( $post_id, 'tl_title_position', wp_kses( $_POST['tl_title_position'], $allowed ) );
	}
	
	if( isset( $_POST['contact_title'] ) ){
		update_post_meta( $post_id, 'contact_title', wp_kses( $_POST['contact_title'], $allowed ) );
	}
	
	if( isset( $_POST['contact_subtitle'] ) ){
		update_post_meta( $post_id, 'contact_subtitle', wp_kses( $_POST['contact_subtitle'], $allowed ) );
	}
	
	if( isset( $_POST['contact_address_title'] ) ){
		update_post_meta( $post_id, 'contact_address_title', wp_kses( $_POST['contact_address_title'], $allowed ) );
	}
	
	if( isset( $_POST['contact_address'] ) ){
		update_post_meta( $post_id, 'contact_address', wp_kses( $_POST['contact_address'], $allowed ) );
	}
	
	if( isset( $_POST['contact_email'] ) ){
		update_post_meta( $post_id, 'contact_email', wp_kses( $_POST['contact_email'], $allowed ) );
	}
	
	if( isset( $_POST['contact_phone'] ) ){
		update_post_meta( $post_id, 'contact_phone', wp_kses( $_POST['contact_phone'], $allowed ) );
	}
	
	if( isset( $_POST['contact_website'] ) ){
		update_post_meta( $post_id, 'contact_website', wp_kses( $_POST['contact_website'], $allowed ) );
	}
	
	if( isset( $_POST['contact_form'] ) ){
		update_post_meta( $post_id, 'contact_form', $_POST['contact_form'] );
	}
	
	if( isset( $_POST['show_map'] ) ){
		update_post_meta( $post_id, 'show_map', wp_kses( $_POST['show_map'], $allowed ) );
	}else{
		update_post_meta( $post_id, 'show_map', 'off' );
	}
	
	if( isset( $_POST['map_position'] ) ){
		update_post_meta( $post_id, 'map_position', wp_kses( $_POST['map_position'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_designation'] ) ){
		update_post_meta( $post_id, 'team_member_designation', wp_kses( $_POST['team_member_designation'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_image'] ) ){
		update_post_meta( $post_id, 'team_member_image', wp_kses( $_POST['team_member_image'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_address'] ) ){
		update_post_meta( $post_id, 'team_member_address', wp_kses( $_POST['team_member_address'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_office_phone'] ) ){
		update_post_meta( $post_id, 'team_member_office_phone', wp_kses( $_POST['team_member_office_phone'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_mobile_phone'] ) ){
		update_post_meta( $post_id, 'team_member_mobile_phone', wp_kses( $_POST['team_member_mobile_phone'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_fax'] ) ){
		update_post_meta( $post_id, 'team_member_fax', wp_kses( $_POST['team_member_fax'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_email'] ) ){
		update_post_meta( $post_id, 'team_member_email', wp_kses( $_POST['team_member_email'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_facebook_url'] ) ){
		update_post_meta( $post_id, 'team_member_facebook_url', wp_kses( $_POST['team_member_facebook_url'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_twitter_url'] ) ){
		update_post_meta( $post_id, 'team_member_twitter_url', wp_kses( $_POST['team_member_twitter_url'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_linkedin_url'] ) ){
		update_post_meta( $post_id, 'team_member_linkedin_url', wp_kses( $_POST['team_member_linkedin_url'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_google_plus_url'] ) ){
		update_post_meta( $post_id, 'team_member_google_plus_url', wp_kses( $_POST['team_member_google_plus_url'], $allowed ) );
	}
	
	if( isset( $_POST['team_member_description'] ) ){
		update_post_meta( $post_id, 'team_member_description', wp_kses( $_POST['team_member_description'], $allowed ) );
	}
	
	if( isset( $_POST['testimonial_author_name'] ) ){
		update_post_meta( $post_id, 'testimonial_author_name', wp_kses( $_POST['testimonial_author_name'], $allowed ) );
	}
	
	if( isset( $_POST['testimonial_author_image'] ) ){
		update_post_meta( $post_id, 'testimonial_author_image', wp_kses( $_POST['testimonial_author_image'], $allowed ) );
	}
	
	if( isset( $_POST['testimonial_author_designation'] ) ){
		update_post_meta( $post_id, 'testimonial_author_designation', wp_kses( $_POST['testimonial_author_designation'], $allowed ) );
	}
	
	if( isset( $_POST['testimonial_author_short_address'] ) ){
		update_post_meta( $post_id, 'testimonial_author_short_address', wp_kses( $_POST['testimonial_author_short_address'], $allowed ) );
	}
	
	if( isset( $_POST['testimonial_testimonial'] ) ){
		update_post_meta( $post_id, 'testimonial_testimonial', wp_kses( $_POST['testimonial_testimonial'], $allowed ) );
	}
	
	if( isset( $_POST['slider_video'] ) ){
		update_post_meta( $post_id, 'slider_video', wp_kses( $_POST['slider_video'], $allowed ) );
	}
	
	if( isset( $_POST['slider_video_iframe'] ) ){
		update_post_meta( $post_id, 'slider_video_iframe', wp_kses( $_POST['slider_video_iframe'], $allowed ) );
	}
	
	if(isset($_POST['layer'])){
		update_post_meta( $post_id, 'slider_layer', $_POST['layer']);
	}else{
		update_post_meta( $post_id, 'slider_layer', '');
	}
	
	if( isset( $_POST['layer_text_position'] ) ){
		update_post_meta( $post_id, 'layer_text_position', $_POST['layer_text_position']);
	}
	
	if( isset( $_POST['slider_layer_active'] ) ){
		update_post_meta( $post_id, 'slider_layer_active', $_POST['slider_layer_active']);
	}
}