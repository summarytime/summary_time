jQuery('#member_image').click(function(e){
	var $this = e,
	frame = frame || {}, props, image;
	if ('function' === typeof frame.open) {
		frame.open();
		return;
	}
	// Create the media frame.
	frame = wp.media.frames.frame = wp.media({
		title: 'Select Member Image',
		className: 'media-frame tl-pb-builder-uploader',
		button: {
			text: 'Insert Member Image'
		},
		multiple: false
	});
	// When an image is selected, run a callback.
	frame.on('select', function () {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();
		// Remove the attachment caption
		attachment.caption = '';
		// Build the image
		props = wp.media.string.props(
			{},
			attachment
		);
		jQuery('#team_member_image').val(attachment.url);
	});
	frame.open();
});

jQuery('#author_image').click(function(e){
	var $this = e,
	frame = frame || {}, props, image;
	if ('function' === typeof frame.open) {
		frame.open();
		return;
	}
	// Create the media frame.
	frame = wp.media.frames.frame = wp.media({
		title: 'Select Author Image',
		className: 'media-frame tl-pb-builder-uploader',
		button: {
			text: 'Insert Author Image'
		},
		multiple: false
	});
	// When an image is selected, run a callback.
	frame.on('select', function () {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();
		// Remove the attachment caption
		attachment.caption = '';
		// Build the image
		props = wp.media.string.props(
			{},
			attachment
		);
		jQuery('#testimonial_author_image').val(attachment.url);
	});
	frame.open();
});

jQuery('#slider_video').click(function(e){
	var $this = e,
	frame = frame || {}, props, video;
	if ('function' === typeof frame.open) {
		frame.open();
		return;
	}
	// Create the media frame.
	frame = wp.media.frames.frame = wp.media({
		title: 'Select Video',
		className: 'media-frame tl-pb-builder-uploader',
		library : { type : 'video'},
		button: {
			text: 'Insert Video'
		},
		multiple: false
	});
	// When an image is selected, run a callback.
	frame.on('select', function () {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();
		// Remove the attachment caption
		attachment.caption = '';
		// Build the image
		props = wp.media.string.props(
			{},
			attachment
		);
		jQuery('[name="slider_video"]').val(attachment.url);
	});
	frame.open();
});
jQuery(".tl-layer-content").sortable({ items: ".tl-layer-wrapper", distance: 10 });
jQuery('#tl_layer_metabox').hide();
jQuery('#postdivrich').hide();
jQuery('#tl_slider_metabox').hide();
jQuery(document).on('click', '#layer-activate', function(){
	jQuery('#tl_layer_metabox').show();
	jQuery('[name="slider_layer_active"]').val('yes');
	jQuery('#postdivrich').hide();
	jQuery('#tl_slider_metabox').hide();
});
jQuery(document).on('click', '#layer-deactivate', function(){
	jQuery('#tl_layer_metabox').hide();
	jQuery('[name="slider_layer_active"]').val('no');
	jQuery('#postdivrich').show();
	jQuery('#tl_slider_metabox').show();
});

if(jQuery('[name="slider_layer_active"]').val()=='yes'){
	jQuery('#tl_layer_metabox').show();
	jQuery('#postdivrich').hide();
	jQuery('#tl_slider_metabox').hide();
}else{
	jQuery('#tl_layer_metabox').hide();
	jQuery('#postdivrich').show();
	jQuery('#tl_slider_metabox').show();
}
jQuery('.my-color-picker').wpColorPicker();