<?php
function team_post_type()
	{
		$labels = array(
			'name' => __( 'Team','tl'),
			'singular_name' => __( 'Team','tl' ),
			'add_new' => __('Add New Member','tl'),
			'add_new_item' => __('Add A New Member','tl'),
			'edit_item' => __('Edit Member','tl'),
			'new_item' => __('Create Member','tl'),
			'view_item' => __('View Member','tl'),
			'search_items' => __('Search Member','tl'),
			'not_found' =>  __('Sorry, no Member found.','tl'),
			'not_found_in_trash' => __('No Member found in trash.','tl'), 
			'parent_item_colon' => ''
		  );
		  $args = array(
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-groups',
			'supports' => array('title')
		  ); 
		  register_post_type(__( 'team', 'tl' ),$args);

	}
	add_action( 'init', 'team_post_type' );
	
	add_filter( 'manage_edit-team_columns', 'team_columns' ) ;
	add_filter( 'manage_edit-team_sortable_columns', 'team_columns' );

	function team_columns( $columns ) {
		$columns['title'] = 'Member Name';
		$columns['designation'] = 'Designation';
		$columns['image'] = 'Image';
		return $columns;
	}
	
	add_action( 'manage_team_posts_custom_column', 'manage_team_columns', 10, 2 );

	function manage_team_columns( $column, $post_id ) {
		global $post;
	
		switch( $column ) {
			case 'designation' :
				$designation = get_post_meta( $post_id, 'team_member_designation', true );
				if ( empty( $designation ) )
					echo __( '-', 'tl' );
				else
					printf( __( '%s', 'tl' ), $designation );
				break;
			case 'image' :
				global $wpdb;
				$image_url = get_post_meta( $post_id, 'team_member_image', true );
				$post_thumbnail_id = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
				if ($post_thumbnail_id) {
					$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id[0], 'tl-column-preview');
					echo '<img src="'.$post_thumbnail_img[0].'">';
				}
			default :
				break;
		}
	}