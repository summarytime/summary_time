<?php
function testimonial_post_type()
	{
		$labels = array(
			'name' => __( 'Testimonial','tl'),
			'singular_name' => __( 'Testimonial','tl' ),
			'add_new' => __('Add New Testimonial','tl'),
			'add_new_item' => __('Add A New Testimonial','tl'),
			'edit_item' => __('Edit Testimonial','tl'),
			'new_item' => __('Create Testimonial','tl'),
			'view_item' => __('View Testimonial','tl'),
			'search_items' => __('Search Testimonial','tl'),
			'not_found' =>  __('Sorry, no Testimonial found.','tl'),
			'not_found_in_trash' => __('No Testimonial found in trash.','tl'), 
			'parent_item_colon' => ''
		  );
		  $args = array(
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-format-chat',
			'supports' => array('title')
		  ); 
		  register_post_type(__( 'testimonial', 'tl' ),$args);

	}
	add_action( 'init', 'testimonial_post_type' );
	
	add_filter( 'manage_edit-testimonial_columns', 'testimonial_columns' ) ;
	add_filter( 'manage_edit-testimonial_sortable_columns', 'testimonial_columns' );

	function testimonial_columns( $columns ) {
		$columns['title'] = 'Testimonial Title';
		$columns['author'] = 'Author';
		$columns['image'] = 'Image';
		return $columns;
	}
	
	add_action( 'manage_testimonial_posts_custom_column', 'manage_testimonial_columns', 10, 2 );

	function manage_testimonial_columns( $column, $post_id ) {
		global $post;
	
		switch( $column ) {
			case 'author' :
				$name = get_post_meta( $post_id, 'testimonial_author_name', true );
				if ( empty( $name ) )
					echo __( 'Unknown', 'tl' );
				else
					printf( __( '%s', 'tl' ), $testimonial );
				break;
			case 'image' :
				global $wpdb;
				$image_url = get_post_meta( $post_id, 'testimonial_author_image', true );
				$post_thumbnail_id = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
				if ($post_thumbnail_id) {
					$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id[0], 'tl-column-preview');
					echo '<img src="'.$post_thumbnail_img[0].'">';
				}
			default :
				break;
		}
	}