<?php
function gallery_post_type()
	{
		$labels = array(
			'name' => __( 'Gallery','tl'),
			'singular_name' => __( 'Gallery','tl' ),
			'add_new' => __('Create New Gallery','tl'),
			'add_new_item' => __('Create A New Gallery','tl'),
			'edit_item' => __('Edit Gallery','tl'),
			'new_item' => __('Create Gallery','tl'),
			'view_item' => __('View Gallery','tl'),
			'search_items' => __('Search Gallery','tl'),
			'not_found' =>  __('Sorry, no Gallery found.','tl'),
			'not_found_in_trash' => __('No Gallery found in trash.','tl'), 
			'parent_item_colon' => ''
		  );
		  $args = array(
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-images-alt2',
			'supports' => array('title','editor','thumbnail')
		  ); 
		  register_post_type(__( 'gallery', 'tl' ),$args);

	}
	add_action( 'init', 'gallery_post_type' );
	add_action( 'init', 'create_gallery_category_taxonomies', 0 );
	function create_gallery_category_taxonomies() {
    register_taxonomy(
        'gallery_category',
        'gallery',
        array(
            'labels' => array(
                'name' => 'Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

function gallery_columns( $columns ) {
    $columns["featured_image"] = "Gallery Image";
	$columns["category"] = "Category";
    return $columns;
}
add_filter('manage_edit-gallery_columns', 'gallery_columns');
add_filter('manage_edit-gallery_sortable_columns', 'gallery_columns');
function gallery_column( $colname, $pid ) {
     if ( $colname == 'featured_image'){
          $post_thumbnail_id = get_post_thumbnail_id($pid);
		  if ($post_thumbnail_id) {
			$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'tl-column-preview');
			echo '<img src="'.$post_thumbnail_img[0].'">';
		}
	 }
	 
	  if ( $colname == 'category'){
		 $term_list = wp_get_post_terms($pid, 'gallery_category', array("fields" => "all"));
         if(isset($term_list) && !is_wp_error($term_list)){
			 $all_term = array();
			 foreach($term_list as $trm){
				 $all_term[] = '<a href="?post_type=gallery&gallery_category='.$trm->slug.'">'.$trm->name.'</a>';
			 }
			 echo implode(',',$all_term);
		 }
	 }
}
add_action('manage_gallery_posts_custom_column', 'gallery_column', 10, 2);