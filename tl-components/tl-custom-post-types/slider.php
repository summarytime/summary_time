<?php
function slider_post_type()
	{
		$labels = array(
			'name' => __( 'Slider','tl'),
			'singular_name' => __( 'Slider','tl' ),
			'add_new' => __('Create New Slider','tl'),
			'add_new_item' => __('Create A New Slider','tl'),
			'edit_item' => __('Edit Slider','tl'),
			'new_item' => __('Create Slider','tl'),
			'view_item' => __('View Slider','tl'),
			'search_items' => __('Search Slider','tl'),
			'not_found' =>  __('Sorry, no Slider found.','tl'),
			'not_found_in_trash' => __('No Slider found in trash.','tl'), 
			'parent_item_colon' => ''
		  );
		  $args = array(
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_in_menu' => true,
			'show_ui' => true, 
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-slides',
			'rewrite' => array('slug' => 'slider', 'with_front' => true),
			'taxonomies' => array('slider_category'),
			'supports' => array('title','editor','thumbnail')
		  ); 
		  register_post_type(__( 'slider', 'tl' ),$args);

	}
	add_action( 'init', 'slider_post_type' );
	add_action( 'init', 'create_slider_category_taxonomies', 0 );
	function create_slider_category_taxonomies() {
    register_taxonomy(
        'slider_category',
        'slider',
        array(
            'labels' => array(
                'name' => 'Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

function slider_columns( $columns ) {
    $columns["featured_image"] = "Slider Image";
	$columns["category"] = "Category";
	$columns["shortcode"] = "Banner Shortcode";
    return $columns;
}
add_action('manage_slider_posts_custom_column', 'slider_column', 10, 2);

add_filter('manage_edit-slider_columns', 'slider_columns');
add_filter('manage_edit-slider_sortable_columns', 'slider_columns');
function slider_column( $colname, $pid ) {
     if ( $colname == 'featured_image'){
          $post_thumbnail_id = get_post_thumbnail_id($pid);
		  if ($post_thumbnail_id) {
			$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'tl-column-preview');
			echo '<img src="'.$post_thumbnail_img[0].'">';
		}else{
			if(get_post_meta( $pid, 'slider_video', true )){
				echo '<div style="width:55px;height:55px;overflow:hidden;">
                <iframe width="55" height="55" src="'.get_post_meta( $pid, 'slider_video', true ).'" frameborder="0" allowfullscreen></iframe>
                </div>';
			}else{
				if(get_post_meta( $pid, 'slider_video_iframe', true )){
					echo '<div style="width:55px;height:55px;overflow:hidden;">'.get_post_meta( $pid, 'slider_video_iframe', true ).'</div>';
				}
			}
		}
	 }
	 
	 if ( $colname == 'category'){
		 $term_list = wp_get_post_terms($pid, 'slider_category', array("fields" => "all"));
         if(isset($term_list) && !is_wp_error($term_list)){
			 $all_term = array();
			 foreach($term_list as $trm){
				 $all_term[] = '<a href="?post_type=slider&slider_category='.$trm->slug.'">'.$trm->name.'</a>';
			 }
			 echo implode(',',$all_term);
		 }
	 }
	 
	 if ( $colname == 'shortcode'){
		 echo '[tl_banner id="'.$pid.'" layout="full_width" parallax="1"]';
	 }
}

add_filter('manage_edit-slider_category_columns', 'slider_category_columns', 5);
add_filter('manage_edit-slider_category_sortable_columns', 'slider_category_columns', 5);
add_action('manage_slider_category_custom_column', 'slider_category_custom_columns', 5, 3);
function slider_category_columns($defaults) {
	$defaults['slider_category_shortcode'] = __('Slider Shortcode', 'tl');
	return $defaults;
}

function slider_category_custom_columns($value, $column_name, $id) {
	if( $column_name == 'slider_category_shortcode' ) {
		$term= get_term($id, 'slider_category');
		$shortcode = '[tl_slider category="'.$term->slug.'" layout="full_width"]';
		return $shortcode;
	}
}

add_action('media_buttons', 'add_slider_button');
function add_slider_button() {
	$pst = get_post();
	if($pst->post_type=='slider'){
		echo '<a href="javascript:void(0);" id="layer-activate" class="button"><i class="fa fa-plus-circle"></i> Activate Slider Layers</a>';
	}
}