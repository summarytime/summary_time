<?php
require_once 'inc/tl_all_shortcodes.php';
class TLShortcodes{

    public $shortcodes = array(
        'grid',
        'tabs',
        'collapse',
        'alerts',
        'wells',
        'buttons',
        'labels',
        'icons',
        'lead',
        'tooltip',
		'fa_icon',
		'pricing_table',
		'progress_bar',
		'bootstrap_caraousel',
		'item_slider',
    );

    public function __construct() {
        add_action( 'init', array( &$this, 'init' ) );
    }

    function init() {
        if( !is_admin() && !in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) {
			wp_enqueue_style( 'tl_shortcodes', get_template_directory_uri(). '/tl-shortcodes/css/shortcodes.css' );
            wp_enqueue_script('tl_init', get_template_directory_uri(). '/tl-shortcodes/js/init.js' ,array('jquery'),'',true);
        } else {
            wp_enqueue_style( 'tl_admin_style', get_template_directory_uri(). '/tl-shortcodes/css/admin.css' );
        }
        if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
            return;
        }
        if ( get_user_option( 'rich_editing' ) == 'true' ) {
            add_filter( 'mce_external_plugins', array( &$this, 'regfiles' ) );
            add_filter( 'mce_buttons_3', array( &$this, 'regbtns' ) );
        }
    }

    function regbtns( $buttons ) {
        foreach ( $this->shortcodes as &$shortcode ) {
        	array_push( $buttons, 'tl_' . $shortcode );
        }
        return $buttons;
    }

    function regfiles( $plgs) {
        foreach ( $this->shortcodes as &$shortcode ) {
            $plgs[ 'tl_' . $shortcode ] = get_template_directory_uri(). '/tl-shortcodes/js/files/' . $shortcode . '.js';
        }
        return $plgs;
    }
}

$tlcodes = new TLShortcodes();