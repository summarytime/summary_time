tinymce.PluginManager.add('tl_item_slider', function(editor, url) {
    editor.addButton('tl_item_slider', {
        tooltip: 'Item Slider',
        icon: 'tl-item-slider',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Item Slider',
                url: url + '/item_slider.html',
                width: 800,
                height: 600
            });
        }
    });
});