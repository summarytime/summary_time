tinymce.PluginManager.add('tl_grid', function(editor, url) {
    editor.addButton('tl_grid', {
        type: 'menubutton',
        tooltip: 'Grid',
        icon: 'tl-grid',
        menu: [
            { text: '12 Columns', onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-1"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            { text: '6 Columns',  onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-2"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            { text: '4 Columns',  onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-3"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-3"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-3"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-3"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            { text: '3 Columns',  onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-4"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-4"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-4"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            { text: '2 Columns',  onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-6"]Text[/tl_col]<br class="nc"/>[tl_col class="col-sm-6"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            { text: '1 Columns',  onclick: function() { editor.insertContent('[tl_row class="row"]<br class="nc"/>[tl_col class="col-sm-12"]Text[/tl_col]<br class="nc"/>[/tl_row]'); } },
            {
                text: 'Custom Grid',
                onclick: function() {
                    tinymce.activeEditor.windowManager.open({
                        title: 'Custom Grid',
                        url: url + '/grid.html',
                        width: 800,
                        height: 600
                    });
                }
            }
        ]
    });
});