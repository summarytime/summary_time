tinymce.PluginManager.add('tl_tooltip', function(editor, url) {
    editor.addButton('tl_tooltip', {
        tooltip: 'Tooltip',
        icon: 'tl-tooltip',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Tooltip',
                url: url + '/tooltip.html',
                width: 800,
                height: 600
            });
        }
    });
});