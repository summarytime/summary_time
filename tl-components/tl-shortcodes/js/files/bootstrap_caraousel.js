tinymce.PluginManager.add('tl_bootstrap_caraousel', function(editor, url) {
    editor.addButton('tl_bootstrap_caraousel', {
        tooltip: 'Bootstrap Caraousel',
        icon: 'tl-bootstrap-caraousel',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Bootstrap Caraousel',
                url: url + '/bootstrap_caraousel.html',
                width: 800,
                height: 600
            });
        }
    });
});