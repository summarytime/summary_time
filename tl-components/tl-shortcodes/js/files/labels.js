
tinymce.PluginManager.add('tl_labels', function(editor, url) {
    editor.addButton('tl_labels', {
        tooltip: 'Labels',
        icon: 'tl-labels',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Labels',
                url: url + '/labels.html',
                width: 800,
                height: 600
            });
        }
    });
});
