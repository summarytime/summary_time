tinymce.PluginManager.add('tl_buttons', function(editor, url) {
    editor.addButton('tl_buttons', {
        tooltip: 'Buttons',
        icon: 'tl-buttons',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Buttons',
                url: url + '/buttons.html',
                width: 800,
                height: 600
            });
        }
    });
});