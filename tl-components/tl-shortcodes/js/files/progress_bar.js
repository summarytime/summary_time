tinymce.PluginManager.add('tl_progress_bar', function(editor, url) {
    editor.addButton('tl_progress_bar', {
        tooltip: 'Progress Bar',
        icon: 'tl-progress-bar',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Progress Bar',
                url: url + '/progress_bar.html',
                width: 800,
                height: 600
            });
        }
    });
});