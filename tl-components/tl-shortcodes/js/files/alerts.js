tinymce.PluginManager.add('tl_alerts', function(editor, url) {
    editor.addButton('tl_alerts', {
        tooltip: 'Alerts',
        icon: 'tl-alerts',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Add an alert',
                url: url + '/alerts.html',
                width: 800,
                height: 600
            });
        }
    });
});
