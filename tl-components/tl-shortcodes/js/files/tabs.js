tinymce.PluginManager.add('tl_tabs', function(editor, url) {
    editor.addButton('tl_tabs', {
        tooltip: 'Tabs',
        icon: 'tl-tabs',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Tabs',
                url: url + '/tabs.html',
                width: 800,
                height: 600
            });
        }
    });
});