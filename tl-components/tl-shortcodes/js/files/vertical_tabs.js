tinymce.PluginManager.add('tl_vertical_tabs', function(editor, url) {
    editor.addButton('tl_vertical_tabs', {
        tooltip: 'Vertical Tabs',
        icon: 'tl-vertical-tabs',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Vertical Tabs',
                url: url + '/vertical_tabs.html',
                width: 800,
                height: 600
            });
        }
    });
});