tinymce.PluginManager.add('tl_fa_icon', function(editor, url) {
    editor.addButton('tl_fa_icon', {
        tooltip: 'Font Awesome Icons',
        icon: 'tl-fa-icon',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Add a fa icon',
                url: url + '/fa_icon.html',
                width: 800,
                height: 600
            });
        }
    });
});
