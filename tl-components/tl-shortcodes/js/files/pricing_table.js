tinymce.PluginManager.add('tl_pricing_table', function(editor, url) {
    editor.addButton('tl_pricing_table', {
        tooltip: 'Pricing Table',
        icon: 'tl-pricing-table',
        onclick: function() {
            tinymce.activeEditor.windowManager.open({
                title: 'Pricing Table',
                url: url + '/pricing_table.html',
                width: 800,
                height: 600
            });
        }
    });
});