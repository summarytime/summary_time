tinymce.PluginManager.add('tl_icons', function(editor, url) {
    editor.addButton('tl_icons', {
        tooltip : 'Icons',
        icon : 'tl-icons',
        onclick : function() {
            tinymce.activeEditor.windowManager.open({
                title : 'Icons',
                url : url + '/icons.html',
                width : 800,
                height : 600
            });
        }
    });
});
