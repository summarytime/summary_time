tinymce.PluginManager.add('tl_lead', function(editor, url) {
    editor.addButton('tl_lead', {
        tooltip: 'Lead',
        icon: 'tl-lead',
        onclick: function() {
            editor.insertContent('[tl_lead]This is a lead text and needs your attention.[/tl_lead]');
        }
    });
});