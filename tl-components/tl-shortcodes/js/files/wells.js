tinymce.PluginManager.add('tl_wells', function(editor, url) {
    editor.addButton('tl_wells', {
        type: 'menubutton',
        tooltip: 'Well',
        icon: 'tl-wells',
        menu: [
            { text: 'Small well',  onclick: function() { editor.insertContent('[tl_well size="sm"]This well needs your attention.[/tl_well]'); } },
            { text: 'Medium well', onclick: function() { editor.insertContent('[tl_well size="md"]This well needs your attention.[/tl_well]'); } },
            { text: 'Large well',  onclick: function() { editor.insertContent('[tl_well size="lg"]This well needs your attention.[/tl_well]'); } }
        ]
    });
});