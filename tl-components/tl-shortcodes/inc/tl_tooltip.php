<?php
function tl_tooltip( $params, $content=null ) {
	ob_start();
    extract( shortcode_atts( array(
        'placement' => 'top',
        'trigger' => 'hover',
        'tooltip_title' => '',
		'tooltip_content' => '',
		'class' => '',
    ), $params ) );

    $placement = (in_array( $placement, array( 'top', 'right', 'bottom', 'left' ) ))? $placement: 'top';
	$trigger = (in_array( $trigger, array( 'click', 'hover', 'focus' ) ))? $trigger: 'hover';
?>
<a href="javascript:void(0);" class="<?php echo $class;?>" data-toggle="popover" title="<?php echo $tooltip_title;?>" data-container="body" data-content="<?php echo $tooltip_content;?>" data-placement="<?php echo $placement;?>" data-trigger="<?php echo $trigger;?>"><?php echo $content;?></a>
<?php
   	$myvariable = ob_get_clean();
    return $myvariable;
}
add_shortcode( 'tl_tooltip', 'tl_tooltip' );
