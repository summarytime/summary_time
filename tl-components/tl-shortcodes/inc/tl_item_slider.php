<?php
function tl_item_slider_shortcode_wp_enqueue_scripts() {
    //wp_register_style( 'tlpb-team-carousel', get_template_directory_uri() . '/includes/css/owl.carousel.min.css', array(), '2.1.1', false);
	//wp_register_script( 'tlpb-team-carousel', get_template_directory_uri() . '/includes/js/owl.carousel.min.js', array(), '2.1.1', false);
}

add_action( 'wp_enqueue_scripts', 'tl_item_slider_shortcode_wp_enqueue_scripts' );
add_shortcode( 'tl_item_slider', 'tl_item_slider_shortcode' );
function tl_item_slider_shortcode( $atts, $content ) {
	ob_start();
	extract( shortcode_atts( array (
	   'items'=>'',
    ), $atts ) );
	$id = uniqid('owl-caraousel-');
?>
<div class="owl-carousel owl-theme" id="<?php echo $id;?>">
	<?php echo do_shortcode($content);?>
</div>
<script>
jQuery('#<?php echo $id;?>').owlCarousel({
	center: false,
	items:<?php echo $items;?>,
	loop:true,
	margin:10,
	autoplay:true,
	autoplayTimeout:2000,
	autoplayHoverPause:false,
	nav: true,
	navText: [
		'<i class="fa fa-angle-left"></i>',
		'<i class="fa fa-angle-right"></i>'
	  ],
	responsiveClass:true,
	responsive:{
		0:{
			items:2,
		},
		360:{
			items:<?php echo ($items-3>0)?$items-3:2;?>,
		},
		600:{
			items:<?php echo ($items-2>0)?$items-2:2;?>,
		},
		1000:{
			items:<?php echo ($items>0)?$items:5;?>,
		}
	}
});
</script>
<?php
	$myvariable = ob_get_clean();
    return $myvariable;
}
add_shortcode( 'tl_item_slides', 'tl_item_slides_shortcode' );
function tl_item_slides_shortcode( $atts, $content ) {
	ob_start();
	extract( shortcode_atts( array (
	   'image'=>'',
	   'alt'=>''
    ), $atts ) );
?>
<div class="item">
	<?php if($image):?>
    <?php $image_id = tl_get_image_id($image);?>
    <?php if(isset($image_id) && $image_id){
		echo wp_get_attachment_image( $image_id, $size, "", array( "class" => "img-responsive" ) );
	}else{
		echo '<img src="'.$image.'" alt="'.$alt.'" />';
	}
	?>
    <?php endif;?>
    <?php if($content):?>
    <?php echo wpautop($content);?>
    <?php endif;?>
</div>
<?php
	$myvariable = ob_get_clean();
    return $myvariable;
}