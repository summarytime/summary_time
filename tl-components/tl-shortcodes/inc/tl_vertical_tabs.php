<?php
add_shortcode( 'vertical_tabs', 'vertical_tabs_shortcode' );
function vertical_tabs_shortcode( $atts, $content ) {
    ob_start();
?>
<div class="row">
	<div class="vtab">
    <?php echo do_shortcode($content);?>
    </div>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'vertical_tab_lis', 'vertical_tab_lis_shortcode' );
function vertical_tab_lis_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'class'=>'',
    ), $atts ) );
?>
<div class="col-xs-3">
	<ul class="nav nav-tabs <?php echo $class;?>">
    <?php echo do_shortcode($content);?>
    </ul>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'vertical_tab_li', 'vertical_tab_li_shortcode' );
function vertical_tab_li_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'target'=>'',
	   'class'=>'',
    ), $atts ) );
?>
<li class="<?php echo $class;?>"><a href="#<?php echo $target;?>" data-toggle="tab"><?php echo $content;?></a></li>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'vertical_tab_contents', 'vertical_tab_contents_shortcode' );
function vertical_tab_contents_shortcode( $atts, $content ) {
    ob_start();
?>
<div class="col-xs-9">
	<div class="tab-content">
	<?php echo do_shortcode($content);?>
	</div>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'vertical_tab_content', 'vertical_tab_content_shortcode' );
function vertical_tab_content_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'target'=>'',
	   'class'=>'',
    ), $atts ) );
?>
<div class="tab-pane <?php echo $class;?>" id="<?php echo $target;?>"><?php echo $content;?></div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}