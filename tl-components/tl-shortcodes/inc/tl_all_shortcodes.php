<?php
include 'tl_grid.php';
include 'tl_tabs.php';
include 'tl_collapse.php';
include 'tl_alert.php';
include 'tl_well.php';
include 'tl_buttons.php';
include 'tl_labels.php';
include 'tl_icons.php';
include 'tl_lead.php';
include 'tl_tooltip.php';
include 'tl_fa_icon.php';
include 'tl_slider.php';
include 'tl_banner.php';
include 'tl_pricing_table.php';
include 'tl_progress_bar.php';
include 'tl_bootstrap_caraousel.php';
include 'tl_item_slider.php';