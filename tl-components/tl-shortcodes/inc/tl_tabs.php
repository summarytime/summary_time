<?php
add_shortcode( 'tabs', 'tabs_shortcode' );
function tabs_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'type'=>'',
    ), $atts ) );
?>
<div class="tl-<?php echo $type;?>-tab <?php echo $type;?>-icon-text <?php echo $type;?>-tab-icon">
    <div id="<?php echo $type;?>Tab">
    <?php echo do_shortcode($content);?>
    </div>
</div>
<script>
jQuery(document).ready(function () {
<?php if($type=='horizontal'):?>
jQuery('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = jQuery(this);
var $info = jQuery('#tabInfo');
var $name = jQuery('span', $info);
$name.text($tab.text());
$info.show();
}
});
<?php endif;?>
<?php if($type=='vertical'):?>
jQuery('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
<?php endif;?>
});
</script>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'tab_lis', 'tab_lis_shortcode' );
function tab_lis_shortcode( $atts, $content ) {
    ob_start();
?>
<ul class="resp-tabs-list">
<?php echo do_shortcode($content);?>
</ul>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'tab_li', 'tab_li_shortcode' );
function tab_li_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'icon'=>'',
    ), $atts ) );
?>
<li><i class="<?php echo $icon;?>"></i> <span class="tl-tab-title1"><?php echo $content;?></span></li>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'tab_contents', 'tab_contents_shortcode' );
function tab_contents_shortcode( $atts, $content ) {
    ob_start();
?>
<div class="resp-tabs-container">
<?php echo do_shortcode($content);?>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'tab_content', 'tab_content_shortcode' );
function tab_content_shortcode( $atts, $content ) {
    ob_start();
?>
<div><?php echo wpautop($content);?></div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}