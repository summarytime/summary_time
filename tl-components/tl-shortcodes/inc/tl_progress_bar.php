<?php
add_shortcode( 'tl_progress_bars', 'tl_progress_bars_shortcode' );
function tl_progress_bars_shortcode( $atts, $content ) {
	ob_start();
	extract( shortcode_atts( array (
	   'id'=>'',
    ), $atts ) );
?>
<div id="<?php echo $id;?>">
	<?php echo do_shortcode($content);?>
</div>
<script>
//jQuery('#<?php //echo $id;?>').progress_fnc();
</script>
<?php
	$myvariable = ob_get_clean();
    return $myvariable;
}
add_shortcode( 'tl_progress_bar', 'tl_progress_bar_shortcode' );
function tl_progress_bar_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'layout'=>'',
	   'number'=>'',
	   'color'=>'',
    ), $atts ) );
	
		list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
		$hex = $r.', '.$g.', '.$b;
        $opacity = 0.5;
        $output = 'rgba('.$hex.','.$opacity.')';
?>
<?php if($layout=='layout-1'):?>
<div class="tl-barWrapper"><span class="progressText"><b><?php echo $content;?></b></span><div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $number;?>" aria-valuemin="0" aria-valuemax="100" style="background-color:<?php echo $color;?>;"><span class="popOver" data-toggle="tooltip" data-placement="top" title="<?php echo $number;?>%"></span></div></div></div>
<?php endif;?>
<?php if($layout=='layout-2'):?>
<div class="cssProgress"><span class="progressText"><b><?php echo $content;?></b></span><div class="progress2"><div class="cssProgress-bar cssProgress-success cssProgress-active" data-percent="<?php echo $number;?>" data-color="<?php echo $output;?>"><span class="cssProgress-label"><?php echo $number;?>%</span></div></div></div>
<?php endif;?>
<?php if($layout=='layout-3'):?>
<div class="cssProgress"><span class="progressText"><b><?php echo $content;?></b></span><div class="progress4 cssProgress-bg"><div class="cssProgress-bar cssProgress-glow" data-percent="<?php echo $number;?>" data-color="<?php echo $output;?>" style="width: <?php echo $number;?>%;"></div></div></div>
<?php endif;?>
<?php if($layout=='layout-4'):?>
<div class="cssProgress"><span class="progressText"><b><?php echo $content;?></b></span><div class="progress1"><div class="cssProgress-bar cssProgress-active" data-percent="<?php echo $number;?>" data-color="<?php echo $output;?>" style="width: <?php echo $number;?>%;"><span class="cssProgress-label"><?php echo $number;?>%</span></div></div></div>
<?php endif;?>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}