<?php
add_shortcode( 'pricing_tables', 'pricing_tables_shortcode' );
function pricing_tables_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'layout'=>'',
    ), $atts ) );
	if($layout=='layout-1'){
		$class = 'tl-pricing-table-101';
	}else{
		if($layout=='layout-2'){
			$class = 'tl-pricing-table-101 tl-pricing-table-102';
		}else{
			if($layout=='layout-3'){
				$class = 'tl-pricing-table-101 tl-pricing-table-102 tl-pricing-flat-103';
			}else{
				if($layout=='layout-4'){
					$class = 'tl-pricing-table-101 tl-pricing-table-102 tl-pricing-flat-103 tl-pricing-circle-104';
				}else{
					$class = '';
				}
			}
		}
	}
?>
<div class="tl-pricing-table <?php echo $class;?>">
<?php echo do_shortcode($content);?>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}

add_shortcode( 'pricing_table', 'pricing_table_shortcode' );
function pricing_table_shortcode( $atts, $content ) {
    ob_start();
	extract( shortcode_atts( array (
	   'column'=>'',
	   'package'=>'',
	   'type'=>'',
	   'featured_text'=>'',
	   'price'=>'',
	   'per'=>'',
	   'button_text'=>'',
	   'button_url'=>'',
    ), $atts ) );
	$cls = 12/$column;
?>
<div class="col-sm-<?php echo $cls;?> col-md-<?php echo $cls;?> col-lg-<?php echo $cls;?>">	
  <div class="tl-pricing-item <?php echo $type=='featured'?'pricing-featured':'';?>">
    <?php if($type=='featured'):?>
    <div class='selected'><?php echo $featured_text;?></div>
    <?php endif;?>
    <div class="tl-pricing-title">
     <?php echo $package;?>
    </div>
    <div class="pricing-value"><?php echo $price;?>
    <span class="undertext"><?php echo $per;?></span>
    </div>
    <?php $con = explode('|',$content);?>
    <ul class="pricing-features">
     <?php foreach($con as $cont):?>
     <li><?php echo $cont;?></li>
     <?php endforeach;?>
    </ul>
    <?php if($button_text):?>
    <a class="btn btn-raised button tl_pricing_btn" href="<?php echo $button_url;?>"><?php echo $button_text;?></a>
    <?php endif;?>
  </div>
</div>
<?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}
