<?php
add_shortcode( 'tl_banner', 'tl_banner_shortcode' );
function tl_banner_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'id'=>'0',
	   'layout'=>'full_width',
	   'parallax'=>'1',
    ), $atts ) );
 ?>
  	<?php if($parallax==0){ $stl = "background-attachment:scroll;";}else{$stl = "";}?>
    
   <?php if($layout=='box'):?>
    <div class="container">
    <?php endif;?>
    <?php $banner = get_post($id);?>
    <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($id) );?>
    <?php if(isset($feat_image) && $feat_image):?>
	<div id="tl-slider-bg" style="<?php echo $stl;?>background-image:url('<?php echo $feat_image;?>');">
    <?php else:?>
    <?php if(get_post_meta( $id, 'slider_video', true )):?>
    <?php // if($parallax==1):?>
    <!--<div class="tl-video-parallaxBg-mp4">
	    <div class="crt-parallaxvideo ccparallax-one">
			<video autoplay muted loop>
			<source src="<?php //echo get_post_meta( $id, 'slider_video', true );?>" type="video/mp4">
			</video>
		</div>
		
		<div class="video-wrapper-content">
			<div class="container">
				<?php //echo wpautop(do_shortcode($banner->post_content));?>
			</div>
		</div>
	  <div class="video-ovaly-border"></div>
	  </div>-->
    <?php //else:?>
    <div class="tl-video-one">
        <figure class="video-inner">
          <video loop muted="muted" autoplay>
            <source src="<?php echo get_post_meta( $id, 'slider_video', true );?>" type="video/mp4">
          </video>
          <div class="video-ovaly-border"></div>
        </figure>
        <div class="video-wrapper-content">
        	<div class="tl-video-content-inner">
            <?php echo wpautop(do_shortcode($banner->post_content));?>
            </div>
        </div>
    </div>
    <?php //endif;?>
    <?php else:?>
    <?php if(get_post_meta( $id, 'slider_video_iframe', true )):?>
    <?php //if($parallax==1):?>
	<!--<div id="youtube" class="bg_parallax" data-type="background" data-speed="10">
         <div id="down_video_bg" class="tl-iframe-videoBg">
             <?php //echo get_post_meta( $id, 'slider_video_iframe', true );?>
         </div>
        
        <div class="youtube-video-content">
            <?php //echo wpautop(do_shortcode($banner->post_content));?>
        </div>
        
        <div class="video-ovaly-border"></div>
        <div class="clearfix"></div>
    </div>-->
    <?php //else:?>
    <div class="tl-video-one">
        <div class="video-container">
            <div class="video-foreground">
            <?php echo get_post_meta( $id, 'slider_video_iframe', true );?>
            </div>
            <div class="video-ovaly-border"></div>
        </div>
        <div class="video-wrapper-content">
            <?php echo wpautop(do_shortcode($banner->post_content));?>
        </div>
    </div>
    <?php //endif;?>
    <?php endif;?>
    <?php endif;?>
    <?php endif;?>
    		<?php if((isset($feat_image) && $feat_image) || (!get_post_meta( $id, 'slider_video', true ) && !get_post_meta( $id, 'slider_video_iframe', true ))):?>
            <div class="container">
                <div class="row">
                <div class="col-sm-12">	
                     <div class="tl-slider-fix-back2 clearfix">
                        <div class="col-lg-12">
                              <?php echo wpautop(do_shortcode($banner->post_content));?>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        	<?php endif;?>
    <?php if(isset($feat_image) && $feat_image):?>  
	</div>
    <?php endif;?>
    <?php if($layout=='box'):?>
    </div>
    <?php endif;?>
    <?php wp_reset_query();?>

 <?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}
?>