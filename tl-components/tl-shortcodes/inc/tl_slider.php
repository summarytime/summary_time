<?php
add_shortcode( 'tl_slider', 'tl_slider_shortcode' );
function tl_slider_shortcode( $atts, $content ) {
    ob_start();
    extract( shortcode_atts( array (
       'category'=>'unknown',
	   'layout'=>'full_width',
    ), $atts ) );
	$key = uniqid();
 ?>
  <?php 
			$tax_query = array();
			$tax_query[] = array(
								'taxonomy' => 'slider_category',
								'field' => 'slug',
								'terms' => $category
							);
			$args = array(
						'post_type' => 'slider',
						'tax_query' => $tax_query
				 );
				 $loop = new WP_Query($args);
				 if($loop->have_posts()) {
	?>
    <?php if($layout=='box'):?>
    <div class="container">
    <?php endif;?>
	<div class="tl-slider-slide tl-full-slider">
		
		<!-- Carousel -->
    	<div id="caraousel-<?php echo $key;?>" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
            	<?php $i=0;?>
            	<?php while($loop->have_posts()) : $loop->the_post();?>
			  	<li data-target="#caraousel-<?php echo $key;?>" data-slide-to="<?php echo $i;?>" <?php echo ($i==0)?'class="active"':'';?>></li>
				<?php $i++;?>
				<?php endwhile;?>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
            	<?php $i=0;?>
            	<?php while($loop->have_posts()) : $loop->the_post();?>
			    <div class="item <?php echo ($i==0)?'active':'';?>">
			    	<?php the_post_thumbnail();?>
                    <!-- Static Header -->
                    <div class="tl-header-text">
                        <div class="col-md-12 text-center">
                            <?php the_content();?>
                        </div>
                    </div><!-- /header-text -->
			    </div>
                <?php $i++;?>
                <?php endwhile;?>
                <?php wp_reset_query();?>
			    
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#caraousel-<?php echo $key;?>" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#caraousel-<?php echo $key;?>" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div><!-- /carousel -->
	
    </div>
    <?php if($layout=='box'):?>
    </div>
    <?php endif;?>
<?php }?>
 <?php
 	$myvariable = ob_get_clean();
    return $myvariable;
}
?>