<?php
add_shortcode( 'tl_bootstrap_caraousel', 'tl_bootstrap_caraousel_shortcode' );
function tl_bootstrap_caraousel_shortcode( $atts, $content ) {
	ob_start();
	extract( shortcode_atts( array (
	   'slides'=>'',
    ), $atts ) );
	$id = 'caraousel-'.time();
?>
<div id="<?php echo $id;?>" class="carousel slide" data-ride="carousel">
	<?php if($slides && $slides>0):?>
	<ol class="carousel-indicators">
    	<?php for($i=0;$i<($slides);$i++):?>
        <li data-target="#<?php echo $id;?>" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0)?'active':'';?>"></li>
        <?php endfor;?>
    </ol>
    <?php endif;?>
    <div class="carousel-inner" role="listbox">
    	<?php echo do_shortcode($content);?>
    </div>
    <a class="left carousel-control" href="#<?php echo $id;?>" role="button" data-slide="prev">
    	<span class="icon-prev" aria-hidden="true"></span>
    	<span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#<?php echo $id;?>" role="button" data-slide="next">
    	<span class="icon-next" aria-hidden="true"></span>
    	<span class="sr-only">Next</span>
    </a>
</div>
<script>
var car_width = jQuery('#<?php echo $id;?>').find('.item').first().width();
var car_height = jQuery('#<?php echo $id;?>').find('.item img.wp-post-image').first().height();
jQuery('#<?php echo $id;?>').find('.item').each(function(){
	jQuery(this).find('img.wp-post-image').attr('sizes','(max-width: '+car_width+'px) 100vw, '+car_width+'px');
	jQuery(this).find('img.wp-post-image').attr('width',car_width);
	jQuery(this).find('img.wp-post-image').attr('height',car_height);
	//jQuery(this).attr('height',jQuery(this).find('img.wp-post-image').height());
});
</script>
<?php
	$myvariable = ob_get_clean();
    return $myvariable;
}
add_shortcode( 'tl_bootstrap_slides', 'tl_bootstrap_slides_shortcode' );
function tl_bootstrap_slides_shortcode( $atts, $content ) {
	ob_start();
	extract( shortcode_atts( array (
	   'image'=>'',
	   'alt'=>'',
	   'class'=>''
    ), $atts ) );
?>
<div class="item <?php echo ($class)?$class:'';?>">
	<?php if($image):?>
    <?php $image_id = tl_get_image_id($image);?>
    <?php if(isset($image_id) && $image_id){
		echo wp_get_attachment_image( $image_id, 'large', "", array( "class" => "caraousel-img" ) );
	}else{
		echo '<img src="'.$image.'" alt="'.$alt.'" />';
	}
	?>
    <?php endif;?>
    <div class="carousel-caption">
    	<?php echo $content;?>
  	</div>
</div>
<?php
	$myvariable = ob_get_clean();
    return $myvariable;
}