<?php
/**
 * Template Name: Contact
 *
 *
 * @package tl
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>


		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); $bc_bg_class	=	tlf_get_option('title_bg_color','');  ?> 
		
    <div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>"<?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
        <div class="container">
		<?php if(get_post_meta( get_the_ID(), 'tl_hide_title', true )!='on'):?>
		
		
            <h1 class="page-title <?php echo (get_post_meta( get_the_ID(), 'tl_title_position', true ))?get_post_meta( get_the_ID(), 'tl_title_position', true ):'';?>">
            <?php the_title(); ?>
            </h1>
			
			<?php endif;
	
			?>
			<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>
        </div>
	</div>


		
		
		
		
<div class="tl-main-outer-wrapper">

<div class="main-content tl-page-fullwidth">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>


	<div class="container">
		<div class="row">
		<div class="tl-cp-common-wrapper clearfix">
			<div id="content" class="main-content-inner tl-cp-main-left1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="tl-contact-title">
                <?php if(get_post_meta( get_the_ID(), 'contact_title', true )):?>
                <h2><?php echo get_post_meta( get_the_ID(), 'contact_title', true );?></h2>
                <?php endif;?>
                <?php if(get_post_meta( get_the_ID(), 'contact_subtitle', true )):?>
                <h5><?php echo get_post_meta( get_the_ID(), 'contact_subtitle', true );?></h5>
                <?php endif;?>
                </div>
                <!---g-map---->
                <?php if(get_post_meta( get_the_ID(), 'show_map', true )=='on' && get_post_meta( get_the_ID(), 'map_position', true )=='top'):?>
                <div class="tl-google-map"><i class="fa fa-map-marker"></i></div>
                <div class="g-location">
                <div id="embedded-map-canvas">
                <?php echo do_shortcode('[wp_gmaps address="'.get_post_meta( get_the_ID(), 'contact_address', true ).'" height="150px" marker="1" infowindow="true" zoom="15"]');?>
                </div>
                </div>
                <?php endif;?>
                <div class="tl-common-gap text-center col-sm-12">
                <?php the_content();?>
                </div>
                <div class="tl-contact-info-wrapper clearfix">
				<div class="tl-message-block">
					<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="tl-row">
					<?php if(get_post_meta( get_the_ID(), 'contact_address_title', true )):?>
					<h3><?php echo get_post_meta( get_the_ID(), 'contact_address_title', true );?></h3>
					<?php endif;?>
					<ul class="contact-info">
					<?php if(get_post_meta( get_the_ID(), 'contact_address', true )):?>
					<li>
					<div class="ci-icons"><i class="fa fa-home"></i></div>
					<address><?php echo get_post_meta( get_the_ID(), 'contact_address', true );?></address>
					</li>
					<?php endif;?>
					<?php if(get_post_meta( get_the_ID(), 'contact_email', true )):?>
					<li>
					<div class="ci-icons"><i class="fa fa-envelope"></i></div>
					<address><a href="mailto:<?php echo get_post_meta( get_the_ID(), 'contact_email', true );?>"><?php echo get_post_meta( get_the_ID(), 'contact_email', true );?></a></address>
					</li>
					<?php endif;?>
					<?php if(get_post_meta( get_the_ID(), 'contact_phone', true )):?>
					<li>
					<div class="ci-icons"><i class="fa fa-phone"></i></div>
					<address><a href="tel:<?php echo get_post_meta( get_the_ID(), 'contact_phone', true );?>"><?php echo get_post_meta( get_the_ID(), 'contact_phone', true );?></a></address>
					</li>
					<?php endif;?>
					<?php if(get_post_meta( get_the_ID(), 'contact_website', true )):?>
					<li>
					<div class="ci-icons"><i class="fa fa-globe"></i></div>
					<address><a href="<?php echo get_post_meta( get_the_ID(), 'contact_website', true );?>"><?php echo get_post_meta( get_the_ID(), 'contact_website', true );?></a></address></li>
					<?php endif;?>
					</ul>
					</div>
                </div>
                </div>
				<div class="tl-message-block">
						<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="tl-row">
						<?php if(get_post_meta( get_the_ID(), 'contact_form', true )):?>
						<?php echo do_shortcode(get_post_meta( get_the_ID(), 'contact_form', true ));?>
						<?php endif;?>
						</div>
					</div>
				</div>
                </div>
                <?php if(get_post_meta( get_the_ID(), 'show_map', true )=='on' && get_post_meta( get_the_ID(), 'map_position', true )=='bottom'):?>
                <div class="map-bottom">
                <div class="tl-google-map"><i class="fa fa-map-marker"></i></div>
                <div class="g-location">
                <div id="embedded-map-canvas">
                <?php echo do_shortcode('[wp_gmaps address="'.get_post_meta( get_the_ID(), 'contact_address', true ).'" height="150px" marker="1" infowindow="true" zoom="15"]');?>
                </div>
                </div>
                </div>
                <?php endif;?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- tl-cp-common-wrapper -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
</div><!-- close srishti-outer-wrapper end  -->

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
