<?php
/**
 * The Template for displaying all single posts.
 *
 * @package tl
 */

get_header(); ?>
		<?php $bc_bg_color	=	tlf_get_option('breadcrumb_background_color',''); ?>
		<?php $bc_bg_position	=	tlf_get_option('breadcrumb_position',''); ?>
<div class="tl-single-outer-wrapper">
<div class="archive-page-header" <?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>
    <div class="container">
	<div class="tl-breadcrumb-page-title">
        <header>
            <h1 class="page-title">
            <?php the_title(); ?>
            </h1>
           
            <?php
			
			?>
        </header><!-- .entry-header -->
		 </div><!-- .tl-breadcrumb-page-title -->
		 
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
			if(tlf_get_option('show_breadcrumb','') ==1)
				{
					tl_custom_breadcrumb();
				}
			?>
			</div>	
		</div>
	</div>
    </div>
</div>

<div class="main-content main-content-padding">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">			
			<div class="tl-contentsingle-wrapper">			
					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="single-content-post-wrapper">
                            <div class="entry-content">
                                <div class="entry-content-thumbnail">
                                    <?php the_post_thumbnail('single-img'); ?>
                                </div>
                                <div class="entry-meta">
                                </div><!-- .entry-meta -->
                                <div class="tl-singlepost-article">
                                <?php the_content(); ?>
                                </div>
                                <?php tl_link_pages(); ?>
                            </div><!-- .entry-content -->
                        </div><!-- .single-content-post-wrapper -->
                        
                            <footer class="entry-meta">
                        
                                <?php edit_post_link( __( 'Edit', 'tl' ), '<span class="edit-link">', '</span>' ); ?>
                            </footer><!-- .entry-meta -->
                        </article>
						
						<?php tl_content_nav( 'nav-below' ); ?>
                        

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() && tlf_get_option('show_comment_single_page',''))
								comments_template();
						?>

					<?php endwhile; // end of the loop. ?>
				</div>
			
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <div class="col-sm-12 col-md-4">
            	<?php get_sidebar(); ?>
            </div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->


</div><!-- close .srishti-outer-wrapper -->
<?php get_footer(); ?>