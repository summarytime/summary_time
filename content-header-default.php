<?php if(tlf_get_option('show_header_top','')):?>
<div class="<?php echo (tlf_get_option('header_top_background_color_scheme','') && tlf_get_option('header_top_background_color_scheme','')!='default')?tlf_get_option('header_top_background_color_scheme'):'tl-naviwrap-color';?>">
    <div class="tl-top-naviwrap">
		<div class="naviwrap-inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
                    	<div class="<?php echo (tlf_get_option('header_top_text_color_scheme','') && tlf_get_option('header_top_text_color_scheme','')!='default')?tlf_get_option('header_top_text_color_scheme'):'tl-phone-color';?>">
						<div class="pull-left tl-phones">
							<ul class="tl-phone">
								<?php 
								$email_id= tlf_get_option( 'email_id', '' );
								$phone_number= tlf_get_option( 'phone_number', '' );
								?>
								<?php if(tlf_get_option('show_phone_number','')):?>
								<li><a href="tel:<?php echo $phone_number; ?>"><i class="fa fa-phone"></i><?php echo $phone_number; ?></a></li>
                                <?php endif;?>
                                <?php if(tlf_get_option('show_email','')):?>
								<li><a href="mailto:<?php echo $email_id; ?>" target="_top"><i class="fa fa-envelope"></i><?php echo $email_id; ?></a></li>
                                <?php endif;?>
							</ul>
						</div>
                        </div>
						<div class="clearfix"></div>
					</div>
                    <?php if(tlf_get_option('show_social_icon','')):?>
					<div class="col-sm-6 col-md-6">
                    	<div class="<?php echo (tlf_get_option('header_top_text_color_scheme','') && tlf_get_option('header_top_text_color_scheme','')!='default')?tlf_get_option('header_top_text_color_scheme'):'tl-social-color';?>">
						<div class="pull-right tl-social">
							<ul class="tl-phone">
							<?php 
								$facebook= tlf_get_option( 'facebook_url', '' );
								$twitter= tlf_get_option( 'twitter_url', '' );
								$linkedin= tlf_get_option( 'linkedin_url', '' );
								$youtube= tlf_get_option( 'youtube_url', '' );
								$instagram= tlf_get_option( 'instagram_url', '' );
								$gplus= tlf_get_option( 'google_plus_url', '' );
								$dribble= tlf_get_option( 'dribble_url', '' );
								$tumbler= tlf_get_option( 'tumblr_url', '' );
								$pintrest=  tlf_get_option( 'pinterest_url', '' );
								?>
								
								<?php if(!empty($facebook)) {?>
								  <li><a class="facebook" href="<?php echo $facebook;?>"><i class="fa fa-facebook"></i></a></li>
								<?php } if(!empty($twitter)) {?>  
								  <li><a class="twitter" href="<?php echo $twitter;?>"><i class="fa fa-twitter"></i></a></li>
								<?php } if(!empty($linkedin)) { ?> 
								  <li><a class="linkedin" href="<?php echo $linkedin;?>"><i class="fa fa-linkedin"></i></a></li>
								<?php } if(!empty($instagram)) { ?>
								  <li><a class="instagram" href="<?php echo $instagram;?>"><i class="fa fa-instagram"></i></a></li>
								   <?php } if(!empty($youtube)) { ?>
								<li><a class="youtube" href="<?php echo $youtube;?>"><i class="fa fa-youtube"></i></a></li>
								 <?php } if(!empty($gplus)) { ?>
								<li><a class="gplus" href="<?php echo $gplus;?>"><i class="fa fa-google-plus"></i></a></li>
								
							  
							   <?php } ?>
							 </ul>
							<div class="clearfix"></div>
						</div>
                        </div>
					</div>
                    <?php endif;?>
				</div>
			</div>
		</div>
    </div>
</div>
<?php endif;?>
<div class="tl-nav-normal <?php echo (tlf_get_option('header_bottom_background_color_scheme','') && tlf_get_option('header_bottom_background_color_scheme','')!='default')?tlf_get_option('header_bottom_background_color_scheme'):'color-cyan';?>">
<?php $header_text_color = tlf_get_option('header_bottom_text_color_scheme','') && tlf_get_option('header_bottom_text_color_scheme','')!='default'?tlf_get_option('header_bottom_text_color_scheme'):'font_white';?>
<div class="tl-main-navbar-wrapper">
	<header>
		<nav class="navbar navbar-default">
		  <div class="container">
			<div class="navbar-header <?php echo $header_text_color;?>">
			   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="sr-only"><?php _e('Toggle navigation','tl') ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
              	<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>"> 
                <?php if(tlf_get_option( 'logo_position', '' )=='left'){
							$logo_cls = 'pull-left';
					}elseif(tlf_get_option( 'logo_position', '' )=='right'){
							$logo_cls = 'pull-right';
					}elseif(tlf_get_option( 'logo_position', '' )=='center'){
							$logo_cls = 'text-center';
					}else{
							$logo_cls = '';
					}
					if(tlf_get_option( 'description_position', '' )=='left'){
							$desc_cls = 'pull-left';
					}elseif(tlf_get_option( 'logo_position', '' )=='right'){
							$desc_cls = 'pull-right';
					}elseif(tlf_get_option( 'logo_position', '' )=='center'){
							$desc_cls = 'text-center';
					}else{
							$desc_cls = '';
					}
				?>
				<?php if(tlf_get_option( 'logo_uploader', '' )):?>
				
					<?php 
					$image_id = tl_get_image_id(tlf_get_option( 'logo_uploader', '' ));
					?>
                    <?php echo wp_get_attachment_image( $image_id, array('200','60'), "", "" );  ?>
					<?php /*?>
                	<?php 
					$image_id = tl_get_image_id(tlf_get_option( 'logo_uploader', '' ));
					$image_thumb = wp_get_attachment_image_src($image_id, 'logo-img');
					?>
                    <img src="<?php echo $image_thumb[0];?>" class="<?php echo $logo_cls;?>">
					<?php */?>
                <?php else:?>
                    <?php if(tlf_get_option( 'text_logo', '' )):?>
                    <div class="site-title <?php echo $logo_cls;?>"><?php echo tlf_get_option( 'text_logo', '' );?></div>
                    <?php else:?>
                    <div class="site-title <?php echo $logo_cls;?>"><?php bloginfo( 'name' ); ?></div>
                    <?php endif;?>
                <?php endif;?>
                </a>
                <?php if(tlf_get_option( 'show_site_description', '' )):?>
                    <?php if(tlf_get_option( 'site_description', '' )):?>
                    <p class="site-description lead <?php echo $desc_cls;?>"><?php echo tlf_get_option( 'site_description', '' ); ?></p>
                    <?php else:?>
                    <p class="site-description lead <?php echo $desc_cls;?>"><?php bloginfo( 'description' ); ?></p>
                    <?php endif;?>
                <?php endif;?>
			</div>
			
			<div class="navbar-collapse collapse navbar-responsive-collapse ">
				<div class="tl-croporate-nav-inner">
					<?php wp_nav_menu(
									array(
										'theme_location' 	=> 'primary',
										'depth'             => 2,
										'container'         => '',
										'container_id'      => '',
										'container_class'   => '',
										'menu_class' 		=> 'nav navbar-nav pull-right '.$header_text_color,
										'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
										'menu_id'			=> 'main-menu',
										'walker' 			=> new wp_bootstrap_navwalker()
									)
								); ?>
					<?php /*?><div class="nav-searchicon <?php echo (tlf_get_option('header_bottom_text_color_scheme','') && tlf_get_option('header_bottom_text_color_scheme','')!='default')?tlf_get_option('header_bottom_text_color_scheme'):'font_white';?>"><a class="column md-trigger" data-toggle="modal" data-target="#myModalsearch" href="#"><i class="material-icons">search</i></a></div>	<?php */?>		
				</div>
			</div>
			<?php /*?> <div class="nav-searchicon tl-mobile"><a class="column md-trigger" data-toggle="modal" data-target="#myModalsearch" href="#"><i class="material-icons">search</i></a></div><?php */?>
		</div>
	  </nav>
	</header>
</div>
</div>
<?php if(tlf_get_option('enable_sticky_header','')):?>
<script>
jQuery(document).ready(function(){
	jQuery(window).scroll(function(){
	  if (jQuery(this).scrollTop() > 200) {
		  jQuery('.tl-main-navbar-wrapper').addClass('tl-fixed-header <?php echo (tlf_get_option('header_bottom_background_color_scheme','') && tlf_get_option('header_bottom_background_color_scheme','')!='default')?tlf_get_option('header_bottom_background_color_scheme'):'color-cyan';?>');
			jQuery('.tl-main-navbar-wrapper').css('top', '0', 'opacity', '1');
	  } else {
		  jQuery('.tl-main-navbar-wrapper').removeClass('tl-fixed-header <?php echo (tlf_get_option('header_bottom_background_color_scheme','') && tlf_get_option('header_bottom_background_color_scheme','')!='default')?tlf_get_option('header_bottom_background_color_scheme'):'color-cyan';?>');
			jQuery('.tl-main-navbar-wrapper').css('top', '-150px', 'opacity', '0');
	  }
	});
}); 
</script>
<?php endif;?>
<script>
jQuery('li.dropdown .dropdown-menu').addClass('<?php echo (tlf_get_option('header_bottom_background_color_scheme','') && tlf_get_option('header_bottom_background_color_scheme','')!='default')?tlf_get_option('header_bottom_background_color_scheme'):'color-cyan';?>');
</script>