<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package tl
 */

get_header(); ?> 
<?php while ( have_posts() ) : the_post(); ?>
<?php if(get_post_meta(get_the_ID(),'tlpb_active',true)=='yes'):?>


		<?php 
		$bc_bg_color	=	tlf_get_option('breadcrumb_background_color','');
		$bc_bg_position	=	tlf_get_option('breadcrumb_position','');
        $bc_bg_class	=	tlf_get_option('title_bg_color',''); 
        $show_brd_crmb	=	tlf_get_option('show_breadcrumb','');
		$show_pg_title	=	get_post_meta( get_the_ID(), 'tl_hide_title', true );
		if(($show_brd_crmb == 1 && !is_home() && !is_front_page()) || $show_pg_title != 'on'):
		?>
<div class="archive-page-header <?php echo !empty($bc_bg_class)?$bc_bg_class:'';?>" <?php echo $bc_bg_color !=''?' style="background-color:'.$bc_bg_color.';"':'';?>>	
    <div class="container">
	<div class="tl-breadcrumb-page-title">
        <header>
			<?php if($show_pg_title !='on'):?>
            <div class="<?php echo (get_post_meta( get_the_ID(), 'tl_title_position', true ))?get_post_meta( get_the_ID(), 'tl_title_position', true ):'';?>">
            <h1 class="page-title">
            <?php the_title(); ?>
            </h1>
            </div>
            <?php endif;
            
            ?>
        </header><!-- .entry-header -->
        </div><!-- .tl-breadcrumb-page-title -->
		<?php
		if($show_brd_crmb == 1):
		?>
		<div class="tl-breadcrumb-style1">	
			<div class="tl-breadcrumb <?php echo $bc_bg_position;?>">
			<?php 
				tl_custom_breadcrumb();
			?>
			</div>	
		</div>	
        <?php
		endif;
		?>
    </div>
</div>
<?php 
endif;
get_template_part( 'content', 'page-builder' ); 
else:
?>
<div class="main-content tl-page-rightsidebar">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
 <div class="container">
  <div class="row">
   <div class="tl-cp-common-wrapper clearfix">
   <div id="content" class="main-content-inner tl-cp-main-left1 col-sm-12 col-md-12 col-lg-12">

     <?php get_template_part( 'content', 'page' ); ?>

     <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || '0' != get_comments_number() )
       comments_template();
     ?>
    
    
      </div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
      </div><!-- tl-common-wrapper -->
   
   
   
   </div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
  </div><!-- close .row -->
 </div><!-- close .container -->
</div><!-- close .main-content -->
<?php endif;?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>