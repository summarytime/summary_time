<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package tl
 */

get_header(); ?>
<div class="tl-search-outer-wrapper">
<div class="archive-page-header">
    <div class="container">
        <header>
            <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'tl' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
            
        </header><!-- .page-header -->
    </div>
</div>
<div class="main-content main-content-padding">	
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner tl-cp-main-left1 tl-cp-main-search col-sm-12 col-md-8">
				

					<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'search' ); ?>

					<?php endwhile; ?>

					<?php tl_pagination(); ?>

				<?php else : ?>

					<?php get_template_part( 'no-results', 'search' ); ?>

				<?php endif; // end of loop. ?>

			
			
			
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
			
			
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            	<?php get_sidebar();?>
            </div>
			
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
</div><!-- close .tl-search-outer-wrapper -->
<?php get_footer(); ?>